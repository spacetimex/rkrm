<?php

// This is the database connection configuration.
return array(
//	'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/database.db',
	// uncomment the following lines to use a MySQL database
	'connectionString' => 'mysql:host=127.0.0.1;dbname=open_crm',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => 'root',
	'charset' => 'utf8',
);