<?php

/**
 * Created by PhpStorm.
 * User: MacheNike
 * Date: 2017/5/19
 * Time: 23:04
 */
class Appcode
{

    const success = Array('code'=>'10000');
    //密码错误
    const error_password = Array('code'=>'10001','message'=>'密码错误！');
    //用户名不存在
    const error_username = Array('code'=>'10002','message'=>'用户名不存在！');

    const error_workflowstep = Array('code'=>'10003','message'=>'工作流的步骤的顺序不能重复！');

    const error_dailylogsexist = Array('code'=>'10004','message'=>'当天的日报已经存在！');

    const error_noauthority = Array('code'=>'10005','message'=>'用户没有权限');

    const error_weeklylogsexist = Array('code'=>'10006','message'=>'周报已经存在！');

    const error_weeklycannotupdate = Array('code'=>'10007','message'=>'周报不允许修改！');



}