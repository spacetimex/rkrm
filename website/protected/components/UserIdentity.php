<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{

    public $user;

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        $user = Yii::app()->db->createCommand("select * from crm_user where username = :username")
            ->bindParam(':username', $this->username, PDO::PARAM_STR)
            ->queryRow();
        $userattr = null;
        if (!isset($user)){
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }elseif ($user['password'] !== $this->password){
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        }else{
            $userattr = User::model()->findByAttributes(array('username' => CHtml::encode($this->username)));
        }

        if(!is_null($userattr)){
            $this->setUser($userattr);
            $this->errorCode = self::ERROR_NONE;
            return !$this->errorCode;
        }else{
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
            return $this->errorCode;
        }
    }

    public function refreshUserInfo($username){
        $userattr = User::model()->findByAttributes(array('username' => CHtml::encode($username)));
        $this->setUser($userattr);
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(CActiveRecord $user)
    {
        $this->user = $user->attributes;
    }


}