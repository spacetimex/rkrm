<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout=null;
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

    public $connection=null;

    public $systemconfig = array();

    public $moduledata = null;

    public function init(){
        $this->connection = Yii::app()->db;
        $this->setSystemConfig();
    }

    public function setSystemConfig(){
        $value=Yii::app()->cache->get('CACHE_SYSTEM');
        if($value===false) {
            $configs = $this->connection->createCommand("select * from sys_config where module = 'system' order by seq asc ")->queryAll();
            foreach ($configs as $config){
                $this->systemconfig[$config['config_key']] = $config;
            }
            return $this->systemconfig;
            Yii::app()->cache->set('CACHE_SYSTEM', $this->systemconfig,60*60);
        }else{
            return $value;
        }
    }

    public function filters()
    {
        return array(
            array(
                'application.filters.Sessionfilter',
            )
        );
    }

/*    public function run($actionID)
    {
        $transaction=$this->connection->beginTransaction();
        try{

            if(($action=$this->createAction($actionID))!==null)
            {
                if(($parent=$this->getModule())===null)
                    $parent=Yii::app();
                if($parent->beforeControllerAction($this,$action))
                {
                    $this->runActionWithFilters($action,$this->filters());
                    $parent->afterControllerAction($this,$action);
                }
            }
            else
                $this->missingAction($actionID);

            $transaction->commit();
        }catch (Exception $e){
            print_r($e);
            $transaction->rollback();
        }
    }*/

    protected function getLastUserInfo(){
        $user_id = Yii::app()->user->user_id;
        $userinfo = $this->connection->createCommand("select * from crm_user where user_id = :user_id")->bindParam(':user_id', $user_id, PDO::PARAM_INT)
            ->queryRow();
        return $userinfo;
    }

    /**
     *
     * 载入模块对应的index
     *
     * @param $module
     */
    protected function viewIndex($module){
        $data = Array();
        $data['module'] = $module;
        $data['moduleinfo'] = $this->connection->createCommand("select * from crm_module where moduleenname = :module")
            ->bindParam(':module', $module, PDO::PARAM_STR)
            ->queryRow();
        $data['view'] = $this->connection->createCommand("select * from crm_module_view where modulename = :module and isdefault = 1")
            ->bindParam(':module', $module, PDO::PARAM_STR)
            ->queryRow();
        $data['viewlist'] = $this->connection->createCommand("select * from crm_module_view where modulename = :module")
            ->bindParam(':module', $module, PDO::PARAM_STR)
            ->queryRow();
        $this->moduledata = $data;
        return $data;
    }

    /**
     * 载入模块对应的Data
     *
     * @param $module
     */
    protected function viewData($module){

    }

    protected function viewShowedit($module){

    }

    protected function viewSavedata($module){

    }

    protected function viewRemove($module){

    }

    protected function viewView($module){

    }



}