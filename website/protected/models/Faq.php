<?php

/**
 * This is the model class for table "crm_faq".
 *
 * The followings are the available columns in table 'crm_faq':
 * @property integer $faq_id
 * @property string $product_name
 * @property integer $faqcategory_id
 * @property string $status
 * @property string $keyword
 * @property string $question
 * @property string $solution
 * @property string $createtime
 */
class Faq extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'crm_faq';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('faqcategory_id', 'numerical', 'integerOnly'=>true),
			array('product_name, status, keyword, question', 'length', 'max'=>255),
			array('solution, createtime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('faq_id, product_name, faqcategory_id, status, keyword, question, solution, createtime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'faq_id' => 'Faq',
			'product_name' => '产品名称',
			'faqcategory_id' => '产品分类ID',
			'status' => '状态',
			'keyword' => '关键字',
			'question' => '问题',
			'solution' => '处理方式',
			'createtime' => '创建时间',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('faq_id',$this->faq_id);
		$criteria->compare('product_name',$this->product_name,true);
		$criteria->compare('faqcategory_id',$this->faqcategory_id);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('keyword',$this->keyword,true);
		$criteria->compare('question',$this->question,true);
		$criteria->compare('solution',$this->solution,true);
		$criteria->compare('createtime',$this->createtime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Faq the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
