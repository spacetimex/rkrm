<?php

/**
 * This is the model class for table "crm_potentials".
 *
 * The followings are the available columns in table 'crm_potentials':
 * @property integer $potentials_id
 * @property string $potentials_name
 * @property integer $customer_id
 * @property string $stage
 * @property integer $possibility
 * @property string $duedate
 * @property string $nextstep
 * @property string $updatetime
 */
class Potentials extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'crm_potentials';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customer_id, possibility', 'numerical', 'integerOnly'=>true),
			array('potentials_name, stage, nextstep', 'length', 'max'=>255),
			array('duedate, updatetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('potentials_id, potentials_name, customer_id, stage, possibility, duedate, nextstep, updatetime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'potentials_id' => '主键',
			'potentials_name' => '销售机会名称',
			'customer_id' => '客户ID',
			'stage' => '销售阶段',
			'possibility' => '成交的可能性',
			'duedate' => '成交日期',
			'nextstep' => '下一阶段',
			'updatetime' => '更新时间',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('potentials_id',$this->potentials_id);
		$criteria->compare('potentials_name',$this->potentials_name,true);
		$criteria->compare('customer_id',$this->customer_id);
		$criteria->compare('stage',$this->stage,true);
		$criteria->compare('possibility',$this->possibility);
		$criteria->compare('duedate',$this->duedate,true);
		$criteria->compare('nextstep',$this->nextstep,true);
		$criteria->compare('updatetime',$this->updatetime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Potentials the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
