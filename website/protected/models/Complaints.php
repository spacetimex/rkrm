<?php

/**
 * This is the model class for table "crm_complaints".
 *
 * The followings are the available columns in table 'crm_complaints':
 * @property string $complaints_id
 * @property string $title
 * @property string $customer
 * @property string $complaints_type
 * @property string $complaints_date
 * @property string $result
 * @property string $urgencydegree
 * @property string $createtime
 */
class Complaints extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'crm_complaints';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, customer, complaints_type, result, urgencydegree', 'length', 'max'=>255),
			array('complaints_date, createtime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('complaints_id, title, customer, complaints_type, complaints_date, result, urgencydegree, createtime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'complaints_id' => '主键',
			'title' => '投诉主题',
			'customer' => '客户',
			'complaints_type' => '投诉类型',
			'complaints_date' => '投诉时间',
			'result' => '处理结果',
			'urgencydegree' => '紧急程度',
			'createtime' => '创建时间',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('complaints_id',$this->complaints_id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('customer',$this->customer,true);
		$criteria->compare('complaints_type',$this->complaints_type,true);
		$criteria->compare('complaints_date',$this->complaints_date,true);
		$criteria->compare('result',$this->result,true);
		$criteria->compare('urgencydegree',$this->urgencydegree,true);
		$criteria->compare('createtime',$this->createtime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Complaints the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
