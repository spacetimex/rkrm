<?php

/**
 * This is the model class for table "crm_vnotes".
 *
 * The followings are the available columns in table 'crm_vnotes':
 * @property integer $vnotes_id
 * @property string $title
 * @property integer $vendors_id
 * @property string $contact_time
 * @property string $contact_type
 * @property string $content
 */
class Vnotes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'crm_vnotes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vendors_id', 'numerical', 'integerOnly'=>true),
			array('title, contact_type', 'length', 'max'=>255),
			array('contact_time, content', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('vnotes_id, title, vendors_id, contact_time, contact_type, content', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'vnotes_id' => '主键',
			'title' => '主题',
			'vendors_id' => '供应商ID',
			'contact_time' => 'Contact Time',
			'contact_type' => '联系类型',
			'content' => '联系内容',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('vnotes_id',$this->vnotes_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('vendors_id',$this->vendors_id);
		$criteria->compare('contact_time',$this->contact_time,true);
		$criteria->compare('contact_type',$this->contact_type,true);
		$criteria->compare('content',$this->content,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Vnotes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
