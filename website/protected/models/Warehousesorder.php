<?php

/**
 * This is the model class for table "crm_warehousesorder".
 *
 * The followings are the available columns in table 'crm_warehousesorder':
 * @property integer $warehousesorder_id
 * @property string $warehousesorder_num
 * @property integer $purchaseorder_id
 * @property string $createtime
 * @property string $status
 * @property integer $amount
 */
class Warehousesorder extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'crm_warehousesorder';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('purchaseorder_id, amount', 'numerical', 'integerOnly'=>true),
			array('warehousesorder_num, status', 'length', 'max'=>255),
			array('createtime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('warehousesorder_id, warehousesorder_num, purchaseorder_id, createtime, status, amount', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'warehousesorder_id' => 'Warehousesorder',
			'warehousesorder_num' => '入库单号',
			'purchaseorder_id' => '进货单ID',
			'createtime' => '创建时间',
			'status' => '状态',
			'amount' => '数量',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('warehousesorder_id',$this->warehousesorder_id);
		$criteria->compare('warehousesorder_num',$this->warehousesorder_num,true);
		$criteria->compare('purchaseorder_id',$this->purchaseorder_id);
		$criteria->compare('createtime',$this->createtime,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('amount',$this->amount);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Warehousesorder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
