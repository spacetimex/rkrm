<?php

/**
 * This is the model class for table "crm_quotes".
 *
 * The followings are the available columns in table 'crm_quotes':
 * @property integer $quotes_id
 * @property string $quotes_number
 * @property string $stage
 * @property integer $potentials_id
 * @property string $total
 * @property string $quotetime
 * @property string $updatetime
 */
class Quotes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'crm_quotes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('potentials_id', 'numerical', 'integerOnly'=>true),
			array('quotes_number, stage, total', 'length', 'max'=>255),
			array('quotetime, updatetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('quotes_id, quotes_number, stage, potentials_id, total, quotetime, updatetime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'quotes_id' => '主键',
			'quotes_number' => '报价单号',
			'stage' => '报价阶段',
			'potentials_id' => '销售机会关联',
			'total' => '总计',
			'quotetime' => '报价日期',
			'updatetime' => '修改时间',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('quotes_id',$this->quotes_id);
		$criteria->compare('quotes_number',$this->quotes_number,true);
		$criteria->compare('stage',$this->stage,true);
		$criteria->compare('potentials_id',$this->potentials_id);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('quotetime',$this->quotetime,true);
		$criteria->compare('updatetime',$this->updatetime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Quotes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
