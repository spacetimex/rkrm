<?php

/**
 * This is the model class for table "crm_memdays".
 *
 * The followings are the available columns in table 'crm_memdays':
 * @property integer $memdays_id
 * @property string $title
 * @property string $customer
 * @property string $memdays_type
 * @property string $memdays_day
 * @property string $calendar_type
 */
class Memdays extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'crm_memdays';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, customer, memdays_type, memdays_day, calendar_type', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('memdays_id, title, customer, memdays_type, memdays_day, calendar_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'memdays_id' => '主键',
			'title' => '标题',
			'customer' => '客户',
			'memdays_type' => '纪念日类型',
			'memdays_day' => '纪念日',
			'calendar_type' => '阳历/阴历',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('memdays_id',$this->memdays_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('customer',$this->customer,true);
		$criteria->compare('memdays_type',$this->memdays_type,true);
		$criteria->compare('memdays_day',$this->memdays_day,true);
		$criteria->compare('calendar_type',$this->calendar_type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Memdays the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
