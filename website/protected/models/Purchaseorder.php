<?php

/**
 * This is the model class for table "crm_purchaseorder".
 *
 * The followings are the available columns in table 'crm_purchaseorder':
 * @property integer $purchaseorder_id
 * @property string $purchaseorder_num
 * @property integer $vendors_id
 * @property string $status
 * @property string $purchase_date
 * @property string $total
 * @property string $paystatus
 * @property string $createtime
 */
class Purchaseorder extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'crm_purchaseorder';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vendors_id', 'numerical', 'integerOnly'=>true),
			array('purchaseorder_num, status, paystatus', 'length', 'max'=>255),
			array('total', 'length', 'max'=>10),
			array('purchase_date, createtime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('purchaseorder_id, purchaseorder_num, vendors_id, status, purchase_date, total, paystatus, createtime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'purchaseorder_id' => 'Purchaseorder',
			'purchaseorder_num' => 'Purchaseorder Num',
			'vendors_id' => '供应商ID',
			'status' => '状态',
			'purchase_date' => '采购日期',
			'total' => '总计',
			'paystatus' => '付款状态',
			'createtime' => 'Createtime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('purchaseorder_id',$this->purchaseorder_id);
		$criteria->compare('purchaseorder_num',$this->purchaseorder_num,true);
		$criteria->compare('vendors_id',$this->vendors_id);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('purchase_date',$this->purchase_date,true);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('paystatus',$this->paystatus,true);
		$criteria->compare('createtime',$this->createtime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Purchaseorder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
