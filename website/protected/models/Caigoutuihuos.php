<?php

/**
 * This is the model class for table "crm_caigoutuihuos".
 *
 * The followings are the available columns in table 'crm_caigoutuihuos':
 * @property integer $caigoutuihuos_id
 * @property string $title
 * @property string $status
 * @property integer $vendors_id
 * @property integer $purchaseorder_id
 * @property string $reason
 */
class Caigoutuihuos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'crm_caigoutuihuos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vendors_id, purchaseorder_id', 'numerical', 'integerOnly'=>true),
			array('title, status, reason', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('caigoutuihuos_id, title, status, vendors_id, purchaseorder_id, reason', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'caigoutuihuos_id' => 'Caigoutuihuos',
			'title' => '主题',
			'status' => '状态',
			'vendors_id' => '供应商ID',
			'purchaseorder_id' => '进货单ID',
			'reason' => '原因',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('caigoutuihuos_id',$this->caigoutuihuos_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('vendors_id',$this->vendors_id);
		$criteria->compare('purchaseorder_id',$this->purchaseorder_id);
		$criteria->compare('reason',$this->reason,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Caigoutuihuos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
