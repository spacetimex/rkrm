<?php

/**
 * This is the model class for table "crm_product".
 *
 * The followings are the available columns in table 'crm_product':
 * @property integer $product_id
 * @property integer $productcategory_id
 * @property string $unit
 * @property string $model
 * @property string $weight
 * @property string $unitprice
 * @property string $costprice
 * @property string $saleprice
 * @property string $description
 * @property string $createtime
 * @property string $productname
 */
class Product extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'crm_product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('productcategory_id', 'numerical', 'integerOnly'=>true),
			array('unit, model, productname', 'length', 'max'=>255),
			array('weight, unitprice, costprice, saleprice', 'length', 'max'=>10),
			array('description, createtime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('product_id, productcategory_id, unit, model, weight, unitprice, costprice, saleprice, description, createtime, productname', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'product_id' => 'Product',
			'productcategory_id' => '产品分类ID',
			'unit' => '单位',
			'model' => '型号',
			'weight' => '重量',
			'unitprice' => '单价',
			'costprice' => '成本',
			'saleprice' => '售价',
			'description' => '描述',
			'createtime' => '创建时间',
			'productname' => '产品名称',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('productcategory_id',$this->productcategory_id);
		$criteria->compare('unit',$this->unit,true);
		$criteria->compare('model',$this->model,true);
		$criteria->compare('weight',$this->weight,true);
		$criteria->compare('unitprice',$this->unitprice,true);
		$criteria->compare('costprice',$this->costprice,true);
		$criteria->compare('saleprice',$this->saleprice,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('createtime',$this->createtime,true);
		$criteria->compare('productname',$this->productname,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
