<?php

/**
 * This is the model class for table "crm_salesorder".
 *
 * The followings are the available columns in table 'crm_salesorder':
 * @property integer $salesorder_id
 * @property string $salesorder_number
 * @property string $stage
 * @property integer $customer_id
 * @property integer $contacter_id
 * @property string $signdate
 * @property string $total
 * @property string $gather_status
 */
class Salesorder extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'crm_salesorder';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customer_id, contacter_id', 'numerical', 'integerOnly'=>true),
			array('salesorder_number, stage, gather_status', 'length', 'max'=>255),
			array('total', 'length', 'max'=>10),
			array('signdate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('salesorder_id, salesorder_number, stage, customer_id, contacter_id, signdate, total, gather_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'salesorder_id' => 'Salesorder',
			'salesorder_number' => '销售订单号',
			'stage' => '订单状态',
			'customer_id' => '客户ID',
			'contacter_id' => '联系人ID',
			'signdate' => '签约日期',
			'total' => '总计',
			'gather_status' => '收款状态',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('salesorder_id',$this->salesorder_id);
		$criteria->compare('salesorder_number',$this->salesorder_number,true);
		$criteria->compare('stage',$this->stage,true);
		$criteria->compare('customer_id',$this->customer_id);
		$criteria->compare('contacter_id',$this->contacter_id);
		$criteria->compare('signdate',$this->signdate,true);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('gather_status',$this->gather_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Salesorder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
