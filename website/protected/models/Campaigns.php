<?php

/**
 * This is the model class for table "crm_campaigns".
 *
 * The followings are the available columns in table 'crm_campaigns':
 * @property integer $campaigns_id
 * @property string $campaigns_name
 * @property string $status
 * @property integer $product_id
 * @property string $campaigns_type
 * @property string $end_date
 * @property string $targetaudience
 * @property integer $targetsize
 * @property string $description
 */
class Campaigns extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'crm_campaigns';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_id, targetsize', 'numerical', 'integerOnly'=>true),
			array('campaigns_name, status, campaigns_type, targetaudience', 'length', 'max'=>255),
			array('end_date, description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('campaigns_id, campaigns_name, status, product_id, campaigns_type, end_date, targetaudience, targetsize, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'campaigns_id' => '主键',
			'campaigns_name' => '营销活动名称',
			'status' => '状态',
			'product_id' => '产品_ID',
			'campaigns_type' => '营销活动类型',
			'end_date' => '结束时间',
			'targetaudience' => '目标客户',
			'targetsize' => '目标大小',
			'description' => '描述',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('campaigns_id',$this->campaigns_id);
		$criteria->compare('campaigns_name',$this->campaigns_name,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('campaigns_type',$this->campaigns_type,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('targetaudience',$this->targetaudience,true);
		$criteria->compare('targetsize',$this->targetsize);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Campaigns the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
