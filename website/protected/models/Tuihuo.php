<?php

/**
 * This is the model class for table "crm_tuihuo".
 *
 * The followings are the available columns in table 'crm_tuihuo':
 * @property integer $tuihuo_id
 * @property string $tuihuo_num
 * @property integer $customer_id
 * @property string $tuihuo_date
 * @property string $total
 * @property string $approve_status
 */
class Tuihuo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'crm_tuihuo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customer_id', 'numerical', 'integerOnly'=>true),
			array('tuihuo_num, approve_status', 'length', 'max'=>255),
			array('total', 'length', 'max'=>10),
			array('tuihuo_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('tuihuo_id, tuihuo_num, customer_id, tuihuo_date, total, approve_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tuihuo_id' => 'Tuihuo',
			'tuihuo_num' => 'Tuihuo Num',
			'customer_id' => 'Customer',
			'tuihuo_date' => 'Tuihuo Date',
			'total' => 'Total',
			'approve_status' => 'Approve Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tuihuo_id',$this->tuihuo_id);
		$criteria->compare('tuihuo_num',$this->tuihuo_num,true);
		$criteria->compare('customer_id',$this->customer_id);
		$criteria->compare('tuihuo_date',$this->tuihuo_date,true);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('approve_status',$this->approve_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tuihuo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
