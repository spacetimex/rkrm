<?php

class Sessionfilter extends CFilter
{

    protected function preFilter($filterChain)
    {
        if (!Yii::app()->user->isGuest || $_SERVER['REQUEST_URI'] === '/y.php?r=site/login' || $_SERVER['REQUEST_URI'] === '/y.php?r=site/welcome') {
            return true;
        }
        header('Location:/index.html');
    }

}