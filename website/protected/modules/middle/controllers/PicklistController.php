<?php

class PicklistController extends Controller
{
	public function actionGetdata()
	{
        $picklistkey = $_REQUEST['picklistkey'];
        $picklistvalue = $_REQUEST['picklistvalue'];
        $data = $this->connection->createCommand("select picklist_value as id,picklist_value as text from crm_picklist where picklist_key = :picklistkey")
            ->bindParam(':picklistkey',$picklistkey,PDO::PARAM_STR)
            ->queryAll();
        foreach ($data as &$d){
            if($d['id']==$picklistvalue){
                $d['selected'] = true;
            }else{
                $d['selected'] = false;
            }
        }
        echo json_encode($data);
	}
}