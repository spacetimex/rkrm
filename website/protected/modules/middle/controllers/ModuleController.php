<?php

class ModuleController extends Controller
{
    public function actionGetdata()
    {
        $current = $_REQUEST['id'];
        $data = $this->connection->createCommand("select modulename as text,moduleenname as id from crm_module;")
            ->queryAll();
        foreach ($data as &$d){
            if($d['id']==$current){
                $d['selected'] = true;
            }else{
                $d['selected'] = false;
            }
        }
        echo json_encode($data);
    }
}