<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class RestfulController extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout=null;
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

    public $connection=null;

    public function init(){
        $this->connection = Yii::app()->db;
    }

    public function filters()
    {
        return array(
            array(
                'restful.filters.Tokenfilter',
            )
        );
    }

    public function run($actionID)
    {
        $transaction=$this->connection->beginTransaction();
        try{

            if(($action=$this->createAction($actionID))!==null)
            {
                if(($parent=$this->getModule())===null)
                    $parent=Yii::app();
                if($parent->beforeControllerAction($this,$action))
                {
                    $this->runActionWithFilters($action,$this->filters());
                    $parent->afterControllerAction($this,$action);
                }
            }
            else
                $this->missingAction($actionID);

            $transaction->commit();
        }catch (Exception $e){
            print_r($e);
            $transaction->rollback();
        }
    }

    protected function getLastUserInfo(){
        $user_id = Yii::app()->user->user_id;
        $userinfo = $this->connection->createCommand("select * from crm_user where user_id = :user_id")->bindParam(':user_id', $user_id, PDO::PARAM_INT)
            ->queryRow();
        return $userinfo;
    }




}