[
    {
        "id": "workspace",
        "text": "工作台",
        "attributes": {
            "accesskey": "workspace"
        },
        "children": [
            {
                "id": "workspace_site",
                "text": "控制台",
                "attributes": {
                    "accesskey": "workspace_site"
                }
            },
            {
                "id": "workspace_announcements",
                "text": "公告",
                "attributes": {
                    "accesskey": "workspace_announcements"
                },
                "children": [
                    {
                        "id": "workspace_announcements_view",
                        "text": "查看公告",
                        "attributes": {
                            "accesskey": "workspace_announcements_view"
                        }
                    },
                    {
                        "id": "workspace_announcements_add",
                        "text": "添加公告",
                        "attributes": {
                            "accesskey": "workspace_announcements_add"
                        }
                    },
                    {
                        "id": "workspace_announcements_edit",
                        "text": "编辑公告",
                        "attributes": {
                            "accesskey": "workspace_announcements_edit"
                        }
                    },
                    {
                        "id": "workspace_announcements_remove",
                        "text": "删除公告",
                        "attributes": {
                            "accesskey": "workspace_announcements_remove"
                        }
                    }
                ]
            },
            {
                "id": "workspace_dailylogs",
                "text": "日报",
                "attributes": {
                    "accesskey": "workspace_dailylogs"
                },
                "children": [
                    {
                        "id": "workspace_dailylogs_view",
                        "text": "查看日报",
                        "attributes": {
                            "accesskey": "workspace_dailylogs_view"
                        }
                    },
                    {
                        "id": "workspace_dailylogs_add",
                        "text": "添加日报",
                        "attributes": {
                            "accesskey": "workspace_dailylogs_add"
                        }
                    },
                    {
                        "id": "workspace_dailylogs_edit",
                        "text": "编辑日报",
                        "attributes": {
                            "accesskey": "workspace_dailylogs_edit"
                        }
                    }
                ]
            }
        ]
    },
    {
        "id": "campaigns",
        "text": "营销",
        "attributes": {
            "accesskey": "campaigns"
        },
        "children": [
            {
                "id": "campaigns_module",
                "text": "营销活动",
                "attributes": {
                    "accesskey": "campaigns_module"
                },
                "children": [
                    {
                        "id": "campaigns_view",
                        "text": "查看营销活动",
                        "attributes": {
                            "accesskey": "campaigns_view"
                        }
                    },
                    {
                        "id": "campaigns_add",
                        "text": "添加营销活动",
                        "attributes": {
                            "accesskey": "campaigns_add"
                        }
                    },
                    {
                        "id": "campaigns_edit",
                        "text": "编辑营销活动",
                        "attributes": {
                            "accesskey": "campaigns_edit"
                        }
                    },
                    {
                        "id": "campaigns_remove",
                        "text": "删除营销活动",
                        "attributes": {
                            "accesskey": "campaigns_remove"
                        }
                    }
                ]
            }
        ]
    },
    {
        "id": "customer",
        "text": "客户",
        "attributes": {
            "accesskey": "customer"
        },
        "children": [
            {
                "id": "customer_module",
                "text": "客户",
                "attributes": {
                    "accesskey": "customer_module"
                },
                "children": [
                    {
                        "id": "customer_module_view",
                        "text": "查看客户",
                        "attributes": {
                            "accesskey": "customer_module_view"
                        }
                    },
                    {
                        "id": "customer_module_add",
                        "text": "添加客户",
                        "attributes": {
                            "accesskey": "customer_module_add"
                        }
                    },
                    {
                        "id": "customer_module_edit",
                        "text": "编辑客户",
                        "attributes": {
                            "accesskey": "customer_module_edit"
                        }
                    },
                    {
                        "id": "customer_module_remove",
                        "text": "删除客户",
                        "attributes": {
                            "accesskey": "customer_module_remove"
                        }
                    }
                ]
            },
            {
                "id": "contacter_module",
                "text": "联系人",
                "attributes": {
                    "accesskey": "contacter_module"
                },
                "children": [
                    {
                        "id": "contacter_module_view",
                        "text": "查看联系人",
                        "attributes": {
                            "accesskey": "contacter_module_view"
                        }
                    },
                    {
                        "id": "contacter_module_add",
                        "text": "添加联系人",
                        "attributes": {
                            "accesskey": "contacter_module_add"
                        }
                    },
                    {
                        "id": "contacter_module_edit",
                        "text": "编辑联系人",
                        "attributes": {
                            "accesskey": "contacter_module_edit"
                        }
                    },
                    {
                        "id": "contacter_module_remove",
                        "text": "删除联系人",
                        "attributes": {
                            "accesskey": "contacter_module_remove"
                        }
                    }
                ]
            },
            {
                "id": "contactlog_module",
                "text": "联系记录",
                "attributes": {
                    "accesskey": "contactlog_module"
                },
                "children": [
                    {
                        "id": "contactlog_module_view",
                        "text": "查看联系记录",
                        "attributes": {
                            "accesskey": "contactlog_module_view"
                        }
                    },
                    {
                        "id": "contactlog_module_add",
                        "text": "添加联系记录",
                        "attributes": {
                            "accesskey": "contactlog_module_add"
                        }
                    },
                    {
                        "id": "contactlog_module_edit",
                        "text": "编辑联系记录",
                        "attributes": {
                            "accesskey": "contactlog_module_edit"
                        }
                    },
                    {
                        "id": "contactlog_module_remove",
                        "text": "删除联系记录",
                        "attributes": {
                            "accesskey": "contactlog_module_remove"
                        }
                    }
                ]
            },
            {
                "id": "memdays_module",
                "text": "纪念日",
                "attributes": {
                    "accesskey": "memdays_module"
                },
                "children": [
                    {
                        "id": "memdays_module_view",
                        "text": "查看纪念日",
                        "attributes": {
                            "accesskey": "memdays_module_view"
                        }
                    },
                    {
                        "id": "memdays_module_add",
                        "text": "添加纪念日",
                        "attributes": {
                            "accesskey": "memdays_module_add"
                        }
                    },
                    {
                        "id": "memdays_module_edit",
                        "text": "编辑纪念日",
                        "attributes": {
                            "accesskey": "memdays_module_edit"
                        }
                    },
                    {
                        "id": "memdays_module_remove",
                        "text": "删除纪念日",
                        "attributes": {
                            "accesskey": "memdays_module_remove"
                        }
                    }
                ]
            }
        ]
    },
    {
        "id": "product",
        "text": "产品",
        "attributes": {
            "accesskey": "product"
        },
        "children": [
            {
                "id": "product_module",
                "text": "产品",
                "attributes": {
                    "accesskey": "product_module"
                },
                "children": [
                    {
                        "id": "product_module_view",
                        "text": "查看产品",
                        "attributes": {
                            "accesskey": "product_module_view"
                        }
                    },
                    {
                        "id": "product_module_add",
                        "text": "添加产品",
                        "attributes": {
                            "accesskey": "product_module_add"
                        }
                    },
                    {
                        "id": "product_module_edit",
                        "text": "编辑产品",
                        "attributes": {
                            "accesskey": "product_module_edit"
                        }
                    },
                    {
                        "id": "product_module_remove",
                        "text": "删除产品",
                        "attributes": {
                            "accesskey": "product_module_remove"
                        }
                    }
                ]
            },
            {
                "id": "productcategory_module",
                "text": "产品分类",
                "attributes": {
                    "accesskey": "productcategory_module"
                },
                "children": [
                    {
                        "id": "productcategory_module_add",
                        "text": "添加产品分类",
                        "attributes": {
                            "accesskey": "productcategory_module_add"
                        }
                    },
                    {
                        "id": "productcategory_module_edit",
                        "text": "编辑产品分类",
                        "attributes": {
                            "accesskey": "productcategory_module_edit"
                        }
                    },
                    {
                        "id": "productcategory_module_remove",
                        "text": "删除产品分类",
                        "attributes": {
                            "accesskey": "productcategory_module_remove"
                        }
                    }
                ]
            }
        ]
    },
    {
        "id": "sale",
        "text": "销售",
        "attributes": {
            "accesskey": "sale"
        },
        "children": [
            {
                "id": "potentials_module",
                "text": "销售机会",
                "attributes": {
                    "accesskey": "potentials_module"
                },
                "children": [
                    {
                        "id": "potentials_module_add",
                        "text": "添加销售机会",
                        "attributes": {
                            "accesskey": "potentials_module_add"
                        }
                    },
                    {
                        "id": "potentials_module_edit",
                        "text": "编辑销售机会",
                        "attributes": {
                            "accesskey": "potentials_module_edit"
                        }
                    },
                    {
                        "id": "potentials_module_remove",
                        "text": "删除销售机会",
                        "attributes": {
                            "accesskey": "potentials_module_remove"
                        }
                    }
                ]
            }
        ]
    },
    {
        "id": "aftersale",
        "text": "售后",
        "attributes": {
            "accesskey": "aftersale"
        },
        "children": [
            {
                "id": "accountrecordss_module",
                "text": "客户服务",
                "attributes": {
                    "accesskey": "accountrecordss_module"
                },
                "children": [
                    {
                        "id": "accountrecordss_module_view",
                        "text": "查看客户服务",
                        "attributes": {
                            "accesskey": "accountrecordss_module_view"
                        }
                    },
                    {
                        "id": "accountrecordss_module_add",
                        "text": "添加客户服务",
                        "attributes": {
                            "accesskey": "accountrecordss_module_add"
                        }
                    },
                    {
                        "id": "accountrecordss_module_edit",
                        "text": "编辑客户服务",
                        "attributes": {
                            "accesskey": "accountrecordss_module_edit"
                        }
                    }
                ]
            },
            {
                "id": "complaints_module",
                "text": "客户投诉",
                "attributes": {
                    "accesskey": "complaints_module"
                },
                "children": [
                    {
                        "id": "complaints_module_view",
                        "text": "查看客户投诉",
                        "attributes": {
                            "accesskey": "complaints_module_view"
                        }
                    },
                    {
                        "id": "complaints_module_add",
                        "text": "添加客户投诉",
                        "attributes": {
                            "accesskey": "complaints_module_add"
                        }
                    },
                    {
                        "id": "complaints_module_edit",
                        "text": "编辑客户投诉",
                        "attributes": {
                            "accesskey": "complaints_module_edit"
                        }
                    }
                ]
            },
            {
                "id": "faq_module",
                "text": "常见问题",
                "attributes": {
                    "accesskey": "faq_module"
                },
                "children": [
                    {
                        "id": "faq_module_view",
                        "text": "查看常见问题",
                        "attributes": {
                            "accesskey": "faq_module_view"
                        }
                    },
                    {
                        "id": "faq_module_add",
                        "text": "添加常见问题",
                        "attributes": {
                            "accesskey": "faq_module_add"
                        }
                    },
                    {
                        "id": "faq_module_edit",
                        "text": "编辑常见问题",
                        "attributes": {
                            "accesskey": "faq_module_edit"
                        }
                    }
                ]
            },
            {
                "id": "faqcategory_module",
                "text": "常见问题分类",
                "attributes": {
                    "accesskey": "faqcategory_module"
                },
                "children": [
                    {
                        "id": "faqcategory_module_view",
                        "text": "查看常见问题分类",
                        "attributes": {
                            "accesskey": "faqcategory_module_view"
                        }
                    },
                    {
                        "id": "faqcategory_module_add",
                        "text": "添加常见问题分类",
                        "attributes": {
                            "accesskey": "faqcategory_module_add"
                        }
                    },
                    {
                        "id": "faqcategory_module_edit",
                        "text": "编辑常见问题分类",
                        "attributes": {
                            "accesskey": "faqcategory_module_edit"
                        }
                    }
                ]
            }
        ]
    },
    {
        "id": "sysconfig",
        "text": "系统设置",
        "attributes": {
            "accesskey": "sysconfig"
        },
        "children": [
            {
                "id": "sysconfig_user_module",
                "text": "系统用户",
                "attributes": {
                    "accesskey": "sysconfig_user_module"
                },
                "children": [
                    {
                        "id": "sysconfig_user_module_view",
                        "text": "查看系统用户",
                        "attributes": {
                            "accesskey": "sysconfig_user_module_view"
                        }
                    },
                    {
                        "id": "sysconfig_user_module_add",
                        "text": "添加系统用户",
                        "attributes": {
                            "accesskey": "sysconfig_user_module_add"
                        }
                    },
                    {
                        "id": "sysconfig_user_module_edit",
                        "text": "编辑系统用户",
                        "attributes": {
                            "accesskey": "sysconfig_user_module_edit"
                        }
                    }
                ]
            },
            {
                "id": "sysconfig_role_module",
                "text": "角色权限",
                "attributes": {
                    "accesskey": "sysconfig_role_module"
                },
                "children": [
                    {
                        "id": "sysconfig_role_module_add",
                        "text": "添加角色权限",
                        "attributes": {
                            "accesskey": "sysconfig_role_module_add"
                        }
                    },
                    {
                        "id": "sysconfig_role_module_edit",
                        "text": "编辑角色权限",
                        "attributes": {
                            "accesskey": "sysconfig_role_module_edit"
                        }
                    },
                    {
                        "id": "sysconfig_role_module_remove",
                        "text": "删除角色权限",
                        "attributes": {
                            "accesskey": "sysconfig_role_module_remove"
                        }
                    }
                ]
            },
            {
                "id": "sysconfig_picklist_module",
                "text": "数据字典",
                "attributes": {
                    "accesskey": "sysconfig_picklist_module"
                },
                "children": [
                    {
                        "id": "sysconfig_picklist_module_add",
                        "text": "添加数据字典",
                        "attributes": {
                            "accesskey": "sysconfig_picklist_module_add"
                        }
                    },
                    {
                        "id": "sysconfig_picklist_module_edit",
                        "text": "编辑数据字典",
                        "attributes": {
                            "accesskey": "sysconfig_picklist_module_edit"
                        }
                    },
                    {
                        "id": "sysconfig_picklist_module_remove",
                        "text": "删除数据字典",
                        "attributes": {
                            "accesskey": "sysconfig_picklist_module_remove"
                        }
                    }
                ]
            },
            {
                "id": "sysconfig_workflow_module",
                "text": "审批流程",
                "attributes": {
                    "accesskey": "sysconfig_workflow_module"
                },
                "children": [
                    {
                        "id": "sysconfig_workflow_module_add",
                        "text": "添加审批流程",
                        "attributes": {
                            "accesskey": "sysconfig_workflow_module_add"
                        }
                    },
                    {
                        "id": "sysconfig_workflow_module_edit",
                        "text": "编辑审批流程",
                        "attributes": {
                            "accesskey": "sysconfig_workflow_module_edit"
                        }
                    },
                    {
                        "id": "sysconfig_workflow_module_remove",
                        "text": "删除审批流程",
                        "attributes": {
                            "accesskey": "sysconfig_workflow_module_remove"
                        }
                    },
                    {
                        "id": "sysconfig_workflow_module_config",
                        "text": "配置审批流程",
                        "attributes": {
                            "accesskey": "sysconfig_workflow_module_config"
                        }
                    }
                ]
            },
            {
                "id": "sysconfig_workflowmodule_module",
                "text": "流程模块",
                "attributes": {
                    "accesskey": "sysconfig_workflowmodule_module"
                },
                "children": [
                    {
                        "id": "sysconfig_workflowmodule_module_config",
                        "text": "配置流程模块",
                        "attributes": {
                            "accesskey": "sysconfig_workflowmodule_module_config"
                        }
                    }
                ]
            }
        ]
    }
    ,
    {
        "id": "personalconfig",
        "text": "个人设置",
        "attributes": {
            "accesskey": "personalconfig"
        },
        "children": [
            {
                "id": "personalconfig_config",
                "text": "个人设置界面",
                "attributes": {
                    "accesskey": "personalconfig_config"
                }
            }
        ]
    }
]