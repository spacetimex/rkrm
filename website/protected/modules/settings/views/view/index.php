<table class="easyui-datagrid" id="settings_view_list"
	   fit="true" border="false"
	   singleSelect="true" fitColumns="true" pagination="true"
	   idField="view_id" url="y.php?r=settings/view/getdata" toolbar="#toolbar_view">
	<thead>
	<tr>
		<th field="modulecnname" width="60">模块</th>
		<th field="viewname" width="60" >视图名称</th>
		<th field="isdefault" width="60" formatter="formatter_isdefault">默认</th>
		<th field="iskeyview" width="60" formatter="formatter_iskeyview">关键</th>
	</tr>
	</thead>
</table>
<div id="toolbar_view">
	<?php
//	if (Yii::app()->view->checkAccess('sysconfig_view_module_add')) {
		?>
		<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加视图','y.php?r=settings/view/showedit&view_id=0')" /a>
		<?php
//	}
//	if (Yii::app()->view->checkAccess('sysconfig_view_module_edit')) {
		?>
		<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="view_showedit()"/a>
		<?php
//	}
//	if (Yii::app()->view->checkAccess('sysconfig_view_module_remove')) {
		?>
		<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="view_remove()" />
		<?php
//	}
	?>

</div>
<script>
	function view_showedit() {
		//获取选中的列
		var selected = $("#settings_view_list").datagrid("getSelected");
		if(selected==null){
			alert('请选择行！');
		}else{
			//进行编辑操作
			loadWin('编辑视图','y.php?r=settings/view/showedit&view_id='+selected.view_id);
		}
	}
	function view_remove() {
		var selected = $("#settings_view_list").datagrid("getSelected");
		if(selected==null){
			alert('请选择行！');
		}else{
			$.messager.confirm('确认','您确认想要删除记录吗？',function(r){
				if (r){
					$.post('y.php?r=settings/view/remove',{view_id:selected.view_id},function (res) {
						$("#settings_view_list").datagrid("reload");
					});
				}
			});
		}
	}
	function formatter_isdefault(value,row,index) {
		if(value == 1){
			return "<span class='green'><i class=\"fa fa-check \"></i></span>";
		}else{
			return "<span class='red'><i class=\"fa fa-close\"></i></span>";
		}
	}
	function formatter_iskeyview(value,row,index) {
		if(value == 1){
			return "<span class='green'><i class=\"fa fa-check \"></i></span>";
		}else{
			return "<span class='red'><i class=\"fa fa-close\"></i></span>";
		}
	}
</script>