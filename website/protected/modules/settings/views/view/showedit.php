<form id="form_view_showedit" method="post" action="y.php?r=settings/view/savedata">
    <input type="hidden" name="view_id" value="">
    <table class="tab_form">
        <tr>
            <td>
                <select class="easyui-combobox" name="modulename" url="y.php?r=middle/module/getdata&id=<?php echo $view['modulename'];?>"
                        data-options="label:'模块:',required:true,
        valueField: 'id',
        textField: 'text',
">
                </select>
            </td>
            <td><input class="easyui-textbox" name="viewname"
                       data-options="label:'视图名称:',required:true" value="<?php echo $view['viewname']; ?>"></td>
            <td><label class="textbox-label textbox-label-before">默认视图:</label><input id="switchIsdefault" class="easyui-switchbutton"
                                                                                      onText="是" offText="否" value="1" <?php echo $view['isdefault']==1?"checked":""; ?>>
                <input type="hidden" name="isdefault" value="<?php echo $view['isdefault']; ?>">
            </td>
        </tr>
        <tr>
            <td><label class="textbox-label textbox-label-before">关键视图:</label><input id="switchIskeyview" class="easyui-switchbutton"
                                                                                      onText="是" offText="否"  value="1" <?php echo $view['iskeyview']==1?"checked":""; ?>>
                <input type="hidden" name="iskeyview" value="<?php echo $view['iskeyview']; ?>">
            </td>
            <td><input class="easyui-textbox" name="seq"
                       data-options="label:'排序:',required:true" ></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3">行SQL：</td>
        </tr>
        <tr>
            <td colspan="3"><textarea style="width: 61%;resize: none;height: 80px;" class="iptborder" data-options="label:'行SQL:',required:true" name="viewcountsql" ></textarea></td>
        </tr>
        <tr>
            <td colspan="3">结果集SQL：</td>
        </tr>
        <tr>
            <td colspan="3"><textarea style="width: 61%;resize: none;height: 80px;" class="iptborder" data-options="label:'结果集SQL:',required:true" name="viewquerysql" ></textarea></td>
        </tr>
        <tr>
            <td colspan="3">列表JSON：</td>
        </tr>
        <tr>
            <td colspan="3"><textarea style="width: 61%;resize: none;height: 80px;" class="iptborder" data-options="required:true" name="viewcolumnsjson" ></textarea></td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
</div>
<script>
    var dataobj = <?php echo json_encode($view) ?>;
    $(function () {
        if (dataobj == null) {

        } else {
            $('#form_view_showedit').form('load', dataobj);
        }

        $('#switchIsdefault').switchbutton({
            onChange: function(checked){
                if(checked == true){
                    $("input[name=isdefault]").val(1);
                }else{
                    $("input[name=isdefault]").val(0);
                }
            }
        });

        $('#switchIskeyview').switchbutton({
            onChange: function(checked){
                if(checked == true){
                    $("input[name=iskeyview]").val(1);
                }else{
                    $("input[name=iskeyview]").val(0);
                }
            }
        })

    });
    function submitForm() {
        $('#form_view_showedit').form('submit', {
            success: function () {
                $('#win_main').window("close");
                $("#settings_view_list").datagrid("reload");
            }
        });
    }
    function clearForm() {
        $('#form_view_showedit').form('clear');
        if (dataobj != null) {
            $("input[name=view_id]").val(dataobj.view_id);
        }
    }

</script>