<table class="easyui-datagrid" id="settings_workflowmodule_list"
	   fit="true" border="false"
	   singleSelect="true" fitColumns="true" pagination="false"
	   idField="module_id" url="y.php?r=settings/workflowmodule/getmodule" toolbar="#toolbar_workflowmodule">
	<thead>
	<tr>
		<th field="modulename" width="60">模块名称</th>
	</tr>
	</thead>
</table>
<div id="toolbar_workflowmodule">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="workflowmodule_showedit()"/a>
</div>
<script>
	function workflowmodule_showedit() {
		//获取选中的列
		var selected = $("#settings_workflowmodule_list").datagrid("getSelected");
		if(selected==null){
			alert('请选择行！');
		}else{
			//进行编辑操作
			loadWin('编辑模块关联的工作流','y.php?r=settings/workflowmodule/showedit&module_id='+selected.module_id);
		}
	}
</script>