<form id="form_workflowmodule_showedit" method="post" action="y.php?r=settings/workflowmodule/savedata">
    <input type="hidden" name="module" value="<?php echo $module['moduleenname']; ?>">
    <input type="hidden" name="workflows" value="<?php echo $workflowids; ?>">
    <table class="tab_form">
        <tr>
            <td>
                选择需要关联的工作流：
                <ul id="workflowmodule_workflow_tree"
                    class="easyui-tree" data-options="url:'y.php?r=settings/workflow/loadworkflowtree',method:'get',animate:true,checkbox:true"></ul>
            </td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
</div>
<script>
    $(function () {
        $('#workflowmodule_workflow_tree').tree({
            onLoadSuccess: treeloadsuccessful
        });

    });
    function treeloadsuccessful() {
        var workflows = $("input[name=workflows]").val();
        if(workflows != "" && workflows !=null ){
            //初始化树形菜单的数据
            var res = workflows.split(",");
            for(var i=0;i<res.length;i++){
                var node = $('#workflowmodule_workflow_tree').tree('find', res[i]);
                $('#workflowmodule_workflow_tree').tree('check', node.target);
            }
        }
    }
    function getWorkflowChecked(){
        var nodes = $('#workflowmodule_workflow_tree').tree('getChecked');
        var s = '';
        for(var i=0; i<nodes.length; i++){
            if (s != '') s += ',';
            s += nodes[i].id
        }
        return s;
    }
    function submitForm() {
        var workflows = getWorkflowChecked();
//        if(workflows == ''){
//            alert('请选择工作流程！');
//            return;
//        }
        $("input[name=workflows]").val(workflows);

        $('#form_workflowmodule_showedit').form('submit', {
            success: function (data) {
                $('#win_main').window("close");
            }
        });
    }
</script>