<table class="easyui-datagrid" id="messagetemplate_list"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true"
       idField="messagetemplate_id" url="y.php?r=settings/messagetemplate/getdata" toolbar="#toolbar_messagetemplate">
    <thead>
    <tr>
        <th field="signname" width="60">短信签名</th>
        <th field="templatecode" width="60">短信模板CODE</th>
    </tr>
    </thead>
</table>
<div id="toolbar_messagetemplate">
    <?php
    if (Yii::app()->user->checkAccess('sysconfig_messagetemplate_module_add')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加短信模板','y.php?r=settings/messagetemplate/showedit&messagetemplate_id=0')" /a>
        <?php
    }
    if (Yii::app()->user->checkAccess('sysconfig_messagetemplate_module_edit')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="messagetemplate_showedit()"/a>
        <?php
    }
    if (Yii::app()->user->checkAccess('sysconfig_messagetemplate_module_remove')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="messagetemplate_remove()" />
        <?php
    }
    ?>
</div>
<script>
    function messagetemplate_showedit() {
        //获取选中的列
        var selected = $("#messagetemplate_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('编辑短信模板','y.php?r=settings/messagetemplate/showedit&messagetemplate_id='+selected.messagetemplate_id);
        }
    }
    function messagetemplate_remove() {
        var selected = $("#messagetemplate_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=settings/messagetemplate/remove',{messagetemplate_id:selected.messagetemplate_id},function (res) {
                        $("#messagetemplate_list").datagrid("reload");
                    });
                }
            });
        }
    }
</script>