<form id="form_messagetemplate_showedit" method="post" action="y.php?r=settings/messagetemplate/savedata">
    <input type="hidden" name="messagetemplate_id" value="">
    <table class="tab_form">
        <tr>
            <td><input class="easyui-textbox" name="signname" data-options="label:'短信签名:',required:true"></td>
            <td><input class="easyui-textbox" name="templatecode"
                       data-options="label:'短信模板CODE:',required:true"></td>
            <td>
            </td>
        </tr>
        </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
</div>
<script>
    var dataobj = <?php echo json_encode($messagetemplate) ?>;
    $(function () {
        $('#form_messagetemplate_showedit').form('load',dataobj);
        if(dataobj!=null){
            $("input[name=messagetemplate]").textbox({
                readonly:true
            });
        }
    });
    function submitForm() {
        $('#form_messagetemplate_showedit').form('submit',{
            success: function(){
                $('#win_main').window("close");
                $("#messagetemplate_list").datagrid("reload");
            }
        });
    }
    function clearForm() {
        $('#form_messagetemplate_showedit').form('clear');
        if(dataobj!=null){
            $("input[name=messagetemplate_id]").val(dataobj.messagetemplate_id);
        }
    }
</script>