<table class="easyui-datagrid" id="loginhistory_list"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true"
       idField="loginhistory_id" url="y.php?r=settings/loginhistory/getdata" toolbar="#toolbar_loginhistory">
    <thead>
    <tr>
        <th field="message" width="60">登入信息</th>
        <th field="createtime" width="60">时间</th>
    </tr>
    </thead>
</table>
<div id="toolbar_loginhistory">
    <select id="loginhistory_user_id" style="width: 120px;" class="easyui-combobox"
            data-options="url:'y.php?r=middle/user/getdata',valueField:'id',textField:'text'">
    </select>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true" onclick="queryHistory()" /a>
</div>
<script>
    function queryHistory() {
        var cbovalue = $('#loginhistory_user_id').combobox("getValue");
        $('#loginhistory_list').datagrid('load', {
            user_id: cbovalue
        });

    }
</script>