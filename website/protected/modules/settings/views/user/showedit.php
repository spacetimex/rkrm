<form id="form_user_showedit" method="post" action="y.php?r=settings/user/savedata">
    <input type="hidden" name="user_id" value="">
    <table class="tab_form">
        <tr>
            <td><input class="easyui-textbox" name="username"
                       data-options="label:'用户:',required:true,readonly:true"></td>
            <td><input class="easyui-textbox" name="realname"
                       data-options="label:'真实姓名:',required:true,readonly:true"></td>
            <td>
                <select class="easyui-combotree" name="roles[]" label="角色:" multiple  data-options="url:'y.php?r=settings/role/getuserroles&userid=<?php echo $user['username']; ?>',required:true">
                </select></td>
            <td>
            </td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
</div>
<script>
    var dataobj = <?php echo json_encode($user) ?>;
    $(function () {
        if(dataobj==null){
            //设置input可写
            $('#form_user_showedit').find("input[name=username]").textbox({
                readonly:false
            });
            $('#form_user_showedit').find("input[name=realname]").textbox({
                readonly:false
            });
        }else{
            $('#form_user_showedit').form('load', dataobj);
        }

    });
    function submitForm() {
        $('#form_user_showedit').form('submit', {
            success: function () {
                $('#win_main').window("close");
                $("#settings_user_list").datagrid("reload");
            }
        });
    }
    function clearForm() {
        $('#form_user_showedit').form('clear');
        if (dataobj != null) {
            $("input[name=user_id]").val(dataobj.user_id);
        }
    }
</script>