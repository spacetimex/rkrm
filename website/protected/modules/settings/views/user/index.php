<table class="easyui-datagrid" id="settings_user_list"
	   fit="true" border="false"
	   singleSelect="true" fitColumns="true" pagination="true"
	   idField="user_id" url="y.php?r=settings/user/getdata" toolbar="#toolbar_user">
	<thead>
	<tr>
		<th field="username" width="60">登入名</th>
		<th field="realname" width="60">真实姓名</th>
	</tr>
	</thead>
</table>
<div id="toolbar_user">
	<?php
	if (Yii::app()->user->checkAccess('sysconfig_user_module_add')) {
		?>
		<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加用户','y.php?r=settings/user/showedit&user_id=0')" /a>
		<?php
	}
	if (Yii::app()->user->checkAccess('sysconfig_user_module_edit')) {
		?>
		<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="user_showedit()"/a>
		<?php
	}
	if (Yii::app()->user->checkAccess('sysconfig_user_module_remove')) {
		?>
		<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="user_remove()" />
		<?php
	}
	?>

</div>
<script>
	function user_showedit() {
		//获取选中的列
		var selected = $("#settings_user_list").datagrid("getSelected");
		if(selected==null){
			alert('请选择行！');
		}else{
			//进行编辑操作
			loadWin('编辑用户','y.php?r=settings/user/showedit&user_id='+selected.user_id);
		}
	}
	function user_remove() {
		var selected = $("#settings_user_list").datagrid("getSelected");
		if(selected==null){
			alert('请选择行！');
		}else{
			$.messager.confirm('确认','您确认想要删除记录吗？',function(r){
				if (r){
					$.post('y.php?r=settings/user/remove',{user_id:selected.user_id},function (res) {
						$("#settings_user_list").datagrid("reload");
					});
				}
			});
		}
	}

</script>