<form id="form_workflow_showedit" method="post" action="y.php?r=settings/workflow/savedata">
    <input type="hidden" name="id" value="">
    <table class="tab_form">
        <tr>
            <td><input class="easyui-textbox" name="name" data-options="label:'名称:',required:true"></td>
            <td><input class="easyui-textbox" name="description"
                       data-options="label:'描述:',required:true"></td>
            <td>
                <input class="easyui-textbox" name="code" data-options="label:'编码:',required:true">
            </td>
        </tr>
        </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">清空</a>
</div>
<script>
    var dataobj = <?php echo json_encode($workflow) ?>;
    $(function () {
        $('#form_workflow_showedit').form('load',dataobj);
    });
    function submitForm() {
        $('#form_workflow_showedit').form('submit',{
            success: function(){
                $('#win_main').window("close");
                $("#workflow_list").datagrid("reload");
            }
        });
    }
    function clearForm() {
        $('#form_workflow_showedit').form('clear');
        if(dataobj!=null){
            $("input[name=workflow_id]").val(dataobj.id);
        }
    }
</script>