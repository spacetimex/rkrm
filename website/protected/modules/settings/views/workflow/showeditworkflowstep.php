<div class="easyui-layout" data-options="fit:true" >
    <div data-options="region:'west'" style="width:50%">
        <table class="easyui-datagrid" id="workflow_showeditworkflowstep"
               fit="true" border="false"
               singleSelect="true" fitColumns="true" pagination="false"
               idField="id" url="y.php?r=settings/workflow/getworkflowsteps&id=<?php echo $workflow['id']; ?>"  toolbar="#toolbar_workflowstep">
            <thead>
            <tr>
                <th field="name" width="60">步骤名称</th>
                <th field="description" width="60">描述</th>
                <th field="step_level" width="60">顺序</th>
            </tr>
            </thead>
        </table>
        <div id="toolbar_workflowstep">
            <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="addNewStep()" />
            <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="removeStep()" />
        </div>
    </div>
    <div data-options="region:'center'">
        <form id="form_workflowstep_showedit" method="post" action="y.php?r=settings/workflow/saveworkflowstep">
            <input type="hidden" name="step_id" value="">
            <input type="hidden" name="code" value="<?php echo $workflow['code']; ?>">
            <input type="hidden" name="workflow_id" value="<?php echo $workflow['id']; ?>">
            <input type="hidden" name="users">
            <table class="tab_form">
                <tr>
                    <td><input class="easyui-textbox" name="name" id="name" data-options="label:'步骤名称:',required:true"/></td>
                </tr>
                <tr>
                    <td><input class="easyui-textbox" name="description" id="description"
                               data-options="label:'描述:',required:true"/></td>
                </tr>
                <tr>
                    <td>
                        <input class="easyui-textbox" name="step_level" id="step_level" data-options="label:'顺序:',required:true"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        选择审核人员：
                        <ul id="workflowstep_selectusers_tree"
                            class="easyui-tree" data-options="url:'y.php?r=settings/user/loadusertree',method:'get',animate:true,checkbox:true"></ul>
                    </td>
                </tr>
            </table>
        </form>
        <div style="text-align:center;padding:5px 0">
            <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
        </div>
    </div>
</div>
<script>
    $(function () {

        $('#workflow_showeditworkflowstep').datagrid({
            onClickRow: function(index, row){

                //选择步骤的行项目的时候，需要进行编辑操作
                $("#form_workflowstep_showedit").find("input[name=step_id]").val(row.id);
                $("#form_workflowstep_showedit").find("#name").textbox('setValue',row.name);
                $("#form_workflowstep_showedit").find("#description").textbox('setValue',row.description);
                $("#form_workflowstep_showedit").find("#step_level").textbox('setValue',row.step_level);

                //获取stepusers
                $.get('y.php?r=settings/workflowstep/getstepusers',{'stepid':row.id},function (res) {
                    //tree进行清空，然后进行赋值
                    var nodes = $('#workflowstep_selectusers_tree').tree('getChecked');
                    for(var i=0;i<nodes.length;i++){
                        $('#workflowstep_selectusers_tree').tree('uncheck',nodes[i].target);
                    }
                    for(var i=0;i<res.length;i++){
                        var node = $('#workflowstep_selectusers_tree').tree('find', res[i].user_id);
                        $('#workflowstep_selectusers_tree').tree('check', node.target);
                    }
                },'json');
            }
        });

    });
    function submitForm() {
        var users = getUsersChecked();
        if(users == ''){
            alert('请选择审核人员！');
            return;
        }
        $("input[name=users]").val(users);
        $('#form_workflowstep_showedit').form('submit', {
            success: function (data) {
                var json = eval('('+data+')');
                if(json.code == '10003'){
                    alert(json.message);
                }
                $("#workflow_showeditworkflowstep").datagrid("reload");
            }
        });
    }
    function getUsersChecked(){
        var nodes = $('#workflowstep_selectusers_tree').tree('getChecked');
        var s = '';
        for(var i=0; i<nodes.length; i++){
            if (s != '') s += ',';
            s += nodes[i].id
        }
        return s;
    }
    function addNewStep() {
        $("#form_workflowstep_showedit").find("input[name=step_id]").val("");
        $("#form_workflowstep_showedit").find("input[name=users]").val("");
        $("#form_workflowstep_showedit").find("#name").textbox('clear');
        $("#form_workflowstep_showedit").find("#description").textbox('clear');
        $("#form_workflowstep_showedit").find("#step_level").textbox('clear');

        var nodes = $('#workflowstep_selectusers_tree').tree('getChecked');
        for(var i=0;i<nodes.length;i++){
            $('#workflowstep_selectusers_tree').tree('uncheck',nodes[i].target);
        }
    }
    //进行步骤删除
    function removeStep() {
        var selected = $("#workflow_showeditworkflowstep").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=settings/workflowstep/removestep',{stepid:selected.id},function (res) {
                        $("#workflow_showeditworkflowstep").datagrid("reload");

                        //删除完之后，需要将form表单的数据清空
                        addNewStep();
                    });
                }
            });
        }
    }
</script>
