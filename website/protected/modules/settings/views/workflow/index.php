<table class="easyui-datagrid" id="workflow_list"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true"
       idField="id" url="y.php?r=settings/workflow/getdata" toolbar="#toolbar_workflow">
    <thead>
    <tr>
        <th field="name" width="60">工作流名称</th>
        <th field="description" width="60">描述</th>
        <th field="addtime" width="60">添加时间</th>
        <th field="code" width="60">工作流编号</th>
    </tr>
    </thead>
</table>
<div id="toolbar_workflow">
    <?php
    if (Yii::app()->user->checkAccess('sysconfig_workflow_module_add')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加工作流','y.php?r=settings/workflow/showedit&id=0')" /a>
        <?php
    }
    if (Yii::app()->user->checkAccess('sysconfig_workflow_module_edit')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="workflow_showedit()"/a>
        <?php
    }
    if (Yii::app()->user->checkAccess('sysconfig_workflow_module_remove')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="workflow_remove()" />
        <?php
    }
    if (Yii::app()->user->checkAccess('sysconfig_workflow_module_config')) {
        ?>
        <a href="#" title="配置工作流" class="easyui-linkbutton" data-options="iconCls:'icon-cog',plain:true" onclick="workflow_config()" />
        <?php
    }
    ?>
</div>
<script>
    function workflow_showedit() {
        //获取选中的列
        var selected = $("#workflow_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('编辑工作流','y.php?r=settings/workflow/showedit&id='+selected.id);
        }
    }
    function workflow_remove() {
        var selected = $("#workflow_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=settings/workflow/remove',{id:selected.id},function (res) {
                        $("#workflow_list").datagrid("reload");
                    });
                }
            });
        }
    }
    /**
     * 工作流进行配置
     */
    function workflow_config() {
        //获取选中的列
        var selected = $("#workflow_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('配置工作流流程','y.php?r=settings/workflow/showeditworkflowstep&id='+selected.id);
        }
    }
</script>