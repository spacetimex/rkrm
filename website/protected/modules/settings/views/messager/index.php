<form id="form_messager_showedit" method="post" action="y.php?r=settings/messager/saveconfig">
    <input type="hidden" name="module" value="message"/>
    <table class="tab_form">
        <tr>
            <td>云片网短信设置</td>
        </tr>
        <?php
        foreach ($configs as $config) {
            ?>
            <tr>
                <td><input class="easyui-textbox input_text" name="<?php echo $config['config_key']; ?>"
                           data-options="label:'<?php echo $config['config_description']; ?>:',required:true"
                           value="<?php echo $config['config_value']; ?>"></td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td><a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
            </td>
        </tr>
    </table>
    <script>
        function submitForm() {
            $('#form_messager_showedit').form('submit', {
                success: function (data) {
                    var json = eval('(' + data + ')');
                    if (json.code == '10000') {
                      alert("短信配置修改成功！");
                    }
                }
            });
        }
    </script>