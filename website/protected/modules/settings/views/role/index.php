<table class="easyui-datagrid" id="role_list"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true"
       idField="name" url="y.php?r=settings/role/getdata" toolbar="#toolbar_role">
    <thead>
    <tr>
        <th field="name" width="60">角色名称</th>
        <th field="description" width="60">描述</th>
    </tr>
    </thead>
</table>
<div id="toolbar_role">
    <?php
    if (Yii::app()->user->checkAccess('sysconfig_role_module_add')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加角色','y.php?r=settings/role/showedit&name=0')" /a>
        <?php
    }
    if (Yii::app()->user->checkAccess('sysconfig_role_module_edit')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="role_showedit()"/a>
        <?php
    }
    if (Yii::app()->user->checkAccess('sysconfig_role_module_remove')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="role_remove()" />
        <?php
    }
    ?>
</div>
<script>
    function role_showedit() {
        //获取选中的列
        var selected = $("#role_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('编辑角色','y.php?r=settings/role/showedit&name='+selected.name);
        }
    }
    function role_remove() {
        var selected = $("#role_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=settings/role/remove',{name:selected.name},function (res) {
                        $("#role_list").datagrid("reload");
                    });
                }
            });
        }
    }
</script>