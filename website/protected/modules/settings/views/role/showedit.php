<form id="form_role_showedit" method="post" action="y.php?r=settings/role/savedata">
    <input type="hidden" name="accesskeys" value=""/>
    <table class="tab_form">
        <tr>
            <td><input class="easyui-textbox" name="name"
                       data-options="label:'角色:',required:true"></td>
            <td><input class="easyui-textbox" name="description"
                       data-options="label:'描述:',required:true"></td>
            <td>
             </td>
        </tr>
        <tr>
            <td>全选：<input type="checkbox" id="role_checked_all" onchange="changeTreeChecked()"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3">
                <ul id="settings_role_showedit" class="easyui-tree"  data-options="url:'y.php?r=settings/role/tree',method:'get',animate:true,checkbox:true,cascadeCheck:false"></ul>
            </td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
</div>
<script>
    var dataobj = <?php echo json_encode($role) ?>;
    var items = <?php echo json_encode($item); ?>;
    $(function () {
        $('#form_role_showedit').form('load', dataobj);
        $("#settings_role_showedit").tree({
            onLoadSuccess:function (node,data) {
                if(items.length>0){
                    for(var i=0;i<items.length;i++){
                        var node = $('#settings_role_showedit').tree('find', items[i].child);
                        if(node!=null){
                            $('#settings_role_showedit').tree('check', node.target);
                        }
                    }
                }
            }
        });
    });
    function submitForm() {
        var accesskeys = getChecked();
        $("input[name=accesskeys]").val(accesskeys);
        $('#form_role_showedit').form('submit', {
            success: function () {
                $('#win_main').window("close");
                $("#role_list").datagrid("reload");
            }
        });
    }
    function clearForm() {
        $('#form_role_showedit').form('clear');
        if (dataobj != null) {
            $("input[name=role_id]").val(dataobj.role_id);
        }
    }
    function getChecked(){
        var nodes = $('#settings_role_showedit').tree('getChecked');
        var s = '';
        for(var i=0; i<nodes.length; i++){
            if (s != '') s += ',';
            s += nodes[i].accesskey
        }
        return s;
    }
    //将树形菜单全选或者不选中
    function changeTreeChecked() {
        var ischecked = $("#role_checked_all").is(":checked");
        if(ischecked == true){
            //全部选中
            var roots = $('#settings_role_showedit').tree('getRoots');
            for(var i=0;i<roots.length;i++){
                $('#settings_role_showedit').tree('check', roots[i].target);
                //获取getChildren
                var level2_children = $('#settings_role_showedit').tree('getChildren', roots[i].target);
                for(var level2_index=0;level2_index<level2_children.length;level2_index++){
                    $('#settings_role_showedit').tree('check', level2_children[level2_index].target);
                    //获取getChildren
                    var level3_children = $('#settings_role_showedit').tree('getChildren', level2_children[level2_index].target);
                    for(var level3_index=0;level3_index<level3_children.length;level3_index++){
                        $('#settings_role_showedit').tree('check', level3_children[level3_index].target);
                    }
                }
            }
        }else{
            var roots = $('#settings_role_showedit').tree('getRoots');
            for(var i=0;i<roots.length;i++){
                $('#settings_role_showedit').tree('uncheck', roots[i].target);
                //获取getChildren
                var level2_children = $('#settings_role_showedit').tree('getChildren', roots[i].target);
                for(var level2_index=0;level2_index<level2_children.length;level2_index++){
                    $('#settings_role_showedit').tree('uncheck', level2_children[level2_index].target);
                    //获取getChildren
                    var level3_children = $('#settings_role_showedit').tree('getChildren', level2_children[level2_index].target);
                    for(var level3_index=0;level3_index<level3_children.length;level3_index++){
                        $('#settings_role_showedit').tree('uncheck', level3_children[level3_index].target);
                    }
                }
            }
        }

    }
</script>