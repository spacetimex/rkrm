<form id="form_panel_showedit" method="post" action="y.php?r=settings/panel/savedata">
    <input type="hidden" name="panel_id" value="">
    <table class="tab_form">
        <tr>
            <td>
                <select class="easyui-combobox" name="modulename" url="y.php?r=middle/module/getdata&id=<?php echo $panel['moduleenname'];?>"
                        data-options="label:'模块:',required:true,
        valueField: 'id',
        textField: 'text',
">
                </select>
            </td>
            <td><input class="easyui-textbox" name="panelname"
                       data-options="label:'面板名称:',required:true" style="width: 90%;" value="<?php echo $panel['panelname']; ?>"></td>
            <td><input class="easyui-textbox" name="seq"
                       data-options="label:'排序:',required:true" style="width: 90%;" ></td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
</div>
<script>
    var dataobj = <?php echo json_encode($panel) ?>;
    $(function () {
        if (dataobj == null) {

        } else {
            $('#form_panel_showedit').form('load', dataobj);
        }
    });
    function submitForm() {
        $('#form_panel_showedit').form('submit', {
            success: function () {
                $('#win_main').window("close");
                $("#settings_panel_list").datagrid("reload");
            }
        });
    }
    function clearForm() {
        $('#form_panel_showedit').form('clear');
        if (dataobj != null) {
            $("input[name=panel_id]").val(dataobj.panel_id);
        }
    }

</script>