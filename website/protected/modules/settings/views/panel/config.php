<form id="form_panel_showedit" method="post" action="y.php?r=settings/panel/saveconfig">
    <input type="hidden" name="panel_id" value="<?php echo $panel_id; ?>">
    <input type="hidden" name="moduleenname" value="<?php echo $moduleenname; ?>">
    <table class="tab_form">
        <?php
        foreach ($columns as $c){
            ?>
            <tr>
                <td class="tal"><input type="checkbox" name="columnnames[]" id="ck_<?php echo $c['columnname'] ?>" disabled="disabled" value="<?php echo $c['columnname'] ?>"/><?php echo $c['columnlabel'] ?></td>
            </tr>
            <?php
        }
        ?>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
</div>
<script>
    var uncheckedcolumns = <?php echo json_encode($uncheckedcolumns) ?>;
    var panelchecked = <?php echo json_encode($panelchecked) ?>;
    $(function () {
        //将可以选择的项目，设置成可以选择
        if(uncheckedcolumns.length>0){
            for(var i=0;i<uncheckedcolumns.length;i++){
                $("#ck_"+uncheckedcolumns[i].columnname).removeAttr("disabled");
            }
        }
        if(panelchecked.length>0){
            for(var i=0;i<panelchecked.length;i++){
                for(var i=0;i<panelchecked.length;i++){
                    $("#ck_"+panelchecked[i].columnname).removeAttr("disabled");
                    $("#ck_"+panelchecked[i].columnname).attr("checked","true");
                }
            }
        }
    });
    function submitForm() {
        $('#form_panel_showedit').form('submit', {
            success: function () {
                $('#win_main').window("close");
                $("#settings_panel_list").datagrid("reload");
            }
        });
    }
</script>