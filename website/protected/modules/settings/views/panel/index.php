<table class="easyui-datagrid" id="settings_panel_list"
       fit="true" border="false"
       singleSelect="true" fitColumns="true"
       idField="panel_id" url="y.php?r=settings/panel/getdata" toolbar="#toolbar_panel">
    <thead>
    <tr>
        <th field="modulecnname" width="60">模块</th>
        <th field="moduleenname" width="60">编码</th>
        <th field="panelname" width="60">面板名称</th>
        <th field="seq" width="60" >排序</th>
    </tr>
    </thead>
</table>
<div id="toolbar_panel">
    <select id="panel_module_modulename" style="width: 120px;" class="easyui-combobox"
            data-options="url:'y.php?r=middle/module/getdata&id=',valueField:'id',textField:'text'">
    </select>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true" onclick="querypanel()" /a>
    <?php
    //	if (Yii::app()->panel->checkAccess('sysconfig_panel_module_add')) {
    ?>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加面板','y.php?r=settings/panel/showedit&panel_id=0')" /a>
    <?php
    //	}
    //	if (Yii::app()->panel->checkAccess('sysconfig_panel_module_remove')) {
    ?>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="panel_showedit()"/a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="panel_remove()" />
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-cog',plain:true" onclick="panel_config()" />
    <?php
    //	}
    ?>

</div>
<script>
    function panel_remove() {
        var selected = $("#settings_panel_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=settings/panel/remove',{panel_id:selected.panel_id},function (res) {
                        $("#settings_panel_list").datagrid("reload");
                    });
                }
            });
        }
    }
    function formatter_display(value,row,index) {
        if(value == 1){
            return "<span class='green'><i class=\"fa fa-check \"></i></span>";
        }else{
            return "<span class='red'><i class=\"fa fa-close\"></i></span>";
        }
    }
    function formatter_ismust(value,row,index) {
        if(value == 1){
            return "<span class='green'><i class=\"fa fa-check \"></i></span>";
        }else{
            return "<span class='red'><i class=\"fa fa-close\"></i></span>";
        }
    }
    function panel_showedit() {
        var selected = $("#settings_panel_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('编辑视图','y.php?r=settings/panel/showedit&panel_id='+selected.panel_id);
        }
    }
    function querypanel() {
        var cbovalue = $('#panel_module_modulename').combobox("getValue");
        $('#settings_panel_list').datagrid('load', {
            modulename: cbovalue
        });
    }
    function panel_config() {
        var selected = $("#settings_panel_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            loadWin('编辑表单排版','y.php?r=settings/panel/config&moduleenname='+selected.moduleenname+'&panel_id='+selected.panel_id);
        }
    }
</script>