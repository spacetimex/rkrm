<form id="form_system_showedit" method="post" action="y.php?r=settings/system/saveconfig">
    <input type="hidden" name="module" value="message"/>
    <table class="tab_form">
        <tr>
            <td>系统设置</td>
        </tr>
        <?php
        foreach ($configs as $config) {
            ?>
            <tr>
                <td><input class="easyui-textbox" name="<?php echo $config['config_key']; ?>"
                           data-options="label:'<?php echo $config['config_description']; ?>:',required:true"
                           value="<?php echo $config['config_value']; ?>"></td>
            </tr>
            <?php
        }
        if (Yii::app()->user->checkAccess('sysconfig_system_module_save')) {
            ?>
            <tr>
                <td><a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
    <script>
        function submitForm() {
            $('#form_system_showedit').form('submit', {
                success: function (data) {
                    var json = eval('(' + data + ')');
                    if (json.code == '10000') {
                        alert("系统配置修改成功！");
                    }
                }
            });
        }
    </script>