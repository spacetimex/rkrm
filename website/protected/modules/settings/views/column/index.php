<table class="easyui-datagrid" id="settings_column_list"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true"
       idField="column_id" url="y.php?r=settings/column/getdata" toolbar="#toolbar_column" data-options="pageSize:100,pageList:[100]" >
    <thead>
    <tr>
        <th field="columnname" width="60">字段名称</th>
        <th field="columnlabel" width="60" >标签</th>
        <th field="columntype" width="60" >字段类型</th>
        <th field="columndatatype" width="60" >数据类型</th>
        <th field="display" width="60" formatter="formatter_display" >是否展示</th>
        <th field="ismust" width="60" formatter="formatter_ismust" >是否必须</th>
        <th field="modulecnname" width="60" >模块</th>
    </tr>
    </thead>
</table>
<div id="toolbar_column">
    <select id="column_module_modulename" style="width: 120px;" class="easyui-combobox"
            data-options="url:'y.php?r=middle/module/getdata&id=',valueField:'id',textField:'text'">
    </select>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true" onclick="queryColumn()" /a>
    <?php
    //	if (Yii::app()->column->checkAccess('sysconfig_column_module_add')) {
    ?>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加字段','y.php?r=settings/column/showedit&column_id=0')" /a>
    <?php
    //	}
    //	if (Yii::app()->column->checkAccess('sysconfig_column_module_remove')) {
    ?>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="column_remove()" />
    <?php
    //	}
    ?>

</div>
<script>
    function column_remove() {
        var selected = $("#settings_column_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=settings/column/remove',{column_id:selected.column_id},function (res) {
                        $("#settings_column_list").datagrid("reload");
                    });
                }
            });
        }
    }
    function formatter_display(value,row,index) {
        if(value == 1){
            return "<span class='green'><i class=\"fa fa-check \"></i></span>";
        }else{
            return "<span class='red'><i class=\"fa fa-close\"></i></span>";
        }
    }
    function formatter_ismust(value,row,index) {
        if(value == 1){
            return "<span class='green'><i class=\"fa fa-check \"></i></span>";
        }else{
            return "<span class='red'><i class=\"fa fa-close\"></i></span>";
        }
    }
    function queryColumn() {
        var cbovalue = $('#column_module_modulename').combobox("getValue");
        $('#settings_column_list').datagrid('load', {
            modulename: cbovalue
        });

    }
</script>