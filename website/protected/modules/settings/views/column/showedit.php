<form id="form_column_showedit" method="post" action="y.php?r=settings/column/savedata">
    <input type="hidden" name="column_id" value="">
    <table class="tab_form">
        <tr>
            <td>
                <select class="easyui-combobox" name="modulename" url="y.php?r=middle/module/getdata&id="
                        data-options="label:'模块:',required:true,
        valueField: 'id',
        textField: 'text',
">
                </select>
            </td>
            <td><input class="easyui-textbox" name="columnname"
                       data-options="label:'字段名称-英文:',required:true"></td>
            <td><input class="easyui-textbox" name="columnlabel"
                       data-options="label:'字段描述:',required:true"></td>
        </tr>
        <tr>
            <td><select class="easyui-combobox" name="columntype"  data-options="label:'字段类型:',required:true" >
                    <option value="basic">基本类型</option>
                </select></td>
            <td><input class="easyui-textbox" name="columndatatype"
                       data-options="label:'数据类型:',required:true" ></td>
            <td><label class="textbox-label textbox-label-before">字段是否展示:</label><input id="switchIsdisplay"
            class="easyui-switchbutton" onText="是" offText="否" value="1" >
                <input type="hidden" name="display" ></td>
        </tr>
        <tr>
            <td><label class="textbox-label textbox-label-before">必须字段:</label><input id="switchIsmust"
                                                                                      class="easyui-switchbutton" onText="是" offText="否" value="1" >
                <input type="hidden" name="ismust" ></td>
            <td></td><td></td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
</div>
<script>
    $(function () {
        $('#switchIsdisplay').switchbutton({
            onChange: function(checked){
                if(checked == true){
                    $("input[name=display]").val(1);
                }else{
                    $("input[name=display]").val(0);
                }
            }
        });
        $('#switchIsmust').switchbutton({
            onChange: function(checked){
                if(checked == true){
                    $("input[name=ismust]").val(1);
                }else{
                    $("input[name=ismust]").val(0);
                }
            }
        });
    });
    function submitForm() {
        $('#form_column_showedit').form('submit', {
            success: function () {
                $('#win_main').window("close");
                $("#settings_column_list").datagrid("reload");
            }
        });
    }
</script>