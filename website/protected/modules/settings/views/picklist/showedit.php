<form id="form_picklist_showedit" method="post" action="y.php?r=settings/picklist/savedata">
    <input type="hidden" name="picklistkey_id" value="">
    <table class="tab_form">
        <tr>
            <td><input class="easyui-textbox" name="picklistkey" data-options="label:'数据字典键:',required:true"></td>
            <td><input class="easyui-textbox" name="description"
                       data-options="label:'描述:',required:true"></td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <textarea style="width: 61%;resize: none;height: 80px;" class="iptborder" data-options="label:'数据字典键:',required:true" name="picklistvalues" ><?php echo $pickliststr; ?></textarea>
                <span class="block">每行为一个数据</span>
            </td>
        </tr>
        </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">清空</a>
</div>
<script>
    var dataobj = <?php echo json_encode($picklistkey) ?>;
    $(function () {
        $('#form_picklist_showedit').form('load',dataobj);
        if(dataobj!=null){
            $("input[name=picklistkey]").textbox({
                readonly:true
            });
        }
    });
    function submitForm() {
        $('#form_picklist_showedit').form('submit',{
            success: function(){
                $('#win_main').window("close");
                $("#picklist_list").datagrid("reload");
            }
        });
    }
    function clearForm() {
        $('#form_picklist_showedit').form('clear');
        if(dataobj!=null){
            $("input[name=picklist_id]").val(dataobj.picklist_id);
        }
    }
</script>