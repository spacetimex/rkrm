<table class="easyui-datagrid" id="picklist_list"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true"
       idField="picklistkey_id" url="y.php?r=settings/picklist/getdata" toolbar="#toolbar_picklist">
    <thead>
    <tr>
        <th field="description" width="60">描述</th>
        <th field="picklistkey" width="60">数据字典键</th>
    </tr>
    </thead>
</table>
<div id="toolbar_picklist">
    <?php
    if (Yii::app()->user->checkAccess('sysconfig_picklist_module_add')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加数据字典','y.php?r=settings/picklist/showedit&picklistkey_id=0')" /a>
        <?php
    }
    if (Yii::app()->user->checkAccess('sysconfig_picklist_module_edit')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="picklist_showedit()"/a>
        <?php
    }
    if (Yii::app()->user->checkAccess('sysconfig_picklist_module_remove')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="picklist_remove()" />
        <?php
    }
    ?>
</div>
<script>
    function picklist_showedit() {
        //获取选中的列
        var selected = $("#picklist_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('编辑数据字典','y.php?r=settings/picklist/showedit&picklistkey_id='+selected.picklistkey_id);
        }
    }
    function picklist_remove() {
        var selected = $("#picklist_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=settings/picklist/remove',{picklistkey_id:selected.picklistkey_id},function (res) {
                        $("#picklist_list").datagrid("reload");
                    });
                }
            });
        }
    }
</script>