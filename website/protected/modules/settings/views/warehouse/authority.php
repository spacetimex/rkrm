<form id="form_authority_showedit" method="post" action="y.php?r=settings/warehouse/saveauthority">
    <input type="hidden" name="warehouse_id" value="<?php echo $warehouse['warehouse_id']; ?>">
    <input type="hidden" name="users" value="<?php echo $userids; ?>">
    <table class="tab_form">
        <tr>
            <td>
                选择需要关联的用户：
                <ul id="authority_user_tree"
                    class="easyui-tree" data-options="url:'y.php?r=middle/user/getdata',method:'get',animate:true,checkbox:true"></ul>
            </td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
</div>
<script>
    $(function () {
        $('#authority_user_tree').tree({
            onLoadSuccess: treeloadsuccessful
        });

    });
    function treeloadsuccessful() {
        var users = $("input[name=users]").val();
        if(users != "" && users !=null ){
            //初始化树形菜单的数据
            var res = users.split(",");
            for(var i=0;i<res.length;i++){
                var node = $('#authority_user_tree').tree('find', res[i]);
                $('#authority_user_tree').tree('check', node.target);
            }
        }
    }
    function getuserChecked(){
        var nodes = $('#authority_user_tree').tree('getChecked');
        var s = '';
        for(var i=0; i<nodes.length; i++){
            if (s != '') s += ',';
            s += nodes[i].id
        }
        return s;
    }
    function submitForm() {
        var users = getuserChecked();
//        if(users == ''){
//            alert('请选择工作流程！');
//            return;
//        }
        $("input[name=users]").val(users);

        $('#form_authority_showedit').form('submit', {
            success: function (data) {
                $('#win_main').window("close");
            }
        });
    }
</script>