<table id="settings_warehouse_list" class="easyui-datagrid"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true" toolbar="#toolbar_settings_warehouse"
       idField="warehouse_id" url="y.php?r=settings/warehouse/getdata">
    <thead>
    <tr>
        <th field="warehouse_name" width="60">仓库名称</th>
        <th field="description" width="60">描述</th>
        <th field="enabled" width="60" data-options="formatter:format_warehouse_list_enabled">启用</th>
    </tr>
    </thead>
</table>
<div id="toolbar_settings_warehouse">
    <?php
    if (Yii::app()->user->checkAccess('sysconfig_warehouse_module_add')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加仓库','y.php?r=settings/warehouse/showedit&warehouse_id=0')" /a>
        <?php
    }
    if (Yii::app()->user->checkAccess('sysconfig_warehouse_module_edit')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="settings_warehouse_showedit()" />
        <?php
    }
    if (Yii::app()->user->checkAccess('sysconfig_warehouse_module_authority')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-man',plain:true" onclick="settings_warehouse_authority()" />
        <?php
    }
    if (Yii::app()->user->checkAccess('sysconfig_warehouse_module_remove')) {
        ?>
        <a href="#" class="easyui-linkbutton" title="禁用仓库" data-options="iconCls:'icon-remove',plain:true" onclick="settings_warehouse_remove()" />
        <?php
    }
    ?>
</div>
<script>
    function settings_warehouse_showedit() {
        //获取选中的列
        var selected = $("#settings_warehouse_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('添加仓库','y.php?r=settings/warehouse/showedit&warehouse_id='+selected.warehouse_id);
        }
    }
    function settings_warehouse_remove() {
        var selected = $("#settings_warehouse_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除仓库吗？',function(r){
                if (r){
                    $.post('y.php?r=settings/warehouse/remove',{warehouse_id:selected.warehouse_id},function (res) {
                        $("#settings_warehouse_list").datagrid("reload");
                    });
                }
            });
        }
    }
    function format_warehouse_list_enabled(value,row,index) {
        if(value == '1'){
            return "启用";
        }else{
            return "禁用";
        }
    }
    /**
     * 权限设置
     */
    function settings_warehouse_authority() {
        var selected = $("#settings_warehouse_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('编辑模块关联的工作流','y.php?r=settings/warehouse/authority&warehouse_id='+selected.warehouse_id);
        }
    }
</script>
