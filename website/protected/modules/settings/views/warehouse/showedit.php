<form id="form_warehouse_showedit" method="post" action="y.php?r=settings/warehouse/savedata">
    <input type="hidden" name="warehouse_id" value="">
    <table class="tab_form">
        <tr>
            <td><input class="easyui-textbox" name="warehouse_name"  data-options="label:'仓库名:',required:true"></td>
            <td><input class="easyui-textbox" name="description"
                       data-options="label:'描述:'"></td>
            <td></td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
</div>
<script>
    var dataobj = <?php echo json_encode($warehouse) ?>;
    $(function () {
        $('#form_warehouse_showedit').form('load',dataobj);
    });
    function submitForm() {
        $('#form_warehouse_showedit').form('submit',{
            success: function(){
                $('#win_main').window("close");
                $("#settings_warehouse_list").datagrid("reload");
            }
        });
    }
    function clearForm() {
        $('#form_warehouse_showedit').form('clear');
        if(dataobj!=null){
            $("input[name=warehouse_id]").val(dataobj.warehouse_id);
        }
    }
</script>