<?php

class LoginhistoryController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
    
    public function actionGetdata(){

        $user_id = $_REQUEST['user_id'];
        $querystr = "";
        if(!is_null($user_id)){
            $querystr .= " and user_id = ".$user_id;
        }

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_loginhistory  where 1=1 {$querystr}")->queryScalar();
        $result = $this->connection->createCommand("select * from crm_loginhistory where 1=1 {$querystr} order by createtime desc limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

}