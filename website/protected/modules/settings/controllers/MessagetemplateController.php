<?php

class MessagetemplateController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from sys_messagetemplate where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select * from sys_messagetemplate where deleted = 0  limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $messagetemplate_id = $_REQUEST['messagetemplate_id'];

        $messagetemplate = null;
        if($messagetemplate_id==0){
            //新增
        }else{
            //编辑
            $messagetemplate = $this->connection->createCommand("select * from sys_messagetemplate where messagetemplate_id = :messagetemplate_id")->bindParam(':messagetemplate_id',$messagetemplate_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['messagetemplate'=>$messagetemplate]);
    }

    public function actionSavedata(){

        $messagetemplate_id = $_REQUEST['messagetemplate_id'];
        $signname = $_REQUEST['signname'];
        $templatecode = $_REQUEST['templatecode'];

        if($messagetemplate_id!=null){
            //编辑
            $this->connection->createCommand("update sys_messagetemplate set signname = :signname,templatecode = :templatecode where messagetemplate_id = :messagetemplate_id")
                ->bindParam(':messagetemplate_id',$messagetemplate_id,PDO::PARAM_INT)
                ->bindParam(':signname',$signname,PDO::PARAM_STR)
                ->bindParam(':templatecode',$templatecode,PDO::PARAM_INT)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into sys_messagetemplate (signname,templatecode) values (:signname,:templatecode) ")
                ->bindParam(':signname',$signname,PDO::PARAM_STR)
                ->bindParam(':templatecode',$templatecode,PDO::PARAM_INT)
                ->execute();
        }
    }

    public function actionRemove(){
        $messagetemplate_id = $_REQUEST['messagetemplate_id'];
        $this->connection->createCommand("update sys_messagetemplate set deleted = 1 
where messagetemplate_id = :messagetemplate_id")
            ->bindParam(':messagetemplate_id',$messagetemplate_id,PDO::PARAM_INT)
            ->execute();
    }
    
}