<?php

class ViewController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata()
    {

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page - 1) * $rows;
        $total = $this->connection->createCommand("select count(1) from crm_module_view where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select cmv.*,cm.modulename modulecnname from crm_module_view cmv 
left join crm_module cm on cmv.modulename = cm.moduleenname  where cmv.deleted = 0  limit {$rows}
 offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $view_id = $_REQUEST['view_id'];

        $view = null;
        if($view_id==0){
            //新增
        }else{
            //编辑
            $view = $this->connection->createCommand("select * from crm_module_view where view_id = :view_id")->bindParam(':view_id',$view_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['view'=>$view]);
    }

    public function actionSavedata()
    {

        $view_id = $_REQUEST['view_id'];
        $modulename = $_REQUEST['modulename'];
        $viewname = $_REQUEST['viewname'];
        $isdefault = $_REQUEST['isdefault'];
        $iskeyview = $_REQUEST['iskeyview'];
        $seq = $_REQUEST['seq'];
        $viewcountsql = $_REQUEST['viewcountsql'];
        $viewquerysql = $_REQUEST['viewquerysql'];
        $viewcolumnsjson = $_REQUEST['viewcolumnsjson'];

        if ($view_id != null) {
            //编辑
            $this->connection->createCommand("update crm_module_view set modulename = :modulename,isdefault = :isdefault,iskeyview=:iskeyview,
seq=:seq,viewname=:viewname,viewcountsql=:viewcountsql,viewquerysql=:viewquerysql,viewcolumnsjson=:viewcolumnsjson 
where view_id = :view_id")
                ->bindParam(':view_id', $view_id, PDO::PARAM_INT)
                ->bindParam(':modulename', $modulename, PDO::PARAM_STR)
                ->bindParam(':isdefault', $isdefault, PDO::PARAM_STR)
                ->bindParam(':iskeyview', $iskeyview, PDO::PARAM_STR)
                ->bindParam(':seq', $seq, PDO::PARAM_STR)
                ->bindParam(':viewname', $viewname, PDO::PARAM_STR)
                ->bindParam(':viewcountsql', $viewcountsql, PDO::PARAM_STR)
                ->bindParam(':viewquerysql', $viewquerysql, PDO::PARAM_STR)
                ->bindParam(':viewcolumnsjson', $viewcolumnsjson, PDO::PARAM_STR)
                ->execute();
        } else {
            //添加
            $this->connection->createCommand("insert into crm_module_view (modulename,isdefault,iskeyview,seq,viewname,viewcountsql,viewquerysql,viewcolumnsjson) values 
(:modulename,:isdefault,:iskeyview,:seq,:viewname,:viewcountsql,:viewquerysql,:viewcolumnsjson ) ")
                ->bindParam(':modulename', $modulename, PDO::PARAM_STR)
                ->bindParam(':isdefault', $isdefault, PDO::PARAM_STR)
                ->bindParam(':iskeyview', $iskeyview, PDO::PARAM_STR)
                ->bindParam(':seq', $seq, PDO::PARAM_STR)
                ->bindParam(':viewname', $viewname, PDO::PARAM_STR)
                ->bindParam(':viewcountsql', $viewcountsql, PDO::PARAM_STR)
                ->bindParam(':viewquerysql', $viewquerysql, PDO::PARAM_STR)
                ->bindParam(':viewcolumnsjson', $viewcolumnsjson, PDO::PARAM_STR)
                ->execute();
        }
    }

    public function actionRemove()
    {
        $view_id = $_REQUEST['view_id'];
        $this->connection->createCommand("update crm_module_view set deleted = 1 
where view_id = :view_id")
            ->bindParam(':view_id', $view_id, PDO::PARAM_INT)
            ->execute();
    }

}