<?php

class ColumnController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata()
    {
        $modulename = isset($_REQUEST['modulename'])?$_REQUEST['modulename']:null;
        $qstr = "";
        $qstr_res = "";
        if(!is_null($modulename) && $modulename!=""){
            $qstr .=" and modulename = '".$modulename."'";
            $qstr_res .= " and cm.moduleenname = '".$modulename."'";
        }

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page - 1) * $rows;
        $total = $this->connection->createCommand("select count(1) from crm_module_column where deleted = 0 ".$qstr)->queryScalar();
        $result = $this->connection->createCommand("select cmv.*,cm.modulename modulecnname from crm_module_column cmv 
left join crm_module cm on cmv.modulename = cm.moduleenname  where cmv.deleted = 0 ".$qstr_res." limit {$rows}
 offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){
        $this->render('showedit');
    }

    public function actionSavedata()
    {

        $modulename = $_REQUEST['modulename'];
        $columnname = $_REQUEST['columnname'];
        $columnlabel = $_REQUEST['columnlabel'];
        $columntype = $_REQUEST['columntype'];
        $columndatatype = $_REQUEST['columndatatype'];
        $display = $_REQUEST['display'];
        $ismust = $_REQUEST['ismust'];

        $module =  $this->connection->createCommand("select * from crm_module where moduleenname = :modulename")
            ->bindParam(':modulename', $modulename, PDO::PARAM_STR)
            ->queryRow();
        //添加
        $this->connection->createCommand("alter table {$module['moduletable']} add {$columnname} {$columndatatype}")
            ->execute();
        $this->connection->createCommand("alter table {$module['moduletable']} modify column {$columnname} int comment :columnlabel")
            ->bindParam(':columnlabel', $columnlabel, PDO::PARAM_STR)
            ->execute();
        $this->connection->createCommand("insert into crm_module_column (columnname, columnlabel, columntype, columndatatype,
 display, ismust, modulename) values (:columnname,:columnlabel,:columntype,:columndatatype,:display,:ismust,:modulename) ")
            ->bindParam(':columnname',$columnname, PDO::PARAM_STR)
            ->bindParam(':columnlabel', $columnlabel, PDO::PARAM_STR)
            ->bindParam(':columntype',$columntype, PDO::PARAM_STR)
            ->bindParam(':columndatatype', $columndatatype, PDO::PARAM_STR)
            ->bindParam(':display',$display, PDO::PARAM_STR)
            ->bindParam(':ismust', $ismust, PDO::PARAM_STR)
            ->bindParam(':modulename', $modulename, PDO::PARAM_STR)
            ->execute();

    }

    public function actionRemove()
    {
        $column_id = $_REQUEST['column_id'];
        $this->connection->createCommand("update crm_module_column set deleted = 1 
where column_id = :column_id")
            ->bindParam(':column_id', $column_id, PDO::PARAM_INT)
            ->execute();
    }

}