<?php

class WorkflowController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from oa_workflow where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select * from oa_workflow where deleted = 0  limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $id = $_REQUEST['id'];

        $workflow = null;
        if($id==0){
            //新增
        }else{
            /**
             * 编辑操作
             */
            $workflow = $this->connection->createCommand("select * from oa_workflow where id = :id")->bindParam(':id',$id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['workflow'=>$workflow]);
    }

    public function actionRemove(){
        $id = $_REQUEST['id'];
        $this->connection->createCommand("update oa_workflow set deleted = 1 
where id = :id")
            ->bindParam(':id',$id,PDO::PARAM_INT)
            ->execute();
    }

    /**
     * 工作流，进行编辑之前，需要查询系统中是否还存在，为结束的工作流程，如果还有未存在的工作流程。则不能进行编辑
     */
    public function actionShoweditworkflowstep(){

        $id = $_REQUEST['id'];
        $workflow = $this->connection->createCommand("select * from oa_workflow where id = :id")->bindParam(':id',$id,PDO::PARAM_INT)->queryRow();


        $this->render('showeditworkflowstep',['workflow'=>$workflow]);
    }

    public function actionSaveworkflowstep(){
        $workflow_id = $_REQUEST['workflow_id'];
        $step_id = $_REQUEST['step_id'];
        $code = $_REQUEST['code'];
        $step_level = $_REQUEST['step_level'];
        $name = $_REQUEST['name'];
        $description = $_REQUEST['description'];
        $users = $_REQUEST['users'];
        $users_list = explode(",", $users);

        //判断设置的工作流的步骤的step_level是否重复
        $levelexists = $this->connection->createCommand("select * from oa_workflow_step where workflow_id = :workflow_id and step_level = :step_level and id!=:step_id")
            ->bindParam(':workflow_id',$workflow_id,PDO::PARAM_INT)
            ->bindParam(':step_level',$step_level,PDO::PARAM_INT)
            ->bindParam(':step_id',$step_id,PDO::PARAM_INT)
            ->queryRow();

        if($levelexists!=null){
            //对应的步骤的step已经存在，不允许重复
            echo json_encode(Appcode::error_workflowstep);
        }else{

            //进行工作流的保存,1.STEP表格，关联的user表
            if($step_id == null){
                //insert操作
                $this->connection->createCommand("insert into oa_workflow_step (workflow_id, name, description, step_level, code) values 
(:workflow_id, :name, :description, :step_level, :code)")
                    ->bindParam(':workflow_id',$workflow_id,PDO::PARAM_INT)
                    ->bindParam(':name',$name,PDO::PARAM_STR)
                    ->bindParam(':description',$description,PDO::PARAM_STR)
                    ->bindParam(':step_level',$step_level,PDO::PARAM_STR)
                    ->bindParam(':code',$code,PDO::PARAM_STR)
                    ->execute();

                //将users插入到对应的表
                $step_id = $this->connection->getLastInsertID();

                foreach ($users_list as $u){
                    $this->connection->createCommand("insert into oa_workflow_user (workflow_step_id, user_id, workflow_id) values 
(:workflow_step_id, :user_id, :workflow_id)")
                        ->bindParam(':workflow_step_id',$step_id,PDO::PARAM_INT)
                        ->bindParam(':user_id',$u,PDO::PARAM_STR)
                        ->bindParam(':workflow_id',$workflow_id,PDO::PARAM_STR)
                        ->execute();
                }

            }else{
                //update操作
                $this->connection->createCommand("update oa_workflow_step set name = :name, description = :description,
 step_level = :step_level where id = :workflow_step_id")
                    ->bindParam(':name',$name,PDO::PARAM_STR)
                    ->bindParam(':description',$description,PDO::PARAM_STR)
                    ->bindParam(':step_level',$step_level,PDO::PARAM_STR)
                    ->bindParam(':workflow_step_id',$step_id,PDO::PARAM_INT)
                    ->execute();

                //清空之前步骤中设置的USERS
                $this->connection->createCommand("delete from oa_workflow_user where workflow_step_id = :workflow_step_id")
                    ->bindParam(':workflow_step_id',$step_id,PDO::PARAM_INT)
                    ->execute();

                foreach ($users_list as $u){
                    $this->connection->createCommand("insert into oa_workflow_user (workflow_step_id, user_id, workflow_id) values 
(:workflow_step_id, :user_id, :workflow_id)")
                        ->bindParam(':workflow_step_id',$step_id,PDO::PARAM_INT)
                        ->bindParam(':user_id',$u,PDO::PARAM_STR)
                        ->bindParam(':workflow_id',$workflow_id,PDO::PARAM_STR)
                        ->execute();
                }

            }

            echo json_encode(Appcode::success);
        }

    }

    public function actionGetworkflowsteps(){
        $id = $_REQUEST['id'];
        $workflow = $this->connection->createCommand("select * from oa_workflow where id = :id")->bindParam(':id',$id,PDO::PARAM_INT)->queryRow();
        $workflowsteps = $this->connection->createCommand("select * from oa_workflow_step where workflow_id = :workflow_id order by step_level asc")->bindParam(':workflow_id',$id,PDO::PARAM_INT)->queryAll();

        $data = Array();
        $data['rows'] = $workflowsteps;

        echo json_encode($data);
    }

    public function actionSavedata(){
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $description = $_REQUEST['description'];
        $code = $_REQUEST['code'];

        if($id==null&&$id==""){
            //添加
            $this->connection->createCommand("insert into oa_workflow (name,description,code) values 
(:name,:description,:code) ")
                ->bindParam(':name',$name,PDO::PARAM_STR)
                ->bindParam(':description',$description,PDO::PARAM_STR)
                ->bindParam(':code',$code,PDO::PARAM_STR)
                ->execute();
        }else{
            //编辑
            $this->connection->createCommand("update oa_workflow set name = :name,
description = :description,code=:code,type=:type where id = :id")
                ->bindParam(':id',$id,PDO::PARAM_INT)
                ->bindParam(':name',$name,PDO::PARAM_STR)
                ->bindParam(':description',$description,PDO::PARAM_STR)
                ->bindParam(':code',$code,PDO::PARAM_STR)
                ->execute();
        }
    }

    public function actionLoadworkflowtree(){
        $result = $this->connection->createCommand("SELECT id AS id,name as text FROM oa_workflow")->queryAll();
        echo json_encode($result);
    }

}