<?php

class SystemController extends Controller
{
    public function actionIndex()
    {
        $configs = $this->connection->createCommand("select * from sys_config where module = 'system' order by seq asc ")->queryAll();

        $this->render('index',['configs'=>$configs]);
    }

    public function actionSaveconfig(){
        if (Yii::app()->user->checkAccess('sysconfig_system_module_save')) {
            $systemTitle = $_REQUEST['systemTitle'];
            $systemLogo = $_REQUEST['systemLogo'];
            $systemLoginIcon = $_REQUEST['systemLoginIcon'];
            $systemWebIcon = $_REQUEST['systemWebIcon'];

            $this->connection->createCommand("update sys_config set config_value = {$systemTitle} where config_key = 'systemTitle' and module = 'system' ")->execute();
            $this->connection->createCommand("update sys_config set config_value = {$systemLogo} where config_key = 'systemLogo' and module = 'system' ")->execute();
            $this->connection->createCommand("update sys_config set config_value = {$systemLoginIcon} where config_key = 'systemLoginIcon' and module = 'system' ")->execute();
            $this->connection->createCommand("update sys_config set config_value = {$systemWebIcon} where config_key = 'systemWebIcon' and module = 'system' ")->execute();

            Yii::app()->cache->delete('CACHE_SYSTEM');

            echo json_encode(Appcode::success);
        }else{
            echo json_encode(Appcode::error_noauthority);
        }
    }
}