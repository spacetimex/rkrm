<?php

class MessagerController extends Controller
{
    public function actionIndex()
    {
        $configs = $this->connection->createCommand("select * from sys_config where module = 'messager' order by seq asc ")->queryAll();

        $this->render('index',['configs'=>$configs]);
    }

    public function actionSaveconfig(){
        $apikey = $_REQUEST['apikey'];

        $this->connection->createCommand("update sys_config set config_value = {$apikey} where config_key = 'apikey' and module = 'messager' ")->execute();

        echo json_encode(Appcode::success);
    }

    public function actionSendtest(){
        $apikey = "";
        $mobile = "18115762950";
        $yunPianUtils = new YunPianMessageUtil();
        $yunPianUtils->sendTextMessage($apikey,$mobile);
    }
}