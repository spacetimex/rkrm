<?php

class WarehouseController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_warehouse where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select * from crm_warehouse where deleted = 0 limit {$start},{$rows}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $warehouse_id = $_REQUEST['warehouse_id'];

        $warehouse = null;
        if($warehouse_id==0){
            //新增
        }else{
            //编辑
            $warehouse = $this->connection->createCommand("select * from crm_warehouse where warehouse_id = :warehouse_id")->bindParam(':warehouse_id',$warehouse_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['warehouse'=>$warehouse]);
    }

    public function actionSavedata(){

        $warehouse_id = $_REQUEST['warehouse_id'];
        $warehouse_name = $_REQUEST['warehouse_name'];
        $description = $_REQUEST['description'];

        if($warehouse_id!=null && $warehouse_id!=""){
            //编辑
            $this->connection->createCommand("update crm_warehouse set warehouse_name = :warehouse_name,
description=:description where warehouse_id = :warehouse_id")
                ->bindParam(':warehouse_id',$warehouse_id,PDO::PARAM_INT)
                ->bindParam(':warehouse_name',$warehouse_name,PDO::PARAM_STR)
                ->bindParam(':description',$description,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_warehouse (warehouse_name,description) 
values (:warehouse_name,:description) ")
                ->bindParam(':warehouse_name',$warehouse_name,PDO::PARAM_STR)
                ->bindParam(':description',$description,PDO::PARAM_STR)
                ->execute();
        }
    }

    public function actionRemove(){
        $warehouse_id = $_REQUEST['warehouse_id'];
        $this->connection->createCommand("update crm_warehouse set deleted = 1 
where warehouse_id = :warehouse_id")
            ->bindParam(':warehouse_id',$warehouse_id,PDO::PARAM_INT)
            ->execute();
    }

    public function actionAuthority(){

        $warehouse_id = $_REQUEST['warehouse_id'];

        $warehouse = $this->connection->createCommand("select * from crm_warehouse where warehouse_id = :warehouse_id")
            ->bindParam(':warehouse_id',$warehouse_id,PDO::PARAM_INT)
            ->queryRow();

        $userids = $this->connection->createCommand("SELECT GROUP_CONCAT(user_id) FROM crm_warehouse_user where warehouse_id = :warehouse_id")
            ->bindParam(':warehouse_id',$warehouse_id,PDO::PARAM_STR)
            ->queryScalar();

        $this->render('authority',['warehouse'=>$warehouse,'userids'=>$userids]);
    }
    
    public function actionSaveauthority(){
        $warehouse_id = $_REQUEST['warehouse_id'];
        $users = $_REQUEST['users'];

        $users_list = explode(",", $users);

        //清空数据
        $this->connection->createCommand("delete from crm_warehouse_user where warehouse_id = :warehouse_id")
            ->bindParam(':warehouse_id',$warehouse_id,PDO::PARAM_STR)
            ->execute();

        //录入数据
        foreach ($users_list as $w){
            $this->connection->createCommand("insert into crm_warehouse_user (user_id,warehouse_id) values (:user_id,:warehouse_id)")
                ->bindParam(':user_id',$w,PDO::PARAM_STR)
                ->bindParam(':warehouse_id',$warehouse_id,PDO::PARAM_STR)
                ->execute();
        }
    }


}