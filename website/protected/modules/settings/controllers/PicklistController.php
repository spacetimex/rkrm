<?php

class PicklistController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_picklistkey where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select * from crm_picklistkey where deleted = 0  limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $picklistkey_id = $_REQUEST['picklistkey_id'];
        $pickliststr = "";

        $picklistkey = null;
        if($picklistkey_id==0){
            //新增
        }else{
            //编辑
            $picklistkey = $this->connection->createCommand("select * from crm_picklistkey where picklistkey_id = :picklistkey_id")->bindParam(':picklistkey_id',$picklistkey_id,PDO::PARAM_INT)->queryRow();

            $picklistlist = $this->connection->createCommand("select * from crm_picklist where picklist_key = :picklistkey")
                ->bindParam(':picklistkey',$picklistkey['picklistkey'],PDO::PARAM_STR)
                ->queryAll();
            if(sizeof($picklistlist)>0){
                $i=1;
                foreach ($picklistlist as $p){
                    if($i==sizeof($picklistlist)){
                        $pickliststr.=$p['picklist_value'];
                    }else{
                        $pickliststr.=$p['picklist_value']."\r\n";
                    }
                    $i ++;
                }
            }
        }

        $this->render('showedit',['picklistkey'=>$picklistkey,'pickliststr'=>$pickliststr]);
    }

    public function actionSavedata(){

        $picklistkey_id = $_REQUEST['picklistkey_id'];
        $description = $_REQUEST['description'];
        $picklistkey = $_REQUEST['picklistkey'];
        $picklistvalues = trim($_REQUEST['picklistvalues']);

        if($picklistkey_id!=null && $picklistkey_id!=""){
            //编辑
            $this->connection->createCommand("update crm_picklistkey set description = :description
where picklistkey_id = :picklistkey_id")
                ->bindParam(':picklistkey_id',$picklistkey_id,PDO::PARAM_INT)
                ->bindParam(':description',$description,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_picklistkey (description,picklistkey) 
values (:description,:picklistkey) ")
                ->bindParam(':description',$description,PDO::PARAM_STR)
                ->bindParam(':picklistkey',$picklistkey,PDO::PARAM_STR)
                ->execute();

            $picklistkey_id = $this->connection->getLastInsertID();
        }

        //清理数据
        $picklistarray = explode("\r\n", $picklistvalues);
        $this->connection->createCommand("delete from crm_picklist where picklist_key = :picklistkey ")
            ->bindParam(':picklistkey',$picklistkey,PDO::PARAM_STR)
            ->execute();
        //进行数据录入
        foreach ($picklistarray as $p){
            $p = trim($p);
            $this->connection->createCommand("insert into crm_picklist (picklist_key,picklist_value) values (:picklist_key,:picklist_value) ")
                ->bindParam(':picklist_key',$picklistkey,PDO::PARAM_STR)
                ->bindParam(':picklist_value',$p,PDO::PARAM_STR)
                ->execute();
        }
    }

    public function actionRemove(){
        $picklistkey_id = $_REQUEST['picklistkey_id'];
        $this->connection->createCommand("update crm_picklistkey set deleted = 1 
where picklistkey_id = :picklistkey_id")
            ->bindParam(':picklistkey_id',$picklistkey_id,PDO::PARAM_INT)
            ->execute();
    }



}