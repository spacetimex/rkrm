<?php
class WorkflowstepController extends Controller
{

    public function actionGetstepusers(){
        $stepid = $_REQUEST['stepid'];

        $result = $this->connection->createCommand("select * from oa_workflow_user where workflow_step_id = :stepid")
            ->bindParam(':stepid',$stepid,PDO::PARAM_INT)
            ->queryAll();

        echo json_encode($result);
    }

    public function actionRemovestep(){
        $stepid = $_REQUEST['stepid'];

        $this->connection->createCommand("delete from oa_workflow_step where id = :workflow_step_id")
            ->bindParam(':workflow_step_id',$stepid,PDO::PARAM_INT)
            ->execute();
    }

}