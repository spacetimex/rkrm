<?php

/**
 * Class WorkflowmoduleController
 *
 * 模块关联哪些工作流配置
 */
class WorkflowmoduleController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetmodule(){

        $result = $this->connection->createCommand("select * from crm_module order by module_id asc")->queryAll();

        $data = Array();
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $module_id = $_REQUEST['module_id'];

        $module = $this->connection->createCommand("select * from crm_module where module_id = :module_id")
            ->bindParam(':module_id',$module_id,PDO::PARAM_INT)
            ->queryRow();

        $workflowids = $this->connection->createCommand("SELECT GROUP_CONCAT(workflowid) FROM crm_workflow_module where module = :module")
            ->bindParam(':module',$module['moduleenname'],PDO::PARAM_STR)
            ->queryScalar();

        $this->render('showedit',['module'=>$module,'workflowids'=>$workflowids]);
    }

    public function actionSavedata(){
        $module = $_REQUEST['module'];
        $workflows = $_REQUEST['workflows'];

        $workflows_list = explode(",", $workflows);

        //清空数据
        $this->connection->createCommand("delete from crm_workflow_module where module = :module")
            ->bindParam(':module',$module,PDO::PARAM_STR)
            ->execute();

        //录入数据
        foreach ($workflows_list as $w){
            $this->connection->createCommand("insert into crm_workflow_module (workflowid,module) values (:workflowid,:module)")
                ->bindParam(':workflowid',$w,PDO::PARAM_STR)
                ->bindParam(':module',$module,PDO::PARAM_STR)
                ->execute();
        }
    }
}