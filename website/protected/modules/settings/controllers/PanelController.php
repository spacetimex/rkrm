<?php

class PanelController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata()
    {
        $modulename = isset($_REQUEST['modulename']) ? $_REQUEST['modulename'] : null;
        $qstr_res = "";
        $data = Array();
        if (!is_null($modulename) && $modulename != "") {
            $qstr_res .= " and cmp.moduleenname = '" . $modulename . "'";

            $result = $this->connection->createCommand(" select cmp.*,cm.modulename modulecnname from crm_module_panel cmp
  inner join crm_module cm on cmp.moduleenname = cm.moduleenname  " . $qstr_res . " where cmp.deleted = 0 an order by cmp.seq desc ")->queryAll();

            $data['rows'] = $result;
        }

        echo json_encode($data);
    }

    public function actionShowedit(){
        $panel_id = $_REQUEST['panel_id'];

        $panel = null;
        if($panel_id==0){
            //新增
        }else{
            //编辑
            $panel = $this->connection->createCommand("select * from crm_module_panel where panel_id = :panel_id")->bindParam(':panel_id',$panel_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['panel'=>$panel]);
    }
    
    public function actionConfig()
    {

        $moduleenname = $_REQUEST['moduleenname'];
        $panel_id = $_REQUEST['panel_id'];

        $uncheckedcolumns = $this->connection->createCommand(" SELECT
            *
        FROM
            crm_module_column cmc
        WHERE
            cmc.modulename = '{$moduleenname}'
        AND  (cmc.columntype = 'basic' or cmc.columntype = 'relation') 
        AND cmc.columnname NOT IN (
            SELECT
                columnname
            FROM
                crm_module_panel_column WHERE
            moduleenname = '{$moduleenname}'
        ) and cmc.display = 1")->queryAll();

        $columns = $this->connection->createCommand(" SELECT
            *
        FROM
            crm_module_column cmc
        WHERE
            cmc.modulename = '{$moduleenname}'
        AND  (cmc.columntype = 'basic' or cmc.columntype = 'relation') 
         and cmc.display = 1" )->queryAll();

        $panelchecked = $this->connection->createCommand(" SELECT
            *
        FROM
            crm_module_column cmc
        WHERE
            cmc.modulename = '{$moduleenname}'
        AND (cmc.columntype = 'basic' or cmc.columntype = 'relation') 
        AND cmc.columnname IN (
            SELECT
                columnname
            FROM
                crm_module_panel_column WHERE
            modulename = '{$moduleenname}' and panel_id = {$panel_id}
        )  and cmc.display = 1")->queryAll();

        $this->render('config',['columns'=>$columns,'uncheckedcolumns'=>$uncheckedcolumns,'panelchecked'=>$panelchecked,'panel_id'=>$panel_id,'moduleenname'=>$moduleenname]);
    }

    public function actionSaveconfig()
    {
        $panel_id = $_REQUEST['panel_id'];
        $columnnames = $_REQUEST['columnnames'];
        $moduleenname = $_REQUEST['moduleenname'];

        if(sizeof($columnnames)>0){
            $this->connection->createCommand("delete from crm_module_panel_column where panel_id = :panel_id")
                ->bindParam(':panel_id',$panel_id,PDO::PARAM_INT)
                ->execute();
            $i = sizeof($columnnames);
            foreach ($columnnames as $columnname){
                $this->connection->createCommand("insert into crm_module_panel_column (panel_id,columnname,seq,moduleenname) 
values (:panel_id,:columnname,:seq,:moduleenname)")
                    ->bindParam(':panel_id',$panel_id,PDO::PARAM_INT)
                    ->bindParam(':columnname',$columnname,PDO::PARAM_STR)
                    ->bindParam(':seq',$i,PDO::PARAM_STR)
                    ->bindParam(':moduleenname',$moduleenname,PDO::PARAM_STR)
                    ->execute();
                $i--;
            }
        }
    }

    public function actionSavedata()
    {

        $panel_id = $_REQUEST['panel_id'];
        $modulename = $_REQUEST['modulename'];
        $panelname = $_REQUEST['panelname'];
        $seq = $_REQUEST['seq'];

        if ($panel_id != null) {
            //编辑
            $this->connection->createCommand("update crm_module_panel set  
seq=:seq,panelname=:panelname where panel_id = :panel_id")
                ->bindParam(':panel_id', $panel_id, PDO::PARAM_INT)
                ->bindParam(':seq', $seq, PDO::PARAM_STR)
                ->bindParam(':panelname', $panelname, PDO::PARAM_STR)
                ->execute();
        } else {
            //添加
            $this->connection->createCommand("insert into crm_module_panel (moduleenname,seq,panelname) values 
(:moduleenname,:seq,:panelname) ")
                ->bindParam(':moduleenname', $modulename, PDO::PARAM_STR)
                ->bindParam(':seq', $seq, PDO::PARAM_STR)
                ->bindParam(':panelname', $panelname, PDO::PARAM_STR)
                ->execute();
        }
    }

    public function actionRemove()
    {
        $panel_id = $_REQUEST['panel_id'];
        $this->connection->createCommand("update crm_module_panel set deleted = 1 
where panel_id = :panel_id")
            ->bindParam(':panel_id', $panel_id, PDO::PARAM_INT)
            ->execute();
    }


}