-- 部署删除权限脚本
-- 删除密码修改权限
delete from sys_authitemchild WHERE child = 'personalconfig_config';
-- 删除角色管理权限
delete from sys_authitemchild WHERE child = 'sysconfig_role_module_add';
delete from sys_authitemchild WHERE child = 'sysconfig_role_module_edit';
delete from sys_authitemchild WHERE child = 'sysconfig_role_module_remove';
