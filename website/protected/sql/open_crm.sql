/*
Navicat MySQL Data Transfer

Source Server         : 腾讯云-MYSQL
Source Server Version : 50628
Source Host           : 59851c7d4af21.sh.cdb.myqcloud.com:5383
Source Database       : ranko_crm_standard

Target Server Type    : MYSQL
Target Server Version : 50628
File Encoding         : 65001

Date: 2017-12-02 19:27:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `crm_accountrecordss`
-- ----------------------------
DROP TABLE IF EXISTS `crm_accountrecordss`;
CREATE TABLE `crm_accountrecordss` (
  `accountrecordss_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '主题',
  `customer` varchar(255) DEFAULT NULL COMMENT '客户',
  `start_date` date DEFAULT NULL COMMENT '开始时间',
  `serviceitem` text COMMENT '服务内容',
  `feedback` text COMMENT '反馈',
  `contacter_id` int(255) DEFAULT NULL COMMENT '联系人',
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  `description` int(11) DEFAULT NULL COMMENT '描述信息',
  PRIMARY KEY (`accountrecordss_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='客户联系记录';

-- ----------------------------
-- Records of crm_accountrecordss
-- ----------------------------
INSERT INTO `crm_accountrecordss` VALUES ('1', 'i', 'i', '2017-05-04', '\r\n                                p[[[pp[p[p[', '\r\n                                -0-                            ', '90090990', '2017-11-08 23:00:36', '0', '0');
INSERT INTO `crm_accountrecordss` VALUES ('2', '123123', '123', '2017-05-13', '123', '123', '123', '0000-00-00 00:00:00', '0', null);
INSERT INTO `crm_accountrecordss` VALUES ('3', '测试一一上1111', '总部', '2017-07-06', '23232', '23323', '13333', '2017-08-28 14:26:08', '0', null);
INSERT INTO `crm_accountrecordss` VALUES ('4', '555', 'ttt', '2017-07-31', 'aaa', 'ttt', '111', '2017-11-08 23:07:09', '1', null);

-- ----------------------------
-- Table structure for `crm_announcements`
-- ----------------------------
DROP TABLE IF EXISTS `crm_announcements`;
CREATE TABLE `crm_announcements` (
  `announcements_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '主题',
  `announcements_type` varchar(255) DEFAULT NULL COMMENT '公告类型',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `content` text COMMENT '内容',
  `deleted` tinyint(255) NOT NULL DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  PRIMARY KEY (`announcements_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='公告';

-- ----------------------------
-- Records of crm_announcements
-- ----------------------------
INSERT INTO `crm_announcements` VALUES ('1', 'cs999544554', '重要通知', '2017-08-21 10:30:33', '2017-08-22 10:30:36', '\r\n                                54545454                            ', '1');
INSERT INTO `crm_announcements` VALUES ('2', 'cs999544554', '重要通知', '2017-08-21 10:30:33', '2017-08-22 10:30:36', '54545454', '1');
INSERT INTO `crm_announcements` VALUES ('3', 'cs999544554', '重要通知', '2017-08-21 10:30:33', '2017-08-22 10:30:36', '54545454', '1');
INSERT INTO `crm_announcements` VALUES ('4', 'cs999544554', '重要通知', '2017-08-21 10:30:33', '2017-08-22 10:30:36', '54545454', '1');
INSERT INTO `crm_announcements` VALUES ('5', '33378788787', '重要通知', '2017-08-21 10:30:33', '2017-08-22 10:30:36', '\r\n                                \r\n                                \r\n                                545454547888                            ', '0');
INSERT INTO `crm_announcements` VALUES ('6', 'cs999544554', '重要通知', '2017-08-21 10:30:33', '2017-08-22 10:30:36', '54545454', '1');
INSERT INTO `crm_announcements` VALUES ('7', 'cs9995445549999', '重要通知', '2017-08-21 10:30:33', '2017-08-22 10:30:36', '\r\n                                \r\n                                5454545489889                            ', '0');
INSERT INTO `crm_announcements` VALUES ('8', 'cs999544554', '重要通知', '2017-08-21 10:30:33', '2017-08-22 10:30:36', '54545454', '0');
INSERT INTO `crm_announcements` VALUES ('9', 'cs999544554', '重要通知', '2017-08-21 10:30:33', '2017-08-22 10:30:36', '54545454', '0');
INSERT INTO `crm_announcements` VALUES ('10', 'cs999544554', '重要通知', '2017-08-21 10:30:33', '2017-08-22 10:30:36', '54545454', '0');
INSERT INTO `crm_announcements` VALUES ('11', 'cs999544554', '重要通知', '2017-08-21 10:30:33', '2017-08-22 10:30:36', '54545454', '0');
INSERT INTO `crm_announcements` VALUES ('12', 'cs999544554', '重要通知', '2017-08-21 10:30:33', '2017-08-22 10:30:36', '54545454', '1');
INSERT INTO `crm_announcements` VALUES ('13', 'cs999544554', '重要通知', '2017-08-21 10:30:33', '2017-08-22 10:30:36', '54545454', '1');
INSERT INTO `crm_announcements` VALUES ('14', '111', '重要通知', '2017-11-01 09:24:59', '2017-11-01 09:24:55', '1111', '0');

-- ----------------------------
-- Table structure for `crm_balances`
-- ----------------------------
DROP TABLE IF EXISTS `crm_balances`;
CREATE TABLE `crm_balances` (
  `balances_id` int(11) NOT NULL COMMENT '主键',
  PRIMARY KEY (`balances_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='库存余额';

-- ----------------------------
-- Records of crm_balances
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_billings`
-- ----------------------------
DROP TABLE IF EXISTS `crm_billings`;
CREATE TABLE `crm_billings` (
  `billings_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `createtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `billings_num` varchar(255) DEFAULT NULL COMMENT '发票编号',
  PRIMARY KEY (`billings_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='发票';

-- ----------------------------
-- Records of crm_billings
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_caigous`
-- ----------------------------
DROP TABLE IF EXISTS `crm_caigous`;
CREATE TABLE `crm_caigous` (
  `caigous_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '采购单主键',
  `caigous_num` varchar(255) DEFAULT NULL COMMENT '采购单号',
  `vendor_id` int(11) DEFAULT NULL COMMENT '供应商ID',
  `salesorder_id` int(11) DEFAULT NULL COMMENT '订单ID',
  `caigou_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '采购日期',
  `status` varchar(255) DEFAULT NULL COMMENT '采购状态',
  `deleted` tinyint(4) DEFAULT NULL COMMENT '删除标识位：0未删除，1删除',
  PRIMARY KEY (`caigous_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='采购表';

-- ----------------------------
-- Records of crm_caigous
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_caigoutuihuos`
-- ----------------------------
DROP TABLE IF EXISTS `crm_caigoutuihuos`;
CREATE TABLE `crm_caigoutuihuos` (
  `caigoutuihuos_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '主题',
  `status` varchar(255) DEFAULT NULL COMMENT '状态',
  `vendors_id` int(11) DEFAULT NULL COMMENT '供应商ID',
  `purchaseorder_id` int(11) DEFAULT NULL COMMENT '进货单ID',
  `reason` varchar(255) DEFAULT NULL COMMENT '原因',
  `deleted` tinyint(4) DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  PRIMARY KEY (`caigoutuihuos_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='采购退货单';

-- ----------------------------
-- Records of crm_caigoutuihuos
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_campaigns`
-- ----------------------------
DROP TABLE IF EXISTS `crm_campaigns`;
CREATE TABLE `crm_campaigns` (
  `campaigns_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `campaigns_name` varchar(255) DEFAULT NULL COMMENT '营销活动名称',
  `status` varchar(255) DEFAULT NULL COMMENT '状态',
  `product_id` int(11) DEFAULT NULL COMMENT '产品',
  `campaigns_type` varchar(255) DEFAULT NULL COMMENT '营销活动类型',
  `end_date` datetime DEFAULT NULL COMMENT '结束时间',
  `targetaudience` varchar(255) DEFAULT NULL COMMENT '目标客户',
  `targetsize` int(11) DEFAULT NULL COMMENT '目标大小',
  `description` text COMMENT '描述',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  `budget` int(11) DEFAULT NULL COMMENT '预算',
  PRIMARY KEY (`campaigns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='营销活动';

-- ----------------------------
-- Records of crm_campaigns
-- ----------------------------
INSERT INTO `crm_campaigns` VALUES ('1', '大甩卖', '激活', '1', '激活', '2017-10-22 15:19:35', '99', '100', '\r\n                                \r\n                    \r\n                    \r\n                    \r\n                    \r\n                    \r\n                    \r\n                    \r\n                    <h4>\r\n                    \r\n                    \r\n                    \r\n                    \r\n                    \r\n                    \r\n                    老板带着小姨子跑了，亏本大甩卖</h4>                                                                                                                                                            ', '0', '22');
INSERT INTO `crm_campaigns` VALUES ('2', '范德萨223', '无', '7', '无', '2017-06-29 11:13:10', '发', '0', '\r\n                    \r\n                    \r\n                    \r\n                    范德萨范德萨juju----                ', '0', null);
INSERT INTO `crm_campaigns` VALUES ('3', '123ff', '无', '0', '计划中', '2017-07-17 10:42:36', '123', '123', '\r\n                    \r\n                    \r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <p style=\"margin: 1em 0px 0pt 21pt; line-height: 150%; text-indent: -21pt; mso-char-indent-count: 0.0000; mso-list: l0 level1 lfo1;\" class=\"15\"><font color=\"#000000\"><span style=\"font-family: 宋体; font-size: 12pt; font-weight: bold; mso-bidi-font-family: 微软雅黑;\"><span style=\"mso-list: Ignore;\">一、<span>&nbsp;</span></span></span><b><span style=\'font-family: 宋体; font-size: 12pt; font-weight: bold; mso-bidi-font-family: 微软雅黑; mso-spacerun: \"yes\";\'><font face=\"宋体\">存在问题与解决办法</font></span></b><b><span style=\'font-family: 宋体; font-size: 12pt; font-weight: bold; mso-bidi-font-family: 微软雅黑; mso-spacerun: \"yes\";\'><?xml:namespace prefix = o ns = \"urn:schemas-microsoft-com:office:office\" /><o:p></o:p></span></b></font></p><p style=\"margin: 1em 0px 0pt 21pt; line-height: 150%; text-indent: -21pt; mso-char-indent-count: 0.0000; mso-list: l1 level1 lfo2;\" class=\"15\"><font color=\"#000000\"><span style=\'font-family: 微软雅黑; font-size: 12pt; mso-spacerun: \"yes\"; mso-ascii-font-family: 宋体; mso-hansi-font-family: 宋体;\'></span></font><span style=\'font-family: 微软雅黑; font-size: 12pt; mso-spacerun: \"yes\"; mso-ascii-font-family: 宋体; mso-hansi-font-family: 宋体;\'><o:p></o:p></span>&nbsp;</p><!--EndFragment-->&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ', '1', null);
INSERT INTO `crm_campaigns` VALUES ('4', '东东', '无', '0', '无', '2017-07-11 16:51:16', '东东', '0', '咚咚咚', '1', null);
INSERT INTO `crm_campaigns` VALUES ('5', 'fddd', '计划中', '2', '激活', '2017-07-13 09:00:38', 'sa', '111', '\r\n                                \r\n                    q1qwdccq1qcqq\r\n                                                                                ', '0', '3434');
INSERT INTO `crm_campaigns` VALUES ('6', '11', '无', '0', '无', '2017-07-31 17:25:00', '11', '11', '1111', '1', null);
INSERT INTO `crm_campaigns` VALUES ('7', '2321', '无', '0', '无', '2017-08-31 15:28:35', '213', '123', '<h6><font size=\"3\">dsdasdas</font></h6>\r\n                                    ', '1', null);
INSERT INTO `crm_campaigns` VALUES ('8', '营销活动', '计划中', '1', '激活', '2017-08-19 08:46:10', '目标人群', '100', '营销活动的备注\r\n                                    ', '0', null);
INSERT INTO `crm_campaigns` VALUES ('9', 'd', '计划中', '3', '计划中', '2017-08-08 22:09:54', 'dd', '1', '\r\n                                xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx &nbsp; ffffffffff                            ', '0', '88');
INSERT INTO `crm_campaigns` VALUES ('10', '1', '计划中', '5', '计划中', '2017-08-25 09:52:24', '111', '121', '2341', '1', null);
INSERT INTO `crm_campaigns` VALUES ('11', '等等', '无', '1', '无', '2017-08-31 20:26:54', 'dd', '0', 'ddd', '0', null);
INSERT INTO `crm_campaigns` VALUES ('12', '买一送十', '取消', '5', '取消', '2017-10-12 15:20:57', '12', '15', '太亏，不可能做', '0', null);
INSERT INTO `crm_campaigns` VALUES ('13', '大甩卖', '取消', '1', '激活', '2017-10-23 16:24:01', '100', '120', '\r\n                    跳楼大甩卖\r\n                                                    ', '0', null);
INSERT INTO `crm_campaigns` VALUES ('14', '111', '计划中', '1', '计划中', '2017-10-13 09:31:35', '111', '123', '\r\n                                    <br>', '0', null);
INSERT INTO `crm_campaigns` VALUES ('15', '33', '3', '3', '33', '2017-11-06 15:41:15', '3', '33', '333', '1', '3');

-- ----------------------------
-- Table structure for `crm_charges`
-- ----------------------------
DROP TABLE IF EXISTS `crm_charges`;
CREATE TABLE `crm_charges` (
  `charges_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `charges_num` varchar(255) DEFAULT NULL COMMENT '应付款编号',
  `amount` decimal(10,0) DEFAULT NULL COMMENT '总计',
  `changes_date` datetime DEFAULT NULL COMMENT '付款日期',
  `summary` varchar(255) DEFAULT NULL COMMENT '摘要',
  `deleted` tinyint(255) DEFAULT NULL COMMENT '删除标识：0未删除，1删除',
  `createtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`charges_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='应付款';

-- ----------------------------
-- Records of crm_charges
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_chargesrecords`
-- ----------------------------
DROP TABLE IF EXISTS `crm_chargesrecords`;
CREATE TABLE `crm_chargesrecords` (
  `chargesrecords_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chargesrecords_num` varchar(255) DEFAULT NULL COMMENT '付款单号',
  `createtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`chargesrecords_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='付款单';

-- ----------------------------
-- Records of crm_chargesrecords
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_checks`
-- ----------------------------
DROP TABLE IF EXISTS `crm_checks`;
CREATE TABLE `crm_checks` (
  `checks_id` int(11) NOT NULL COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `status` varchar(255) DEFAULT NULL COMMENT '状态',
  `warehouse_id` int(11) DEFAULT NULL COMMENT '仓库',
  `createtime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`checks_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='盘点';

-- ----------------------------
-- Records of crm_checks
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_competitor`
-- ----------------------------
DROP TABLE IF EXISTS `crm_competitor`;
CREATE TABLE `crm_competitor` (
  `competitor_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '竞争对手ID',
  `competitor_name` varchar(255) DEFAULT NULL COMMENT '竞争对手',
  `advantage` text COMMENT '优势',
  `power` varchar(255) DEFAULT NULL COMMENT '竞争力',
  `updatetime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识符号,0未删除，1删除',
  `disadvantaged` int(11) DEFAULT NULL COMMENT '劣势',
  `strategy` int(11) DEFAULT NULL COMMENT '策略',
  `description` int(11) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`competitor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='竞争对手';

-- ----------------------------
-- Records of crm_competitor
-- ----------------------------
INSERT INTO `crm_competitor` VALUES ('1', '667', '676', '6766767', '2017-09-20 08:52:50', '1', null, null, null);
INSERT INTO `crm_competitor` VALUES ('2', '科技公司A33', '资本雄厚', '资本雄厚', '2017-10-16 10:24:34', '0', null, null, null);
INSERT INTO `crm_competitor` VALUES ('3', '33', '33', '3', '2017-11-10 12:27:13', '0', '1', '2', '3');

-- ----------------------------
-- Table structure for `crm_complaints`
-- ----------------------------
DROP TABLE IF EXISTS `crm_complaints`;
CREATE TABLE `crm_complaints` (
  `complaints_id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '投诉主题',
  `customer_id` int(11) DEFAULT NULL COMMENT '客户',
  `complaints_type` varchar(255) DEFAULT NULL COMMENT '投诉类型',
  `complaints_date` datetime DEFAULT NULL COMMENT '投诉时间',
  `result` varchar(255) DEFAULT NULL COMMENT '处理结果',
  `urgencydegree` varchar(255) DEFAULT NULL COMMENT '紧急程度',
  `createtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`complaints_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='客户投诉';

-- ----------------------------
-- Records of crm_complaints
-- ----------------------------
INSERT INTO `crm_complaints` VALUES ('00000000001', '34', '343487878', '23322323', '2017-05-05 00:00:00', '2017-05-05', '高', '2017-05-09 10:43:30');
INSERT INTO `crm_complaints` VALUES ('00000000002', '3434', '3434', '34', '2017-05-06 00:00:00', '2017-05-02', '中', '0000-00-00 00:00:00');
INSERT INTO `crm_complaints` VALUES ('00000000003', '撒大大', '0', '啊大神的', '2017-05-09 00:00:00', '阿达撒事的', '无', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `crm_contacter`
-- ----------------------------
DROP TABLE IF EXISTS `crm_contacter`;
CREATE TABLE `crm_contacter` (
  `contacter_id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT COMMENT '主键',
  `contacter_name` varchar(255) DEFAULT NULL COMMENT '联系人名称',
  `position` varchar(255) DEFAULT NULL COMMENT '级别',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(255) DEFAULT NULL COMMENT '手机',
  `tel` varchar(255) DEFAULT NULL COMMENT '电话',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识，0-未删除，1-删除',
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `address` int(11) DEFAULT NULL COMMENT '称呼',
  `description` int(11) DEFAULT NULL COMMENT '描述信息',
  `fax` int(11) DEFAULT NULL COMMENT '传真',
  `department` int(11) DEFAULT NULL COMMENT '部门',
  PRIMARY KEY (`contacter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='联系人';

-- ----------------------------
-- Records of crm_contacter
-- ----------------------------
INSERT INTO `crm_contacter` VALUES ('0000000001', '王五', '经理', '458820281@qq.com', '1111', '8866', '0', '2017-11-07 22:48:32', '3434', '23233223', null, null);
INSERT INTO `crm_contacter` VALUES ('0000000002', '张小三', '职员', '123@qq.com', '11211341156', '12121345678', '0', '2017-11-07 22:46:54', '444', null, null, null);
INSERT INTO `crm_contacter` VALUES ('0000000003', '23', '', '', '3', '', '1', '0000-00-00 00:00:00', null, null, null, null);
INSERT INTO `crm_contacter` VALUES ('0000000004', '33', '', '', '33', '', '1', '0000-00-00 00:00:00', null, null, null, null);
INSERT INTO `crm_contacter` VALUES ('0000000005', '33', '', '', '333', '', '1', '2017-08-24 14:50:41', null, null, null, null);
INSERT INTO `crm_contacter` VALUES ('0000000006', '王小蒙', '技术经理', '333', 'awdawd', 'awdawd', '0', '2017-10-12 09:51:48', null, null, null, null);

-- ----------------------------
-- Table structure for `crm_contactlog`
-- ----------------------------
DROP TABLE IF EXISTS `crm_contactlog`;
CREATE TABLE `crm_contactlog` (
  `contactlog_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '联系记录',
  `customer_id` int(11) DEFAULT NULL COMMENT '客户',
  `contact_type` varchar(255) DEFAULT NULL COMMENT '联系类型',
  `contact_time` datetime DEFAULT NULL COMMENT '联系时间',
  `status` varchar(255) DEFAULT NULL COMMENT '处理状态',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识，0-未删除，1-删除',
  `salesstage` int(11) DEFAULT NULL COMMENT '销售阶段',
  `customerstatus` int(11) DEFAULT NULL COMMENT '客户状态',
  `revisitdays` int(11) DEFAULT NULL COMMENT '下次回访时间',
  `potentials_id` int(11) DEFAULT NULL COMMENT '销售机会',
  `content` int(11) DEFAULT NULL COMMENT '内容',
  PRIMARY KEY (`contactlog_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='联系记录';

-- ----------------------------
-- Records of crm_contactlog
-- ----------------------------
INSERT INTO `crm_contactlog` VALUES ('1', '展会', '2323', '寄样品', '2017-04-28 11:14:40', '失败', '1', null, null, null, null, null);
INSERT INTO `crm_contactlog` VALUES ('2', '888', '888', '联系小计', '2017-05-03 09:17:17', '已成交', '1', null, null, null, null, null);
INSERT INTO `crm_contactlog` VALUES ('3', '999', '99', '无', '2017-05-03 09:17:28', '无', '1', null, null, null, null, null);
INSERT INTO `crm_contactlog` VALUES ('4', 'swew', '0', '无', '2017-08-24 17:42:19', '潜在', '1', null, null, null, null, null);
INSERT INTO `crm_contactlog` VALUES ('5', 'aaa', '0', '拜访客户', '2017-09-06 18:09:49', '有意向', '0', null, null, null, null, null);
INSERT INTO `crm_contactlog` VALUES ('6', 'aaa', '0', '联系小计', '2017-09-06 18:10:12', '有意向', '0', null, null, null, null, null);
INSERT INTO `crm_contactlog` VALUES ('7', '风复古风格 ', '3', '商机转客户', '2017-09-08 11:32:57', '潜在', '0', '55', '55', '2017', '4', '0');
INSERT INTO `crm_contactlog` VALUES ('8', '33', '3', '33', '1899-11-30 00:00:00', '3', '0', '3', '33', '2017', '3', '0');

-- ----------------------------
-- Table structure for `crm_customer`
-- ----------------------------
DROP TABLE IF EXISTS `crm_customer`;
CREATE TABLE `crm_customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `customer_name` varchar(255) DEFAULT NULL COMMENT '客户名称',
  `phone` varchar(255) DEFAULT NULL COMMENT '手机',
  `status` varchar(255) DEFAULT NULL COMMENT '状态',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `website` int(11) DEFAULT NULL COMMENT '网站',
  `numberofemployees` int(11) DEFAULT NULL COMMENT '员工人数',
  `natureofbusiness` int(11) DEFAULT NULL COMMENT '公司性质',
  `customertype` int(11) DEFAULT NULL COMMENT '客户类型',
  `tel` int(11) DEFAULT NULL COMMENT '电话',
  `firstcontacter` int(11) DEFAULT NULL COMMENT '首要联系人',
  `othertel` int(11) DEFAULT NULL COMMENT '其他电话',
  `fax` int(11) DEFAULT NULL COMMENT '传真',
  `email` int(11) DEFAULT NULL COMMENT 'Email',
  `address` int(11) DEFAULT NULL COMMENT '地址',
  `paymethod` int(11) DEFAULT NULL COMMENT '支付方式',
  `memo` int(11) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COMMENT='客户';

-- ----------------------------
-- Records of crm_customer
-- ----------------------------
INSERT INTO `crm_customer` VALUES ('1', '张三3', '181157453', '有意向', '1', '2017-08-28 22:54:02', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('2', '李四', '18115762951', '潜在', '0', '2017-08-24 14:44:53', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('3', '666', '66', '无', '0', '2017-11-08 17:13:16', '34', '34', '3434', '34', '34', '4', '34', '34', '34', '3434', '44', '44');
INSERT INTO `crm_customer` VALUES ('4', '99', '9999', '无', '1', '2017-08-24 14:44:59', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('5', '   898', '   8989', '无', '0', '2017-08-24 14:45:02', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('6', '123123', '123123', '无', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('7', '笑死', '1116776', '有意向', '1', '2017-10-13 14:05:16', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('8', 'a', 'a', '无', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('9', 'gfg', '1232', '无', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('10', '姜女', '13601950254', '已成交', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('11', '张小娴', '18911772172', '无', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('12', '张小娴', '18911772172', '有意向', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('13', '12312', '12312', '无', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('14', '1', '11', '无', '1', '2017-10-13 09:49:21', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('15', '张三', '13456784567', '无', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('16', '是的但是多所', '阿斯达撒多所', '无', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('17', '888', '888', '有意向', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('18', '大林', '123456', '潜在', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('19', '撒打算', '12121212121', '无', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('20', '撒打算', '12345678901', '已成交', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('21', 'we', 'we', '无', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('22', 'hhhh', 'hhhhhhhhhhh', '无', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('23', 'vnvnn', '1376', '潜在', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('24', '陈杰', '1234241352424', '潜在', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('25', '1', '1', '已成交', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('26', 'test', '112', '潜在', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('27', 'zhangsan', '18115762950', '有意向', '0', '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('28', '456', '456', '无', '1', '2017-10-13 09:48:44', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('29', 'dfsadsaf', '222', '潜在', '0', '2017-08-29 09:37:01', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('30', '李四', '13525553216', '无', '0', '2017-08-29 16:43:53', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('31', '李四', '13243423423', '无', '0', '2017-08-29 16:44:18', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('32', 'f', 'fdfd', '无', '0', '2017-08-31 16:16:23', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('33', '大啊安德森发', '发达发达', '无', '0', '2017-08-31 16:16:36', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('34', '发大发发达', '大方的撒', '无', '0', '2017-08-31 16:16:53', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('35', '徐爱华', '18244125456', '已成交', '0', '2017-09-07 14:30:46', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('36', '大黄蜂', '12312342345', '有意向', '0', '2017-10-13 09:48:10', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `crm_customer` VALUES ('37', '大黄蜂', '12312342345', '有意向', '0', '2017-10-13 09:49:03', null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `crm_dailylogs`
-- ----------------------------
DROP TABLE IF EXISTS `crm_dailylogs`;
CREATE TABLE `crm_dailylogs` (
  `dailylogs_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `summary` text COMMENT '今日总结',
  `tomorrow_plan` text COMMENT '明日计划',
  `createdate` date NOT NULL COMMENT '创建日期',
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `deleted` int(11) NOT NULL DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  PRIMARY KEY (`dailylogs_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='日报表';

-- ----------------------------
-- Records of crm_dailylogs
-- ----------------------------
INSERT INTO `crm_dailylogs` VALUES ('1', '3232332323', '2323', '2017-08-06', '1', '0');
INSERT INTO `crm_dailylogs` VALUES ('2', '2323', '2323', '2017-08-07', '1', '0');
INSERT INTO `crm_dailylogs` VALUES ('3', '323', '2323', '2017-08-07', '1', '0');
INSERT INTO `crm_dailylogs` VALUES ('4', '2323', '23', '2017-08-09', '1', '0');
INSERT INTO `crm_dailylogs` VALUES ('8', '333233232', '3333', '2017-08-22', '1', '0');
INSERT INTO `crm_dailylogs` VALUES ('9', '', '', '2017-08-27', '1', '0');
INSERT INTO `crm_dailylogs` VALUES ('10', '123', '2131231', '2017-08-28', '1', '0');
INSERT INTO `crm_dailylogs` VALUES ('11', '好！', '很好', '2017-08-29', '7', '0');
INSERT INTO `crm_dailylogs` VALUES ('12', '的点点滴滴', '的点点滴滴 ', '2017-09-20', '1', '0');
INSERT INTO `crm_dailylogs` VALUES ('13', '', '', '2017-10-11', '1', '0');
INSERT INTO `crm_dailylogs` VALUES ('14', '业绩良好', '继续努力', '2017-10-13', '7', '0');
INSERT INTO `crm_dailylogs` VALUES ('15', '343434344343', '34344', '2017-11-13', '1', '0');

-- ----------------------------
-- Table structure for `crm_deliverys`
-- ----------------------------
DROP TABLE IF EXISTS `crm_deliverys`;
CREATE TABLE `crm_deliverys` (
  `deliverys_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `deliverys_num` varchar(255) DEFAULT NULL COMMENT '出库单号',
  `status` varchar(255) DEFAULT NULL COMMENT '状态',
  `salesorder_id` int(11) DEFAULT NULL COMMENT '合同订单号',
  `invoice_id` int(11) DEFAULT NULL COMMENT '发货单ID',
  `deliverystime` datetime DEFAULT NULL COMMENT '出库时间',
  `deleted` tinyint(255) DEFAULT NULL COMMENT '删除标识：',
  PRIMARY KEY (`deliverys_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='出库单';

-- ----------------------------
-- Records of crm_deliverys
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_emailgroup`
-- ----------------------------
DROP TABLE IF EXISTS `crm_emailgroup`;
CREATE TABLE `crm_emailgroup` (
  `emailgroup_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `marketinggroup_id` int(11) DEFAULT NULL COMMENT '营销群',
  `status` varchar(255) DEFAULT NULL COMMENT '状态',
  `createtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`emailgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='邮箱发送组';

-- ----------------------------
-- Records of crm_emailgroup
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_emailgroupinfo`
-- ----------------------------
DROP TABLE IF EXISTS `crm_emailgroupinfo`;
CREATE TABLE `crm_emailgroupinfo` (
  `emailgroupinfo_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `emailgroup_id` int(11) DEFAULT NULL COMMENT '邮箱发送组ID',
  `toemail` varchar(255) DEFAULT NULL COMMENT '收件email',
  `status` text COMMENT '状态',
  `createtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`emailgroupinfo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='邮箱发送情况';

-- ----------------------------
-- Records of crm_emailgroupinfo
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_expenses`
-- ----------------------------
DROP TABLE IF EXISTS `crm_expenses`;
CREATE TABLE `crm_expenses` (
  `expenses_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `expenses_num` varchar(255) DEFAULT NULL COMMENT '费用编号',
  `createtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`expenses_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='费用报销';

-- ----------------------------
-- Records of crm_expenses
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_faq`
-- ----------------------------
DROP TABLE IF EXISTS `crm_faq`;
CREATE TABLE `crm_faq` (
  `faq_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `product_name` varchar(255) DEFAULT NULL COMMENT '产品名称',
  `faqcategory_id` int(255) DEFAULT NULL COMMENT '产品分类ID',
  `status` varchar(255) DEFAULT NULL COMMENT '状态',
  `keyword` varchar(255) DEFAULT NULL COMMENT '关键字',
  `question` varchar(255) DEFAULT NULL COMMENT '问题',
  `solution` text COMMENT '处理方式',
  `createtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='知识库';

-- ----------------------------
-- Records of crm_faq
-- ----------------------------
INSERT INTO `crm_faq` VALUES ('1', '百度', '0', '已审阅', '55', '55', '55', '2017-06-26 16:50:39');
INSERT INTO `crm_faq` VALUES ('2', '45', '1', '无', '4545', '45', '45', '0000-00-00 00:00:00');
INSERT INTO `crm_faq` VALUES ('3', '１', '0', '无', '１', '１', '１', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `crm_faqcategory`
-- ----------------------------
DROP TABLE IF EXISTS `crm_faqcategory`;
CREATE TABLE `crm_faqcategory` (
  `faqcategory_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `faqcategoryname` varchar(255) DEFAULT NULL COMMENT '分类名称',
  PRIMARY KEY (`faqcategory_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='知识库分类';

-- ----------------------------
-- Records of crm_faqcategory
-- ----------------------------
INSERT INTO `crm_faqcategory` VALUES ('1', '2222');
INSERT INTO `crm_faqcategory` VALUES ('2', '323');
INSERT INTO `crm_faqcategory` VALUES ('3', 'aaa');
INSERT INTO `crm_faqcategory` VALUES ('4', 'jhj');
INSERT INTO `crm_faqcategory` VALUES ('5', '123123');

-- ----------------------------
-- Table structure for `crm_gathers`
-- ----------------------------
DROP TABLE IF EXISTS `crm_gathers`;
CREATE TABLE `crm_gathers` (
  `gathers_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `gathers_num` varchar(255) DEFAULT NULL COMMENT '应收款编号',
  `customer_id` int(11) DEFAULT NULL COMMENT '客户ID',
  `saleorder_id` int(11) DEFAULT NULL COMMENT '销售单号',
  `invoice_id` int(11) DEFAULT NULL COMMENT '发货单',
  `amount` decimal(10,0) DEFAULT NULL COMMENT '应收金额',
  `gather_date` datetime DEFAULT NULL COMMENT '应收日期',
  `memo` text COMMENT '摘要',
  `deleted` tinyint(255) DEFAULT NULL COMMENT '删除标识:0未删除，1删除',
  PRIMARY KEY (`gathers_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='应收款';

-- ----------------------------
-- Records of crm_gathers
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_gathersrecords`
-- ----------------------------
DROP TABLE IF EXISTS `crm_gathersrecords`;
CREATE TABLE `crm_gathersrecords` (
  `gathersrecords_id` int(11) NOT NULL COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `status` varchar(255) DEFAULT NULL COMMENT '状态',
  `customer_id` int(11) DEFAULT NULL COMMENT '客户ID',
  `summary` varchar(255) DEFAULT NULL COMMENT '摘要',
  `deleted` tinyint(255) DEFAULT NULL COMMENT '删除标识：0未删除，1删除',
  PRIMARY KEY (`gathersrecords_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='收款记录表';

-- ----------------------------
-- Records of crm_gathersrecords
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_invoice`
-- ----------------------------
DROP TABLE IF EXISTS `crm_invoice`;
CREATE TABLE `crm_invoice` (
  `invoice_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `invoice_num` varchar(255) DEFAULT NULL COMMENT '发货单号',
  `customer_id` int(11) DEFAULT NULL COMMENT '客户ID',
  `salesorder_id` int(255) DEFAULT NULL COMMENT '销售单',
  `delivery_time` datetime DEFAULT NULL COMMENT '发货日期',
  `updatetime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识：0，未删除，1删除',
  `description` int(11) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='发货单';

-- ----------------------------
-- Records of crm_invoice
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_loginhistory`
-- ----------------------------
DROP TABLE IF EXISTS `crm_loginhistory`;
CREATE TABLE `crm_loginhistory` (
  `loginhistory_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`loginhistory_id`)
) ENGINE=InnoDB AUTO_INCREMENT=389 DEFAULT CHARSET=utf8 COMMENT='登入历史表';

-- ----------------------------
-- Records of crm_loginhistory
-- ----------------------------
INSERT INTO `crm_loginhistory` VALUES ('5', '1', '超级管理员，退出系统', '2017-09-27 22:41:13');
INSERT INTO `crm_loginhistory` VALUES ('6', '1', '超级管理员，登入系统', '2017-09-27 22:41:15');
INSERT INTO `crm_loginhistory` VALUES ('7', '1', '超级管理员，登入系统', '2017-09-29 20:43:24');
INSERT INTO `crm_loginhistory` VALUES ('8', '1', '超级管理员，登入系统', '2017-09-29 20:43:31');
INSERT INTO `crm_loginhistory` VALUES ('9', '1', '超级管理员，登入系统', '2017-09-29 20:43:48');
INSERT INTO `crm_loginhistory` VALUES ('10', '1', '超级管理员，登入系统', '2017-09-29 20:43:55');
INSERT INTO `crm_loginhistory` VALUES ('11', '1', '超级管理员，登入系统', '2017-09-29 20:44:28');
INSERT INTO `crm_loginhistory` VALUES ('12', '1', '超级管理员，登入系统', '2017-09-29 23:32:59');
INSERT INTO `crm_loginhistory` VALUES ('13', '1', '超级管理员，登入系统', '2017-09-30 15:29:52');
INSERT INTO `crm_loginhistory` VALUES ('14', '1', '超级管理员，登入系统', '2017-09-30 18:31:33');
INSERT INTO `crm_loginhistory` VALUES ('15', '1', '超级管理员，退出系统', '2017-09-30 18:41:09');
INSERT INTO `crm_loginhistory` VALUES ('16', '1', '超级管理员，登入系统', '2017-09-30 18:41:11');
INSERT INTO `crm_loginhistory` VALUES ('17', '1', '超级管理员，登入系统', '2017-09-30 18:41:14');
INSERT INTO `crm_loginhistory` VALUES ('18', '1', '超级管理员，登入系统', '2017-09-30 18:41:20');
INSERT INTO `crm_loginhistory` VALUES ('19', '1', '超级管理员，登入系统', '2017-10-06 17:16:50');
INSERT INTO `crm_loginhistory` VALUES ('20', '1', '超级管理员，登入系统', '2017-10-06 17:45:03');
INSERT INTO `crm_loginhistory` VALUES ('21', '1', '超级管理员，登入系统', '2017-10-06 17:47:27');
INSERT INTO `crm_loginhistory` VALUES ('22', '1', '超级管理员，登入系统', '2017-10-07 12:02:39');
INSERT INTO `crm_loginhistory` VALUES ('23', '1', '超级管理员，登入系统', '2017-10-07 18:06:11');
INSERT INTO `crm_loginhistory` VALUES ('24', '1', '超级管理员，退出系统', '2017-10-07 18:06:28');
INSERT INTO `crm_loginhistory` VALUES ('25', '1', '超级管理员，登入系统', '2017-10-07 18:11:09');
INSERT INTO `crm_loginhistory` VALUES ('26', '1', '超级管理员，退出系统', '2017-10-07 21:20:52');
INSERT INTO `crm_loginhistory` VALUES ('27', '1', '超级管理员，登入系统', '2017-10-07 21:20:55');
INSERT INTO `crm_loginhistory` VALUES ('28', '1', '超级管理员，登入系统', '2017-10-07 21:20:58');
INSERT INTO `crm_loginhistory` VALUES ('29', '1', '超级管理员，登入系统', '2017-10-08 09:23:16');
INSERT INTO `crm_loginhistory` VALUES ('30', '1', '超级管理员，登入系统', '2017-10-08 09:23:19');
INSERT INTO `crm_loginhistory` VALUES ('31', '1', '超级管理员，登入系统', '2017-10-08 09:23:23');
INSERT INTO `crm_loginhistory` VALUES ('32', '1', '超级管理员，登入系统', '2017-10-08 17:43:05');
INSERT INTO `crm_loginhistory` VALUES ('33', '1', '超级管理员，登入系统', '2017-10-08 17:44:57');
INSERT INTO `crm_loginhistory` VALUES ('34', '1', '超级管理员，登入系统', '2017-10-08 21:31:02');
INSERT INTO `crm_loginhistory` VALUES ('35', '1', '超级管理员，退出系统', '2017-10-08 21:31:32');
INSERT INTO `crm_loginhistory` VALUES ('36', '1', '超级管理员，登入系统', '2017-10-09 12:14:54');
INSERT INTO `crm_loginhistory` VALUES ('37', '1', '超级管理员，登入系统', '2017-10-09 15:53:16');
INSERT INTO `crm_loginhistory` VALUES ('38', '1', '超级管理员，登入系统', '2017-10-09 15:53:19');
INSERT INTO `crm_loginhistory` VALUES ('39', '1', '超级管理员，登入系统', '2017-10-09 15:53:27');
INSERT INTO `crm_loginhistory` VALUES ('40', '1', '超级管理员，登入系统', '2017-10-09 15:53:31');
INSERT INTO `crm_loginhistory` VALUES ('41', '1', '超级管理员，登入系统', '2017-10-09 15:53:47');
INSERT INTO `crm_loginhistory` VALUES ('42', '1', '超级管理员，登入系统', '2017-10-09 15:53:50');
INSERT INTO `crm_loginhistory` VALUES ('43', '1', '超级管理员，登入系统', '2017-10-09 15:53:51');
INSERT INTO `crm_loginhistory` VALUES ('44', '1', '超级管理员，登入系统', '2017-10-09 15:53:54');
INSERT INTO `crm_loginhistory` VALUES ('45', '1', '超级管理员，登入系统', '2017-10-09 15:53:55');
INSERT INTO `crm_loginhistory` VALUES ('46', '1', '超级管理员，登入系统', '2017-10-09 15:53:55');
INSERT INTO `crm_loginhistory` VALUES ('47', '1', '超级管理员，登入系统', '2017-10-09 15:53:55');
INSERT INTO `crm_loginhistory` VALUES ('48', '1', '超级管理员，登入系统', '2017-10-09 15:53:55');
INSERT INTO `crm_loginhistory` VALUES ('49', '1', '超级管理员，登入系统', '2017-10-09 15:58:32');
INSERT INTO `crm_loginhistory` VALUES ('50', '1', '超级管理员，登入系统', '2017-10-09 15:58:35');
INSERT INTO `crm_loginhistory` VALUES ('51', '1', '超级管理员，登入系统', '2017-10-09 16:05:47');
INSERT INTO `crm_loginhistory` VALUES ('52', '1', '超级管理员，登入系统', '2017-10-09 16:08:58');
INSERT INTO `crm_loginhistory` VALUES ('53', '1', '超级管理员，退出系统', '2017-10-09 16:10:56');
INSERT INTO `crm_loginhistory` VALUES ('54', '1', '超级管理员，登入系统', '2017-10-09 18:34:19');
INSERT INTO `crm_loginhistory` VALUES ('55', '1', '超级管理员，登入系统', '2017-10-09 20:43:08');
INSERT INTO `crm_loginhistory` VALUES ('56', '1', '超级管理员，登入系统', '2017-10-09 22:06:13');
INSERT INTO `crm_loginhistory` VALUES ('57', '1', '超级管理员，登入系统', '2017-10-09 22:52:12');
INSERT INTO `crm_loginhistory` VALUES ('58', '1', '超级管理员，登入系统', '2017-10-09 22:57:22');
INSERT INTO `crm_loginhistory` VALUES ('59', '1', '超级管理员，登入系统', '2017-10-10 04:20:18');
INSERT INTO `crm_loginhistory` VALUES ('60', '1', '超级管理员，登入系统', '2017-10-10 04:20:21');
INSERT INTO `crm_loginhistory` VALUES ('61', '1', '超级管理员，登入系统', '2017-10-10 04:20:31');
INSERT INTO `crm_loginhistory` VALUES ('62', '1', '超级管理员，登入系统', '2017-10-10 08:56:58');
INSERT INTO `crm_loginhistory` VALUES ('63', '1', '超级管理员，登入系统', '2017-10-10 09:06:09');
INSERT INTO `crm_loginhistory` VALUES ('64', '1', '超级管理员，登入系统', '2017-10-10 09:06:47');
INSERT INTO `crm_loginhistory` VALUES ('65', '1', '超级管理员，登入系统', '2017-10-10 10:07:48');
INSERT INTO `crm_loginhistory` VALUES ('66', '1', '超级管理员，登入系统', '2017-10-10 11:13:12');
INSERT INTO `crm_loginhistory` VALUES ('67', '1', '超级管理员，退出系统', '2017-10-10 11:15:56');
INSERT INTO `crm_loginhistory` VALUES ('68', '7', '销售人员，登入系统', '2017-10-10 11:16:11');
INSERT INTO `crm_loginhistory` VALUES ('69', '1', '超级管理员，登入系统', '2017-10-10 11:21:18');
INSERT INTO `crm_loginhistory` VALUES ('70', '1', '超级管理员，登入系统', '2017-10-10 13:05:50');
INSERT INTO `crm_loginhistory` VALUES ('71', '1', '超级管理员，登入系统', '2017-10-10 14:07:20');
INSERT INTO `crm_loginhistory` VALUES ('72', '1', '超级管理员，登入系统', '2017-10-10 14:22:04');
INSERT INTO `crm_loginhistory` VALUES ('73', '1', '超级管理员，登入系统', '2017-10-10 14:22:16');
INSERT INTO `crm_loginhistory` VALUES ('74', '1', '超级管理员，登入系统', '2017-10-10 14:24:43');
INSERT INTO `crm_loginhistory` VALUES ('75', '1', '超级管理员，登入系统', '2017-10-10 14:31:35');
INSERT INTO `crm_loginhistory` VALUES ('76', '1', '超级管理员，登入系统', '2017-10-10 14:43:13');
INSERT INTO `crm_loginhistory` VALUES ('77', '1', '超级管理员，登入系统', '2017-10-10 15:40:32');
INSERT INTO `crm_loginhistory` VALUES ('78', '1', '超级管理员，登入系统', '2017-10-10 16:51:31');
INSERT INTO `crm_loginhistory` VALUES ('79', '1', '超级管理员，登入系统', '2017-10-10 18:07:34');
INSERT INTO `crm_loginhistory` VALUES ('80', '1', '超级管理员，登入系统', '2017-10-10 18:33:55');
INSERT INTO `crm_loginhistory` VALUES ('81', '1', '超级管理员，退出系统', '2017-10-10 19:29:47');
INSERT INTO `crm_loginhistory` VALUES ('82', '1', '超级管理员，登入系统', '2017-10-10 19:30:57');
INSERT INTO `crm_loginhistory` VALUES ('83', '1', '超级管理员，退出系统', '2017-10-10 19:31:04');
INSERT INTO `crm_loginhistory` VALUES ('84', '7', '销售人员，登入系统', '2017-10-10 19:31:18');
INSERT INTO `crm_loginhistory` VALUES ('85', '7', '销售人员，退出系统', '2017-10-10 19:31:42');
INSERT INTO `crm_loginhistory` VALUES ('86', '1', '超级管理员，登入系统', '2017-10-10 19:32:37');
INSERT INTO `crm_loginhistory` VALUES ('87', '7', '销售人员，登入系统', '2017-10-10 19:39:45');
INSERT INTO `crm_loginhistory` VALUES ('88', '1', '超级管理员，登入系统', '2017-10-10 23:27:50');
INSERT INTO `crm_loginhistory` VALUES ('89', '1', '超级管理员，登入系统', '2017-10-11 11:34:43');
INSERT INTO `crm_loginhistory` VALUES ('90', '1', '超级管理员，登入系统', '2017-10-11 11:35:01');
INSERT INTO `crm_loginhistory` VALUES ('91', '1', '超级管理员，登入系统', '2017-10-11 12:13:20');
INSERT INTO `crm_loginhistory` VALUES ('92', '1', '超级管理员，登入系统', '2017-10-11 12:31:06');
INSERT INTO `crm_loginhistory` VALUES ('93', '1', '超级管理员，登入系统', '2017-10-11 12:49:32');
INSERT INTO `crm_loginhistory` VALUES ('94', '1', '超级管理员，登入系统', '2017-10-11 13:00:36');
INSERT INTO `crm_loginhistory` VALUES ('95', '1', '超级管理员，登入系统', '2017-10-11 13:05:49');
INSERT INTO `crm_loginhistory` VALUES ('96', '1', '超级管理员，登入系统', '2017-10-11 13:12:32');
INSERT INTO `crm_loginhistory` VALUES ('97', '1', '超级管理员，登入系统', '2017-10-11 14:23:42');
INSERT INTO `crm_loginhistory` VALUES ('98', '1', '超级管理员，登入系统', '2017-10-11 17:49:59');
INSERT INTO `crm_loginhistory` VALUES ('99', '1', '超级管理员，登入系统', '2017-10-11 17:50:03');
INSERT INTO `crm_loginhistory` VALUES ('100', '1', '超级管理员，登入系统', '2017-10-11 17:50:07');
INSERT INTO `crm_loginhistory` VALUES ('101', '1', '超级管理员，登入系统', '2017-10-11 17:50:11');
INSERT INTO `crm_loginhistory` VALUES ('102', '1', '超级管理员，登入系统', '2017-10-11 17:50:25');
INSERT INTO `crm_loginhistory` VALUES ('103', '1', '超级管理员，登入系统', '2017-10-11 18:21:03');
INSERT INTO `crm_loginhistory` VALUES ('104', '1', '超级管理员，登入系统', '2017-10-11 18:27:41');
INSERT INTO `crm_loginhistory` VALUES ('105', '1', '超级管理员，登入系统', '2017-10-11 18:45:40');
INSERT INTO `crm_loginhistory` VALUES ('106', '1', '超级管理员，登入系统', '2017-10-11 19:45:23');
INSERT INTO `crm_loginhistory` VALUES ('107', '1', '超级管理员，登入系统', '2017-10-11 20:53:08');
INSERT INTO `crm_loginhistory` VALUES ('108', '1', '超级管理员，登入系统', '2017-10-11 21:49:24');
INSERT INTO `crm_loginhistory` VALUES ('109', '1', '超级管理员，登入系统', '2017-10-11 23:49:47');
INSERT INTO `crm_loginhistory` VALUES ('110', '1', '超级管理员，登入系统', '2017-10-12 08:27:33');
INSERT INTO `crm_loginhistory` VALUES ('111', '1', '超级管理员，登入系统', '2017-10-12 08:42:16');
INSERT INTO `crm_loginhistory` VALUES ('112', '1', '超级管理员，登入系统', '2017-10-12 08:42:51');
INSERT INTO `crm_loginhistory` VALUES ('113', '1', '超级管理员，登入系统', '2017-10-12 09:29:46');
INSERT INTO `crm_loginhistory` VALUES ('114', '1', '超级管理员，登入系统', '2017-10-12 09:34:20');
INSERT INTO `crm_loginhistory` VALUES ('115', '1', '超级管理员，退出系统', '2017-10-12 09:47:26');
INSERT INTO `crm_loginhistory` VALUES ('116', '7', '销售人员，登入系统', '2017-10-12 09:47:45');
INSERT INTO `crm_loginhistory` VALUES ('117', '7', '销售人员，退出系统', '2017-10-12 09:48:09');
INSERT INTO `crm_loginhistory` VALUES ('118', '1', '超级管理员，登入系统', '2017-10-12 09:48:33');
INSERT INTO `crm_loginhistory` VALUES ('119', '1', '超级管理员，登入系统', '2017-10-12 09:49:52');
INSERT INTO `crm_loginhistory` VALUES ('120', '7', '销售人员，登入系统', '2017-10-12 09:50:29');
INSERT INTO `crm_loginhistory` VALUES ('121', '1', '超级管理员，登入系统', '2017-10-12 10:28:20');
INSERT INTO `crm_loginhistory` VALUES ('122', '1', '超级管理员，登入系统', '2017-10-12 10:41:04');
INSERT INTO `crm_loginhistory` VALUES ('123', '1', '超级管理员，登入系统', '2017-10-12 10:56:33');
INSERT INTO `crm_loginhistory` VALUES ('124', '1', '超级管理员，登入系统', '2017-10-12 10:56:37');
INSERT INTO `crm_loginhistory` VALUES ('125', '1', '超级管理员，登入系统', '2017-10-12 10:56:41');
INSERT INTO `crm_loginhistory` VALUES ('126', '1', '超级管理员，登入系统', '2017-10-12 11:00:47');
INSERT INTO `crm_loginhistory` VALUES ('127', '1', '超级管理员，登入系统', '2017-10-12 11:12:18');
INSERT INTO `crm_loginhistory` VALUES ('128', '1', '超级管理员，登入系统', '2017-10-12 11:12:21');
INSERT INTO `crm_loginhistory` VALUES ('129', '1', '超级管理员，登入系统', '2017-10-12 11:12:25');
INSERT INTO `crm_loginhistory` VALUES ('130', '7', '销售人员，登入系统', '2017-10-12 11:16:10');
INSERT INTO `crm_loginhistory` VALUES ('131', '1', '超级管理员，登入系统', '2017-10-12 11:17:45');
INSERT INTO `crm_loginhistory` VALUES ('132', '1', '超级管理员，登入系统', '2017-10-12 11:17:48');
INSERT INTO `crm_loginhistory` VALUES ('133', '1', '超级管理员，登入系统', '2017-10-12 11:17:50');
INSERT INTO `crm_loginhistory` VALUES ('134', '1', '超级管理员，登入系统', '2017-10-12 11:48:14');
INSERT INTO `crm_loginhistory` VALUES ('135', '1', '超级管理员，登入系统', '2017-10-12 11:50:12');
INSERT INTO `crm_loginhistory` VALUES ('136', '1', '超级管理员，登入系统', '2017-10-12 12:02:00');
INSERT INTO `crm_loginhistory` VALUES ('137', '1', '超级管理员，登入系统', '2017-10-12 12:06:55');
INSERT INTO `crm_loginhistory` VALUES ('138', '1', '超级管理员，登入系统', '2017-10-12 12:39:41');
INSERT INTO `crm_loginhistory` VALUES ('139', '1', '超级管理员，登入系统', '2017-10-12 12:39:43');
INSERT INTO `crm_loginhistory` VALUES ('140', '1', '超级管理员，退出系统', '2017-10-12 12:39:49');
INSERT INTO `crm_loginhistory` VALUES ('141', '1', '超级管理员，退出系统', '2017-10-12 12:39:59');
INSERT INTO `crm_loginhistory` VALUES ('142', '1', '超级管理员，登入系统', '2017-10-12 12:41:17');
INSERT INTO `crm_loginhistory` VALUES ('143', '1', '超级管理员，登入系统', '2017-10-12 13:13:52');
INSERT INTO `crm_loginhistory` VALUES ('144', '1', '超级管理员，登入系统', '2017-10-12 13:13:55');
INSERT INTO `crm_loginhistory` VALUES ('145', '1', '超级管理员，登入系统', '2017-10-12 13:14:47');
INSERT INTO `crm_loginhistory` VALUES ('146', '1', '超级管理员，登入系统', '2017-10-12 13:48:06');
INSERT INTO `crm_loginhistory` VALUES ('147', '1', '超级管理员，登入系统', '2017-10-12 14:05:26');
INSERT INTO `crm_loginhistory` VALUES ('148', '1', '超级管理员，登入系统', '2017-10-12 14:06:46');
INSERT INTO `crm_loginhistory` VALUES ('149', '7', '销售人员，登入系统', '2017-10-12 14:06:57');
INSERT INTO `crm_loginhistory` VALUES ('150', '1', '超级管理员，登入系统', '2017-10-12 14:09:39');
INSERT INTO `crm_loginhistory` VALUES ('151', '1', '超级管理员，登入系统', '2017-10-12 14:28:09');
INSERT INTO `crm_loginhistory` VALUES ('152', '1', '超级管理员，退出系统', '2017-10-12 14:29:44');
INSERT INTO `crm_loginhistory` VALUES ('153', '1', '超级管理员，退出系统', '2017-10-12 14:50:23');
INSERT INTO `crm_loginhistory` VALUES ('154', '7', '销售人员，登入系统', '2017-10-12 14:50:32');
INSERT INTO `crm_loginhistory` VALUES ('155', '7', '销售人员，退出系统', '2017-10-12 14:50:49');
INSERT INTO `crm_loginhistory` VALUES ('156', '1', '超级管理员，登入系统', '2017-10-12 14:50:51');
INSERT INTO `crm_loginhistory` VALUES ('157', '1', '超级管理员，退出系统', '2017-10-12 14:51:35');
INSERT INTO `crm_loginhistory` VALUES ('158', '7', '销售人员，登入系统', '2017-10-12 14:51:41');
INSERT INTO `crm_loginhistory` VALUES ('159', '7', '销售人员，退出系统', '2017-10-12 14:51:54');
INSERT INTO `crm_loginhistory` VALUES ('160', '1', '超级管理员，登入系统', '2017-10-12 14:51:56');
INSERT INTO `crm_loginhistory` VALUES ('161', '1', '超级管理员，退出系统', '2017-10-12 15:07:19');
INSERT INTO `crm_loginhistory` VALUES ('162', '7', '销售人员，登入系统', '2017-10-12 15:07:27');
INSERT INTO `crm_loginhistory` VALUES ('163', '7', '销售人员，退出系统', '2017-10-12 15:07:48');
INSERT INTO `crm_loginhistory` VALUES ('164', '1', '超级管理员，登入系统', '2017-10-12 15:07:51');
INSERT INTO `crm_loginhistory` VALUES ('165', '1', '超级管理员，退出系统', '2017-10-12 15:11:37');
INSERT INTO `crm_loginhistory` VALUES ('166', '1', '超级管理员，登入系统', '2017-10-12 15:12:19');
INSERT INTO `crm_loginhistory` VALUES ('167', '1', '超级管理员，登入系统', '2017-10-12 15:28:09');
INSERT INTO `crm_loginhistory` VALUES ('168', '1', '超级管理员，退出系统', '2017-10-12 16:06:42');
INSERT INTO `crm_loginhistory` VALUES ('169', '1', '超级管理员，登入系统', '2017-10-12 16:10:30');
INSERT INTO `crm_loginhistory` VALUES ('170', '8', '客服人员，登入系统', '2017-10-12 16:11:21');
INSERT INTO `crm_loginhistory` VALUES ('171', '8', '客服人员，登入系统', '2017-10-12 16:11:47');
INSERT INTO `crm_loginhistory` VALUES ('172', '1', '超级管理员，退出系统', '2017-10-12 16:11:52');
INSERT INTO `crm_loginhistory` VALUES ('173', '7', '销售人员，登入系统', '2017-10-12 16:11:58');
INSERT INTO `crm_loginhistory` VALUES ('174', '7', '销售人员，登入系统', '2017-10-12 16:12:52');
INSERT INTO `crm_loginhistory` VALUES ('175', '1', '超级管理员，登入系统', '2017-10-12 16:13:37');
INSERT INTO `crm_loginhistory` VALUES ('176', '1', '超级管理员，退出系统', '2017-10-12 16:17:15');
INSERT INTO `crm_loginhistory` VALUES ('177', '1', '超级管理员，登入系统', '2017-10-12 16:17:43');
INSERT INTO `crm_loginhistory` VALUES ('178', '7', '销售人员，退出系统', '2017-10-12 16:20:49');
INSERT INTO `crm_loginhistory` VALUES ('179', '8', '客服人员，登入系统', '2017-10-12 16:21:17');
INSERT INTO `crm_loginhistory` VALUES ('180', '1', '超级管理员，退出系统', '2017-10-12 16:22:04');
INSERT INTO `crm_loginhistory` VALUES ('181', '1', '超级管理员，登入系统', '2017-10-12 16:22:21');
INSERT INTO `crm_loginhistory` VALUES ('182', '8', '客服人员，退出系统', '2017-10-12 16:26:04');
INSERT INTO `crm_loginhistory` VALUES ('183', '1', '超级管理员，登入系统', '2017-10-12 16:26:06');
INSERT INTO `crm_loginhistory` VALUES ('184', '7', '销售人员，退出系统', '2017-10-12 16:30:33');
INSERT INTO `crm_loginhistory` VALUES ('185', '1', '超级管理员，登入系统', '2017-10-12 16:30:35');
INSERT INTO `crm_loginhistory` VALUES ('186', '1', '超级管理员，退出系统', '2017-10-12 16:31:38');
INSERT INTO `crm_loginhistory` VALUES ('187', '1', '超级管理员，登入系统', '2017-10-12 16:32:03');
INSERT INTO `crm_loginhistory` VALUES ('188', '1', '超级管理员，登入系统', '2017-10-12 16:35:57');
INSERT INTO `crm_loginhistory` VALUES ('189', '1', '超级管理员，登入系统', '2017-10-12 16:37:10');
INSERT INTO `crm_loginhistory` VALUES ('190', '1', '超级管理员，登入系统', '2017-10-12 16:42:14');
INSERT INTO `crm_loginhistory` VALUES ('191', '1', '超级管理员，登入系统', '2017-10-12 16:42:17');
INSERT INTO `crm_loginhistory` VALUES ('192', '1', '超级管理员，登入系统', '2017-10-12 16:42:17');
INSERT INTO `crm_loginhistory` VALUES ('193', '1', '超级管理员，登入系统', '2017-10-12 16:53:33');
INSERT INTO `crm_loginhistory` VALUES ('194', '1', '超级管理员，登入系统', '2017-10-12 16:58:12');
INSERT INTO `crm_loginhistory` VALUES ('195', '1', '超级管理员，退出系统', '2017-10-12 17:03:35');
INSERT INTO `crm_loginhistory` VALUES ('196', '7', '销售人员，登入系统', '2017-10-12 17:03:41');
INSERT INTO `crm_loginhistory` VALUES ('197', '7', '销售人员，退出系统', '2017-10-12 17:04:11');
INSERT INTO `crm_loginhistory` VALUES ('198', '1', '超级管理员，登入系统', '2017-10-12 17:04:12');
INSERT INTO `crm_loginhistory` VALUES ('199', '1', '超级管理员，退出系统', '2017-10-12 17:04:41');
INSERT INTO `crm_loginhistory` VALUES ('200', '7', '销售人员，登入系统', '2017-10-12 17:04:46');
INSERT INTO `crm_loginhistory` VALUES ('201', '7', '销售人员，退出系统', '2017-10-12 17:05:27');
INSERT INTO `crm_loginhistory` VALUES ('202', '1', '超级管理员，登入系统', '2017-10-12 17:05:28');
INSERT INTO `crm_loginhistory` VALUES ('203', '1', '超级管理员，退出系统', '2017-10-12 17:07:40');
INSERT INTO `crm_loginhistory` VALUES ('204', '7', '销售人员，登入系统', '2017-10-12 17:07:46');
INSERT INTO `crm_loginhistory` VALUES ('205', '7', '销售人员，退出系统', '2017-10-12 17:12:35');
INSERT INTO `crm_loginhistory` VALUES ('206', '1', '超级管理员，登入系统', '2017-10-12 17:12:37');
INSERT INTO `crm_loginhistory` VALUES ('207', '1', '超级管理员，登入系统', '2017-10-12 17:54:13');
INSERT INTO `crm_loginhistory` VALUES ('208', '1', '超级管理员，登入系统', '2017-10-12 18:32:20');
INSERT INTO `crm_loginhistory` VALUES ('209', '1', '超级管理员，登入系统', '2017-10-12 19:54:55');
INSERT INTO `crm_loginhistory` VALUES ('210', '1', '超级管理员，登入系统', '2017-10-12 22:24:54');
INSERT INTO `crm_loginhistory` VALUES ('211', '1', '超级管理员，退出系统', '2017-10-12 22:25:42');
INSERT INTO `crm_loginhistory` VALUES ('212', '7', '销售人员，登入系统', '2017-10-12 22:25:54');
INSERT INTO `crm_loginhistory` VALUES ('213', '7', '销售人员，退出系统', '2017-10-12 22:26:11');
INSERT INTO `crm_loginhistory` VALUES ('214', '1', '超级管理员，登入系统', '2017-10-12 22:26:13');
INSERT INTO `crm_loginhistory` VALUES ('215', '1', '超级管理员，登入系统', '2017-10-12 22:28:30');
INSERT INTO `crm_loginhistory` VALUES ('216', '1', '超级管理员，登入系统', '2017-10-12 22:29:12');
INSERT INTO `crm_loginhistory` VALUES ('217', '1', '超级管理员，退出系统', '2017-10-12 22:32:38');
INSERT INTO `crm_loginhistory` VALUES ('218', '1', '超级管理员，登入系统', '2017-10-12 22:32:40');
INSERT INTO `crm_loginhistory` VALUES ('219', '1', '超级管理员，退出系统', '2017-10-12 22:36:49');
INSERT INTO `crm_loginhistory` VALUES ('220', '7', '销售人员，登入系统', '2017-10-12 22:36:59');
INSERT INTO `crm_loginhistory` VALUES ('221', '1', '超级管理员，登入系统', '2017-10-12 22:39:19');
INSERT INTO `crm_loginhistory` VALUES ('222', '7', '销售人员，登入系统', '2017-10-12 22:41:52');
INSERT INTO `crm_loginhistory` VALUES ('223', '7', '销售人员，登入系统', '2017-10-12 22:42:42');
INSERT INTO `crm_loginhistory` VALUES ('224', '7', '销售人员，退出系统', '2017-10-12 22:43:04');
INSERT INTO `crm_loginhistory` VALUES ('225', '1', '超级管理员，登入系统', '2017-10-12 22:43:08');
INSERT INTO `crm_loginhistory` VALUES ('226', '1', '超级管理员，登入系统', '2017-10-12 22:43:35');
INSERT INTO `crm_loginhistory` VALUES ('227', '7', '销售人员，登入系统', '2017-10-12 22:45:06');
INSERT INTO `crm_loginhistory` VALUES ('228', '1', '超级管理员，登入系统', '2017-10-12 22:45:54');
INSERT INTO `crm_loginhistory` VALUES ('229', '1', '超级管理员，退出系统', '2017-10-12 22:46:51');
INSERT INTO `crm_loginhistory` VALUES ('230', '7', '销售人员，登入系统', '2017-10-12 22:47:06');
INSERT INTO `crm_loginhistory` VALUES ('231', '1', '超级管理员，登入系统', '2017-10-12 22:57:37');
INSERT INTO `crm_loginhistory` VALUES ('232', '1', '超级管理员，登入系统', '2017-10-12 22:59:57');
INSERT INTO `crm_loginhistory` VALUES ('233', '1', '超级管理员，登入系统', '2017-10-13 07:23:17');
INSERT INTO `crm_loginhistory` VALUES ('234', '1', '超级管理员，登入系统', '2017-10-13 08:45:46');
INSERT INTO `crm_loginhistory` VALUES ('235', '1', '超级管理员，退出系统', '2017-10-13 08:52:02');
INSERT INTO `crm_loginhistory` VALUES ('236', '1', '超级管理员，登入系统', '2017-10-13 08:53:17');
INSERT INTO `crm_loginhistory` VALUES ('237', '1', '超级管理员，登入系统', '2017-10-13 08:53:20');
INSERT INTO `crm_loginhistory` VALUES ('238', '1', '超级管理员，登入系统', '2017-10-13 08:53:30');
INSERT INTO `crm_loginhistory` VALUES ('239', '1', '超级管理员，退出系统', '2017-10-13 09:00:30');
INSERT INTO `crm_loginhistory` VALUES ('240', '7', '销售人员，登入系统', '2017-10-13 09:00:39');
INSERT INTO `crm_loginhistory` VALUES ('241', '7', '销售人员，退出系统', '2017-10-13 09:01:42');
INSERT INTO `crm_loginhistory` VALUES ('242', '1', '超级管理员，登入系统', '2017-10-13 09:03:16');
INSERT INTO `crm_loginhistory` VALUES ('243', '8', '客服人员，登入系统', '2017-10-13 09:05:23');
INSERT INTO `crm_loginhistory` VALUES ('244', '1', '超级管理员，登入系统', '2017-10-13 09:14:04');
INSERT INTO `crm_loginhistory` VALUES ('245', '8', '客服人员，退出系统', '2017-10-13 09:22:39');
INSERT INTO `crm_loginhistory` VALUES ('246', '1', '超级管理员，登入系统', '2017-10-13 09:22:47');
INSERT INTO `crm_loginhistory` VALUES ('247', '1', '超级管理员，退出系统', '2017-10-13 09:22:52');
INSERT INTO `crm_loginhistory` VALUES ('248', '7', '销售人员，登入系统', '2017-10-13 09:23:15');
INSERT INTO `crm_loginhistory` VALUES ('249', '7', '销售人员，退出系统', '2017-10-13 09:29:14');
INSERT INTO `crm_loginhistory` VALUES ('250', '7', '销售人员，登入系统', '2017-10-13 09:29:23');
INSERT INTO `crm_loginhistory` VALUES ('251', '7', '销售人员，退出系统', '2017-10-13 09:37:21');
INSERT INTO `crm_loginhistory` VALUES ('252', '1', '超级管理员，退出系统', '2017-10-13 09:37:33');
INSERT INTO `crm_loginhistory` VALUES ('253', '7', '销售人员，登入系统', '2017-10-13 09:37:36');
INSERT INTO `crm_loginhistory` VALUES ('254', '1', '超级管理员，登入系统', '2017-10-13 09:37:40');
INSERT INTO `crm_loginhistory` VALUES ('255', '1', '超级管理员，退出系统', '2017-10-13 09:37:48');
INSERT INTO `crm_loginhistory` VALUES ('256', '1', '超级管理员，登入系统', '2017-10-13 09:37:52');
INSERT INTO `crm_loginhistory` VALUES ('257', '1', '超级管理员，退出系统', '2017-10-13 09:53:44');
INSERT INTO `crm_loginhistory` VALUES ('258', '1', '超级管理员，登入系统', '2017-10-13 09:53:49');
INSERT INTO `crm_loginhistory` VALUES ('259', '1', '超级管理员，退出系统', '2017-10-13 11:14:17');
INSERT INTO `crm_loginhistory` VALUES ('260', '7', '销售人员，登入系统', '2017-10-13 11:14:26');
INSERT INTO `crm_loginhistory` VALUES ('261', '7', '销售人员，退出系统', '2017-10-13 11:14:35');
INSERT INTO `crm_loginhistory` VALUES ('262', '1', '超级管理员，登入系统', '2017-10-13 11:14:38');
INSERT INTO `crm_loginhistory` VALUES ('263', '7', '销售人员，退出系统', '2017-10-13 11:15:08');
INSERT INTO `crm_loginhistory` VALUES ('264', '8', '客服人员，登入系统', '2017-10-13 11:17:46');
INSERT INTO `crm_loginhistory` VALUES ('265', '1', '超级管理员，登入系统', '2017-10-13 11:32:10');
INSERT INTO `crm_loginhistory` VALUES ('266', '1', '超级管理员，登入系统', '2017-10-13 11:54:22');
INSERT INTO `crm_loginhistory` VALUES ('267', '1', '超级管理员，登入系统', '2017-10-13 12:03:19');
INSERT INTO `crm_loginhistory` VALUES ('268', '1', '超级管理员，登入系统', '2017-10-13 13:15:30');
INSERT INTO `crm_loginhistory` VALUES ('269', '1', '超级管理员，登入系统', '2017-10-13 13:31:33');
INSERT INTO `crm_loginhistory` VALUES ('270', '1', '超级管理员，登入系统', '2017-10-13 13:51:08');
INSERT INTO `crm_loginhistory` VALUES ('271', '1', '超级管理员，登入系统', '2017-10-13 14:00:07');
INSERT INTO `crm_loginhistory` VALUES ('272', '1', '超级管理员，退出系统', '2017-10-13 14:21:55');
INSERT INTO `crm_loginhistory` VALUES ('273', '7', '销售人员，登入系统', '2017-10-13 14:22:06');
INSERT INTO `crm_loginhistory` VALUES ('274', '7', '销售人员，退出系统', '2017-10-13 14:24:02');
INSERT INTO `crm_loginhistory` VALUES ('275', '8', '客服人员，登入系统', '2017-10-13 14:24:11');
INSERT INTO `crm_loginhistory` VALUES ('276', '8', '客服人员，退出系统', '2017-10-13 14:34:54');
INSERT INTO `crm_loginhistory` VALUES ('277', '1', '超级管理员，登入系统', '2017-10-13 14:34:56');
INSERT INTO `crm_loginhistory` VALUES ('278', '1', '超级管理员，退出系统', '2017-10-13 14:40:21');
INSERT INTO `crm_loginhistory` VALUES ('279', '7', '销售人员，登入系统', '2017-10-13 14:40:26');
INSERT INTO `crm_loginhistory` VALUES ('280', '7', '销售人员，退出系统', '2017-10-13 14:40:35');
INSERT INTO `crm_loginhistory` VALUES ('281', '8', '客服人员，登入系统', '2017-10-13 14:40:46');
INSERT INTO `crm_loginhistory` VALUES ('282', '8', '客服人员，退出系统', '2017-10-13 14:44:27');
INSERT INTO `crm_loginhistory` VALUES ('283', '1', '超级管理员，登入系统', '2017-10-13 14:44:28');
INSERT INTO `crm_loginhistory` VALUES ('284', '1', '超级管理员，退出系统', '2017-10-13 14:44:39');
INSERT INTO `crm_loginhistory` VALUES ('285', '7', '销售人员，登入系统', '2017-10-13 14:44:45');
INSERT INTO `crm_loginhistory` VALUES ('286', '7', '销售人员，退出系统', '2017-10-13 14:44:54');
INSERT INTO `crm_loginhistory` VALUES ('287', '1', '超级管理员，登入系统', '2017-10-13 14:44:56');
INSERT INTO `crm_loginhistory` VALUES ('288', '1', '超级管理员，退出系统', '2017-10-13 14:54:05');
INSERT INTO `crm_loginhistory` VALUES ('289', '7', '销售人员，登入系统', '2017-10-13 14:54:12');
INSERT INTO `crm_loginhistory` VALUES ('290', '7', '销售人员，退出系统', '2017-10-13 14:54:22');
INSERT INTO `crm_loginhistory` VALUES ('291', '8', '客服人员，登入系统', '2017-10-13 14:54:30');
INSERT INTO `crm_loginhistory` VALUES ('292', '8', '客服人员，退出系统', '2017-10-13 14:57:38');
INSERT INTO `crm_loginhistory` VALUES ('293', '1', '超级管理员，登入系统', '2017-10-13 14:57:40');
INSERT INTO `crm_loginhistory` VALUES ('294', '1', '超级管理员，登入系统', '2017-10-13 16:58:31');
INSERT INTO `crm_loginhistory` VALUES ('295', '1', '超级管理员，登入系统', '2017-10-13 22:49:00');
INSERT INTO `crm_loginhistory` VALUES ('296', '1', '超级管理员，登入系统', '2017-10-14 08:02:36');
INSERT INTO `crm_loginhistory` VALUES ('297', '1', '超级管理员，登入系统', '2017-10-14 09:23:25');
INSERT INTO `crm_loginhistory` VALUES ('298', '1', '超级管理员，登入系统', '2017-10-14 11:31:27');
INSERT INTO `crm_loginhistory` VALUES ('299', '1', '超级管理员，登入系统', '2017-10-14 11:31:30');
INSERT INTO `crm_loginhistory` VALUES ('300', '1', '超级管理员，登入系统', '2017-10-14 11:31:56');
INSERT INTO `crm_loginhistory` VALUES ('301', '1', '超级管理员，登入系统', '2017-10-14 11:47:53');
INSERT INTO `crm_loginhistory` VALUES ('302', '1', '超级管理员，登入系统', '2017-10-14 13:29:26');
INSERT INTO `crm_loginhistory` VALUES ('303', '1', '超级管理员，登入系统', '2017-10-14 15:30:28');
INSERT INTO `crm_loginhistory` VALUES ('304', '1', '超级管理员，登入系统', '2017-10-15 13:39:06');
INSERT INTO `crm_loginhistory` VALUES ('305', '1', '超级管理员，登入系统', '2017-10-15 14:33:13');
INSERT INTO `crm_loginhistory` VALUES ('306', '1', '超级管理员，登入系统', '2017-10-15 19:31:50');
INSERT INTO `crm_loginhistory` VALUES ('307', '1', '超级管理员，登入系统', '2017-10-15 20:27:09');
INSERT INTO `crm_loginhistory` VALUES ('308', '1', '超级管理员，登入系统', '2017-10-15 20:27:12');
INSERT INTO `crm_loginhistory` VALUES ('309', '1', '超级管理员，登入系统', '2017-10-15 21:09:25');
INSERT INTO `crm_loginhistory` VALUES ('310', '1', '超级管理员，登入系统', '2017-10-15 22:58:13');
INSERT INTO `crm_loginhistory` VALUES ('311', '1', '超级管理员，登入系统', '2017-10-16 07:53:56');
INSERT INTO `crm_loginhistory` VALUES ('312', '1', '超级管理员，登入系统', '2017-10-16 07:54:00');
INSERT INTO `crm_loginhistory` VALUES ('313', '1', '超级管理员，登入系统', '2017-10-16 07:54:14');
INSERT INTO `crm_loginhistory` VALUES ('314', '1', '超级管理员，登入系统', '2017-10-16 09:31:27');
INSERT INTO `crm_loginhistory` VALUES ('315', '1', '超级管理员，登入系统', '2017-10-16 10:00:22');
INSERT INTO `crm_loginhistory` VALUES ('316', '1', '超级管理员，登入系统', '2017-10-16 10:54:38');
INSERT INTO `crm_loginhistory` VALUES ('317', '1', '超级管理员，登入系统', '2017-10-16 16:50:01');
INSERT INTO `crm_loginhistory` VALUES ('318', '1', '超级管理员，退出系统', '2017-10-16 16:51:07');
INSERT INTO `crm_loginhistory` VALUES ('319', '1', '超级管理员，登入系统', '2017-10-16 16:51:09');
INSERT INTO `crm_loginhistory` VALUES ('320', '1', '超级管理员，退出系统', '2017-10-18 10:21:23');
INSERT INTO `crm_loginhistory` VALUES ('321', '1', '超级管理员，登入系统', '2017-10-18 10:21:45');
INSERT INTO `crm_loginhistory` VALUES ('322', '1', '超级管理员，退出系统', '2017-10-18 12:24:44');
INSERT INTO `crm_loginhistory` VALUES ('323', '1', '超级管理员，登入系统', '2017-10-18 16:12:44');
INSERT INTO `crm_loginhistory` VALUES ('324', '1', '超级管理员，退出系统', '2017-10-18 16:19:18');
INSERT INTO `crm_loginhistory` VALUES ('325', '1', '超级管理员，登入系统', '2017-10-18 16:19:19');
INSERT INTO `crm_loginhistory` VALUES ('326', '1', '超级管理员，登入系统', '2017-10-19 22:17:56');
INSERT INTO `crm_loginhistory` VALUES ('327', '1', '超级管理员，登入系统', '2017-10-19 22:20:14');
INSERT INTO `crm_loginhistory` VALUES ('328', '1', '超级管理员，登入系统', '2017-10-22 13:52:05');
INSERT INTO `crm_loginhistory` VALUES ('329', '1', '超级管理员，登入系统', '2017-10-22 14:55:36');
INSERT INTO `crm_loginhistory` VALUES ('330', '1', '超级管理员，登入系统', '2017-10-25 09:55:07');
INSERT INTO `crm_loginhistory` VALUES ('331', '1', '超级管理员，登入系统', '2017-10-28 06:54:54');
INSERT INTO `crm_loginhistory` VALUES ('332', '1', '超级管理员，登入系统', '2017-10-30 22:41:03');
INSERT INTO `crm_loginhistory` VALUES ('333', '1', '超级管理员，登入系统', '2017-10-30 22:41:05');
INSERT INTO `crm_loginhistory` VALUES ('334', '1', '超级管理员，登入系统', '2017-10-30 22:41:08');
INSERT INTO `crm_loginhistory` VALUES ('335', '1', '超级管理员，登入系统', '2017-10-31 22:54:15');
INSERT INTO `crm_loginhistory` VALUES ('336', '1', '超级管理员，登入系统', '2017-11-01 08:06:34');
INSERT INTO `crm_loginhistory` VALUES ('337', '1', '超级管理员，登入系统', '2017-11-05 17:25:20');
INSERT INTO `crm_loginhistory` VALUES ('338', '1', '超级管理员，登入系统', '2017-11-06 12:12:16');
INSERT INTO `crm_loginhistory` VALUES ('339', '1', '超级管理员，登入系统', '2017-11-06 21:36:55');
INSERT INTO `crm_loginhistory` VALUES ('340', '1', '超级管理员，登入系统', '2017-11-07 15:35:18');
INSERT INTO `crm_loginhistory` VALUES ('341', '1', '超级管理员，登入系统', '2017-11-07 15:35:45');
INSERT INTO `crm_loginhistory` VALUES ('342', '1', '超级管理员，登入系统', '2017-11-07 20:04:45');
INSERT INTO `crm_loginhistory` VALUES ('343', '1', '超级管理员，登入系统', '2017-11-08 19:35:08');
INSERT INTO `crm_loginhistory` VALUES ('344', '1', '超级管理员，登入系统', '2017-11-08 22:18:23');
INSERT INTO `crm_loginhistory` VALUES ('345', '1', '超级管理员，登入系统', '2017-11-10 15:02:17');
INSERT INTO `crm_loginhistory` VALUES ('346', '1', '超级管理员，登入系统', '2017-11-11 09:23:46');
INSERT INTO `crm_loginhistory` VALUES ('347', '1', '超级管理员，登入系统', '2017-11-11 09:23:47');
INSERT INTO `crm_loginhistory` VALUES ('348', '1', '超级管理员，登入系统', '2017-11-11 09:23:47');
INSERT INTO `crm_loginhistory` VALUES ('349', '1', '超级管理员，登入系统', '2017-11-11 09:23:47');
INSERT INTO `crm_loginhistory` VALUES ('350', '1', '超级管理员，登入系统', '2017-11-11 09:23:50');
INSERT INTO `crm_loginhistory` VALUES ('351', '1', '超级管理员，登入系统', '2017-11-11 09:24:14');
INSERT INTO `crm_loginhistory` VALUES ('352', '1', '超级管理员，登入系统', '2017-11-11 19:33:11');
INSERT INTO `crm_loginhistory` VALUES ('353', '1', '超级管理员，登入系统', '2017-11-12 00:09:17');
INSERT INTO `crm_loginhistory` VALUES ('354', '1', '超级管理员，登入系统', '2017-11-12 12:17:46');
INSERT INTO `crm_loginhistory` VALUES ('355', '1', '超级管理员，登入系统', '2017-11-12 12:17:49');
INSERT INTO `crm_loginhistory` VALUES ('356', '1', '超级管理员，登入系统', '2017-11-12 12:17:52');
INSERT INTO `crm_loginhistory` VALUES ('357', '1', '超级管理员，退出系统', '2017-11-12 12:59:44');
INSERT INTO `crm_loginhistory` VALUES ('358', '1', '超级管理员，登入系统', '2017-11-12 12:59:47');
INSERT INTO `crm_loginhistory` VALUES ('359', '1', '超级管理员，退出系统', '2017-11-12 13:06:37');
INSERT INTO `crm_loginhistory` VALUES ('360', '1', '超级管理员，登入系统', '2017-11-12 13:31:09');
INSERT INTO `crm_loginhistory` VALUES ('361', '1', '超级管理员，登入系统', '2017-11-12 19:28:15');
INSERT INTO `crm_loginhistory` VALUES ('362', '1', '超级管理员，登入系统', '2017-11-12 19:28:15');
INSERT INTO `crm_loginhistory` VALUES ('363', '1', '超级管理员，登入系统', '2017-11-12 19:28:18');
INSERT INTO `crm_loginhistory` VALUES ('364', '1', '超级管理员，登入系统', '2017-11-12 19:28:22');
INSERT INTO `crm_loginhistory` VALUES ('365', '1', '超级管理员，登入系统', '2017-11-12 19:41:53');
INSERT INTO `crm_loginhistory` VALUES ('366', '1', '超级管理员，登入系统', '2017-11-14 21:20:44');
INSERT INTO `crm_loginhistory` VALUES ('367', '1', '超级管理员，登入系统', '2017-11-14 21:26:33');
INSERT INTO `crm_loginhistory` VALUES ('368', '1', '超级管理员，登入系统', '2017-11-14 23:17:02');
INSERT INTO `crm_loginhistory` VALUES ('369', '1', '超级管理员，登入系统', '2017-11-15 12:01:46');
INSERT INTO `crm_loginhistory` VALUES ('370', '1', '超级管理员，登入系统', '2017-11-17 23:27:37');
INSERT INTO `crm_loginhistory` VALUES ('371', '1', '超级管理员，登入系统', '2017-11-18 08:07:56');
INSERT INTO `crm_loginhistory` VALUES ('372', '1', '超级管理员，退出系统', '2017-11-18 08:08:50');
INSERT INTO `crm_loginhistory` VALUES ('373', '1', '超级管理员，登入系统', '2017-11-18 08:08:52');
INSERT INTO `crm_loginhistory` VALUES ('374', '1', '超级管理员，登入系统', '2017-11-18 08:51:31');
INSERT INTO `crm_loginhistory` VALUES ('375', '1', '超级管理员，登入系统', '2017-11-18 09:18:05');
INSERT INTO `crm_loginhistory` VALUES ('376', '1', '超级管理员，登入系统', '2017-11-18 09:36:21');
INSERT INTO `crm_loginhistory` VALUES ('377', '1', '超级管理员，登入系统', '2017-11-18 19:02:10');
INSERT INTO `crm_loginhistory` VALUES ('378', '1', '超级管理员，登入系统', '2017-11-18 19:02:14');
INSERT INTO `crm_loginhistory` VALUES ('379', '1', '超级管理员，登入系统', '2017-11-18 19:02:17');
INSERT INTO `crm_loginhistory` VALUES ('380', '1', '超级管理员，登入系统', '2017-11-18 19:02:20');
INSERT INTO `crm_loginhistory` VALUES ('381', '1', '超级管理员，登入系统', '2017-11-18 19:02:25');
INSERT INTO `crm_loginhistory` VALUES ('382', '1', '超级管理员，登入系统', '2017-11-19 16:34:35');
INSERT INTO `crm_loginhistory` VALUES ('383', '1', '超级管理员，登入系统', '2017-11-23 09:21:31');
INSERT INTO `crm_loginhistory` VALUES ('384', '1', '超级管理员，登入系统', '2017-11-25 13:45:03');
INSERT INTO `crm_loginhistory` VALUES ('385', '1', '超级管理员，登入系统', '2017-11-28 19:41:55');
INSERT INTO `crm_loginhistory` VALUES ('386', '1', '超级管理员，登入系统', '2017-11-30 22:12:42');
INSERT INTO `crm_loginhistory` VALUES ('387', '1', '超级管理员，登入系统', '2017-11-30 23:11:55');
INSERT INTO `crm_loginhistory` VALUES ('388', '1', '超级管理员，登入系统', '2017-12-02 19:17:37');

-- ----------------------------
-- Table structure for `crm_marketinggroup`
-- ----------------------------
DROP TABLE IF EXISTS `crm_marketinggroup`;
CREATE TABLE `crm_marketinggroup` (
  `marketinggroup_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `groupname` varchar(255) DEFAULT NULL COMMENT '群名称',
  `group_description` varchar(255) DEFAULT NULL COMMENT '群描述',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  PRIMARY KEY (`marketinggroup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='营销群';

-- ----------------------------
-- Records of crm_marketinggroup
-- ----------------------------
INSERT INTO `crm_marketinggroup` VALUES ('1', '2323', '2323232323', '0');

-- ----------------------------
-- Table structure for `crm_marketinggroupinfo`
-- ----------------------------
DROP TABLE IF EXISTS `crm_marketinggroupinfo`;
CREATE TABLE `crm_marketinggroupinfo` (
  `marketinggroupinfo_id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) DEFAULT NULL COMMENT '手机号',
  `marketinggroup_id` int(255) NOT NULL,
  PRIMARY KEY (`marketinggroupinfo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='营销群人员信息表';

-- ----------------------------
-- Records of crm_marketinggroupinfo
-- ----------------------------
INSERT INTO `crm_marketinggroupinfo` VALUES ('4', '23', '1');
INSERT INTO `crm_marketinggroupinfo` VALUES ('5', '2323', '1');
INSERT INTO `crm_marketinggroupinfo` VALUES ('6', '2323', '1');

-- ----------------------------
-- Table structure for `crm_memdays`
-- ----------------------------
DROP TABLE IF EXISTS `crm_memdays`;
CREATE TABLE `crm_memdays` (
  `memdays_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `customer` varchar(255) DEFAULT NULL COMMENT '客户',
  `memdays_type` varchar(255) DEFAULT NULL COMMENT '纪念日类型',
  `memdays_day` varchar(255) DEFAULT NULL COMMENT '纪念日',
  `calendar_type` varchar(255) DEFAULT NULL COMMENT '阳历/阴历',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识，0-未删除，1-删除',
  `memo` int(11) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`memdays_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='客户纪念日';

-- ----------------------------
-- Records of crm_memdays
-- ----------------------------
INSERT INTO `crm_memdays` VALUES ('1', '2323', '2323', '父母生日', '2017-05-03', '阴历', '1', null);
INSERT INTO `crm_memdays` VALUES ('2', '32', '23', '无', '23', '公历', '1', null);
INSERT INTO `crm_memdays` VALUES ('3', 'e3888777', '342323', '无', '2017-08-22', '公历', '0', '99999');
INSERT INTO `crm_memdays` VALUES ('4', '新中国成立78周年', '全中国', '其他', '2017-10-01', '公历', '0', null);
INSERT INTO `crm_memdays` VALUES ('5', '测试', '大黄蜂', '生日', '2017-10-13', '公历', '0', null);

-- ----------------------------
-- Table structure for `crm_messagegroup`
-- ----------------------------
DROP TABLE IF EXISTS `crm_messagegroup`;
CREATE TABLE `crm_messagegroup` (
  `messagegroup_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '主题',
  `messagetemplate_id` int(11) DEFAULT NULL COMMENT '短信模板ID',
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT NULL COMMENT '状态',
  `sendtime` datetime DEFAULT NULL COMMENT '预计发送时间',
  `marketinggroup_id` int(11) DEFAULT NULL COMMENT '营销群ID',
  PRIMARY KEY (`messagegroup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='群发短信表';

-- ----------------------------
-- Records of crm_messagegroup
-- ----------------------------
INSERT INTO `crm_messagegroup` VALUES ('1', 'test', '1', '2017-11-15 12:04:50', 'wait', null, null);

-- ----------------------------
-- Table structure for `crm_messagegroupuser`
-- ----------------------------
DROP TABLE IF EXISTS `crm_messagegroupuser`;
CREATE TABLE `crm_messagegroupuser` (
  `messagegroupuser_id` int(11) NOT NULL COMMENT '群发短信用户主键',
  `messagegroup_id` int(11) DEFAULT NULL COMMENT '群发短信主键',
  `mobile` varchar(255) DEFAULT NULL COMMENT '手机号',
  `sendstatus` varchar(255) DEFAULT NULL COMMENT '发送状态',
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatetime` timestamp NULL DEFAULT NULL,
  `response` text COMMENT '第三方短信接口调用的返回信息',
  `yunpiantemplateid` int(11) DEFAULT NULL COMMENT '云片网短信ID',
  `templatecode` varchar(255) DEFAULT NULL COMMENT '模板的code',
  PRIMARY KEY (`messagegroupuser_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='群发短信相关用户表';

-- ----------------------------
-- Records of crm_messagegroupuser
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_module`
-- ----------------------------
DROP TABLE IF EXISTS `crm_module`;
CREATE TABLE `crm_module` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `modulename` varchar(255) DEFAULT NULL,
  `moduleenname` varchar(255) DEFAULT NULL,
  `moduletable` varchar(255) DEFAULT NULL COMMENT '模块对应的表名',
  `moduleentityid` varchar(255) DEFAULT NULL COMMENT '模块对应的表的主键ID',
  `modulecoulumname` varchar(255) DEFAULT NULL COMMENT '模块实体对应的columnname',
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COMMENT='模块定义';

-- ----------------------------
-- Records of crm_module
-- ----------------------------
INSERT INTO `crm_module` VALUES ('1', '公告', 'announcements', 'crm_announcements', 'announcements_id', 'title');
INSERT INTO `crm_module` VALUES ('2', '营销', 'campaigns', 'crm_campaigns', 'campaigns_id', 'campaigns_name');
INSERT INTO `crm_module` VALUES ('3', '产品', 'product', 'crm_product', 'product_id', 'productname');
INSERT INTO `crm_module` VALUES ('4', '产品分类', 'productcategory', 'crm_productcategory', 'productcategory_id', 'productcategoryname');
INSERT INTO `crm_module` VALUES ('5', '联系人', 'contacter', 'crm_contacter', 'contacter_id', 'contacter_name');
INSERT INTO `crm_module` VALUES ('6', '客户', 'customer', 'crm_customer', 'customer_id', 'customer_name');
INSERT INTO `crm_module` VALUES ('7', '联系记录', 'contactlog', 'crm_contactlog', 'contactlog_id', 'title');
INSERT INTO `crm_module` VALUES ('8', '客户服务', 'accountrecordss', 'crm_accountrecordss', 'accountrecordss_id', 'title');
INSERT INTO `crm_module` VALUES ('9', '纪念日', 'memdays', 'crm_memdays', 'memdays_id', 'title');
INSERT INTO `crm_module` VALUES ('10', '销售机会', 'potentials', 'crm_potentials', 'potentials_id', 'potentials_name');
INSERT INTO `crm_module` VALUES ('11', '报价单', 'quotes', 'crm_quotes', 'quotes_id', 'quotes_number');
INSERT INTO `crm_module` VALUES ('12', '竞争对手', 'competitor', 'crm_competitor', 'competitor_id', 'competitor_name');
INSERT INTO `crm_module` VALUES ('13', '合同订单', 'salesorder', 'crm_salesorder', 'salesorder_id', 'salesorder_number');
INSERT INTO `crm_module` VALUES ('14', '发货单', 'invoice', 'crm_invoice', 'invoice_id', 'invoice_num');
INSERT INTO `crm_module` VALUES ('15', '销售退货单', 'tuihuo', 'crm_tuihuo', 'tuihuo_id', 'tuihuo_num');
INSERT INTO `crm_module` VALUES ('16', '采购', 'caigous', 'crm_caigous', 'caigous_id', 'caigous_num');
INSERT INTO `crm_module` VALUES ('17', '进货单', 'purchasedorder', 'crm_purchaseorder', 'purchaseorder_id', 'purchaseorder_num');
INSERT INTO `crm_module` VALUES ('18', '供应商', 'vendors', 'crm_vendors', 'vendors_id', 'vendor_name');
INSERT INTO `crm_module` VALUES ('19', '供应商联系人', 'vcontacts', 'crm_vcontacts', 'vcontacts_id', 'vcontacts_name');
INSERT INTO `crm_module` VALUES ('20', '供应商联系记录', 'vnotes', 'crm_vnotes', 'vnotes_id', 'title');
INSERT INTO `crm_module` VALUES ('21', '采购退货', 'caigoutuihuos', 'crm_caigoutuihuos', 'caigoutuihuos_id', 'title');
INSERT INTO `crm_module` VALUES ('22', '客户服务', 'accountrecordss', 'crm_accountrecordss', 'accountrecordss_id', 'title');
INSERT INTO `crm_module` VALUES ('23', '客户投诉', 'complaints', 'crm_complaints', 'complaints_id', 'title');
INSERT INTO `crm_module` VALUES ('24', '常见问题', 'faq', 'crm_faq', 'faq_id', 'question');
INSERT INTO `crm_module` VALUES ('25', '常见问题分类', 'faqcategory', 'crm_faqcategory', 'ffaqcategory_id', 'faqcategoryname');
INSERT INTO `crm_module` VALUES ('26', '入库单', 'warehousesorder', 'crm_warehousesorder', 'warehousesorder_id', 'warehousesorder_num');
INSERT INTO `crm_module` VALUES ('27', '出库单', 'deliverys', 'crm_deliverys', 'deliverys_id', 'deliverys_num');
INSERT INTO `crm_module` VALUES ('28', '盘点', 'checks', 'crm_checks', 'checks_id', 'title');
INSERT INTO `crm_module` VALUES ('29', '应收款', 'gathers', 'crm_gathers', 'gathers_id', 'gathers_num');
INSERT INTO `crm_module` VALUES ('30', '应付款', 'charges', 'crm_charges', 'charges_id', 'charges_num');
INSERT INTO `crm_module` VALUES ('31', '付款单', 'chargesrecords', 'crm_chargesrecords', 'chargesrecords_id', 'chargesrecords_num');
INSERT INTO `crm_module` VALUES ('32', '进项发票', 'pobills', 'crm_pobills', 'pobills_id', 'pobills_num');
INSERT INTO `crm_module` VALUES ('33', '费用报销', 'expenses', 'crm_expenses', 'expenses_id', 'expenses_num');
INSERT INTO `crm_module` VALUES ('34', '发票管理', 'billings', 'crm_billings', 'billings_id', 'billings_num');

-- ----------------------------
-- Table structure for `crm_module_column`
-- ----------------------------
DROP TABLE IF EXISTS `crm_module_column`;
CREATE TABLE `crm_module_column` (
  `column_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `columnname` varchar(255) DEFAULT NULL COMMENT '字段名称-英文',
  `columnlabel` varchar(255) DEFAULT NULL COMMENT '字段标签，描述',
  `columntype` varchar(255) DEFAULT NULL COMMENT '字段类型,basic:基本字段',
  `columndatatype` varchar(255) DEFAULT NULL COMMENT '字段的数据类型',
  `display` varchar(4) NOT NULL DEFAULT '1' COMMENT '是否显示',
  `ismust` tinyint(4) DEFAULT NULL COMMENT '是否是必须字段：1-是，2-否',
  `modulename` varchar(255) DEFAULT NULL COMMENT '模块英文名',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `isselected` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否是下拉选择框',
  PRIMARY KEY (`column_id`)
) ENGINE=InnoDB AUTO_INCREMENT=299 DEFAULT CHARSET=utf8 COMMENT='模块的字段';

-- ----------------------------
-- Records of crm_module_column
-- ----------------------------
INSERT INTO `crm_module_column` VALUES ('22', 'announcements_id', '主键', 'primarykey', 'int(11)', '1', '0', 'announcements', '0', '0');
INSERT INTO `crm_module_column` VALUES ('23', 'title', '主题', 'basic', 'varchar(255)', '1', '0', 'announcements', '0', '0');
INSERT INTO `crm_module_column` VALUES ('24', 'announcements_type', '公告类型', 'basic', 'varchar(255)', '1', '0', 'announcements', '0', '1');
INSERT INTO `crm_module_column` VALUES ('25', 'start_time', '开始时间', 'basic', 'datetime', '1', '0', 'announcements', '0', '0');
INSERT INTO `crm_module_column` VALUES ('26', 'end_time', '结束时间', 'basic', 'datetime', '1', '0', 'announcements', '0', '0');
INSERT INTO `crm_module_column` VALUES ('27', 'content', '内容', 'basic', 'text', '1', '0', 'announcements', '0', '0');
INSERT INTO `crm_module_column` VALUES ('28', 'deleted', '删除标识：0未删除，1删除', 'deleted', 'tinyint(4)', '1', '0', 'announcements', '0', '0');
INSERT INTO `crm_module_column` VALUES ('29', 'campaigns_id', '主键', 'primarykey', 'int(11)', '1', '0', 'campaigns', '0', '0');
INSERT INTO `crm_module_column` VALUES ('30', 'campaigns_name', '营销活动名称', 'basic', 'varchar(255)', '1', '0', 'campaigns', '0', '0');
INSERT INTO `crm_module_column` VALUES ('31', 'status', '状态', 'basic', 'varchar(255)', '1', '0', 'campaigns', '0', '0');
INSERT INTO `crm_module_column` VALUES ('32', 'product_id', '产品', 'relation', 'int(11)', '1', '0', 'campaigns', '0', '0');
INSERT INTO `crm_module_column` VALUES ('33', 'campaigns_type', '营销活动类型', 'basic', 'varchar(255)', '1', '0', 'campaigns', '0', '0');
INSERT INTO `crm_module_column` VALUES ('34', 'end_date', '结束时间', 'basic', 'datetime', '1', '0', 'campaigns', '0', '0');
INSERT INTO `crm_module_column` VALUES ('35', 'targetaudience', '目标客户', 'basic', 'varchar(255)', '1', '0', 'campaigns', '0', '0');
INSERT INTO `crm_module_column` VALUES ('36', 'targetsize', '目标大小', 'basic', 'int(11)', '1', '0', 'campaigns', '0', '0');
INSERT INTO `crm_module_column` VALUES ('37', 'description', '描述', 'basic', 'text', '1', '0', 'campaigns', '0', '0');
INSERT INTO `crm_module_column` VALUES ('38', 'deleted', '删除标识：0未删除，1删除', 'deleted', 'tinyint(4)', '1', '0', 'campaigns', '0', '0');
INSERT INTO `crm_module_column` VALUES ('39', 'budget', '预算', 'basic', 'int(11)', '1', '0', 'campaigns', '0', '0');
INSERT INTO `crm_module_column` VALUES ('40', 'product_id', '主键', 'primarykey', 'int(11)', '1', '0', 'product', '0', '0');
INSERT INTO `crm_module_column` VALUES ('41', 'productcategory_id', '产品分类', 'relation', 'int(11)', '1', '0', 'product', '0', '0');
INSERT INTO `crm_module_column` VALUES ('42', 'unit', '单位', 'basic', 'varchar(255)', '1', '0', 'product', '0', '0');
INSERT INTO `crm_module_column` VALUES ('43', 'model', '型号', 'basic', 'varchar(255)', '1', '0', 'product', '0', '0');
INSERT INTO `crm_module_column` VALUES ('44', 'weight', '重量', 'basic', 'decimal(10,0)', '1', '0', 'product', '0', '0');
INSERT INTO `crm_module_column` VALUES ('45', 'unitprice', '单价', 'basic', 'decimal(10,0)', '1', '0', 'product', '0', '0');
INSERT INTO `crm_module_column` VALUES ('46', 'costprice', '成本', 'basic', 'decimal(10,0)', '1', '0', 'product', '0', '0');
INSERT INTO `crm_module_column` VALUES ('47', 'saleprice', '售价', 'basic', 'decimal(10,0)', '1', '0', 'product', '0', '0');
INSERT INTO `crm_module_column` VALUES ('48', 'description', '描述', 'basic', 'text', '1', '0', 'product', '0', '0');
INSERT INTO `crm_module_column` VALUES ('49', 'createtime', '创建时间', 'timestamp', 'timestamp', '1', '0', 'product', '0', '0');
INSERT INTO `crm_module_column` VALUES ('50', 'productname', '产品名称', 'basic', 'varchar(255)', '1', '0', 'product', '0', '0');
INSERT INTO `crm_module_column` VALUES ('51', 'deleted', '删除标识，0-未删除，1-删除', 'deleted', 'tinyint(4)', '1', '0', 'product', '0', '0');
INSERT INTO `crm_module_column` VALUES ('52', 'productcategory_id', '主键', 'primarykey', 'int(11)', '1', '0', 'productcategory', '0', '0');
INSERT INTO `crm_module_column` VALUES ('53', 'productcategoryname', '分类名称', 'basic', 'varchar(255)', '1', '0', 'productcategory', '0', '0');
INSERT INTO `crm_module_column` VALUES ('54', 'deleted', '删除标识，0-未删除，1-删除', 'deleted', 'tinyint(4)', '1', '0', 'productcategory', '0', '0');
INSERT INTO `crm_module_column` VALUES ('55', 'warrantydays', '保修天数', 'basic', 'varchar(255)', '1', '0', 'product', '0', '0');
INSERT INTO `crm_module_column` VALUES ('56', 'website', '网址', 'basic', 'varchar(255)', '1', '0', 'product', '0', '0');
INSERT INTO `crm_module_column` VALUES ('57', 'contacter_id', '主键', 'primarykey', 'int(10) unsigned zerofill', '1', '0', 'contacter', '0', '0');
INSERT INTO `crm_module_column` VALUES ('58', 'contacter_name', '联系人名称', 'basic', 'varchar(255)', '1', '0', 'contacter', '0', '0');
INSERT INTO `crm_module_column` VALUES ('59', 'position', '级别', 'basic', 'varchar(255)', '1', '0', 'contacter', '0', '0');
INSERT INTO `crm_module_column` VALUES ('60', 'email', '邮箱', 'basic', 'varchar(255)', '1', '0', 'contacter', '0', '0');
INSERT INTO `crm_module_column` VALUES ('61', 'phone', '手机', 'basic', 'varchar(255)', '1', '0', 'contacter', '0', '0');
INSERT INTO `crm_module_column` VALUES ('62', 'tel', '电话', 'basic', 'varchar(255)', '1', '0', 'contacter', '0', '0');
INSERT INTO `crm_module_column` VALUES ('63', 'deleted', '删除标识，0-未删除，1-删除', 'deleted', 'tinyint(4)', '1', '0', 'contacter', '0', '0');
INSERT INTO `crm_module_column` VALUES ('64', 'createtime', '创建时间', 'timestamp', 'timestamp', '1', '0', 'contacter', '0', '0');
INSERT INTO `crm_module_column` VALUES ('65', 'address', '称呼', 'basic', 'varchar(255)', '1', '0', 'contacter', '0', '0');
INSERT INTO `crm_module_column` VALUES ('66', 'description', '描述信息', 'basic', 'text', '1', '0', 'contacter', '0', '0');
INSERT INTO `crm_module_column` VALUES ('67', 'customer_id', '主键', 'primarykey', 'int(11)', '1', '0', 'customer', '0', '0');
INSERT INTO `crm_module_column` VALUES ('68', 'customer_name', '客户名称', 'basic', 'varchar(255)', '1', '0', 'customer', '0', '0');
INSERT INTO `crm_module_column` VALUES ('69', 'phone', '手机', 'basic', 'varchar(255)', '1', '0', 'customer', '0', '0');
INSERT INTO `crm_module_column` VALUES ('70', 'status', '状态', 'basic', 'varchar(255)', '1', '0', 'customer', '0', '0');
INSERT INTO `crm_module_column` VALUES ('71', 'deleted', '删除标识：0未删除，1删除', 'deleted', 'tinyint(4)', '1', '0', 'customer', '0', '0');
INSERT INTO `crm_module_column` VALUES ('72', 'createtime', '创建时间', 'timestamp', 'timestamp', '1', '0', 'customer', '0', '0');
INSERT INTO `crm_module_column` VALUES ('73', 'website', '网站', 'basic', 'varchar(255)', '1', '0', 'customer', '0', '0');
INSERT INTO `crm_module_column` VALUES ('74', 'numberofemployees', '员工人数', 'basic', 'int', '1', '0', 'customer', '0', '0');
INSERT INTO `crm_module_column` VALUES ('75', 'natureofbusiness', '公司性质', 'basic', 'varchar(255)', '1', '0', 'customer', '0', '0');
INSERT INTO `crm_module_column` VALUES ('76', 'customertype', '客户类型', 'basic', 'varchar(255)', '1', '0', 'customer', '0', '0');
INSERT INTO `crm_module_column` VALUES ('77', 'tel', '电话', 'basic', 'varchar(255)', '1', '0', 'customer', '0', '0');
INSERT INTO `crm_module_column` VALUES ('78', 'firstcontacter', '首要联系人', 'basic', 'varchar(100)', '1', '0', 'customer', '0', '0');
INSERT INTO `crm_module_column` VALUES ('79', 'othertel', '其他电话', 'basic', 'varchar(100)', '1', '0', 'customer', '0', '0');
INSERT INTO `crm_module_column` VALUES ('80', 'fax', '传真', 'basic', 'varchar(100)', '1', '0', 'customer', '0', '0');
INSERT INTO `crm_module_column` VALUES ('81', 'email', 'Email', 'basic', 'varchar(100)', '1', '0', 'customer', '0', '0');
INSERT INTO `crm_module_column` VALUES ('82', 'address', '地址', 'basic', 'text', '1', '0', 'customer', '0', '0');
INSERT INTO `crm_module_column` VALUES ('83', 'paymethod', '支付方式', 'basic', 'varchar(100)', '1', '0', 'customer', '0', '0');
INSERT INTO `crm_module_column` VALUES ('84', 'memo', '备注', 'basic', 'text', '1', '0', 'customer', '0', '0');
INSERT INTO `crm_module_column` VALUES ('85', 'fax', '传真', 'basic', 'varchar(100)', '1', '0', 'contacter', '0', '0');
INSERT INTO `crm_module_column` VALUES ('86', 'department', '部门', 'basic', 'varchar(100)', '1', '0', 'contacter', '0', '0');
INSERT INTO `crm_module_column` VALUES ('87', 'contactlog_id', '主键', 'primarykey', 'int(11)', '1', '0', 'contactlog', '0', '0');
INSERT INTO `crm_module_column` VALUES ('88', 'title', '联系记录', 'basic', 'varchar(255)', '1', '0', 'contactlog', '0', '0');
INSERT INTO `crm_module_column` VALUES ('89', 'customer_id', '客户', 'relation', 'int(11)', '1', '0', 'contactlog', '0', '0');
INSERT INTO `crm_module_column` VALUES ('90', 'contact_type', '联系类型', 'basic', 'varchar(255)', '1', '0', 'contactlog', '0', '0');
INSERT INTO `crm_module_column` VALUES ('91', 'contact_time', '联系时间', 'basic', 'datetime', '1', '0', 'contactlog', '0', '0');
INSERT INTO `crm_module_column` VALUES ('92', 'status', '处理状态', 'basic', 'varchar(255)', '1', '0', 'contactlog', '0', '0');
INSERT INTO `crm_module_column` VALUES ('93', 'deleted', '删除标识，0-未删除，1-删除', 'deleted', 'tinyint(4)', '1', '0', 'contactlog', '0', '0');
INSERT INTO `crm_module_column` VALUES ('94', 'salesstage', '销售阶段', 'basic', 'varchar(100)', '1', '0', 'contactlog', '0', '0');
INSERT INTO `crm_module_column` VALUES ('95', 'customerstatus', '客户状态', 'basic', 'varchar(100)', '1', '0', 'contactlog', '0', '0');
INSERT INTO `crm_module_column` VALUES ('96', 'revisitdays', '下次回访时间', 'basic', 'datetime', '1', '0', 'contactlog', '0', '0');
INSERT INTO `crm_module_column` VALUES ('97', 'potentials_id', '销售机会', 'relation', 'int', '1', '0', 'contactlog', '0', '0');
INSERT INTO `crm_module_column` VALUES ('98', 'content', '内容', 'basic', 'text', '1', '0', 'contactlog', '0', '0');
INSERT INTO `crm_module_column` VALUES ('109', 'memdays_id', '主键', 'primarykey', 'int(11)', '1', '0', 'memdays', '0', '0');
INSERT INTO `crm_module_column` VALUES ('110', 'title', '标题', 'basic', 'varchar(255)', '1', '0', 'memdays', '0', '0');
INSERT INTO `crm_module_column` VALUES ('111', 'customer', '客户', 'basic', 'varchar(255)', '1', '0', 'memdays', '0', '0');
INSERT INTO `crm_module_column` VALUES ('112', 'memdays_type', '纪念日类型', 'basic', 'varchar(255)', '1', '0', 'memdays', '0', '0');
INSERT INTO `crm_module_column` VALUES ('113', 'memdays_day', '纪念日', 'basic', 'varchar(255)', '1', '0', 'memdays', '0', '0');
INSERT INTO `crm_module_column` VALUES ('114', 'calendar_type', '阳历/阴历', 'basic', 'varchar(255)', '1', '0', 'memdays', '0', '0');
INSERT INTO `crm_module_column` VALUES ('115', 'deleted', '删除标识，0-未删除，1-删除', 'deleted', 'tinyint(4)', '1', '0', 'memdays', '0', '0');
INSERT INTO `crm_module_column` VALUES ('116', 'memo', '备注', 'basic', 'text', '1', '0', 'memdays', '0', '0');
INSERT INTO `crm_module_column` VALUES ('117', 'potentials_id', '主键', 'primarykey', 'int(11)', '1', '0', 'potentials', '0', '0');
INSERT INTO `crm_module_column` VALUES ('118', 'potentials_name', '销售机会名称', 'basic', 'varchar(255)', '1', '0', 'potentials', '0', '0');
INSERT INTO `crm_module_column` VALUES ('119', 'customer_id', '客户ID', 'basic', 'int(11)', '1', '0', 'potentials', '0', '0');
INSERT INTO `crm_module_column` VALUES ('120', 'stage', '销售阶段', 'basic', 'varchar(255)', '1', '0', 'potentials', '0', '0');
INSERT INTO `crm_module_column` VALUES ('121', 'possibility', '成交的可能性', 'basic', 'tinyint(255)', '1', '0', 'potentials', '0', '0');
INSERT INTO `crm_module_column` VALUES ('122', 'duedate', '成交日期', 'basic', 'date', '1', '0', 'potentials', '0', '0');
INSERT INTO `crm_module_column` VALUES ('123', 'nextstep', '下一阶段', 'basic', 'varchar(255)', '1', '0', 'potentials', '0', '0');
INSERT INTO `crm_module_column` VALUES ('124', 'updatetime', '更新时间', 'timestamp', 'timestamp', '1', '0', 'potentials', '0', '0');
INSERT INTO `crm_module_column` VALUES ('125', 'deleted', '删除标识符：0未删除，1删除', 'deleted', 'tinyint(4)', '1', '0', 'potentials', '0', '0');
INSERT INTO `crm_module_column` VALUES ('126', 'description', '描述', 'basic', 'text', '1', '0', 'potentials', '0', '0');
INSERT INTO `crm_module_column` VALUES ('127', 'quotes_id', '主键', 'primarykey', 'int(11)', '1', '0', 'quotes', '0', '0');
INSERT INTO `crm_module_column` VALUES ('128', 'quotes_number', '报价单号', 'basic', 'varchar(255)', '1', '0', 'quotes', '0', '0');
INSERT INTO `crm_module_column` VALUES ('129', 'stage', '报价阶段', 'basic', 'varchar(255)', '1', '0', 'quotes', '0', '0');
INSERT INTO `crm_module_column` VALUES ('130', 'potentials_id', '销售机会', 'relation', 'int(255)', '1', '0', 'quotes', '0', '0');
INSERT INTO `crm_module_column` VALUES ('131', 'total', '总计', 'basic', 'varchar(255)', '1', '0', 'quotes', '0', '0');
INSERT INTO `crm_module_column` VALUES ('132', 'quotetime', '报价日期', 'basic', 'datetime', '1', '0', 'quotes', '0', '0');
INSERT INTO `crm_module_column` VALUES ('133', 'updatetime', '修改时间', 'timestamp', 'timestamp', '1', '0', 'quotes', '0', '0');
INSERT INTO `crm_module_column` VALUES ('134', 'deleted', '删除标识，1是删除，0未删除', 'deleted', 'tinyint(4)', '1', '0', 'quotes', '0', '0');
INSERT INTO `crm_module_column` VALUES ('135', 'description', '描述', 'basic', 'text', '1', '0', 'quotes', '0', '0');
INSERT INTO `crm_module_column` VALUES ('136', 'competitor_id', '竞争对手ID', 'primarykey', 'int(11)', '1', '0', 'competitor', '0', '0');
INSERT INTO `crm_module_column` VALUES ('137', 'competitor_name', '竞争对手', 'basic', 'varchar(255)', '1', '0', 'competitor', '0', '0');
INSERT INTO `crm_module_column` VALUES ('138', 'advantage', '优势', 'basic', 'timestamp', '1', '0', 'competitor', '0', '0');
INSERT INTO `crm_module_column` VALUES ('139', 'power', '竞争力', 'basic', 'varchar(255)', '1', '0', 'competitor', '0', '0');
INSERT INTO `crm_module_column` VALUES ('140', 'updatetime', '更新时间', 'timestamp', 'timestamp', '1', '0', 'competitor', '0', '0');
INSERT INTO `crm_module_column` VALUES ('141', 'deleted', '删除标识符号,0未删除，1删除', 'deleted', 'tinyint(4)', '1', '0', 'competitor', '0', '0');
INSERT INTO `crm_module_column` VALUES ('142', 'disadvantaged', '劣势', 'basic', 'text', '1', '0', 'competitor', '0', '0');
INSERT INTO `crm_module_column` VALUES ('143', 'strategy', '策略', 'basic', 'text', '1', '0', 'competitor', '0', '0');
INSERT INTO `crm_module_column` VALUES ('144', 'description', '备注', 'basic', 'text', '1', '0', 'competitor', '0', '0');
INSERT INTO `crm_module_column` VALUES ('145', 'salesorder_id', '主键', 'primarykey', 'int(11)', '1', '0', 'salesorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('146', 'salesorder_number', '销售订单号', 'basic', 'varchar(255)', '1', '0', 'salesorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('147', 'stage', '订单状态', 'basic', 'varchar(255)', '1', '0', 'salesorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('148', 'customer_id', '客户', 'relation', 'int(11)', '1', '0', 'salesorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('149', 'contacter_id', '联系人', 'relation', 'int(11)', '1', '0', 'salesorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('150', 'signdate', '签约日期', 'basic', 'datetime', '1', '0', 'salesorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('151', 'total', '总计', 'statistics', 'decimal(10,0)', '0', '0', 'salesorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('152', 'gather_status', '收款状态', 'basic', 'varchar(255)', '1', '0', 'salesorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('153', 'deleted', '删除标识：0未删除，1删除', 'deleted', 'tinyint(4)', '1', '0', 'salesorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('154', 'description', '描述信息', 'basic', 'text', '1', '0', 'salesorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('155', 'invoice_id', '主键', 'primarykey', 'int(11)', '1', '0', 'invoice', '0', '0');
INSERT INTO `crm_module_column` VALUES ('156', 'invoice_num', '发货单号', 'basic', 'varchar(255)', '1', '0', 'invoice', '0', '0');
INSERT INTO `crm_module_column` VALUES ('157', 'customer_id', '客户', 'relation', 'int(11)', '1', '0', 'invoice', '0', '0');
INSERT INTO `crm_module_column` VALUES ('158', 'salesorder_id', '销售单', 'relation', 'int(255)', '1', '0', 'invoice', '0', '0');
INSERT INTO `crm_module_column` VALUES ('159', 'delivery_time', '发货日期', 'basic', 'datetime', '1', '0', 'invoice', '0', '0');
INSERT INTO `crm_module_column` VALUES ('160', 'updatetime', '创建时间', 'timestamp', 'timestamp', '1', '0', 'invoice', '0', '0');
INSERT INTO `crm_module_column` VALUES ('161', 'deleted', '删除标识：0，未删除，1删除', 'deleted', 'tinyint(4)', '1', '0', 'invoice', '0', '0');
INSERT INTO `crm_module_column` VALUES ('162', 'description', '描述', 'basic', 'text', '1', '0', 'invoice', '0', '0');
INSERT INTO `crm_module_column` VALUES ('163', 'tuihuo_id', '主键', 'primarykey', 'int(11)', '1', '0', 'tuihuo', '0', '0');
INSERT INTO `crm_module_column` VALUES ('164', 'tuihuo_num', '退货单', 'basic', 'varchar(255)', '1', '0', 'tuihuo', '0', '0');
INSERT INTO `crm_module_column` VALUES ('165', 'customer_id', '客户', 'relation', 'int(11)', '1', '0', 'tuihuo', '0', '0');
INSERT INTO `crm_module_column` VALUES ('166', 'tuihuo_date', '退货日期', 'basic', 'date', '1', '0', 'tuihuo', '0', '0');
INSERT INTO `crm_module_column` VALUES ('167', 'total', '总计', 'basic', 'decimal(10,0)', '1', '0', 'tuihuo', '0', '0');
INSERT INTO `crm_module_column` VALUES ('168', 'approve_status', '审核状态', 'basic', 'varchar(255)', '1', '0', 'tuihuo', '0', '0');
INSERT INTO `crm_module_column` VALUES ('169', 'deleted', '删除标识：0未删除，1删除', 'deleted', 'tinyint(4)', '1', '0', 'tuihuo', '0', '0');
INSERT INTO `crm_module_column` VALUES ('170', 'description', '描述', 'basic', 'text', '1', '0', 'tuihuo', '0', '0');
INSERT INTO `crm_module_column` VALUES ('171', 'caigous_id', '采购单主键', 'primarykey', 'int(11)', '1', '0', 'caigous', '0', '0');
INSERT INTO `crm_module_column` VALUES ('172', 'caigous_num', '采购单号', 'basic', 'varchar(255)', '1', '0', 'caigous', '0', '0');
INSERT INTO `crm_module_column` VALUES ('173', 'vendor_id', '供应商', 'relation', 'int(11)', '1', '0', 'caigous', '0', '0');
INSERT INTO `crm_module_column` VALUES ('174', 'salesorder_id', '订单', 'relation', 'int(11)', '1', '0', 'caigous', '0', '0');
INSERT INTO `crm_module_column` VALUES ('175', 'caigou_date', '采购日期', 'basic', 'datetime', '1', '0', 'caigous', '0', '0');
INSERT INTO `crm_module_column` VALUES ('176', 'status', '采购状态', 'basic', 'varchar(255)', '1', '0', 'caigous', '0', '0');
INSERT INTO `crm_module_column` VALUES ('177', 'deleted', '删除标识位：0未删除，1删除', 'deleted', 'tinyint(4)', '1', '0', 'caigous', '0', '0');
INSERT INTO `crm_module_column` VALUES ('178', 'purchaseorder_id', '主键', 'primarykey', 'int(11)', '1', '0', 'purchasedorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('179', 'purchaseorder_num', '进货单号', 'basic', 'varchar(255)', '1', '0', 'purchasedorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('180', 'vendors_id', '供应商', 'basic', 'int(11)', '1', '0', 'purchasedorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('181', 'status', '状态', 'basic', 'varchar(255)', '1', '0', 'purchasedorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('182', 'purchase_date', '采购日期', 'basic', 'datetime', '1', '0', 'purchasedorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('183', 'total', '总计', 'basic', 'decimal(10,0)', '1', '0', 'purchasedorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('184', 'paystatus', '付款状态', 'basic', 'varchar(255)', '1', '0', 'purchasedorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('185', 'createtime', '创建时间', 'timestamp', 'timestamp', '1', '0', 'purchasedorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('186', 'deleted', '删除标识：0未删除，1删除', 'deleted', 'tinyint(4)', '1', '0', 'purchasedorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('187', 'vendors_id', '主键', 'primarykey', 'int(11)', '1', '0', 'vendors', '0', '0');
INSERT INTO `crm_module_column` VALUES ('188', 'industry', '行业', 'basic', 'varchar(255)', '1', '0', 'vendors', '0', '0');
INSERT INTO `crm_module_column` VALUES ('189', 'phone', '手机', 'basic', 'varchar(255)', '1', '0', 'vendors', '0', '0');
INSERT INTO `crm_module_column` VALUES ('190', 'website', '网站', 'basic', 'varchar(255)', '1', '0', 'vendors', '0', '0');
INSERT INTO `crm_module_column` VALUES ('191', 'city', '城市', 'basic', 'varchar(255)', '1', '0', 'vendors', '0', '0');
INSERT INTO `crm_module_column` VALUES ('192', 'createtime', '创建时间', 'timestamp', 'timestamp', '1', '0', 'vendors', '0', '0');
INSERT INTO `crm_module_column` VALUES ('193', 'deleted', '删除标识：0未删除，1删除', 'deleted', 'tinyint(4)', '1', '0', 'vendors', '0', '0');
INSERT INTO `crm_module_column` VALUES ('194', 'vendor_name', '供应商名称', 'basic', 'varchar(255)', '1', '0', 'vendors', '0', '0');
INSERT INTO `crm_module_column` VALUES ('195', 'vcontacts_id', '主键', 'primarykey', 'int(11)', '1', '0', 'vcontacts', '0', '0');
INSERT INTO `crm_module_column` VALUES ('196', 'vcontacts_name', '供应商联系人', 'basic', 'varchar(255)', '1', '0', 'vcontacts', '0', '0');
INSERT INTO `crm_module_column` VALUES ('197', 'phone', '电话', 'basic', 'varchar(255)', '1', '0', 'vcontacts', '0', '0');
INSERT INTO `crm_module_column` VALUES ('198', 'deleted', '删除标识：0未删除，1删除', 'deleted', 'tinyint(4)', '1', '0', 'vcontacts', '0', '0');
INSERT INTO `crm_module_column` VALUES ('199', 'createtime', '创建时间', 'timestamp', 'timestamp', '1', '0', 'vcontacts', '0', '0');
INSERT INTO `crm_module_column` VALUES ('200', 'vnotes_id', '主键', 'primarykey', 'int(11)', '1', '0', 'vnotes', '0', '0');
INSERT INTO `crm_module_column` VALUES ('201', 'title', '主题', 'basic', 'varchar(255)', '1', '0', 'vnotes', '0', '0');
INSERT INTO `crm_module_column` VALUES ('202', 'vendors_id', '供应商', 'relation', 'int(11)', '1', '0', 'vnotes', '0', '0');
INSERT INTO `crm_module_column` VALUES ('203', 'contact_time', '联系时间', 'basic', 'datetime', '1', '0', 'vnotes', '0', '0');
INSERT INTO `crm_module_column` VALUES ('204', 'contact_type', '联系类型', 'basic', 'varchar(255)', '1', '0', 'vnotes', '0', '0');
INSERT INTO `crm_module_column` VALUES ('205', 'content', '联系内容', 'basic', 'text', '1', '0', 'vnotes', '0', '0');
INSERT INTO `crm_module_column` VALUES ('206', 'deleted', '删除标识：0未删除，1删除', 'deleted', 'tinyint(4)', '1', '0', 'vnotes', '0', '0');
INSERT INTO `crm_module_column` VALUES ('207', 'caigoutuihuos_id', '主键', 'primarykey', 'int(11)', '1', '0', 'caigoutuihuos', '0', '0');
INSERT INTO `crm_module_column` VALUES ('208', 'title', '主题', 'basic', 'varchar(255)', '1', '0', 'caigoutuihuos', '0', '0');
INSERT INTO `crm_module_column` VALUES ('209', 'status', '状态', 'basic', 'varchar(255)', '1', '0', 'caigoutuihuos', '0', '0');
INSERT INTO `crm_module_column` VALUES ('210', 'vendors_id', '供应商ID', 'basic', 'int(11)', '1', '0', 'caigoutuihuos', '0', '0');
INSERT INTO `crm_module_column` VALUES ('211', 'purchaseorder_id', '进货单ID', 'basic', 'int(11)', '1', '0', 'caigoutuihuos', '0', '0');
INSERT INTO `crm_module_column` VALUES ('212', 'reason', '原因', 'basic', 'varchar(255)', '1', '0', 'caigoutuihuos', '0', '0');
INSERT INTO `crm_module_column` VALUES ('213', 'deleted', '删除标识：0未删除，1删除', 'deleted', 'tinyint(4)', '1', '0', 'caigoutuihuos', '0', '0');
INSERT INTO `crm_module_column` VALUES ('214', 'accountrecordss_id', '主键', 'primarykey', 'int(11)', '1', '0', 'accountrecordss', '0', '0');
INSERT INTO `crm_module_column` VALUES ('215', 'title', '主题', 'basic', 'varchar(255)', '1', '0', 'accountrecordss', '0', '0');
INSERT INTO `crm_module_column` VALUES ('216', 'customer', '客户', 'basic', 'varchar(255)', '1', '0', 'accountrecordss', '0', '0');
INSERT INTO `crm_module_column` VALUES ('217', 'start_date', '开始时间', 'basic', 'date', '1', '0', 'accountrecordss', '0', '0');
INSERT INTO `crm_module_column` VALUES ('218', 'serviceitem', '服务内容', 'basic', 'text', '1', '0', 'accountrecordss', '0', '0');
INSERT INTO `crm_module_column` VALUES ('219', 'feedback', '反馈', 'basic', 'text', '1', '0', 'accountrecordss', '0', '0');
INSERT INTO `crm_module_column` VALUES ('220', 'contacter_id', '联系人', 'relation', 'int(11)', '1', '0', 'accountrecordss', '0', '0');
INSERT INTO `crm_module_column` VALUES ('221', 'create_time', '创建时间', 'basic', 'timestamp', '1', '0', 'accountrecordss', '0', '0');
INSERT INTO `crm_module_column` VALUES ('222', 'deleted', '删除标识：0未删除，1删除', 'deleted', 'tinyint(4)', '1', '0', 'accountrecordss', '0', '0');
INSERT INTO `crm_module_column` VALUES ('223', 'description', '描述信息', 'basic', 'int(11)', '1', '0', 'accountrecordss', '0', '0');
INSERT INTO `crm_module_column` VALUES ('224', 'complaints_id', '主键', 'primarykey', 'int(11) unsigned zerofill', '1', '0', 'complaints', '0', '0');
INSERT INTO `crm_module_column` VALUES ('225', 'title', '投诉主题', 'basic', 'varchar(255)', '1', '0', 'complaints', '0', '0');
INSERT INTO `crm_module_column` VALUES ('226', 'customer_id', '客户', 'relation', 'int(11)', '1', '0', 'complaints', '0', '0');
INSERT INTO `crm_module_column` VALUES ('227', 'complaints_type', '投诉类型', 'basic', 'varchar(255)', '1', '0', 'complaints', '0', '0');
INSERT INTO `crm_module_column` VALUES ('228', 'complaints_date', '投诉时间', 'basic', 'datetime', '1', '0', 'complaints', '0', '0');
INSERT INTO `crm_module_column` VALUES ('229', 'result', '处理结果', 'basic', 'varchar(255)', '1', '0', 'complaints', '0', '0');
INSERT INTO `crm_module_column` VALUES ('230', 'urgencydegree', '紧急程度', 'basic', 'varchar(255)', '1', '0', 'complaints', '0', '0');
INSERT INTO `crm_module_column` VALUES ('231', 'createtime', '创建时间', 'timestamp', 'timestamp', '1', '0', 'complaints', '0', '0');
INSERT INTO `crm_module_column` VALUES ('232', 'faq_id', '主键', 'primarykey', 'int(11)', '1', '0', 'faq', '0', '0');
INSERT INTO `crm_module_column` VALUES ('233', 'product_name', '产品名称', 'basic', 'varchar(255)', '1', '0', 'faq', '0', '0');
INSERT INTO `crm_module_column` VALUES ('234', 'faqcategory_id', '产品分类', 'relation', 'int(255)', '1', '0', 'faq', '0', '0');
INSERT INTO `crm_module_column` VALUES ('235', 'status', '状态', 'basic', 'varchar(255)', '1', '0', 'faq', '0', '0');
INSERT INTO `crm_module_column` VALUES ('236', 'keyword', '关键字', 'basic', 'varchar(255)', '1', '0', 'faq', '0', '0');
INSERT INTO `crm_module_column` VALUES ('237', 'question', '问题', 'basic', 'varchar(255)', '1', '0', 'faq', '0', '0');
INSERT INTO `crm_module_column` VALUES ('238', 'solution', '处理方式', 'basic', 'text', '1', '0', 'faq', '0', '0');
INSERT INTO `crm_module_column` VALUES ('239', 'createtime', '创建时间', 'timestamp', 'timestamp', '1', '0', 'faq', '0', '0');
INSERT INTO `crm_module_column` VALUES ('240', 'faqcategory_id', '主键', 'primarykey', 'int(11)', '1', '0', 'faqcategory', '0', '0');
INSERT INTO `crm_module_column` VALUES ('241', 'faqcategoryname', '分类名称', 'basic', 'varchar(255)', '1', '0', 'faqcategory', '0', '0');
INSERT INTO `crm_module_column` VALUES ('247', 'warehousesorder_id', '主键', 'primarykey', 'int(11)', '1', '0', 'warehousesorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('248', 'warehousesorder_num', '入库单号', 'basic', 'varchar(255)', '1', '0', 'warehousesorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('249', 'purchaseorder_id', '进货单', 'relation', 'int(11)', '1', '0', 'warehousesorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('250', 'createtime', '创建时间', 'timestamp', 'timestamp', '1', '0', 'warehousesorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('251', 'status', '状态', 'basic', 'varchar(255)', '1', '0', 'warehousesorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('252', 'amount', '数量', 'basic', 'int(255)', '1', '0', 'warehousesorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('253', 'deleted', '删除标识：0未删除，1删除', 'deleted', 'tinyint(4)', '1', '0', 'warehousesorder', '0', '0');
INSERT INTO `crm_module_column` VALUES ('254', 'deliverys_id', '主键', 'primarykey', 'int(11)', '1', '0', 'deliverys', '0', '0');
INSERT INTO `crm_module_column` VALUES ('255', 'deliverys_num', '出库单号', 'basic', 'varchar(255)', '1', '0', 'deliverys', '0', '0');
INSERT INTO `crm_module_column` VALUES ('256', 'status', '状态', 'basic', 'varchar(255)', '1', '0', 'deliverys', '0', '0');
INSERT INTO `crm_module_column` VALUES ('257', 'salesorder_id', '合同订单号', 'relation', 'int(11)', '1', '0', 'deliverys', '0', '0');
INSERT INTO `crm_module_column` VALUES ('258', 'invoice_id', '发货单', 'relation', 'int(11)', '1', '0', 'deliverys', '0', '0');
INSERT INTO `crm_module_column` VALUES ('259', 'deliverystime', '出库时间', 'basic', 'datetime', '1', '0', 'deliverys', '0', '0');
INSERT INTO `crm_module_column` VALUES ('260', 'deleted', '删除标识：', 'deleted', 'tinyint(255)', '1', '0', 'deliverys', '0', '0');
INSERT INTO `crm_module_column` VALUES ('261', 'checks_id', '主键', 'primarykey', 'int(11)', '1', '0', 'checks', '0', '0');
INSERT INTO `crm_module_column` VALUES ('262', 'title', '标题', 'basic', 'varchar(255)', '1', '0', 'checks', '0', '0');
INSERT INTO `crm_module_column` VALUES ('263', 'status', '状态', 'basic', 'varchar(255)', '1', '0', 'checks', '0', '0');
INSERT INTO `crm_module_column` VALUES ('264', 'warehouse_id', '仓库', 'basic', 'int(11)', '1', '0', 'checks', '0', '0');
INSERT INTO `crm_module_column` VALUES ('265', 'createtime', '创建时间', 'timestamp', 'timestamp', '1', '0', 'checks', '0', '0');
INSERT INTO `crm_module_column` VALUES ('266', 'gathers_id', '主键', 'primarykey', 'int(11)', '1', '0', 'gathers', '0', '0');
INSERT INTO `crm_module_column` VALUES ('267', 'gathers_num', '应收款编号', 'basic', 'varchar(255)', '1', '0', 'gathers', '0', '0');
INSERT INTO `crm_module_column` VALUES ('268', 'customer_id', '客户', 'relation', 'int(11)', '1', '0', 'gathers', '0', '0');
INSERT INTO `crm_module_column` VALUES ('269', 'saleorder_id', '销售单号', 'relation', 'int(11)', '1', '0', 'gathers', '0', '0');
INSERT INTO `crm_module_column` VALUES ('270', 'invoice_id', '发货单', 'basic', 'int(11)', '1', '0', 'gathers', '0', '0');
INSERT INTO `crm_module_column` VALUES ('271', 'amount', '应收金额', 'basic', 'decimal(10,0)', '1', '0', 'gathers', '0', '0');
INSERT INTO `crm_module_column` VALUES ('272', 'gather_date', '应收日期', 'basic', 'datetime', '1', '0', 'gathers', '0', '0');
INSERT INTO `crm_module_column` VALUES ('273', 'memo', '摘要', 'text', 'text', '1', '0', 'gathers', '0', '0');
INSERT INTO `crm_module_column` VALUES ('274', 'deleted', '删除标识:0未删除，1删除', 'deleted', 'tinyint(255)', '1', '0', 'gathers', '0', '0');
INSERT INTO `crm_module_column` VALUES ('275', 'charges_id', '主键', 'primarykey', 'int(11)', '1', '0', 'charges', '0', '0');
INSERT INTO `crm_module_column` VALUES ('276', 'charges_num', '应付款编号', 'basic', 'varchar(255)', '1', '0', 'charges', '0', '0');
INSERT INTO `crm_module_column` VALUES ('277', 'amount', '总计', 'basic', 'decimal(10,0)', '1', '0', 'charges', '0', '0');
INSERT INTO `crm_module_column` VALUES ('278', 'changes_date', '付款日期', 'basic', 'datetime', '1', '0', 'charges', '0', '0');
INSERT INTO `crm_module_column` VALUES ('279', 'summary', '摘要', 'basic', 'varchar(255)', '1', '0', 'charges', '0', '0');
INSERT INTO `crm_module_column` VALUES ('280', 'deleted', '删除标识：0未删除，1删除', 'deleted', 'tinyint(255)', '1', '0', 'charges', '0', '0');
INSERT INTO `crm_module_column` VALUES ('281', 'createtime', '创建时间', 'timestamp', 'timestamp', '1', '0', 'charges', '0', '0');
INSERT INTO `crm_module_column` VALUES ('287', 'chargesrecords_id', '主键', 'primarykey', 'int(11)', '1', '0', 'chargesrecords', '0', '0');
INSERT INTO `crm_module_column` VALUES ('288', 'chargesrecords_num', '付款单号', 'basic', 'varchar(255)', '1', '0', 'chargesrecords', '0', '0');
INSERT INTO `crm_module_column` VALUES ('289', 'createtime', '创建时间', 'timestamp', 'timestamp', '1', '0', 'chargesrecords', '0', '0');
INSERT INTO `crm_module_column` VALUES ('290', 'pobills_id', '主键', 'primarykey', 'int(11)', '1', '0', 'pobills', '0', '0');
INSERT INTO `crm_module_column` VALUES ('291', 'pobills_num', '进项发票单号', 'basic', 'varchar(255)', '1', '0', 'pobills', '0', '0');
INSERT INTO `crm_module_column` VALUES ('292', 'createtime', '创建时间', 'timestamp', 'timestamp', '1', '0', 'pobills', '0', '0');
INSERT INTO `crm_module_column` VALUES ('293', 'expenses_id', '主键', 'primarykey', 'int(11)', '1', '0', 'expenses', '0', '0');
INSERT INTO `crm_module_column` VALUES ('294', 'expenses_num', '费用编号', 'basic', 'varchar(255)', '1', '0', 'expenses', '0', '0');
INSERT INTO `crm_module_column` VALUES ('295', 'createtime', '创建时间', 'timestamp', 'timestamp', '1', '0', 'expenses', '0', '0');
INSERT INTO `crm_module_column` VALUES ('296', 'billings_id', '主键', 'primarykey', 'int(11)', '1', '0', 'billings', '0', '0');
INSERT INTO `crm_module_column` VALUES ('297', 'createtime', '创建时间', 'timestamp', 'timestamp', '1', '0', 'billings', '0', '0');
INSERT INTO `crm_module_column` VALUES ('298', 'billings_num', '发票编号', 'basic', 'varchar(255)', '1', '0', 'billings', '0', '0');

-- ----------------------------
-- Table structure for `crm_module_panel`
-- ----------------------------
DROP TABLE IF EXISTS `crm_module_panel`;
CREATE TABLE `crm_module_panel` (
  `panel_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `panelname` varchar(255) DEFAULT NULL COMMENT '面板的标题',
  `seq` int(255) NOT NULL DEFAULT '100' COMMENT '排序',
  `moduleenname` varchar(255) DEFAULT NULL COMMENT '模块',
  `deleted` tinyint(255) NOT NULL DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  PRIMARY KEY (`panel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COMMENT='字段的对应的PANEL的值';

-- ----------------------------
-- Records of crm_module_panel
-- ----------------------------
INSERT INTO `crm_module_panel` VALUES ('1', '基本信息', '100', 'announcements', '0');
INSERT INTO `crm_module_panel` VALUES ('2', '时间信息', '99', 'announcements', '0');
INSERT INTO `crm_module_panel` VALUES ('4', '营销活动信息', '100', 'campaigns', '0');
INSERT INTO `crm_module_panel` VALUES ('5', '预期&实际', '99', 'campaigns', '0');
INSERT INTO `crm_module_panel` VALUES ('6', '描述信息', '98', 'campaigns', '0');
INSERT INTO `crm_module_panel` VALUES ('7', '产品信息', '100', 'product', '0');
INSERT INTO `crm_module_panel` VALUES ('8', '价格信息', '99', 'product', '0');
INSERT INTO `crm_module_panel` VALUES ('9', '描述信息', '98', 'product', '0');
INSERT INTO `crm_module_panel` VALUES ('10', '联系人信息', '100', 'contacter', '0');
INSERT INTO `crm_module_panel` VALUES ('11', '描述信息', '99', 'contacter', '0');
INSERT INTO `crm_module_panel` VALUES ('12', '分类信息', '100', 'productcategory', '0');
INSERT INTO `crm_module_panel` VALUES ('13', '基本信息', '100', 'customer', '0');
INSERT INTO `crm_module_panel` VALUES ('14', ' 联系信息', '99', 'customer', '0');
INSERT INTO `crm_module_panel` VALUES ('15', ' 地址信息', '98', 'customer', '0');
INSERT INTO `crm_module_panel` VALUES ('16', ' 银行财务信息', '97', 'customer', '0');
INSERT INTO `crm_module_panel` VALUES ('17', ' 描述信息', '96', 'customer', '0');
INSERT INTO `crm_module_panel` VALUES ('18', '联系记录信息', '100', 'contactlog', '0');
INSERT INTO `crm_module_panel` VALUES ('19', '基本信息', '100', 'accountrecordss', '0');
INSERT INTO `crm_module_panel` VALUES ('20', '描述信息', '99', 'accountrecordss', '0');
INSERT INTO `crm_module_panel` VALUES ('21', '基本信息', '100', 'memdays', '0');
INSERT INTO `crm_module_panel` VALUES ('22', '描述信息', '99', 'memdays', '0');
INSERT INTO `crm_module_panel` VALUES ('23', ' 销售机会信息', '100', 'potentials', '0');
INSERT INTO `crm_module_panel` VALUES ('24', ' 描述信息', '99', 'potentials', '0');
INSERT INTO `crm_module_panel` VALUES ('25', '报价单信息', '100', 'quotes', '0');
INSERT INTO `crm_module_panel` VALUES ('26', '描述信息', '99', 'quotes', '0');
INSERT INTO `crm_module_panel` VALUES ('27', '基本信息', '100', 'competitor', '0');
INSERT INTO `crm_module_panel` VALUES ('28', '优势', '99', 'competitor', '0');
INSERT INTO `crm_module_panel` VALUES ('29', '劣势', '98', 'competitor', '0');
INSERT INTO `crm_module_panel` VALUES ('30', ' 应对策略', '97', 'competitor', '0');
INSERT INTO `crm_module_panel` VALUES ('31', '备注', '96', 'competitor', '0');
INSERT INTO `crm_module_panel` VALUES ('32', '合同订单信息', '100', 'salesorder', '0');
INSERT INTO `crm_module_panel` VALUES ('33', '描述信息', '99', 'salesorder', '0');
INSERT INTO `crm_module_panel` VALUES ('34', ' 发货单信息', '100', 'invoice', '0');
INSERT INTO `crm_module_panel` VALUES ('35', '描述信息', '99', 'invoice', '0');
INSERT INTO `crm_module_panel` VALUES ('36', ' 基本信息', '100', 'tuihuo', '0');
INSERT INTO `crm_module_panel` VALUES ('37', '描述信息', '99', 'tuihuo', '0');

-- ----------------------------
-- Table structure for `crm_module_panel_column`
-- ----------------------------
DROP TABLE IF EXISTS `crm_module_panel_column`;
CREATE TABLE `crm_module_panel_column` (
  `panel_column_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `panel_id` int(11) DEFAULT NULL COMMENT '面板ID',
  `columnname` varchar(255) NOT NULL COMMENT '字段名称',
  `seq` int(255) DEFAULT '100' COMMENT '排序',
  `moduleenname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`panel_column_id`)
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=utf8 COMMENT='面板对应的字段表';

-- ----------------------------
-- Records of crm_module_panel_column
-- ----------------------------
INSERT INTO `crm_module_panel_column` VALUES ('26', '1', 'title', '3', 'announcements');
INSERT INTO `crm_module_panel_column` VALUES ('27', '1', 'announcements_type', '2', 'announcements');
INSERT INTO `crm_module_panel_column` VALUES ('28', '1', 'content', '1', 'announcements');
INSERT INTO `crm_module_panel_column` VALUES ('29', '2', 'start_time', '2', 'announcements');
INSERT INTO `crm_module_panel_column` VALUES ('30', '2', 'end_time', '1', 'announcements');
INSERT INTO `crm_module_panel_column` VALUES ('31', '6', 'description', '1', 'campaigns');
INSERT INTO `crm_module_panel_column` VALUES ('32', '5', 'budget', '1', 'campaigns');
INSERT INTO `crm_module_panel_column` VALUES ('33', '4', 'campaigns_name', '7', 'campaigns');
INSERT INTO `crm_module_panel_column` VALUES ('34', '4', 'status', '6', 'campaigns');
INSERT INTO `crm_module_panel_column` VALUES ('35', '4', 'product_id', '5', 'campaigns');
INSERT INTO `crm_module_panel_column` VALUES ('36', '4', 'campaigns_type', '4', 'campaigns');
INSERT INTO `crm_module_panel_column` VALUES ('37', '4', 'end_date', '3', 'campaigns');
INSERT INTO `crm_module_panel_column` VALUES ('38', '4', 'targetaudience', '2', 'campaigns');
INSERT INTO `crm_module_panel_column` VALUES ('39', '4', 'targetsize', '1', 'campaigns');
INSERT INTO `crm_module_panel_column` VALUES ('40', '9', 'description', '1', 'product');
INSERT INTO `crm_module_panel_column` VALUES ('41', '8', 'unitprice', '3', 'product');
INSERT INTO `crm_module_panel_column` VALUES ('42', '8', 'costprice', '2', 'product');
INSERT INTO `crm_module_panel_column` VALUES ('43', '8', 'saleprice', '1', 'product');
INSERT INTO `crm_module_panel_column` VALUES ('54', '7', 'unit', '6', 'product');
INSERT INTO `crm_module_panel_column` VALUES ('55', '7', 'model', '5', 'product');
INSERT INTO `crm_module_panel_column` VALUES ('56', '7', 'weight', '4', 'product');
INSERT INTO `crm_module_panel_column` VALUES ('57', '7', 'productname', '3', 'product');
INSERT INTO `crm_module_panel_column` VALUES ('58', '7', 'warrantydays', '2', 'product');
INSERT INTO `crm_module_panel_column` VALUES ('59', '7', 'website', '1', 'product');
INSERT INTO `crm_module_panel_column` VALUES ('66', '11', 'description', '1', 'contacter');
INSERT INTO `crm_module_panel_column` VALUES ('67', '12', 'productcategoryname', '1', 'productcategory');
INSERT INTO `crm_module_panel_column` VALUES ('78', '13', 'customer_name', '6', 'customer');
INSERT INTO `crm_module_panel_column` VALUES ('79', '13', 'status', '5', 'customer');
INSERT INTO `crm_module_panel_column` VALUES ('80', '13', 'website', '4', 'customer');
INSERT INTO `crm_module_panel_column` VALUES ('81', '13', 'numberofemployees', '3', 'customer');
INSERT INTO `crm_module_panel_column` VALUES ('82', '13', 'natureofbusiness', '2', 'customer');
INSERT INTO `crm_module_panel_column` VALUES ('83', '13', 'customertype', '1', 'customer');
INSERT INTO `crm_module_panel_column` VALUES ('84', '14', 'phone', '6', 'customer');
INSERT INTO `crm_module_panel_column` VALUES ('85', '14', 'tel', '5', 'customer');
INSERT INTO `crm_module_panel_column` VALUES ('86', '14', 'firstcontacter', '4', 'customer');
INSERT INTO `crm_module_panel_column` VALUES ('87', '14', 'othertel', '3', 'customer');
INSERT INTO `crm_module_panel_column` VALUES ('88', '14', 'fax', '2', 'customer');
INSERT INTO `crm_module_panel_column` VALUES ('89', '14', 'email', '1', 'customer');
INSERT INTO `crm_module_panel_column` VALUES ('90', '15', 'address', '1', 'customer');
INSERT INTO `crm_module_panel_column` VALUES ('91', '16', 'paymethod', '1', 'customer');
INSERT INTO `crm_module_panel_column` VALUES ('92', '17', 'memo', '1', 'customer');
INSERT INTO `crm_module_panel_column` VALUES ('93', '10', 'contacter_name', '8', 'contacter');
INSERT INTO `crm_module_panel_column` VALUES ('94', '10', 'position', '7', 'contacter');
INSERT INTO `crm_module_panel_column` VALUES ('95', '10', 'email', '6', 'contacter');
INSERT INTO `crm_module_panel_column` VALUES ('96', '10', 'phone', '5', 'contacter');
INSERT INTO `crm_module_panel_column` VALUES ('97', '10', 'tel', '4', 'contacter');
INSERT INTO `crm_module_panel_column` VALUES ('98', '10', 'address', '3', 'contacter');
INSERT INTO `crm_module_panel_column` VALUES ('99', '10', 'fax', '2', 'contacter');
INSERT INTO `crm_module_panel_column` VALUES ('100', '10', 'department', '1', 'contacter');
INSERT INTO `crm_module_panel_column` VALUES ('111', '19', 'title', '6', 'accountrecordss');
INSERT INTO `crm_module_panel_column` VALUES ('112', '19', 'customer', '5', 'accountrecordss');
INSERT INTO `crm_module_panel_column` VALUES ('113', '19', 'start_date', '4', 'accountrecordss');
INSERT INTO `crm_module_panel_column` VALUES ('114', '19', 'serviceitem', '3', 'accountrecordss');
INSERT INTO `crm_module_panel_column` VALUES ('115', '19', 'feedback', '2', 'accountrecordss');
INSERT INTO `crm_module_panel_column` VALUES ('116', '19', 'contacter', '1', 'accountrecordss');
INSERT INTO `crm_module_panel_column` VALUES ('117', '20', 'description', '1', 'accountrecordss');
INSERT INTO `crm_module_panel_column` VALUES ('118', '21', 'title', '5', 'memdays');
INSERT INTO `crm_module_panel_column` VALUES ('119', '21', 'customer', '4', 'memdays');
INSERT INTO `crm_module_panel_column` VALUES ('120', '21', 'memdays_type', '3', 'memdays');
INSERT INTO `crm_module_panel_column` VALUES ('121', '21', 'memdays_day', '2', 'memdays');
INSERT INTO `crm_module_panel_column` VALUES ('122', '21', 'calendar_type', '1', 'memdays');
INSERT INTO `crm_module_panel_column` VALUES ('123', '22', 'memo', '1', 'memdays');
INSERT INTO `crm_module_panel_column` VALUES ('124', '23', 'potentials_name', '6', 'potentials');
INSERT INTO `crm_module_panel_column` VALUES ('125', '23', 'customer_id', '5', 'potentials');
INSERT INTO `crm_module_panel_column` VALUES ('126', '23', 'stage', '4', 'potentials');
INSERT INTO `crm_module_panel_column` VALUES ('127', '23', 'possibility', '3', 'potentials');
INSERT INTO `crm_module_panel_column` VALUES ('128', '23', 'duedate', '2', 'potentials');
INSERT INTO `crm_module_panel_column` VALUES ('129', '23', 'nextstep', '1', 'potentials');
INSERT INTO `crm_module_panel_column` VALUES ('130', '24', 'description', '1', 'potentials');
INSERT INTO `crm_module_panel_column` VALUES ('131', '25', 'quotes_number', '5', 'quotes');
INSERT INTO `crm_module_panel_column` VALUES ('132', '25', 'stage', '4', 'quotes');
INSERT INTO `crm_module_panel_column` VALUES ('133', '25', 'total', '3', 'quotes');
INSERT INTO `crm_module_panel_column` VALUES ('134', '25', 'quotetime', '2', 'quotes');
INSERT INTO `crm_module_panel_column` VALUES ('135', '25', 'updatetime', '1', 'quotes');
INSERT INTO `crm_module_panel_column` VALUES ('136', '26', 'description', '1', 'quotes');
INSERT INTO `crm_module_panel_column` VALUES ('137', '27', 'competitor_name', '2', 'competitor');
INSERT INTO `crm_module_panel_column` VALUES ('138', '27', 'power', '1', 'competitor');
INSERT INTO `crm_module_panel_column` VALUES ('139', '28', 'advantage', '1', 'competitor');
INSERT INTO `crm_module_panel_column` VALUES ('140', '29', 'disadvantaged', '1', 'competitor');
INSERT INTO `crm_module_panel_column` VALUES ('141', '30', 'strategy', '1', 'competitor');
INSERT INTO `crm_module_panel_column` VALUES ('142', '31', 'description', '1', 'competitor');
INSERT INTO `crm_module_panel_column` VALUES ('143', '32', 'salesorder_number', '7', 'salesorder');
INSERT INTO `crm_module_panel_column` VALUES ('144', '32', 'stage', '6', 'salesorder');
INSERT INTO `crm_module_panel_column` VALUES ('145', '32', 'customer_id', '5', 'salesorder');
INSERT INTO `crm_module_panel_column` VALUES ('146', '32', 'contacter_id', '4', 'salesorder');
INSERT INTO `crm_module_panel_column` VALUES ('147', '32', 'signdate', '3', 'salesorder');
INSERT INTO `crm_module_panel_column` VALUES ('149', '32', 'gather_status', '1', 'salesorder');
INSERT INTO `crm_module_panel_column` VALUES ('153', '34', 'invoice_num', '4', 'invoice');
INSERT INTO `crm_module_panel_column` VALUES ('154', '34', 'customer_id', '3', 'invoice');
INSERT INTO `crm_module_panel_column` VALUES ('155', '34', 'salesorder_id', '2', 'invoice');
INSERT INTO `crm_module_panel_column` VALUES ('156', '34', 'delivery_time', '1', 'invoice');
INSERT INTO `crm_module_panel_column` VALUES ('157', '35', 'description', '1', 'invoice');
INSERT INTO `crm_module_panel_column` VALUES ('158', '36', 'tuihuo_num', '5', 'tuihuo');
INSERT INTO `crm_module_panel_column` VALUES ('159', '36', 'customer_id', '4', 'tuihuo');
INSERT INTO `crm_module_panel_column` VALUES ('160', '36', 'tuihuo_date', '3', 'tuihuo');
INSERT INTO `crm_module_panel_column` VALUES ('161', '36', 'total', '2', 'tuihuo');
INSERT INTO `crm_module_panel_column` VALUES ('162', '36', 'approve_status', '1', 'tuihuo');
INSERT INTO `crm_module_panel_column` VALUES ('164', '37', 'description', '1', 'tuihuo');
INSERT INTO `crm_module_panel_column` VALUES ('175', '18', 'title', '10', 'contactlog');
INSERT INTO `crm_module_panel_column` VALUES ('176', '18', 'customer_id', '9', 'contactlog');
INSERT INTO `crm_module_panel_column` VALUES ('177', '18', 'contact_type', '8', 'contactlog');
INSERT INTO `crm_module_panel_column` VALUES ('178', '18', 'contact_time', '7', 'contactlog');
INSERT INTO `crm_module_panel_column` VALUES ('179', '18', 'status', '6', 'contactlog');
INSERT INTO `crm_module_panel_column` VALUES ('180', '18', 'salesstage', '5', 'contactlog');
INSERT INTO `crm_module_panel_column` VALUES ('181', '18', 'customerstatus', '4', 'contactlog');
INSERT INTO `crm_module_panel_column` VALUES ('182', '18', 'revisitdays', '3', 'contactlog');
INSERT INTO `crm_module_panel_column` VALUES ('183', '18', 'potentials_id', '2', 'contactlog');
INSERT INTO `crm_module_panel_column` VALUES ('184', '18', 'content', '1', 'contactlog');
INSERT INTO `crm_module_panel_column` VALUES ('185', '33', 'description', '1', 'salesorder');

-- ----------------------------
-- Table structure for `crm_module_view`
-- ----------------------------
DROP TABLE IF EXISTS `crm_module_view`;
CREATE TABLE `crm_module_view` (
  `view_id` int(11) NOT NULL AUTO_INCREMENT,
  `modulename` varchar(255) DEFAULT NULL COMMENT '模块英文，用于关联',
  `viewname` varchar(255) DEFAULT NULL COMMENT '视图名称',
  `isdefault` int(11) NOT NULL DEFAULT '0' COMMENT '是否是默认视图',
  `iskeyview` int(11) NOT NULL DEFAULT '0' COMMENT '是否是关键视图',
  `seq` int(11) NOT NULL DEFAULT '0' COMMENT '视图排序',
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `viewcountsql` text,
  `viewquerysql` text,
  `viewcolumnsjson` text COMMENT '视图页面的页头的JSON字符串',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  PRIMARY KEY (`view_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='模块视图表';

-- ----------------------------
-- Records of crm_module_view
-- ----------------------------
INSERT INTO `crm_module_view` VALUES ('1', 'announcements', '默认视图', '1', '0', '100', '2017-11-06 22:56:48', 'select count(announcements_id) from crm_announcements', 'select announcements_id,title,announcements_type,start_time,end_time from crm_announcements', '[\r\n    {\r\n        \"field\": \"title\",\r\n        \"title\": \"主题\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"announcements_type\",\r\n        \"title\": \"公告类型\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"start_time\",\r\n        \"title\": \"开始时间\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"end_time\",\r\n        \"title\": \"结束时间\",\r\n        \"width\": 10\r\n    }\r\n]', '0');
INSERT INTO `crm_module_view` VALUES ('2', 'campaigns', '默认视图', '1', '0', '100', '2017-11-06 12:59:59', 'select count(1) from crm_campaigns where deleted = 0 ', 'select c.*,p.productname from crm_campaigns c left join crm_product p on c.product_id = p.product_id where c.deleted = 0', '[\r\n    {\r\n        \"field\": \"campaigns_name\",\r\n        \"title\": \"活动名称\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"status\",\r\n        \"title\": \"状态\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"productname\",\r\n        \"title\": \"产品\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"campaigns_type\",\r\n        \"title\": \"营销活动类型\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"end_date\",\r\n        \"title\": \"结束日期\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"targetaudience\",\r\n        \"title\": \"目标听众\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"targetsize\",\r\n        \"title\": \"目标大小\",\r\n        \"width\": 10\r\n    }\r\n]', '0');
INSERT INTO `crm_module_view` VALUES ('3', 'product', '默认视图', '1', '0', '100', '2017-11-06 22:34:31', 'select count(1) from crm_product where deleted = 0', 'select f.*,c.productcategoryname from crm_product f LEFT JOIN crm_productcategory c on f.productcategory_id = c.productcategory_id where f.deleted = 0 ', '[\r\n    {\r\n        \"field\": \"productname\",\r\n        \"title\": \"产品\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"productcategoryname\",\r\n        \"title\": \"产品分类\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"weight\",\r\n        \"title\": \"重量\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"unit\",\r\n        \"title\": \"单位\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"model\",\r\n        \"title\": \"型号\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"unitprice\",\r\n        \"title\": \"单价\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"costprice\",\r\n        \"title\": \"成本价\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"saleprice\",\r\n        \"title\": \"销售价\",\r\n        \"width\": 10\r\n    }\r\n]', '0');
INSERT INTO `crm_module_view` VALUES ('4', 'productcategory', '默认视图', '1', '0', '100', '2017-11-06 22:47:32', 'select count(1) from crm_productcategory where deleted = 0', 'select * from crm_productcategory where deleted = 0  ', '[\r\n    {\r\n        \"field\": \"productcategoryname\",\r\n        \"title\": \"产品分类\",\r\n        \"width\": 10\r\n    }\r\n]', '0');
INSERT INTO `crm_module_view` VALUES ('5', 'contacter', '默认视图', '1', '0', '100', '2017-11-07 22:38:48', 'select count(1) from crm_contacter where deleted = 0 ', 'select * from crm_contacter where deleted = 0  ', '[\r\n    {\r\n        \"field\": \"contacter_name\",\r\n        \"title\": \"姓名\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"position\",\r\n        \"title\": \"职位\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"email\",\r\n        \"title\": \"Email\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"tel\",\r\n        \"title\": \"办公电话\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"phone\",\r\n        \"title\": \"手机\",\r\n        \"width\": 10\r\n    }\r\n]', '0');
INSERT INTO `crm_module_view` VALUES ('6', 'customer', '默认视图', '1', '0', '100', '2017-11-08 17:05:21', 'select count(1) from crm_customer where deleted = 0 ', 'select * from crm_customer where deleted = 0 ', '[\r\n    {\r\n        \"field\": \"customer_name\",\r\n        \"title\": \"客户姓名\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"phone\",\r\n        \"title\": \"客户电话\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"status\",\r\n        \"title\": \"状态\",\r\n        \"width\": 10\r\n    }\r\n]\r\n', '0');
INSERT INTO `crm_module_view` VALUES ('7', 'contactlog', '默认视图', '1', '0', '100', '2017-11-13 14:22:12', 'select count(1) from crm_contactlog where deleted = 0 ', 'select *,DATE_FORMAT(contact_time,\'%Y-%m-%d %T\') as contact_time_format,cc.customer_name,cp.potentials_name from crm_contactlog \r\nLEFT JOIN crm_customer cc on crm_contactlog.customer_id = cc.customer_id \r\nLEFT JOIN crm_potentials cp on crm_contactlog.potentials_id = cp.potentials_id\r\nwhere crm_contactlog.deleted = 0 ', '[\r\n    {\r\n        \"field\": \"title\",\r\n        \"title\": \"主题\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"customer_name\",\r\n        \"title\": \"客户\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"contact_type\",\r\n        \"title\": \"联系类型\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"status\",\r\n        \"title\": \"客户状态\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"contact_time_format\",\r\n        \"title\": \"联系时间\",\r\n        \"width\": 10\r\n    }\r\n]', '0');
INSERT INTO `crm_module_view` VALUES ('8', 'accountrecordss', '默认视图', '1', '0', '100', '2017-11-08 22:52:41', 'select count(1) from crm_accountrecordss where deleted = 0', 'select * from crm_accountrecordss  where deleted = 0', '[\r\n    {\r\n        \"field\": \"title\",\r\n        \"title\": \"主题\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"customer\",\r\n        \"title\": \"客户\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"contacter\",\r\n        \"title\": \"联系人\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"start_date\",\r\n        \"title\": \"服务时间\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"serviceitem\",\r\n        \"title\": \"服务项目\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"feedback\",\r\n        \"title\": \"客户反馈\",\r\n        \"width\": 10\r\n    }\r\n]', '0');
INSERT INTO `crm_module_view` VALUES ('9', 'memdays', '默认视图', '1', '0', '100', '2017-11-09 12:28:10', 'select count(1) from crm_memdays where deleted = 0 ', 'select * from crm_memdays where deleted = 0  ', '[\r\n    {\r\n        \"field\": \"title\",\r\n        \"title\": \"主题\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"customer\",\r\n        \"title\": \"客户\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"memdays_type\",\r\n        \"title\": \"纪念日类型\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"calendar_type\",\r\n        \"title\": \"日历类型\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"memdays_day\",\r\n        \"title\": \"纪念日\",\r\n        \"width\": 10\r\n    }\r\n]', '0');
INSERT INTO `crm_module_view` VALUES ('10', 'potentials', '默认视图', '1', '0', '100', '2017-11-10 09:28:45', 'select count(1) from crm_potentials where deleted = 0 ', 'select c.*,u.realname from crm_potentials c left join crm_user\r\n u on c.customer_id = u.user_id where c.deleted = 0 ', '[\r\n    {\r\n        \"field\": \"potentials_name\",\r\n        \"title\": \"销售机会名称\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"realname\",\r\n        \"title\": \"客户\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"stage\",\r\n        \"title\": \"销售阶段\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"possibility\",\r\n        \"title\": \"成交可能性\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"duedate\",\r\n        \"title\": \"成交日期\",\r\n        \"width\": 10\r\n    },{\r\n        \"field\": \"nextstep\",\r\n        \"title\": \"下一阶段\",\r\n        \"width\": 10\r\n    }\r\n]', '0');
INSERT INTO `crm_module_view` VALUES ('11', 'quotes', '默认视图', '1', '0', '100', '2017-11-10 12:10:07', 'select count(1) from crm_quotes where deleted = 0 ', 'select c.*,p.potentials_name from crm_quotes c left join crm_potentials\r\n p on c.potentials_id = p.potentials_id where c.deleted = 0  ', '[\r\n    {\r\n        \"field\": \"quotes_number\",\r\n        \"title\": \"报价单号\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"stage\",\r\n        \"title\": \"报价阶段\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"potentials_name\",\r\n        \"title\": \"销售机会\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"total\",\r\n        \"title\": \"总价\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"quotetime\",\r\n        \"title\": \"报价日期\",\r\n        \"width\": 10\r\n    }\r\n]', '0');
INSERT INTO `crm_module_view` VALUES ('12', 'competitor', '默认视图', '1', '0', '100', '2017-11-10 12:26:52', 'select count(1) from crm_competitor where deleted = 0 ', 'select c.* from crm_competitor c where c.deleted = 0 ', '[\r\n    {\r\n        \"field\": \"competitor_name\",\r\n        \"title\": \"竞争对手\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"advantage\",\r\n        \"title\": \"优势\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"power\",\r\n        \"title\": \"竞争力\",\r\n        \"width\": 10\r\n    }\r\n]', '0');
INSERT INTO `crm_module_view` VALUES ('13', 'salesorder', '默认视图', '1', '0', '100', '2017-11-10 15:07:50', 'select count(1) from crm_salesorder where deleted = 0 ', 'select c.*,u.realname,cc.customer_id,cc.customer_name,c_co.contacter_name from crm_salesorder c left join \r\ncrm_user u on c.customer_id = u.user_id  left JOIN crm_customer cc on c.customer_id = cc.customer_id LEFT JOIN crm_contacter \r\nc_co on c_co.contacter_id = c.contacter_id where c.deleted = 0 ', '[\r\n    {\r\n        \"field\": \"salesorder_number\",\r\n        \"title\": \"销售订单号\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"stage\",\r\n        \"title\": \"订单状态\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"customer_name\",\r\n        \"title\": \"客户\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"contacter_name\",\r\n        \"title\": \"联系人\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"signdate\",\r\n        \"title\": \"签约日期\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"gather_status\",\r\n        \"title\": \"收款状态\",\r\n        \"width\": 10\r\n    }\r\n]', '0');
INSERT INTO `crm_module_view` VALUES ('14', 'invoice', '默认视图', '1', '0', '100', '2017-11-10 15:47:45', 'select count(1) from crm_invoice where deleted = 0 ', 'select c.*,u.realname from crm_invoice c left join crm_user\r\n u on c.customer_id = u.user_id where c.deleted = 0  ', '[\r\n    {\r\n        \"field\": \"invoice_num\",\r\n        \"title\": \"发货单\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"customer\",\r\n        \"title\": \"客户\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"salesorder\",\r\n        \"title\": \"订单\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"delivery_time\",\r\n        \"title\": \"发货日期\",\r\n        \"width\": 10\r\n    }\r\n]', '0');
INSERT INTO `crm_module_view` VALUES ('15', 'tuihuo', '默认视图', '1', '0', '100', '2017-11-11 09:37:07', 'select count(1) from crm_tuihuo where deleted = 0 ', 'select c.*,u.customer_name from crm_tuihuo c left join crm_customer\r\n u on c.customer_id = u.customer_id where c.deleted = 0  ', '[\r\n    {\r\n        \"field\": \"tuihuo_num\",\r\n        \"title\": \"退货单\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"customer_name\",\r\n        \"title\": \"客户\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"tuihuo_date\",\r\n        \"title\": \"退货日期\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"total\",\r\n        \"title\": \"总计\",\r\n        \"width\": 10\r\n    },\r\n    {\r\n        \"field\": \"approve_status\",\r\n        \"title\": \"审核状态\",\r\n        \"width\": 10\r\n    }\r\n]', '0');

-- ----------------------------
-- Table structure for `crm_module_view_column`
-- ----------------------------
DROP TABLE IF EXISTS `crm_module_view_column`;
CREATE TABLE `crm_module_view_column` (
  `viewcolumn_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `view_id` int(11) DEFAULT NULL COMMENT '视图ID',
  `columnname` varchar(255) DEFAULT NULL COMMENT '列名',
  `seq` varchar(255) DEFAULT NULL COMMENT '排序',
  `createtime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `moduleenname` varchar(255) DEFAULT NULL COMMENT '模块名',
  PRIMARY KEY (`viewcolumn_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='模块视图的字段';

-- ----------------------------
-- Records of crm_module_view_column
-- ----------------------------
INSERT INTO `crm_module_view_column` VALUES ('2', '1', 'title', '2', '2017-10-23 16:57:06', 'announcements');

-- ----------------------------
-- Table structure for `crm_monthlylogs`
-- ----------------------------
DROP TABLE IF EXISTS `crm_monthlylogs`;
CREATE TABLE `crm_monthlylogs` (
  `monthlylogs_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `summary` text COMMENT '本月总结',
  `nextmonth_plan` text COMMENT '下月计划',
  `createdate` date NOT NULL COMMENT '创建日期',
  `user_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  PRIMARY KEY (`monthlylogs_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='月报表';

-- ----------------------------
-- Records of crm_monthlylogs
-- ----------------------------
INSERT INTO `crm_monthlylogs` VALUES ('1', '643343', '55', '2017-08-09', '1', '0');
INSERT INTO `crm_monthlylogs` VALUES ('2', '哈哈哈', '嘻嘻嘻', '2017-10-12', '7', '0');
INSERT INTO `crm_monthlylogs` VALUES ('3', 'hao', 'hao', '2017-10-13', '7', '0');
INSERT INTO `crm_monthlylogs` VALUES ('4', '3443', '4343', '2017-11-18', '1', '0');
INSERT INTO `crm_monthlylogs` VALUES ('5', '34', '4343', '2017-11-18', '1', '0');

-- ----------------------------
-- Table structure for `crm_picklist`
-- ----------------------------
DROP TABLE IF EXISTS `crm_picklist`;
CREATE TABLE `crm_picklist` (
  `picklist_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `picklist_key` varchar(255) DEFAULT NULL COMMENT '选择项的KEY',
  `picklist_value` varchar(255) DEFAULT NULL COMMENT '选择项的VALUES',
  PRIMARY KEY (`picklist_id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COMMENT='下拉选择框枚举值';

-- ----------------------------
-- Records of crm_picklist
-- ----------------------------
INSERT INTO `crm_picklist` VALUES ('49', 'stage', '无');
INSERT INTO `crm_picklist` VALUES ('50', 'stage', '初期沟通');
INSERT INTO `crm_picklist` VALUES ('51', 'stage', '立项评估');
INSERT INTO `crm_picklist` VALUES ('52', 'stage', '需求分析');
INSERT INTO `crm_picklist` VALUES ('53', 'stage', '方案指定');
INSERT INTO `crm_picklist` VALUES ('54', 'stage', '招投标/竞争');
INSERT INTO `crm_picklist` VALUES ('55', 'stage', '商务谈判');
INSERT INTO `crm_picklist` VALUES ('56', 'stage', '谈成结束');
INSERT INTO `crm_picklist` VALUES ('57', 'stage', '未成结束');
INSERT INTO `crm_picklist` VALUES ('59', 'gather_status', '已创建');
INSERT INTO `crm_picklist` VALUES ('60', 'contact_type', '邮箱');
INSERT INTO `crm_picklist` VALUES ('61', 'contact_type', '手机');
INSERT INTO `crm_picklist` VALUES ('62', 'contact_type', '座机');
INSERT INTO `crm_picklist` VALUES ('63', 'announcements_type', '重要通知');

-- ----------------------------
-- Table structure for `crm_picklistkey`
-- ----------------------------
DROP TABLE IF EXISTS `crm_picklistkey`;
CREATE TABLE `crm_picklistkey` (
  `picklistkey` varchar(255) DEFAULT NULL COMMENT '字典的KEY',
  `description` varchar(255) DEFAULT NULL COMMENT '字典的描述',
  `picklistkey_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键自动递增',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识，0未删除，1删除',
  PRIMARY KEY (`picklistkey_id`),
  UNIQUE KEY `u_picklistkey_picklistkey` (`picklistkey`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='字典表的键';

-- ----------------------------
-- Records of crm_picklistkey
-- ----------------------------
INSERT INTO `crm_picklistkey` VALUES ('stage', '销售阶段', '5', '0');
INSERT INTO `crm_picklistkey` VALUES ('321313', '12312', '6', '0');
INSERT INTO `crm_picklistkey` VALUES ('发的撒', '方式', '7', '0');
INSERT INTO `crm_picklistkey` VALUES ('对对对', 'ww', '8', '0');
INSERT INTO `crm_picklistkey` VALUES ('gather_status', '收款状态', '9', '0');
INSERT INTO `crm_picklistkey` VALUES ('contact_type', '联系类型', '10', '0');
INSERT INTO `crm_picklistkey` VALUES ('announcements_type', '公告类型', '11', '0');

-- ----------------------------
-- Table structure for `crm_pobills`
-- ----------------------------
DROP TABLE IF EXISTS `crm_pobills`;
CREATE TABLE `crm_pobills` (
  `pobills_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pobills_num` varchar(255) DEFAULT NULL COMMENT '进项发票单号',
  `createtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`pobills_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='进项发票';

-- ----------------------------
-- Records of crm_pobills
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_potentials`
-- ----------------------------
DROP TABLE IF EXISTS `crm_potentials`;
CREATE TABLE `crm_potentials` (
  `potentials_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `potentials_name` varchar(255) DEFAULT NULL COMMENT '销售机会名称',
  `customer_id` int(11) DEFAULT NULL COMMENT '客户ID',
  `stage` varchar(255) DEFAULT NULL COMMENT '销售阶段',
  `possibility` tinyint(255) DEFAULT '0' COMMENT '成交的可能性',
  `duedate` date DEFAULT NULL COMMENT '成交日期',
  `nextstep` varchar(255) DEFAULT NULL COMMENT '下一阶段',
  `updatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识符：0未删除，1删除',
  `description` int(11) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`potentials_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='销售机会';

-- ----------------------------
-- Records of crm_potentials
-- ----------------------------
INSERT INTO `crm_potentials` VALUES ('1', 'test', '3', '谈成结束', '75', '2017-08-10', '3333', '2017-08-10 14:02:42', '1', null);
INSERT INTO `crm_potentials` VALUES ('2', '23777问问', '12', '无', '25', '2017-08-10', '2323', '2017-11-10 11:56:42', '0', '0');
INSERT INTO `crm_potentials` VALUES ('3', 'aaa', '2', '立项评估', '25', '2017-08-19', 'aaa', '2017-09-14 14:02:54', '1', null);
INSERT INTO `crm_potentials` VALUES ('4', 'tcd', '8', '立项评估', '100', '2017-08-29', '001', '0000-00-00 00:00:00', '0', null);
INSERT INTO `crm_potentials` VALUES ('5', '产品', '1', '初期沟通', '100', '2017-10-13', '继续沟通', '0000-00-00 00:00:00', '0', null);

-- ----------------------------
-- Table structure for `crm_product`
-- ----------------------------
DROP TABLE IF EXISTS `crm_product`;
CREATE TABLE `crm_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `productcategory_id` int(11) DEFAULT NULL COMMENT '产品分类ID',
  `unit` varchar(255) DEFAULT NULL COMMENT '单位',
  `model` varchar(255) DEFAULT NULL COMMENT '型号',
  `weight` decimal(10,0) DEFAULT NULL COMMENT '重量',
  `unitprice` decimal(10,0) DEFAULT NULL COMMENT '单价',
  `costprice` decimal(10,0) DEFAULT NULL COMMENT '成本',
  `saleprice` decimal(10,0) DEFAULT NULL COMMENT '售价',
  `description` text COMMENT '描述',
  `createtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `productname` varchar(255) DEFAULT NULL COMMENT '产品名称',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识，0-未删除，1-删除',
  `warrantydays` varchar(255) DEFAULT NULL COMMENT '保修天数',
  `website` int(11) DEFAULT NULL COMMENT '网址',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='产品';

-- ----------------------------
-- Records of crm_product
-- ----------------------------
INSERT INTO `crm_product` VALUES ('1', '11', '克', 'C型', '100', '10', '5', '15', '商品名称描述333', '2017-09-13 11:11:11', '陶瓷环毛坯', '0', null, null);
INSERT INTO `crm_product` VALUES ('2', '3', '23', '23', '23', '2', '2', '22', '222', '2017-07-30 22:00:04', '23233434', '1', null, null);
INSERT INTO `crm_product` VALUES ('3', '1', ' 23', ' 23', '23', '2222', '222', '222', '222', '2017-10-13 09:24:54', '23额', '1', null, null);
INSERT INTO `crm_product` VALUES ('4', '1', '1', '1', '1', '1', '1', '1', '1', '2017-06-12 15:17:17', '1', '1', null, null);
INSERT INTO `crm_product` VALUES ('5', '5', '东东', '东东', '0', '0', '0', '0', '东东', '2017-10-13 09:24:51', '东东', '1', null, null);
INSERT INTO `crm_product` VALUES ('6', '3', 'da', '212312', '11', '11', '22', '22', 'ertertetwert', '2017-10-13 09:24:48', '24342432', '1', null, null);
INSERT INTO `crm_product` VALUES ('7', '3', '2', '2', '2', '2', '2', '2', '2', '2017-10-13 09:24:45', 'aa', '1', null, null);
INSERT INTO `crm_product` VALUES ('8', '12', 't', '货车', '10', '9999999999', '9999999999', '9999999999', '\r\n                                正宗汽车人                            ', '2017-11-07 20:07:30', '擎天柱', '0', '33', '33');
INSERT INTO `crm_product` VALUES ('9', '13', 'kg88', '隐身娃', '30', '9999999999', '9999999999', '9999999999', '\r\n                                \r\n                                \r\n                                野生葫芦娃                                                                                    ', '2017-11-07 17:24:24', '六娃999999', '0', null, null);
INSERT INTO `crm_product` VALUES ('10', '13', 'kg', '4', '30', '-1111111', '-222', '-152', '喷火娃', '0000-00-00 00:00:00', '四娃', '0', null, null);
INSERT INTO `crm_product` VALUES ('11', '13', '7', '7', '7', '7', '7', '7', '777', '0000-00-00 00:00:00', '7', '0', null, null);

-- ----------------------------
-- Table structure for `crm_productcategory`
-- ----------------------------
DROP TABLE IF EXISTS `crm_productcategory`;
CREATE TABLE `crm_productcategory` (
  `productcategory_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `productcategoryname` varchar(255) DEFAULT NULL COMMENT '分类名称',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识，0-未删除，1-删除',
  PRIMARY KEY (`productcategory_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='产品分类表';

-- ----------------------------
-- Records of crm_productcategory
-- ----------------------------
INSERT INTO `crm_productcategory` VALUES ('1', '88889999443', '1');
INSERT INTO `crm_productcategory` VALUES ('2', '212121', '1');
INSERT INTO `crm_productcategory` VALUES ('3', '电视柜', '0');
INSERT INTO `crm_productcategory` VALUES ('4', '数码', '0');
INSERT INTO `crm_productcategory` VALUES ('5', '的', '1');
INSERT INTO `crm_productcategory` VALUES ('6', '的', '1');
INSERT INTO `crm_productcategory` VALUES ('7', 'qr', '1');
INSERT INTO `crm_productcategory` VALUES ('8', '请问v999', '1');
INSERT INTO `crm_productcategory` VALUES ('9', '1231231231', '1');
INSERT INTO `crm_productcategory` VALUES ('10', '000', '1');
INSERT INTO `crm_productcategory` VALUES ('11', '123', '1');
INSERT INTO `crm_productcategory` VALUES ('12', '汽车人', '0');
INSERT INTO `crm_productcategory` VALUES ('13', '葫芦娃', '0');
INSERT INTO `crm_productcategory` VALUES ('14', '奥特曼', '0');
INSERT INTO `crm_productcategory` VALUES ('15', ' ', '1');

-- ----------------------------
-- Table structure for `crm_purchaseorder`
-- ----------------------------
DROP TABLE IF EXISTS `crm_purchaseorder`;
CREATE TABLE `crm_purchaseorder` (
  `purchaseorder_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `purchaseorder_num` varchar(255) DEFAULT NULL COMMENT '进货单号',
  `vendors_id` int(11) DEFAULT NULL COMMENT '供应商',
  `status` varchar(255) DEFAULT NULL COMMENT '状态',
  `purchase_date` datetime DEFAULT NULL COMMENT '采购日期',
  `total` decimal(10,0) DEFAULT NULL COMMENT '总计',
  `paystatus` varchar(255) DEFAULT NULL COMMENT '付款状态',
  `createtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `deleted` tinyint(4) DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  PRIMARY KEY (`purchaseorder_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='进货单';

-- ----------------------------
-- Records of crm_purchaseorder
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_quotes`
-- ----------------------------
DROP TABLE IF EXISTS `crm_quotes`;
CREATE TABLE `crm_quotes` (
  `quotes_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `quotes_number` varchar(255) DEFAULT NULL COMMENT '报价单号',
  `stage` varchar(255) DEFAULT NULL COMMENT '报价阶段',
  `potentials_id` int(255) DEFAULT NULL COMMENT '销售机会关联',
  `total` varchar(255) DEFAULT NULL COMMENT '总计',
  `quotetime` datetime DEFAULT NULL COMMENT '报价日期',
  `updatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识，1是删除，0未删除',
  `description` int(11) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`quotes_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='报价单';

-- ----------------------------
-- Records of crm_quotes
-- ----------------------------
INSERT INTO `crm_quotes` VALUES ('1', '201709190144', '需求分析', '4', '100', '2017-09-19 13:54:56', '2017-11-10 12:11:22', '0', '8989');
INSERT INTO `crm_quotes` VALUES ('2', 'wewe', '需求分析', '4', 'ewwwe', '2017-09-19 19:06:49', '2017-11-10 12:13:10', '1', null);
INSERT INTO `crm_quotes` VALUES ('3', '777', '招投标/竞争', '4', '77', '2017-09-12 20:36:42', '2017-11-10 12:13:10', '0', null);

-- ----------------------------
-- Table structure for `crm_salesorder`
-- ----------------------------
DROP TABLE IF EXISTS `crm_salesorder`;
CREATE TABLE `crm_salesorder` (
  `salesorder_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `salesorder_number` varchar(255) DEFAULT NULL COMMENT '销售订单号',
  `stage` varchar(255) DEFAULT NULL COMMENT '订单状态',
  `customer_id` int(11) DEFAULT NULL COMMENT '客户ID',
  `contacter_id` int(11) DEFAULT NULL COMMENT '联系人ID',
  `signdate` datetime DEFAULT NULL COMMENT '签约日期',
  `total` decimal(10,0) DEFAULT NULL COMMENT '总计',
  `gather_status` varchar(255) DEFAULT NULL COMMENT '收款状态',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  `description` text COMMENT '描述信息',
  `tax` tinyint(4) DEFAULT NULL COMMENT '税收比例',
  `adjustment` decimal(10,0) DEFAULT NULL COMMENT '金额调整',
  `createtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`salesorder_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='订单';

-- ----------------------------
-- Records of crm_salesorder
-- ----------------------------
INSERT INTO `crm_salesorder` VALUES ('2', '6767676767', '需求分析', '5', '6', '2017-09-20 15:10:22', '8', '已创建', '0', '\r\n                                023222222222222', null, null, '2017-11-18 09:57:42');
INSERT INTO `crm_salesorder` VALUES ('3', '7676', '初期沟通', '7', '6', '2017-09-20 15:18:02', '6', '已创建', '0', '90000000000', null, null, '2017-11-18 09:57:42');
INSERT INTO `crm_salesorder` VALUES ('4', '漫画', '招投标/竞争', '37', '1', '2017-10-16 11:06:45', null, '已创建', '0', null, null, null, '2017-11-18 09:57:42');

-- ----------------------------
-- Table structure for `crm_salesorder_items`
-- ----------------------------
DROP TABLE IF EXISTS `crm_salesorder_items`;
CREATE TABLE `crm_salesorder_items` (
  `salesorder_item_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单明细ID',
  `salesorder_id` int(11) DEFAULT NULL COMMENT '订单主键',
  `product_id` int(11) DEFAULT NULL COMMENT '产品ID',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `price` decimal(10,0) DEFAULT NULL COMMENT '价格',
  `memo` varchar(255) DEFAULT NULL COMMENT '备注',
  `amount` decimal(10,0) DEFAULT NULL COMMENT '小计',
  `createtime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '时间戳',
  PRIMARY KEY (`salesorder_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单明细表';

-- ----------------------------
-- Records of crm_salesorder_items
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_system`
-- ----------------------------
DROP TABLE IF EXISTS `crm_system`;
CREATE TABLE `crm_system` (
  `system_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`system_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统配置表';

-- ----------------------------
-- Records of crm_system
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_tuihuo`
-- ----------------------------
DROP TABLE IF EXISTS `crm_tuihuo`;
CREATE TABLE `crm_tuihuo` (
  `tuihuo_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tuihuo_num` varchar(255) DEFAULT NULL COMMENT '退货单',
  `customer_id` int(11) DEFAULT NULL COMMENT '客户',
  `tuihuo_date` date DEFAULT NULL COMMENT '退货日期',
  `total` decimal(10,0) DEFAULT NULL COMMENT '总计',
  `approve_status` varchar(255) DEFAULT NULL COMMENT '审核状态',
  `deleted` tinyint(4) DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  `description` int(11) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`tuihuo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='退货单号';

-- ----------------------------
-- Records of crm_tuihuo
-- ----------------------------
INSERT INTO `crm_tuihuo` VALUES ('1', '323', '6', '0000-00-00', '33', '33', '0', '0');

-- ----------------------------
-- Table structure for `crm_user`
-- ----------------------------
DROP TABLE IF EXISTS `crm_user`;
CREATE TABLE `crm_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(255) DEFAULT NULL COMMENT '登入名称',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `realname` varchar(255) DEFAULT NULL COMMENT '真实姓名',
  `deleted` tinyint(4) DEFAULT '0' COMMENT '删除标识，0是未删除，1是删除',
  `userimage` varchar(255) NOT NULL DEFAULT 'css/images/avator/male.png' COMMENT '用户头像',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `u_user_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of crm_user
-- ----------------------------
INSERT INTO `crm_user` VALUES ('1', 'ranko', '123456', '超级管理员', '0', 'css/images/avator/female.png');
INSERT INTO `crm_user` VALUES ('7', 'salesman', '123456', '销售人员', '0', 'css/images/avator/female.png');
INSERT INTO `crm_user` VALUES ('8', 'servicestaff', '123456', '客服人员', '0', 'css/images/avator/male.png');
INSERT INTO `crm_user` VALUES ('10', 'admin', null, 'admin', '0', 'css/images/avator/male.png');
INSERT INTO `crm_user` VALUES ('11', '1', null, '1', '0', 'css/images/avator/male.png');
INSERT INTO `crm_user` VALUES ('12', '2', null, '2', '0', 'css/images/avator/male.png');

-- ----------------------------
-- Table structure for `crm_vcontacts`
-- ----------------------------
DROP TABLE IF EXISTS `crm_vcontacts`;
CREATE TABLE `crm_vcontacts` (
  `vcontacts_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `vcontacts_name` varchar(255) DEFAULT NULL COMMENT '供应商联系人',
  `phone` varchar(255) DEFAULT NULL COMMENT '电话',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`vcontacts_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='供应商联系人';

-- ----------------------------
-- Records of crm_vcontacts
-- ----------------------------
INSERT INTO `crm_vcontacts` VALUES ('1', '22', '323', '0', '2017-10-17 11:02:20');
INSERT INTO `crm_vcontacts` VALUES ('2', '2323', '33', '1', '2017-10-18 16:25:55');
INSERT INTO `crm_vcontacts` VALUES ('3', '992323', '99', '0', '2017-10-18 16:40:44');

-- ----------------------------
-- Table structure for `crm_vendors`
-- ----------------------------
DROP TABLE IF EXISTS `crm_vendors`;
CREATE TABLE `crm_vendors` (
  `vendors_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `industry` varchar(255) DEFAULT NULL COMMENT '行业',
  `phone` varchar(255) DEFAULT NULL COMMENT '手机',
  `website` varchar(255) DEFAULT NULL COMMENT '网站',
  `city` varchar(255) DEFAULT NULL COMMENT '城市',
  `createtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `deleted` tinyint(4) DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  `vendor_name` varchar(255) DEFAULT NULL COMMENT '供应商名称',
  PRIMARY KEY (`vendors_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='供应商';

-- ----------------------------
-- Records of crm_vendors
-- ----------------------------
INSERT INTO `crm_vendors` VALUES ('1', '77', '77', '77', '7733', '2017-10-17 10:15:48', '1', '7888');
INSERT INTO `crm_vendors` VALUES ('2', '883434', '8', '8', '82323', '2017-10-18 17:30:10', '0', '8');
INSERT INTO `crm_vendors` VALUES ('3', '23', '23', '23', '23', '2017-10-18 17:30:56', '1', '23');

-- ----------------------------
-- Table structure for `crm_vnotes`
-- ----------------------------
DROP TABLE IF EXISTS `crm_vnotes`;
CREATE TABLE `crm_vnotes` (
  `vnotes_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '主题',
  `vendors_id` int(11) DEFAULT NULL COMMENT '供应商ID',
  `contact_time` datetime DEFAULT NULL COMMENT '联系时间',
  `contact_type` varchar(255) DEFAULT NULL COMMENT '联系类型',
  `content` text COMMENT '联系内容',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  PRIMARY KEY (`vnotes_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='供应商联系记录';

-- ----------------------------
-- Records of crm_vnotes
-- ----------------------------
INSERT INTO `crm_vnotes` VALUES ('1', '2323', '1', '2017-10-19 11:20:32', '邮箱', '2323', '0');

-- ----------------------------
-- Table structure for `crm_warehouse`
-- ----------------------------
DROP TABLE IF EXISTS `crm_warehouse`;
CREATE TABLE `crm_warehouse` (
  `warehouse_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `warehouse_name` varchar(255) DEFAULT NULL COMMENT '仓库名称',
  `description` varchar(255) DEFAULT NULL COMMENT '仓库描述',
  `enabled` tinyint(255) NOT NULL DEFAULT '1' COMMENT '是否启用：1启用，0禁用',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  PRIMARY KEY (`warehouse_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='仓库设置';

-- ----------------------------
-- Records of crm_warehouse
-- ----------------------------
INSERT INTO `crm_warehouse` VALUES ('1', '仓库1', '仓库1', '1', '1');
INSERT INTO `crm_warehouse` VALUES ('2', '555', '555', '1', '0');
INSERT INTO `crm_warehouse` VALUES ('3', '5559999', '555', '1', '0');
INSERT INTO `crm_warehouse` VALUES ('4', '88', '888', '1', '1');

-- ----------------------------
-- Table structure for `crm_warehouse_user`
-- ----------------------------
DROP TABLE IF EXISTS `crm_warehouse_user`;
CREATE TABLE `crm_warehouse_user` (
  `warehouse_id` int(11) NOT NULL COMMENT '仓库ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='仓库和用户的关系表';

-- ----------------------------
-- Records of crm_warehouse_user
-- ----------------------------
INSERT INTO `crm_warehouse_user` VALUES ('1', '7');
INSERT INTO `crm_warehouse_user` VALUES ('1', '8');
INSERT INTO `crm_warehouse_user` VALUES ('3', '1');
INSERT INTO `crm_warehouse_user` VALUES ('3', '7');
INSERT INTO `crm_warehouse_user` VALUES ('3', '8');
INSERT INTO `crm_warehouse_user` VALUES ('3', '10');
INSERT INTO `crm_warehouse_user` VALUES ('3', '11');
INSERT INTO `crm_warehouse_user` VALUES ('3', '12');

-- ----------------------------
-- Table structure for `crm_warehousesorder`
-- ----------------------------
DROP TABLE IF EXISTS `crm_warehousesorder`;
CREATE TABLE `crm_warehousesorder` (
  `warehousesorder_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `warehousesorder_num` varchar(255) DEFAULT NULL COMMENT '入库单号',
  `purchaseorder_id` int(11) DEFAULT NULL COMMENT '进货单ID',
  `createtime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` varchar(255) DEFAULT NULL COMMENT '状态',
  `amount` int(255) DEFAULT NULL COMMENT '数量',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  PRIMARY KEY (`warehousesorder_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='入库单';

-- ----------------------------
-- Records of crm_warehousesorder
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_warehousetransfers`
-- ----------------------------
DROP TABLE IF EXISTS `crm_warehousetransfers`;
CREATE TABLE `crm_warehousetransfers` (
  `warehousetransfers_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `outwarehouse_id` int(11) DEFAULT NULL COMMENT '出货仓库',
  `inwarehouse_id` int(11) DEFAULT NULL COMMENT '进货仓库',
  `checkuser` varchar(255) DEFAULT NULL COMMENT '验货人',
  `status` varchar(255) DEFAULT NULL COMMENT '状态',
  `summary` varchar(255) DEFAULT NULL COMMENT '摘要',
  `deleted` tinyint(255) DEFAULT NULL COMMENT '删除：0未删除，1删除',
  PRIMARY KEY (`warehousetransfers_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='库间调拨';

-- ----------------------------
-- Records of crm_warehousetransfers
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_weeklylogs`
-- ----------------------------
DROP TABLE IF EXISTS `crm_weeklylogs`;
CREATE TABLE `crm_weeklylogs` (
  `weeklylogs_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `summary` text COMMENT '本周总结',
  `nextweek_plan` text COMMENT '下周计划',
  `createdate` date NOT NULL COMMENT '创建日期',
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `deleted` tinyint(255) NOT NULL DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  `year` int(255) NOT NULL DEFAULT '0' COMMENT '年',
  `week` int(255) NOT NULL DEFAULT '0' COMMENT '第几周',
  PRIMARY KEY (`weeklylogs_id`),
  UNIQUE KEY `UNSEQ_WEEKLYLOGS_YEAR_WEEK` (`year`,`week`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='周报表';

-- ----------------------------
-- Records of crm_weeklylogs
-- ----------------------------
INSERT INTO `crm_weeklylogs` VALUES ('12', '989232323', '988980o-0-0-0', '2017-11-13', '1', '0', '2017', '46');
INSERT INTO `crm_weeklylogs` VALUES ('14', '545445', '5454', '2017-11-13', '1', '0', '2017', '2');

-- ----------------------------
-- Table structure for `crm_widget`
-- ----------------------------
DROP TABLE IF EXISTS `crm_widget`;
CREATE TABLE `crm_widget` (
  `widget_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `widget_name` varchar(255) DEFAULT NULL COMMENT '组件名称',
  `widget_filename` varchar(255) DEFAULT NULL COMMENT '组件的文件名',
  `seq` tinyint(4) DEFAULT NULL COMMENT '组件排序',
  PRIMARY KEY (`widget_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='首页的widget配置表';

-- ----------------------------
-- Records of crm_widget
-- ----------------------------
INSERT INTO `crm_widget` VALUES ('1', '统计数据', 'statistic_data', '0');
INSERT INTO `crm_widget` VALUES ('2', '公司公告', 'company_announcements', '1');
INSERT INTO `crm_widget` VALUES ('3', '公司应收款月度同比', 'monthly_collection', '2');
INSERT INTO `crm_widget` VALUES ('4', '应收款应付款客户汇总', 'collection_of_receivables_payable_customer', '3');
INSERT INTO `crm_widget` VALUES ('5', '公司最近6个月销售情况', 'sales_in_the_last_six_months', '4');
INSERT INTO `crm_widget` VALUES ('6', '公司最近6个月回款任务完成情况', 'completed_the_completion_in_the_last_six_months', '5');
INSERT INTO `crm_widget` VALUES ('7', '库存资产', 'inventory_assets', '6');
INSERT INTO `crm_widget` VALUES ('8', '公司销售额月度同比', 'sales_volume_monthly', '7');
INSERT INTO `crm_widget` VALUES ('9', '日/周/月报', 'reporting_in', '8');
INSERT INTO `crm_widget` VALUES ('10', '金额较大的销售机会', 'sales_opportunities', '9');
INSERT INTO `crm_widget` VALUES ('11', '公司年度销售情况', 'annual_sales', '10');
INSERT INTO `crm_widget` VALUES ('12', '公司最近6个月销售任务完成情况', 'sales_task_completion_status', '11');
INSERT INTO `crm_widget` VALUES ('13', '销售漏斗', 'funnel', '12');

-- ----------------------------
-- Table structure for `crm_workflow_module`
-- ----------------------------
DROP TABLE IF EXISTS `crm_workflow_module`;
CREATE TABLE `crm_workflow_module` (
  `workflowid` int(11) NOT NULL COMMENT '工作流ID',
  `module` varchar(255) DEFAULT NULL COMMENT '模块',
  PRIMARY KEY (`workflowid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工作流和模块的对应关系';

-- ----------------------------
-- Records of crm_workflow_module
-- ----------------------------
INSERT INTO `crm_workflow_module` VALUES ('11', 'announcements');
INSERT INTO `crm_workflow_module` VALUES ('12', 'announcements');
INSERT INTO `crm_workflow_module` VALUES ('13', 'announcements');
INSERT INTO `crm_workflow_module` VALUES ('2323', '23');

-- ----------------------------
-- Table structure for `oa_workflow`
-- ----------------------------
DROP TABLE IF EXISTS `oa_workflow`;
CREATE TABLE `oa_workflow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '工作流的名字',
  `description` text NOT NULL COMMENT '描述',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '数据插入的时间',
  `code` varchar(20) NOT NULL COMMENT '调用字符串，用于与程序结合',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识：0未删除,1删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='工作流表';

-- ----------------------------
-- Records of oa_workflow
-- ----------------------------
INSERT INTO `oa_workflow` VALUES ('11', '667', '6776', '2017-07-21 16:53:27', '6776', '1');
INSERT INTO `oa_workflow` VALUES ('12', '123', '123', '2017-10-13 09:55:56', '123', '1');
INSERT INTO `oa_workflow` VALUES ('13', '456', '456', '2017-10-13 09:55:59', '456', '1');
INSERT INTO `oa_workflow` VALUES ('14', '手动阀', '发顺丰', '2017-07-26 08:57:10', '465464', '1');
INSERT INTO `oa_workflow` VALUES ('15', '工作流1', '1', '2017-10-13 09:56:02', '1', '1');
INSERT INTO `oa_workflow` VALUES ('16', '啊工', '123456', '2017-10-13 09:56:04', '123456', '1');
INSERT INTO `oa_workflow` VALUES ('17', '111', '123', '2017-10-13 09:56:07', '1234', '1');
INSERT INTO `oa_workflow` VALUES ('18', '777', '77', '2017-10-16 17:12:06', '77', '0');

-- ----------------------------
-- Table structure for `oa_workflow_log`
-- ----------------------------
DROP TABLE IF EXISTS `oa_workflow_log`;
CREATE TABLE `oa_workflow_log` (
  `workflowlog_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志ID',
  `workflow_id` int(11) DEFAULT NULL COMMENT '工作流ID',
  `workflow_step_id` int(11) DEFAULT NULL COMMENT '工作流步骤',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `status` varchar(255) DEFAULT NULL COMMENT '状态,Y通过，N未通过,',
  `mark` tinytext COMMENT '备注',
  `createtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`workflowlog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工作流历史记录表';

-- ----------------------------
-- Records of oa_workflow_log
-- ----------------------------

-- ----------------------------
-- Table structure for `oa_workflow_step`
-- ----------------------------
DROP TABLE IF EXISTS `oa_workflow_step`;
CREATE TABLE `oa_workflow_step` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) NOT NULL COMMENT '所属的工作流',
  `name` varchar(255) NOT NULL COMMENT '工作流步骤的名称',
  `description` text NOT NULL COMMENT '工作流步骤的描述',
  `step_level` tinyint(4) NOT NULL DEFAULT '0' COMMENT '该工作流步骤所处的第几步,如果为99代表已经审核完成',
  `code` varchar(20) NOT NULL COMMENT '主要用于权限辅助调用',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '数据增加的日期',
  PRIMARY KEY (`id`),
  KEY `workflow_id` (`workflow_id`) USING BTREE,
  KEY `step_level` (`step_level`) USING BTREE,
  KEY `code` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COMMENT='工作流的详细步骤';

-- ----------------------------
-- Records of oa_workflow_step
-- ----------------------------
INSERT INTO `oa_workflow_step` VALUES ('17', '11', '23', '23', '23', '6776', '2017-07-10 15:13:39');
INSERT INTO `oa_workflow_step` VALUES ('18', '12', '121', '111', '1', '123', '2017-07-24 15:11:08');
INSERT INTO `oa_workflow_step` VALUES ('19', '13', '44', '44', '44', '456', '2017-07-24 15:12:16');
INSERT INTO `oa_workflow_step` VALUES ('20', '13', '55', '33', '3', '456', '2017-07-24 15:13:08');
INSERT INTO `oa_workflow_step` VALUES ('21', '13', '66', '66', '66', '456', '2017-07-24 15:12:45');
INSERT INTO `oa_workflow_step` VALUES ('22', '12', '231', '123', '123', '123', '2017-07-31 17:12:46');
INSERT INTO `oa_workflow_step` VALUES ('23', '15', '步骤2', '1', '1', '1', '2017-08-22 23:59:23');
INSERT INTO `oa_workflow_step` VALUES ('24', '15', '3', '33', '0', '1', '2017-09-27 22:23:25');
INSERT INTO `oa_workflow_step` VALUES ('25', '13', '55', '666', '0', '456', '2017-09-27 22:46:19');
INSERT INTO `oa_workflow_step` VALUES ('26', '15', '7878', '78', '78', '1', '2017-09-27 22:48:30');
INSERT INTO `oa_workflow_step` VALUES ('29', '15', '111', '1234', '2', '1', '2017-10-12 14:57:23');
INSERT INTO `oa_workflow_step` VALUES ('31', '18', '777', '77', '77', '77', '2017-10-16 17:12:33');

-- ----------------------------
-- Table structure for `oa_workflow_user`
-- ----------------------------
DROP TABLE IF EXISTS `oa_workflow_user`;
CREATE TABLE `oa_workflow_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_step_id` int(11) NOT NULL COMMENT '工作流步骤的ID',
  `user_id` int(11) NOT NULL COMMENT '后台管理员的ID',
  `workflow_id` int(11) NOT NULL COMMENT '工作流ID',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `workflow_step_id` (`workflow_step_id`) USING BTREE,
  KEY `workflow_id` (`workflow_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COMMENT='工作流操作人';

-- ----------------------------
-- Records of oa_workflow_user
-- ----------------------------
INSERT INTO `oa_workflow_user` VALUES ('37', '16', '1', '11');
INSERT INTO `oa_workflow_user` VALUES ('38', '16', '2', '11');
INSERT INTO `oa_workflow_user` VALUES ('39', '16', '3', '11');
INSERT INTO `oa_workflow_user` VALUES ('41', '17', '1', '11');
INSERT INTO `oa_workflow_user` VALUES ('42', '17', '2', '11');
INSERT INTO `oa_workflow_user` VALUES ('43', '17', '3', '11');
INSERT INTO `oa_workflow_user` VALUES ('52', '21', '1', '13');
INSERT INTO `oa_workflow_user` VALUES ('67', '22', '1', '12');
INSERT INTO `oa_workflow_user` VALUES ('68', '22', '7', '12');
INSERT INTO `oa_workflow_user` VALUES ('69', '24', '1', '15');
INSERT INTO `oa_workflow_user` VALUES ('70', '23', '1', '15');
INSERT INTO `oa_workflow_user` VALUES ('71', '23', '7', '15');
INSERT INTO `oa_workflow_user` VALUES ('74', '19', '7', '13');
INSERT INTO `oa_workflow_user` VALUES ('75', '20', '7', '13');
INSERT INTO `oa_workflow_user` VALUES ('77', '25', '7', '13');
INSERT INTO `oa_workflow_user` VALUES ('78', '26', '8', '15');
INSERT INTO `oa_workflow_user` VALUES ('90', '29', '1', '15');
INSERT INTO `oa_workflow_user` VALUES ('91', '29', '7', '15');
INSERT INTO `oa_workflow_user` VALUES ('92', '28', '1', '17');
INSERT INTO `oa_workflow_user` VALUES ('93', '28', '7', '17');
INSERT INTO `oa_workflow_user` VALUES ('94', '27', '1', '17');
INSERT INTO `oa_workflow_user` VALUES ('95', '27', '7', '17');
INSERT INTO `oa_workflow_user` VALUES ('96', '18', '1', '12');
INSERT INTO `oa_workflow_user` VALUES ('97', '30', '1', '18');
INSERT INTO `oa_workflow_user` VALUES ('98', '30', '7', '18');
INSERT INTO `oa_workflow_user` VALUES ('99', '31', '1', '18');
INSERT INTO `oa_workflow_user` VALUES ('100', '31', '7', '18');
INSERT INTO `oa_workflow_user` VALUES ('101', '31', '8', '18');

-- ----------------------------
-- Table structure for `sys_authassignment`
-- ----------------------------
DROP TABLE IF EXISTS `sys_authassignment`;
CREATE TABLE `sys_authassignment` (
  `itemname` varchar(64) NOT NULL COMMENT '权限管理表',
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text COMMENT '权限管理表',
  PRIMARY KEY (`itemname`,`userid`),
  CONSTRAINT `sys_authassignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `sys_authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限_认证项赋权关系';

-- ----------------------------
-- Records of sys_authassignment
-- ----------------------------
INSERT INTO `sys_authassignment` VALUES ('test', 'test', null, 'N;');
INSERT INTO `sys_authassignment` VALUES ('客服专员', '1', null, 'N;');
INSERT INTO `sys_authassignment` VALUES ('客服专员', '2', null, 'N;');
INSERT INTO `sys_authassignment` VALUES ('客服专员', 'admin', null, 'N;');
INSERT INTO `sys_authassignment` VALUES ('客服专员', 'ranko', null, 'N;');
INSERT INTO `sys_authassignment` VALUES ('客服专员', 'servicestaff', null, 'N;');
INSERT INTO `sys_authassignment` VALUES ('超级管理员', 'admin', null, 'N;');
INSERT INTO `sys_authassignment` VALUES ('超级管理员', 'ranko', null, 'N;');
INSERT INTO `sys_authassignment` VALUES ('销售人员', 'admin', null, 'N;');
INSERT INTO `sys_authassignment` VALUES ('销售人员', 'salesman', null, 'N;');

-- ----------------------------
-- Table structure for `sys_authitem`
-- ----------------------------
DROP TABLE IF EXISTS `sys_authitem`;
CREATE TABLE `sys_authitem` (
  `name` varchar(64) NOT NULL COMMENT '权限管理表',
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  `deleted` tinyint(255) NOT NULL DEFAULT '0' COMMENT '0未删除，1删除',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限_功能模块子项目';

-- ----------------------------
-- Records of sys_authitem
-- ----------------------------
INSERT INTO `sys_authitem` VALUES ('accountrecordss_module', '0', '客户服务', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('accountrecordss_module_add', '0', '添加客户服务', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('accountrecordss_module_edit', '0', '编辑客户服务', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('accountrecordss_module_view', '0', '查看客户服务', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('aftersale', '0', '售后', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('campaigns', '0', '营销', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('campaigns_add', '0', '添加营销活动', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('campaigns_edit', '0', '编辑营销活动', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('campaigns_module', '0', '营销活动', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('campaigns_remove', '0', '删除营销活动', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('campaigns_view', '0', '查看营销活动', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('competitor_module', '0', '竞争对手', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('competitor_module_add', '0', '添加竞争对手', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('competitor_module_edit', '0', '编辑竞争对手', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('competitor_module_remove', '0', '删除竞争对手', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('complaints_module', '0', '客户投诉', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('complaints_module_add', '0', '添加客户投诉', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('complaints_module_edit', '0', '编辑客户投诉', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('complaints_module_view', '0', '查看客户投诉', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('contacter_module', '0', '联系人', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('contacter_module_add', '0', '添加联系人', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('contacter_module_edit', '0', '编辑联系人', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('contacter_module_remove', '0', '删除联系人', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('contacter_module_view', '0', '查看联系人', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('contactlog_module', '0', '联系记录', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('contactlog_module_add', '0', '添加联系记录', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('contactlog_module_edit', '0', '编辑联系记录', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('contactlog_module_remove', '0', '删除联系记录', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('contactlog_module_view', '0', '查看联系记录', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('customer', '0', '客户', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('customer_module', '0', '客户', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('customer_module_add', '0', '添加客户', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('customer_module_edit', '0', '编辑客户', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('customer_module_remove', '0', '删除客户', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('customer_module_view', '0', '查看客户', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('faqcategory_module', '0', '常见问题分类', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('faqcategory_module_add', '0', '添加常见问题分类', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('faqcategory_module_edit', '0', '编辑常见问题分类', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('faqcategory_module_view', '0', '查看常见问题分类', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('faq_module', '0', '常见问题', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('faq_module_add', '0', '添加常见问题', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('faq_module_edit', '0', '编辑常见问题', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('faq_module_view', '0', '查看常见问题', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('finance', '0', '财务', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('memdays_module', '0', '纪念日', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('memdays_module_add', '0', '添加纪念日', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('memdays_module_edit', '0', '编辑纪念日', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('memdays_module_remove', '0', '删除纪念日', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('memdays_module_view', '0', '查看纪念日', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('personalconfig', '0', '个人设置', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('personalconfig_config', '0', '个人设置界面', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('potentials_module', '0', '销售机会', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('potentials_module_add', '0', '添加销售机会', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('potentials_module_edit', '0', '编辑销售机会', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('potentials_module_remove', '0', '删除销售机会', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('potentials_module_view', '0', '查看销售机会', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('product', '0', '产品', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('productcategory_module', '0', '产品分类', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('productcategory_module_add', '0', '添加产品分类', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('productcategory_module_edit', '0', '编辑产品分类', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('productcategory_module_remove', '0', '删除产品分类', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('product_module', '0', '产品', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('product_module_add', '0', '添加产品', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('product_module_edit', '0', '编辑产品', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('product_module_remove', '0', '删除产品', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('product_module_view', '0', '查看产品', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('purchase', '0', '采购', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('quotes_module', '0', '报价单', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('quotes_module_add', '0', '报价单添加', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('quotes_module_edit', '0', '报价单编辑', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('quotes_module_remove', '0', '报价单删除', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sale', '0', '销售', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('salesorder_module', '0', '合同订单', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('salesorder_module_add', '0', '添加合同订单', null, null, '0');
INSERT INTO `sys_authitem` VALUES ('salesorder_module_edit', '0', '修改合同订单', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('salesorder_module_remove', '0', '删除合同订单', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('stock', '0', '库存', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig', '0', '系统设置', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_loginhistory_module', '0', '登入历史记录', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_messager_module', '0', '消息设置', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_messagetemplate_module', '0', '模板设置', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_picklist_module', '0', '数据字典', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_picklist_module_add', '0', '添加数据字典', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_picklist_module_edit', '0', '编辑数据字典', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_picklist_module_remove', '0', '删除数据字典', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_role_module', '0', '角色权限', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_role_module_add', '0', '添加角色权限', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_role_module_edit', '0', '编辑角色权限', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_role_module_remove', '0', '删除角色权限', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_system_module', '0', '系统配置', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_system_module_save', '0', '系统配置保存', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_user_module', '0', '系统用户', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_user_module_add', '0', '添加系统用户', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_user_module_edit', '0', '编辑系统用户', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_user_module_remove', '0', '删除系统用户', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_user_module_view', '0', '查看系统用户', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_warehouse_module', '0', '多仓库设置', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_warehouse_module_add', '0', '新增仓库', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_warehouse_module_authority', '0', '仓库授权', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_warehouse_module_edit', '0', '编辑仓库', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_warehouse_module_remove', '0', '仓库删除', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_workflowmodule_module', '0', '流程模块', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_workflowmodule_module_config', '0', '配置流程模块', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_workflow_module', '0', '审批流程', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_workflow_module_add', '0', '添加审批流程', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_workflow_module_config', '0', '配置审批流程', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_workflow_module_edit', '0', '编辑审批流程', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('sysconfig_workflow_module_remove', '0', '删除审批流程', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('test', '2', 'test', null, 'N;', '1');
INSERT INTO `sys_authitem` VALUES ('vcontacts_module', '0', '供应商联系人', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('vcontacts_module_add', '0', '添加供应商联系人', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('vcontacts_module_edit', '0', '编辑供应商联系人', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('vcontacts_module_remove', '0', '删除供应商联系人', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('vendors_module', '0', '供应商', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('vendors_module_add', '0', '添加供应商', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('vendors_module_edit', '0', '修改供应商', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('vendors_module_remove', '0', '删除供应商', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('workspace', '0', '工作台', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('workspace_announcements', '0', '公告', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('workspace_announcements_add', '0', '添加公告', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('workspace_announcements_edit', '0', '编辑公告', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('workspace_announcements_remove', '0', '删除公告', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('workspace_announcements_view', '0', '查看公告', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('workspace_dailylogs', '0', '日报', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('workspace_dailylogs_add', '0', '添加日报', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('workspace_dailylogs_edit', '0', '编辑日报', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('workspace_dailylogs_view', '0', '查看日报', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('workspace_site', '0', '控制台', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('客服专员', '2', '客服专员', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('测试', '2', '测试角色', null, 'N;', '1');
INSERT INTO `sys_authitem` VALUES ('管理员2', '2', '管理员2', null, 'N;', '1');
INSERT INTO `sys_authitem` VALUES ('超级管理员', '2', '超级管理员', null, 'N;', '0');
INSERT INTO `sys_authitem` VALUES ('销售人员', '2', '销售人员', null, 'N;', '0');

-- ----------------------------
-- Table structure for `sys_authitemchild`
-- ----------------------------
DROP TABLE IF EXISTS `sys_authitemchild`;
CREATE TABLE `sys_authitemchild` (
  `parent` varchar(64) NOT NULL COMMENT '权限管理表',
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`) USING BTREE,
  CONSTRAINT `sys_authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `sys_authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sys_authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `sys_authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限_认证项父子关系';

-- ----------------------------
-- Records of sys_authitemchild
-- ----------------------------
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'accountrecordss_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'accountrecordss_module');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'accountrecordss_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'accountrecordss_module_add');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'accountrecordss_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'accountrecordss_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'accountrecordss_module_view');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'aftersale');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'aftersale');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'campaigns');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'campaigns');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'campaigns');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'campaigns_add');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'campaigns_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'campaigns_edit');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'campaigns_edit');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'campaigns_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'campaigns_module');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'campaigns_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'campaigns_remove');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'campaigns_view');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'campaigns_view');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'campaigns_view');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'competitor_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'competitor_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'competitor_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'competitor_module_remove');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'complaints_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'complaints_module');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'complaints_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'complaints_module_add');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'complaints_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'complaints_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'complaints_module_view');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'contacter_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'contacter_module');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'contacter_module');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'contacter_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'contacter_module_add');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'contacter_module_add');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'contacter_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'contacter_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'contacter_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'contacter_module_remove');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'contacter_module_view');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'contacter_module_view');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'contacter_module_view');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'contactlog_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'contactlog_module');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'contactlog_module');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'contactlog_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'contactlog_module_add');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'contactlog_module_add');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'contactlog_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'contactlog_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'contactlog_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'contactlog_module_remove');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'contactlog_module_view');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'contactlog_module_view');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'contactlog_module_view');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'customer');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'customer');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'customer');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'customer_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'customer_module');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'customer_module');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'customer_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'customer_module_add');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'customer_module_add');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'customer_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'customer_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'customer_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'customer_module_remove');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'customer_module_view');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'customer_module_view');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'customer_module_view');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'faqcategory_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'faqcategory_module');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'faqcategory_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'faqcategory_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'faqcategory_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'faqcategory_module_view');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'faq_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'faq_module');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'faq_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'faq_module_add');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'faq_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'faq_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'faq_module_view');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'finance');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'memdays_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'memdays_module');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'memdays_module');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'memdays_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'memdays_module_add');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'memdays_module_add');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'memdays_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'memdays_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'memdays_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'memdays_module_remove');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'memdays_module_view');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'memdays_module_view');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'memdays_module_view');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'personalconfig');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'personalconfig');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'personalconfig');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'personalconfig_config');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'personalconfig_config');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'personalconfig_config');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'potentials_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'potentials_module');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'potentials_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'potentials_module_add');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'potentials_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'potentials_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'potentials_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'potentials_module_remove');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'potentials_module_remove');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'product');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'product');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'product');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'productcategory_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'productcategory_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'productcategory_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'productcategory_module_remove');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'product_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'product_module');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'product_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'product_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'product_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'product_module_remove');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'product_module_view');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'product_module_view');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'product_module_view');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'purchase');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'quotes_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'quotes_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'quotes_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'quotes_module_remove');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'sale');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sale');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'sale');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'salesorder_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'salesorder_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'salesorder_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'salesorder_module_remove');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'stock');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_loginhistory_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_messager_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_messagetemplate_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_picklist_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_picklist_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_picklist_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_picklist_module_remove');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_role_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_role_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_role_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_role_module_remove');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_user_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_user_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_user_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_user_module_view');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_warehouse_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_warehouse_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_warehouse_module_authority');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_warehouse_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_warehouse_module_remove');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_workflowmodule_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_workflowmodule_module_config');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_workflow_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_workflow_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_workflow_module_config');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_workflow_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'sysconfig_workflow_module_remove');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'vcontacts_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'vcontacts_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'vcontacts_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'vcontacts_module_remove');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'vendors_module');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'vendors_module_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'vendors_module_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'vendors_module_remove');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'workspace');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'workspace');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'workspace');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'workspace_announcements');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'workspace_announcements');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'workspace_announcements');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'workspace_announcements_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'workspace_announcements_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'workspace_announcements_remove');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'workspace_announcements_view');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'workspace_announcements_view');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'workspace_announcements_view');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'workspace_dailylogs');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'workspace_dailylogs');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'workspace_dailylogs');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'workspace_dailylogs_add');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'workspace_dailylogs_add');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'workspace_dailylogs_add');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'workspace_dailylogs_edit');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'workspace_dailylogs_edit');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'workspace_dailylogs_edit');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'workspace_dailylogs_view');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'workspace_dailylogs_view');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'workspace_dailylogs_view');
INSERT INTO `sys_authitemchild` VALUES ('客服专员', 'workspace_site');
INSERT INTO `sys_authitemchild` VALUES ('超级管理员', 'workspace_site');
INSERT INTO `sys_authitemchild` VALUES ('销售人员', 'workspace_site');

-- ----------------------------
-- Table structure for `sys_config`
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '配置主键',
  `module` varchar(255) DEFAULT NULL COMMENT '类型',
  `config_key` varchar(255) DEFAULT NULL COMMENT '配置的KEY',
  `config_value` varchar(255) DEFAULT NULL COMMENT '配置的值',
  `seq` tinyint(4) DEFAULT NULL COMMENT '变量排序',
  `config_description` varchar(255) DEFAULT NULL COMMENT '配置的描述',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', 'messager', 'apikey', '333', '0', 'APIKEY');
INSERT INTO `sys_config` VALUES ('3', 'system', 'systemTitle', '蓝科客户管理系统', '0', '系统标题');
INSERT INTO `sys_config` VALUES ('4', 'system', 'systemLogo', 'assets/system/logo_icon.png', '1', '系统LOGO');
INSERT INTO `sys_config` VALUES ('5', 'system', 'systemLoginIcon', 'assets/system/logo_icon.png', '2', '登入页面ICON');
INSERT INTO `sys_config` VALUES ('6', 'system', 'systemWebIcon', 'assets/system/favicon.ico', '3', '网站的ICON');

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menutext` varchar(255) DEFAULT NULL COMMENT '功能项目描述',
  `menupermission` varchar(255) DEFAULT NULL COMMENT '功能项目，shiro权限配置',
  `seq` int(11) NOT NULL DEFAULT '0' COMMENT '顺序',
  `type` varchar(255) DEFAULT NULL COMMENT '类型：menu,menuitem,module',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '上一层的菜单',
  `menuitemurl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `UNIQUE_SYS_MENU_MENUPERMISSION` (`menupermission`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('27', '工作台', 'workspace', '100', 'menu', '0', null);
INSERT INTO `sys_menu` VALUES ('28', '控制台', 'workspace_site', '0', 'menuitem', '27', null);
INSERT INTO `sys_menu` VALUES ('29', '公告', 'workspace_announcements', '0', 'menuitem', '27', null);
INSERT INTO `sys_menu` VALUES ('30', '查看公告', 'workspace_announcements_view', '0', 'module', '29', null);
INSERT INTO `sys_menu` VALUES ('31', '添加公告', 'workspace_announcements_add', '0', 'module', '29', null);
INSERT INTO `sys_menu` VALUES ('32', '编辑公告', 'workspace_announcements_edit', '0', 'module', '29', null);
INSERT INTO `sys_menu` VALUES ('33', '删除公告', 'workspace_announcements_remove', '0', 'module', '29', null);
INSERT INTO `sys_menu` VALUES ('34', '日报', 'workspace_dailylogs', '0', 'menuitem', '27', null);
INSERT INTO `sys_menu` VALUES ('35', '查看日报', 'workspace_dailylogs_view', '0', 'module', '34', null);
INSERT INTO `sys_menu` VALUES ('36', '添加日报', 'workspace_dailylogs_add', '0', 'module', '34', null);
INSERT INTO `sys_menu` VALUES ('37', '编辑日报', 'workspace_dailylogs_edit', '0', 'module', '34', null);
INSERT INTO `sys_menu` VALUES ('38', '营销', 'campaigns', '99', 'menu', '0', null);
INSERT INTO `sys_menu` VALUES ('39', '营销活动', 'campaigns_module', '0', 'menuitem', '38', null);
INSERT INTO `sys_menu` VALUES ('40', '查看营销活动', 'campaigns_view', '0', 'module', '39', null);
INSERT INTO `sys_menu` VALUES ('41', '添加营销活动', 'campaigns_add', '0', 'module', '39', null);
INSERT INTO `sys_menu` VALUES ('42', '编辑营销活动', 'campaigns_edit', '0', 'module', '39', null);
INSERT INTO `sys_menu` VALUES ('43', '删除营销活动', 'campaigns_remove', '0', 'module', '39', null);
INSERT INTO `sys_menu` VALUES ('44', '客户', 'customer', '98', 'menu', '0', null);
INSERT INTO `sys_menu` VALUES ('45', '客户', 'customer_module', '0', 'menuitem', '44', null);
INSERT INTO `sys_menu` VALUES ('46', '查看客户', 'customer_module_view', '0', 'module', '45', null);
INSERT INTO `sys_menu` VALUES ('47', '添加客户', 'customer_module_add', '0', 'module', '45', null);
INSERT INTO `sys_menu` VALUES ('48', '编辑客户', 'customer_module_edit', '0', 'module', '45', null);
INSERT INTO `sys_menu` VALUES ('49', '删除客户', 'customer_module_remove', '0', 'module', '45', null);
INSERT INTO `sys_menu` VALUES ('50', '联系人', 'contacter_module', '0', 'menuitem', '44', null);
INSERT INTO `sys_menu` VALUES ('51', '查看联系人', 'contacter_module_view', '0', 'module', '50', null);
INSERT INTO `sys_menu` VALUES ('52', '添加联系人', 'contacter_module_add', '0', 'module', '50', null);
INSERT INTO `sys_menu` VALUES ('53', '编辑联系人', 'contacter_module_edit', '0', 'module', '50', null);
INSERT INTO `sys_menu` VALUES ('54', '删除联系人', 'contacter_module_remove', '0', 'module', '50', null);
INSERT INTO `sys_menu` VALUES ('55', '联系记录', 'contactlog_module', '0', 'menuitem', '44', null);
INSERT INTO `sys_menu` VALUES ('56', '查看联系记录', 'contactlog_module_view', '0', 'module', '55', null);
INSERT INTO `sys_menu` VALUES ('57', '添加联系记录', 'contactlog_module_add', '0', 'module', '55', null);
INSERT INTO `sys_menu` VALUES ('58', '编辑联系记录', 'contactlog_module_edit', '0', 'module', '55', null);
INSERT INTO `sys_menu` VALUES ('59', '删除联系记录', 'contactlog_module_remove', '0', 'module', '55', null);
INSERT INTO `sys_menu` VALUES ('60', '纪念日', 'memdays_module', '0', 'menuitem', '44', null);
INSERT INTO `sys_menu` VALUES ('61', '查看纪念日', 'memdays_module_view', '0', 'module', '60', null);
INSERT INTO `sys_menu` VALUES ('62', '添加纪念日', 'memdays_module_add', '0', 'module', '60', null);
INSERT INTO `sys_menu` VALUES ('63', '编辑纪念日', 'memdays_module_edit', '0', 'module', '60', null);
INSERT INTO `sys_menu` VALUES ('64', '删除纪念日', 'memdays_module_remove', '0', 'module', '60', null);
INSERT INTO `sys_menu` VALUES ('65', '产品', 'product', '97', 'menu', '0', null);
INSERT INTO `sys_menu` VALUES ('66', '产品', 'product_module', '0', 'menuitem', '65', null);
INSERT INTO `sys_menu` VALUES ('67', '查看产品', 'product_module_view', '0', 'module', '66', null);
INSERT INTO `sys_menu` VALUES ('68', '添加产品', 'product_module_add', '0', 'module', '66', null);
INSERT INTO `sys_menu` VALUES ('69', '编辑产品', 'product_module_edit', '0', 'module', '66', null);
INSERT INTO `sys_menu` VALUES ('70', '删除产品', 'product_module_remove', '0', 'module', '66', null);
INSERT INTO `sys_menu` VALUES ('71', '产品分类', 'productcategory_module', '0', 'menuitem', '65', null);
INSERT INTO `sys_menu` VALUES ('72', '添加产品分类', 'productcategory_module_add', '0', 'module', '71', null);
INSERT INTO `sys_menu` VALUES ('73', '编辑产品分类', 'productcategory_module_edit', '0', 'module', '71', null);
INSERT INTO `sys_menu` VALUES ('74', '删除产品分类', 'productcategory_module_remove', '0', 'module', '71', null);
INSERT INTO `sys_menu` VALUES ('75', '销售', 'sale', '96', 'menu', '0', null);
INSERT INTO `sys_menu` VALUES ('76', '销售机会', 'potentials_module', '0', 'menuitem', '75', null);
INSERT INTO `sys_menu` VALUES ('77', '添加销售机会', 'potentials_module_add', '0', 'module', '76', null);
INSERT INTO `sys_menu` VALUES ('78', '编辑销售机会', 'potentials_module_edit', '0', 'module', '76', null);
INSERT INTO `sys_menu` VALUES ('79', '删除销售机会', 'potentials_module_remove', '0', 'module', '76', null);
INSERT INTO `sys_menu` VALUES ('80', '售后', 'aftersale', '94', 'menu', '0', null);
INSERT INTO `sys_menu` VALUES ('81', '客户服务', 'accountrecordss_module', '0', 'menuitem', '80', null);
INSERT INTO `sys_menu` VALUES ('82', '查看客户服务', 'accountrecordss_module_view', '0', 'module', '81', null);
INSERT INTO `sys_menu` VALUES ('83', '添加客户服务', 'accountrecordss_module_add', '0', 'module', '81', null);
INSERT INTO `sys_menu` VALUES ('84', '编辑客户服务', 'accountrecordss_module_edit', '0', 'module', '81', null);
INSERT INTO `sys_menu` VALUES ('85', '客户投诉', 'complaints_module', '0', 'menuitem', '80', null);
INSERT INTO `sys_menu` VALUES ('86', '查看客户投诉', 'complaints_module_view', '0', 'module', '85', null);
INSERT INTO `sys_menu` VALUES ('87', '添加客户投诉', 'complaints_module_add', '0', 'module', '85', null);
INSERT INTO `sys_menu` VALUES ('88', '编辑客户投诉', 'complaints_module_edit', '0', 'module', '85', null);
INSERT INTO `sys_menu` VALUES ('89', '常见问题', 'faq_module', '0', 'menuitem', '80', null);
INSERT INTO `sys_menu` VALUES ('90', '查看常见问题', 'faq_module_view', '0', 'module', '89', null);
INSERT INTO `sys_menu` VALUES ('91', '添加常见问题', 'faq_module_add', '0', 'module', '89', null);
INSERT INTO `sys_menu` VALUES ('92', '编辑常见问题', 'faq_module_edit', '0', 'module', '89', null);
INSERT INTO `sys_menu` VALUES ('93', '常见问题分类', 'faqcategory_module', '0', 'menuitem', '80', null);
INSERT INTO `sys_menu` VALUES ('94', '查看常见问题分类', 'faqcategory_module_view', '0', 'module', '93', null);
INSERT INTO `sys_menu` VALUES ('95', '添加常见问题分类', 'faqcategory_module_add', '0', 'module', '93', null);
INSERT INTO `sys_menu` VALUES ('96', '编辑常见问题分类', 'faqcategory_module_edit', '0', 'module', '93', null);
INSERT INTO `sys_menu` VALUES ('97', '系统设置', 'sysconfig', '91', 'menu', '0', null);
INSERT INTO `sys_menu` VALUES ('98', '系统用户', 'sysconfig_user_module', '0', 'menuitem', '97', null);
INSERT INTO `sys_menu` VALUES ('99', '查看系统用户', 'sysconfig_user_module_view', '0', 'module', '98', null);
INSERT INTO `sys_menu` VALUES ('100', '添加系统用户', 'sysconfig_user_module_add', '0', 'module', '98', null);
INSERT INTO `sys_menu` VALUES ('101', '编辑系统用户', 'sysconfig_user_module_edit', '0', 'module', '98', null);
INSERT INTO `sys_menu` VALUES ('102', '角色权限', 'sysconfig_role_module', '0', 'menuitem', '97', null);
INSERT INTO `sys_menu` VALUES ('103', '添加角色权限', 'sysconfig_role_module_add', '0', 'module', '102', null);
INSERT INTO `sys_menu` VALUES ('104', '编辑角色权限', 'sysconfig_role_module_edit', '0', 'module', '102', null);
INSERT INTO `sys_menu` VALUES ('105', '删除角色权限', 'sysconfig_role_module_remove', '0', 'module', '102', null);
INSERT INTO `sys_menu` VALUES ('106', '数据字典', 'sysconfig_picklist_module', '0', 'menuitem', '97', null);
INSERT INTO `sys_menu` VALUES ('107', '添加数据字典', 'sysconfig_picklist_module_add', '0', 'module', '106', null);
INSERT INTO `sys_menu` VALUES ('108', '编辑数据字典', 'sysconfig_picklist_module_edit', '0', 'module', '106', null);
INSERT INTO `sys_menu` VALUES ('109', '删除数据字典', 'sysconfig_picklist_module_remove', '0', 'module', '106', null);
INSERT INTO `sys_menu` VALUES ('110', '审批流程', 'sysconfig_workflow_module', '0', 'menuitem', '97', null);
INSERT INTO `sys_menu` VALUES ('111', '添加审批流程', 'sysconfig_workflow_module_add', '0', 'module', '110', null);
INSERT INTO `sys_menu` VALUES ('112', '编辑审批流程', 'sysconfig_workflow_module_edit', '0', 'module', '110', null);
INSERT INTO `sys_menu` VALUES ('113', '删除审批流程', 'sysconfig_workflow_module_remove', '0', 'module', '110', null);
INSERT INTO `sys_menu` VALUES ('114', '配置审批流程', 'sysconfig_workflow_module_config', '0', 'module', '110', null);
INSERT INTO `sys_menu` VALUES ('115', '流程模块', 'sysconfig_workflowmodule_module', '0', 'menuitem', '97', null);
INSERT INTO `sys_menu` VALUES ('116', '配置流程模块', 'sysconfig_workflowmodule_module_config', '0', 'module', '115', null);
INSERT INTO `sys_menu` VALUES ('117', '个人设置', 'personalconfig', '90', 'menu', '0', null);
INSERT INTO `sys_menu` VALUES ('118', '个人设置界面', 'personalconfig_config', '0', 'menuitem', '117', null);
INSERT INTO `sys_menu` VALUES ('119', '报价单', 'quotes_module', '0', 'menuitem', '75', null);
INSERT INTO `sys_menu` VALUES ('120', '添加报价单', 'quotes_module_add', '0', 'module', '119', null);
INSERT INTO `sys_menu` VALUES ('121', '编辑报价单', 'quotes_module_edit', '0', 'module', '119', null);
INSERT INTO `sys_menu` VALUES ('122', '删除报价单', 'quotes_module_remove', '0', 'module', '119', null);
INSERT INTO `sys_menu` VALUES ('123', '竞争对手', 'competitor_module', '0', 'menuitem', '75', null);
INSERT INTO `sys_menu` VALUES ('124', '添加竞争对手', 'competitor_module_add', '0', 'module', '123', null);
INSERT INTO `sys_menu` VALUES ('126', '修改竞争对手', 'competitor_module_edit', '0', 'module', '123', null);
INSERT INTO `sys_menu` VALUES ('127', '删除竞争对手', 'competitor_module_remove', '0', 'module', '123', null);
INSERT INTO `sys_menu` VALUES ('128', '合同订单', 'salesorder_module', '0', 'menuitem', '75', null);
INSERT INTO `sys_menu` VALUES ('129', '添加合同订单', 'salesorder_module_add', '0', 'module', '128', null);
INSERT INTO `sys_menu` VALUES ('130', '修改合同订单', 'salesorder_module_edit', '0', 'module', '128', null);
INSERT INTO `sys_menu` VALUES ('131', '删除合同订单', 'salesorder_module_remove', '0', 'module', '128', null);
INSERT INTO `sys_menu` VALUES ('132', '消息设置', 'sysconfig_messager_module', '0', 'menuitem', '97', null);
INSERT INTO `sys_menu` VALUES ('133', '模板设置', 'sysconfig_messagetemplate_module', '0', 'menuitem', '97', null);
INSERT INTO `sys_menu` VALUES ('134', '登入历史记录', 'sysconfig_loginhistory_module', '0', 'menuitem', '97', null);
INSERT INTO `sys_menu` VALUES ('135', '多仓库设置', 'sysconfig_warehouse_module', '0', 'menuitem', '97', null);
INSERT INTO `sys_menu` VALUES ('136', '新增仓库', 'sysconfig_warehouse_module_add', '0', 'module', '135', null);
INSERT INTO `sys_menu` VALUES ('137', '编辑仓库', 'sysconfig_warehouse_module_edit', '0', 'module', '135', null);
INSERT INTO `sys_menu` VALUES ('138', '仓库授权', 'sysconfig_warehouse_module_authority', '0', 'module', '135', null);
INSERT INTO `sys_menu` VALUES ('139', '删除仓库', 'sysconfig_warehouse_module_remove', '0', 'module', '135', null);
INSERT INTO `sys_menu` VALUES ('140', '供应商', 'vendors_module', '0', 'menuitem', '141', null);
INSERT INTO `sys_menu` VALUES ('141', '采购', 'purchase', '95', 'menu', '0', null);
INSERT INTO `sys_menu` VALUES ('142', '库存', 'stock', '93', 'menu', '0', null);
INSERT INTO `sys_menu` VALUES ('143', '财务', 'finance', '92', 'menu', '0', null);
INSERT INTO `sys_menu` VALUES ('144', '供应商联系人', 'vcontacts_module', '0', 'menuitem', '141', null);
INSERT INTO `sys_menu` VALUES ('145', '添加供应商联系人', 'vcontacts_module_add', '0', 'module', '144', null);
INSERT INTO `sys_menu` VALUES ('146', '修改供应商联系人', 'vcontacts_module_edit', '0', 'module', '144', null);
INSERT INTO `sys_menu` VALUES ('147', '删除供应商联系人', 'vcontacts_module_remove', '0', 'module', '144', null);
INSERT INTO `sys_menu` VALUES ('148', '添加供应商', 'vendors_module_add', '0', 'module', '140', null);
INSERT INTO `sys_menu` VALUES ('150', '修改供应商', 'vendors_module_edit', '0', 'module', '140', null);
INSERT INTO `sys_menu` VALUES ('151', '删除供应商', 'vendors_module_remove', '0', 'module', '140', null);

-- ----------------------------
-- Table structure for `sys_messagetemplate`
-- ----------------------------
DROP TABLE IF EXISTS `sys_messagetemplate`;
CREATE TABLE `sys_messagetemplate` (
  `messagetemplate_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `memo` varchar(255) DEFAULT NULL COMMENT '短信描述',
  `templatecode` varchar(255) DEFAULT NULL COMMENT '短信模板CODE',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识：0未删除，1删除',
  `yunpiantemplateid` int(11) DEFAULT NULL COMMENT '云片网模板ID',
  PRIMARY KEY (`messagetemplate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='短信模板配置表';

-- ----------------------------
-- Records of sys_messagetemplate
-- ----------------------------
INSERT INTO `sys_messagetemplate` VALUES ('1', '232', '23230000', '0', null);
INSERT INTO `sys_messagetemplate` VALUES ('2', '666', '666', '1', null);
