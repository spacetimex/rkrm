<table id="monthlylogs_list" class="easyui-datagrid"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true" toolbar="#toolbar_monthlylogs"
       idField="monthlylogs_id" url="y.php?r=monthlylogs/getdata">
    <thead>
    <tr>
        <th field="createdate" width="60">月报日期</th>
    </tr>
    </thead>
</table>
<div id="toolbar_monthlylogs">
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-view',plain:true" onclick="monthlylogs_view()" /a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加月报','y.php?r=monthlylogs/showedit&monthlylogs_id=0')" /a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="monthlylogs_showedit()" />
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="monthlylogs_remove()" />
</div>
<script>
    function monthlylogs_showedit() {
        //获取选中的列
        var selected = $("#monthlylogs_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('添加月报','y.php?r=monthlylogs/showedit&monthlylogs_id='+selected.monthlylogs_id);
        }
    }
    function monthlylogs_remove() {
        var selected = $("#monthlylogs_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=monthlylogs/remove',{monthlylogs_id:selected.monthlylogs_id},function (res) {
                        $("#monthlylogs_list").datagrid("reload");
                    });
                }
            });
        }
    }
    function monthlylogs_view() {
        var selected = $("#monthlylogs_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            loadWin('查看月报','y.php?r=monthlylogs/view&monthlylogs_id='+selected.monthlylogs_id);
        }
    }
</script>
