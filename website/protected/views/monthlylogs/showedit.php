<form id="form_monthlylogs_showedit" method="post" action="y.php?r=monthlylogs/savedata">
    <input type="hidden" name="monthlylogs_id" value="">
    <table class="tab_form">
        <tr>
            <td>本月总结：</td>
        </tr>
        <tr>
            <td>
                <textarea style="width: 61%;resize: none;height: 80px;" class="iptborder" data-options="label:'本月总结:',required:true" name="summary" ></textarea>
            </td>
        </tr>
        <tr>
            <td>下月计划：</td>
        </tr>
        <tr>
            <td>
                <textarea style="width: 61%;resize: none;height: 80px;" class="iptborder" data-options="label:'下月计划:',required:true" name="nextmonth_plan" ></textarea>
            </td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">清空</a>
</div>
<script>
    var dataobj = <?php echo json_encode($monthlylogs) ?>;
    $(function () {
        $('#form_monthlylogs_showedit').form('load',dataobj);
    });
    function submitForm() {
        $('#form_monthlylogs_showedit').form('submit',{
            success: function(){
                $('#win_main').window("close");
                $("#monthlylogs_list").datagrid("reload");
            }
        });
    }
    function clearForm() {
        $('#form_monthlylogs_showedit').form('clear');
        if(dataobj!=null){
            $("input[name=monthlylogs_id]").val(dataobj.monthlylogs_id);
        }
    }
</script>