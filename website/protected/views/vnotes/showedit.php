<form id="form_vendor_showedit" method="post" action="y.php?r=vnotes/savedata">
    <input type="hidden" name="vnotes_id" value="">
    <table class="tab_form">
        <tr>
            <td><input class="easyui-textbox" name="industry"  data-options="label:'主题:',required:true"></td>
            <td><input class="easyui-textbox" name="vendors_id"
                       data-options="label:'供应商:',required:true"></td>
            <td><input class="easyui-textbox" name="contact_time"
                       data-options="label:'联系时间:',required:true"></td>
        </tr>
        <tr>
            <td><input class="easyui-textbox" name="contact_type"
                       data-options="label:'城市:',required:true"></td>
            <td><input class="easyui-textbox" name="content"
                       data-options="label:'联系内容:',required:true"></td>
            <td></td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
</div>
<script>
    var dataobj = <?php echo json_encode($vnotes) ?>;
    $(function () {
        $('#form_vendor_showedit').form('load',dataobj);
    });
    function submitForm() {
        $('#form_vendor_showedit').form('submit',{
            success: function(){
                $('#win_main').window("close");
                $("#vnotes_list").datagrid("reload");
            }
        });
    }
</script>