<table class="easyui-datagrid" id="vnotes_list"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true"
       idField="vnotes_id" url="y.php?r=vnotes/getdata" toolbar="#toolbar_vnotes">
    <thead>
    <tr>
        <th field="title" width="60">主题</th>
        <th field="vendor_name" width="60">供应商</th>
        <th field="contact_time" width="80">联系时间</th>
        <th field="contact_type" width="60">联系类型</th>
    </tr>
    </thead>
</table>
<div id="toolbar_vnotes">
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加供应商联系记录','y.php?r=vnotes/showedit&vnotes_id=0')" /a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="vnotes_showedit()"/a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="vnotes_remove()" />
</div>
<script>
    function vnotes_showedit() {
        //获取选中的列
        var selected = $("#vnotes_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('编辑供应商联系记录','y.php?r=vnotes/showedit&vnotes_id='+selected.vnotes_id);
        }
    }
    function vnotes_remove() {
        var selected = $("#vnotes_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=vnotes/remove',{vnotes_id:selected.vnotes_id},function (res) {
                        $("#vnotes_list").datagrid("reload");
                    });
                }
            });
        }
    }
</script>