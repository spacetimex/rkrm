<table class="easyui-datagrid" id="purchaseorder_list"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true"
       idField="purchaseorder_id" url="y.php?r=purchaseorder/getdata" toolbar="#toolbar_purchaseorder">
    <thead>
    <tr>
        <th field="purchaseorder_num" width="60">进货单号</th>
        <th field="vendors" width="60">供应商</th>
        <th field="status" width="60">状态</th>
        <th field="purchase_date" width="60">采购日期</th>
        <th field="total" width="60">总计</th>
        <th field="paystatus" width="60">支付状态</th>
    </tr>
    </thead>
</table>
<div id="toolbar_purchaseorder">
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加进货单','y.php?r=purchaseorder/showedit&purchaseorder_id=0')" /a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="purchaseorder_showedit()"/a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="purchaseorder_remove()" />
</div>
<script>
    function purchaseorder_showedit() {
        //获取选中的列
        var selected = $("#purchaseorder_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('编辑进货单','y.php?r=purchaseorder/showedit&purchaseorder_id='+selected.purchaseorder_id);
        }
    }
    function purchaseorder_remove() {
        var selected = $("#purchaseorder_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=purchaseorder/remove',{purchaseorder_id:selected.purchaseorder_id},function (res) {
                        $("#purchaseorder_list").datagrid("reload");
                    });
                }
            });
        }
    }
</script>