<table class="easyui-datagrid" id="vendors_list"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true"
       idField="vendors_id" url="y.php?r=vendors/getdata" toolbar="#toolbar_vendors">
    <thead>
    <tr>
        <th field="vendor_name" width="60">供应商名称</th>
        <th field="industry" width="60">行业</th>
        <th field="phone" width="60">客户</th>
        <th field="website" width="60">销售阶段</th>
        <th field="city" width="60">成交可能性</th>
    </tr>
    </thead>
</table>
<div id="toolbar_vendors">
    <?php
    if (Yii::app()->user->checkAccess('vendors_module_add')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加供应商','y.php?r=vendors/showedit&vendors_id=0')" /a>
        <?php
    }
    if (Yii::app()->user->checkAccess('vendors_module_edit')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="vendors_showedit()"/a>
        <?php
    }
    if (Yii::app()->user->checkAccess('vendors_module_remove')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="vendors_remove()" />
        <?php
    }
    ?>
</div>
<script>
    function vendors_showedit() {
        //获取选中的列
        var selected = $("#vendors_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('编辑供应商','y.php?r=vendors/showedit&vendors_id='+selected.vendors_id);
        }
    }
    function vendors_remove() {
        var selected = $("#vendors_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=vendors/remove',{vendors_id:selected.vendors_id},function (res) {
                        $("#vendors_list").datagrid("reload");
                    });
                }
            });
        }
    }
</script>