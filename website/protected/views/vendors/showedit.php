<form id="form_vendor_showedit" method="post" action="y.php?r=vendors/savedata">
    <input type="hidden" name="vendors_id" value="">
    <table class="tab_form">
        <tr>
            <td><input class="easyui-textbox" name="industry"  data-options="label:'行业:',required:true"></td>
            <td><input class="easyui-textbox" name="phone"
                       data-options="label:'手机:',required:true"></td>
            <td><input class="easyui-textbox" name="website"
                       data-options="label:'网站:',required:true"></td>
        </tr>
        <tr>
            <td><input class="easyui-textbox" name="city"
                       data-options="label:'城市:',required:true"></td>
            <td><input class="easyui-textbox" name="vendor_name"
                       data-options="label:'供应商名称:',required:true"></td>
            <td></td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
</div>
<script>
    var dataobj = <?php echo json_encode($vendors) ?>;
    $(function () {
        $('#form_vendor_showedit').form('load',dataobj);
    });
    function submitForm() {
        $('#form_vendor_showedit').form('submit',{
            success: function(){
                $('#win_main').window("close");
                $("#vendors_list").datagrid("reload");
            }
        });
    }
</script>