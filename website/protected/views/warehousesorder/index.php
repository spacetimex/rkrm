<table class="easyui-datagrid" id="warehousesorder_list"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true"
       idField="warehousesorder_id" url="y.php?r=warehousesorder/getdata" toolbar="#toolbar_warehousesorder">
    <thead>
    <tr>
        <th field="warehousesorder_num" width="60">入库单号</th>
        <th field="purchaseorder" width="60">进货单</th>
        <th field="status" width="60">状态</th>
        <th field="amount" width="60">总计</th>
    </tr>
    </thead>
</table>
<div id="toolbar_warehousesorder">
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加入库单','y.php?r=warehousesorder/showedit&warehousesorder_id=0')" /a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="warehousesorder_showedit()"/a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="warehousesorder_remove()" />
</div>
<script>
    function warehousesorder_showedit() {
        //获取选中的列
        var selected = $("#warehousesorder_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('编辑入库单','y.php?r=warehousesorder/showedit&warehousesorder_id='+selected.warehousesorder_id);
        }
    }
    function warehousesorder_remove() {
        var selected = $("#warehousesorder_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=warehousesorder/remove',{warehousesorder_id:selected.warehousesorder_id},function (res) {
                        $("#warehousesorder_list").datagrid("reload");
                    });
                }
            });
        }
    }
</script>