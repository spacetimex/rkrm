<table class="easyui-datagrid" id="caigoutuihuos_list"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true"
       idField="caigoutuihuos_id" url="y.php?r=caigoutuihuos/getdata" toolbar="#toolbar_caigoutuihuos">
    <thead>
    <tr>
        <th field="title" width="60">主题</th>
        <th field="status" width="60">状态</th>
        <th field="vendors" width="60">供应商</th>
        <th field="purchaseorder" width="60">进货单</th>
        <th field="reason" width="60">原因</th>
    </tr>
    </thead>
</table>
<div id="toolbar_caigoutuihuos">
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加采购退货单','y.php?r=caigoutuihuos/showedit&caigoutuihuos_id=0')" /a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="caigoutuihuos_showedit()"/a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="caigoutuihuos_remove()" />
</div>
<script>
    function caigoutuihuos_showedit() {
        //获取选中的列
        var selected = $("#caigoutuihuos_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('编辑采购退货单','y.php?r=caigoutuihuos/showedit&caigoutuihuos_id='+selected.caigoutuihuos_id);
        }
    }
    function caigoutuihuos_remove() {
        var selected = $("#caigoutuihuos_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=caigoutuihuos/remove',{caigoutuihuos_id:selected.caigoutuihuos_id},function (res) {
                        $("#caigoutuihuos_list").datagrid("reload");
                    });
                }
            });
        }
    }
</script>