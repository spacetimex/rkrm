<table class="easyui-datagrid" id="caigous_list"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true"
       idField="caigous_id" url="y.php?r=caigous/getdata" toolbar="#toolbar_caigous">
    <thead>
    <tr>
        <th field="caigous_num" width="60">采购申请单</th>
        <th field="status" width="60">状态</th>
    </tr>
    </thead>
</table>
<div id="toolbar_caigous">
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加采购申请','y.php?r=caigous/showedit&caigous_id=0')" /a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="caigous_showedit()"/a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="caigous_remove()" />
</div>
<script>
    function caigous_showedit() {
        //获取选中的列
        var selected = $("#caigous_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('编辑采购申请','y.php?r=caigous/showedit&caigous_id='+selected.caigous_id);
        }
    }
    function caigous_remove() {
        var selected = $("#caigous_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=caigous/remove',{caigous_id:selected.caigous_id},function (res) {
                        $("#caigous_list").datagrid("reload");
                    });
                }
            });
        }
    }
</script>