<table class="easyui-datagrid" id="deliverys_list"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true"
       idField="deliverys_id" url="y.php?r=deliverys/getdata" toolbar="#toolbar_deliverys">
    <thead>
    <tr>
        <th field="deliverys_num" width="60">出库单</th>
        <th field="status" width="60">状态</th>
        <th field="salesorder" width="60">合同订单</th>
        <th field="invoice" width="60">发货单</th>
        <th field="deliverystime" width="60">出库时间</th>
    </tr>
    </thead>
</table>
<div id="toolbar_deliverys">
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加出库单','y.php?r=deliverys/showedit&deliverys_id=0')" /a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="deliverys_showedit()"/a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="deliverys_remove()" />
</div>
<script>
    function deliverys_showedit() {
        //获取选中的列
        var selected = $("#deliverys_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('编辑出库单','y.php?r=deliverys/showedit&deliverys_id='+selected.deliverys_id);
        }
    }
    function deliverys_remove() {
        var selected = $("#deliverys_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=deliverys/remove',{deliverys_id:selected.deliverys_id},function (res) {
                        $("#deliverys_list").datagrid("reload");
                    });
                }
            });
        }
    }
</script>