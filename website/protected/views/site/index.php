<div style="overflow-y: scroll;" id="site_index_div">
    <div id="home_portal" style="overflow-x: hidden;overflow-y: hidden;">
        <div style="width:33%;">
            <?php
            foreach ($widgets_first as $first){
                ?>
                <div title="<?php echo $first['widget_name']; ?>" style="height:350px;">
                    <?php
                    $this->renderPartial("../widget/".$first['widget_filename']);
                    ?>
                </div>
                <?php
            }
            ?>
        </div>
        <div style="width: 34%;">
            <?php
            foreach ($widgets_second as $second){
                ?>
                <div title="<?php echo $second['widget_name']; ?>" style="height:350px;">
                    <?php
                    $this->renderPartial("../widget/".$second['widget_filename']);
                    ?>
                </div>
                <?php
            }
            ?>
        </div>
        <div style="width: 34%;">
            <?php
            foreach ($widgets_third as $third){
                ?>
                <div title="<?php echo $third['widget_name']; ?>" style="height:350px;">
                    <?php
                    $this->renderPartial("../widget/".$third['widget_filename']);
                    ?>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <script>
        $('#home_portal').portal({
            border: false,
//            fit: true
        });
        var panels = $('#home_portal').portal('getPanels');
        for (var i = 0; i < panels.length; i++) {
            $('#home_portal').portal('disableDragging', panels[i]);
        }
        $(function () {
            $("#site_index_div").css("overflow", "hidden");
        });
    </script>
</div>