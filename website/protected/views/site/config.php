<form id="form_user_config_showedit" method="post" action="y.php?r=site/saveconfig">
    <input type="hidden" name="user_id" value="<?php echo $userinfo['user_id']; ?>">
    <input type="hidden" name="userimage" value="<?php echo $userinfo['userimage']; ?>">
    <table class="tab_form">
        <tr>
            <td>用户头像：</td>
            <td colspan="2">
                <img class="grayborder" src="css/images/avator/female.png" />
                <img class="grayborder" src="css/images/avator/male.png" />
            </td>
        </tr>
        <tr>
            <td><input class="easyui-textbox" name="password"
                       data-options="label:'新密码:'"></td>
            <td></td><td></td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
</div>
<script>
    $(function () {
        activeAvator();
        bindAvatorClick();
    });
    function activeAvator() {
        var imgs = $(".grayborder");
        var userimage = $("input[name=userimage]").val();
        for(var i=0;i<imgs.length;i++){
            //如果相等，增加class样式
            if($(imgs[i]).attr("src") == userimage){
                $(imgs[i]).addClass("blueborder");
                break;
            }
        }
    }
    function bindAvatorClick() {
        $(".grayborder").bind("click",function () {
            var src = $(this).attr("src");
            $("input[name=userimage]").val(src);
            $(".grayborder").removeClass("blueborder");
            $(this).addClass("blueborder");
        });
    }
    function submitForm() {
        $('#form_user_config_showedit').form('submit',{
            success: function(){
                //重新设置头像
                var userimage = $("input[name=userimage]").val();
                $("#myAvator").attr("src",userimage);
                $('#win_main').window("close");
            }
        });
    }
</script>