<div style="overflow-y: scroll;" id="site_index_div">
    <div id="home_portal" style="overflow-x: hidden;overflow-y: hidden;">
        <div style="width:50%;">
            <div title="统计数据" style="height:350px;">
                <table id="site_index_statistics_datagrid" class="easyui-datagrid"
                       data-options="border:false,rownumbers:true,singleSelect:true,method:'get',fitColumns:true,
                       toolbar:'#site_index_statistics',url:'y.php?r=portlet/statisticaldata&period=week'">
                    <thead>
                    <tr>
                        <th data-options="field:'statisticaltype',width:80">类型</th>
                        <th data-options="field:'statisticaldata',width:80">数据</th>
                    </tr>
                    </thead>
                </table>
                <div id="site_index_statistics" style="padding:2px 5px;">
                    <select id="site_index_statistics_cbo" class="easyui-combobox" panelHeight="auto" style="width:100px">
                        <option value="week">周</option>
                        <option value="month">月</option>
                        <option value="quarter">季</option>
                    </select>
                    <a href="#" class="easyui-linkbutton" onclick="switchPeriod()" iconCls="icon-search">筛选</a>
                </div>
                <script>
                    function switchPeriod() {
                        var period = $('#site_index_statistics_cbo').combobox('getValue');
                        $('#site_index_statistics_datagrid').datagrid({
                            url:'y.php?r=portlet/statisticaldata&period='+period
                        });
                        $("#site_index_statistics_datagrid").datagrid("reload");
                    }
                </script>
            </div>
            <!--<div title="公司应收款月度同比" style="height:200px;">

            </div>

            <div title="公司最近6个月销售情况" style="height:200px;">

            </div>
            <div title="库存资产" style="height:200px;">

            </div>-->
        </div>
        <div style="width: 50%;">
            <div title="公司公告" style="height:350px;">
                <table class="easyui-datagrid"
                       fit="true" border="false"
                       singleSelect="true" fitColumns="true" pagination="true"
                       idField="announcements_id" url="y.php?r=portlet/announcements" >
                    <thead>
                    <tr>
                        <th field="title" width="60" formatter="site_index_formatAnnouncementsTitle" >主题</th>
                        <th field="announcements_type" width="60">公告类型</th>
                        <th field="start_time" width="60">开始时间</th>
                        <th field="end_time" width="60">结束时间</th>
                    </tr>
                    </thead>
                </table>
                <script>
                    function site_index_formatAnnouncementsTitle(value,row,index) {
                        return "<a onclick='site_index_AnnouncementShow("+row.announcements_id+")'>"+value+"</a>";
                    }
                    function site_index_AnnouncementShow(announcements_id) {
                        loadWin('查看公告','y.php?r=announcements/view&announcements_id='+announcements_id);
                    }
                </script>
            </div>
        </div>
        <!--        <div style="width:34%;">-->
        <!--<div title="应收款应付款客户汇总" style="height:200px;">

        </div>
        <div title="公司最近6个月回款任务完成情况" style="height:200px;">

        </div>
        <div title="公司销售额月度同比" style="height:200px;">

        </div>
        <div title="金额较大的销售机会" style="height:200px;">

        </div>-->
        <!--        </div>-->
        <!--        <div style="width:33%;">-->
        <!--<div title="公司最近6个月销售任务完成情况" style="height:200px;">

        </div>
        <div title="日/周/月报" style="height:200px;">

        </div>
        <div title="公司年度销售情况" style="height:200px;">

        </div>
        <div title="销售漏斗" style="height:200px;">

        </div>-->
        <!--        </div>-->
    </div>
    <script>
        $('#home_portal').portal({
            border: false,
//            fit: true
        });
        var panels = $('#home_portal').portal('getPanels');
        for (var i = 0; i < panels.length; i++) {
            $('#home_portal').portal('disableDragging', panels[i]);
        }
        $(function () {
            $("#site_index_div").css("overflow", "hidden");
        });
    </script>
</div>