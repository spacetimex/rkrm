<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php
        echo $this->systemconfig['systemTitle']['config_value'];
        ?>
    </title>
    <link rel="shortcut icon" type="image/x-icon"
          href="<?php echo $this->systemconfig['systemWebIcon']['config_value']; ?>"/>
    <link rel="stylesheet" type="text/css" href="easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="easyui/themes/icon.css">
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    <script type="text/javascript" src="easyui/jquery.min.js"></script>
    <script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="easyui/plugin/portal/jquery.portal.js"></script>
    <script type="text/javascript" src="easyui/plugin/easyui.resize.js"></script>
    <script type="text/javascript" src="easyui/plugin/baseloading.js"></script>
    <link rel="stylesheet" type="text/css" href="easyui/plugin/portal/portal.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="easyui/easyui-lang-zh_CN.js"></script>
    <link rel="stylesheet" type="text/css" href="easyui/plugin/texteditor/texteditor.css">
    <script type="text/javascript" src="easyui/plugin/texteditor/jquery.texteditor.js"></script>
    <script type="text/javascript" src="css/app.js"></script>
</head>
<body id="layout_body" class="easyui-layout">
<div data-options="region:'north',split:false" style="height:50px;">
    <div class="easyui-panel menulink" data-options="fit:true,border:false"
         style="background: #73C8FF !important; color: #00438a !important;font-weight: bold;">
        <span class="fl">
            <img class="fl" src="<?php echo $this->systemconfig['systemLogo']['config_value']; ?>"
                 style="height:28px;">
            <span class="fl" style="display: inline-block;margin: 5px;">客户关系管理系统【企业版】</span>
        </span>
        <span class="fr">
                    <?php
                    if (Yii::app()->user->checkAccess('workspace')) {
                        ?>
                        <a class="easyui-menubutton"
                           data-options="menu:'#menu_workspace',iconCls:'icon-menu-chart'">工作台</a>
                        <?php
                    }
                    if (Yii::app()->user->checkAccess('campaigns')) {
                        ?>
                        <a class="easyui-menubutton" data-options="menu:'#menu_campaigns',iconCls:'icon-menu-voice'">营销</a>
                        <?php
                    }
                    if (Yii::app()->user->checkAccess('customer')) {
                        ?>
                        <a class="easyui-menubutton" data-options="menu:'#menu_customer',iconCls:'icon-menu-user'">客户</a>
                        <?php
                    }
                    if (Yii::app()->user->checkAccess('product')) {
                        ?>
                        <a class="easyui-menubutton" data-options="menu:'#menu_product',iconCls:'icon-menu-puzzle'">产品</a>
                        <?php
                    }
                    if (Yii::app()->user->checkAccess('sale')) {
                        ?>
                        <a class="easyui-menubutton" data-options="menu:'#menu_sale',iconCls:'icon-menu-increase'">销售</a>
                        <?php
                    }
                    if (Yii::app()->user->checkAccess('purchase')) {
                        ?>
                        <a class="easyui-menubutton" data-options="menu:'#menu_purchase',iconCls:'icon-menu-shopper'">采购</a>
                        <?php
                    }
                    if (Yii::app()->user->checkAccess('aftersale')) {
                        ?>
                        <a class="easyui-menubutton" data-options="menu:'#menu_aftersale',iconCls:'icon-menu-telephone'">售后</a>
                        <?php
                    }
                    if (Yii::app()->user->checkAccess('stock')) {
                        ?>
                        <a class="easyui-menubutton" data-options="menu:'#menu_stock',iconCls:'icon-menu-warehouse'">库存</a>
                        <?php
                    }
                    if (Yii::app()->user->checkAccess('finance')) {
                        ?>
                        <a class="easyui-menubutton" data-options="menu:'#menu_finance',iconCls:'icon-menu-money'">财务</a>
                        <?php
                    }
                    if (Yii::app()->user->checkAccess('sysconfig')) {
                        ?>
                        <a class="easyui-menubutton" data-options="menu:'#menu_sysconfig',iconCls:'icon-menu-cogs'">系统设置</a>
                        <?php
                    }
                    ?>
            <a href="#" class="easyui-menubutton " data-options="menu:'#menu_settings',iconCls:'icon-man'"><?php
                echo Yii::app()->user->realname;
                ?></a>
        </span>
    </div>
    <div id="menu_workspace" class="menunavbar" data-options="noline:true,itemHeight:30" style="width:100px;">
        <?php
        if (Yii::app()->user->checkAccess('workspace_site')) {
            ?>
            <div><a href="javascript:showTab('控制台','y.php?r=site/index')">控制台</a></div>
            <?php
        }
        if (Yii::app()->user->checkAccess('workspace_announcements')) {
            ?>
            <div><a href="javascript:showTab('公告','y.php?r=announcements/index')">公告</a></div>
            <?php
        }
        if (Yii::app()->user->checkAccess('workspace_dailylogs')) {
            ?>
            <div><a href="javascript:showTab('日报','y.php?r=dailylogs/index')">日报</a></div>
            <?php
        }
        ?>
        <div><a href="javascript:showTab('周报','y.php?r=weeklylogs/index')">周报</a></div>
        <div><a href="javascript:showTab('月报','y.php?r=monthlylogs/index')">月报</a></div>
        <div><a href="javascript:showTab('工作流','y.php?r=myworkflow/index')">工作流</a></div>
    </div>
    <div id="menu_campaigns" class="menunavbar" data-options="noline:true,itemHeight:30"  style="width: 100px;">
        <?php
        if (Yii::app()->user->checkAccess('campaigns_module')) {
            ?>
            <div><a href="javascript:showTab('营销活动','y.php?r=campaigns/index')">营销活动</a></div>
            <?php
        }
        ?>
        <div><a href="javascript:showTab('营销群','y.php?r=marketinggroup/index')">营销群</a></div>
        <div><a href="javascript:showTab('群发短信','y.php?r=messagegroup/index')">群发短信</a></div>
        <div><a href="javascript:showTab('群发邮件','y.php?r=emailgroup/index')">群发邮件</a></div>
    </div>
    <div id="menu_customer" class="menunavbar" data-options="noline:true,itemHeight:30"  style="width: 100px;">
        <?php
        if (Yii::app()->user->checkAccess('customer_module')) {
            ?>
            <div><a href="javascript:showTab('客户','y.php?r=customer/index')">客户</a></div>
            <?php
        }
        if (Yii::app()->user->checkAccess('contacter_module')) {
            ?>
            <div><a href="javascript:showTab('联系人','y.php?r=contacter/index')">联系人</a></div>
            <?php
        }
        if (Yii::app()->user->checkAccess('contactlog_module')) {
            ?>
            <div><a href="javascript:showTab('联系记录','y.php?r=contactlog/index')">联系记录</a></div>
            <?php
        }
        if (Yii::app()->user->checkAccess('memdays_module')) {
        ?>
        <div><a href="javascript:showTab('纪念日','y.php?r=memdays/index')">纪念日</a></div>
    <?php
    }
    ?>
    </div>
    <div id="menu_product" class="menunavbar" data-options="noline:true,itemHeight:30"  style="width: 100px;">
        <?php
        if (Yii::app()->user->checkAccess('product_module')) {
            ?>
            <div><a href="javascript:showTab('产品','y.php?r=product/index')">产品</a></div>
            <?php
        }
        if (Yii::app()->user->checkAccess('productcategory_module')) {
            ?>
            <div><a href="javascript:showTab('产品分类','y.php?r=productcategory/index')">产品分类</a>
            </div>
            <?php
        }
        ?>
    </div>
    <div id="menu_sale" class="menunavbar" data-options="noline:true,itemHeight:30"  style="width: 100px;">
        <?php
        if (Yii::app()->user->checkAccess('potentials_module')) {
            ?>
            <div><a href="javascript:showTab('销售机会','y.php?r=potentials/index')">销售机会</a></div>
            <?php
        }
        if (Yii::app()->user->checkAccess('quotes_module')) {
            ?>
            <div><a href="javascript:showTab('报价单','y.php?r=quotes/index')">报价单</a></div>
            <?php
        }
        if (Yii::app()->user->checkAccess('competitor_module')) {
            ?>
            <div><a href="javascript:showTab('竞争对手','y.php?r=competitor/index')">竞争对手</a></div>
            <?php
        }
        ?>
        <div><a href="javascript:showTab('合同订单','y.php?r=salesorder/index')">合同订单</a></div>
        <div><a href="javascript:showTab('发货单','y.php?r=invoice/index')">发货单</a></div>
        <div><a href="javascript:showTab('销售退货单','y.php?r=tuihuo/index')">销售退货单</a></div>
    </div>
    <div id="menu_purchase" class="menunavbar" data-options="noline:true,itemHeight:30"  style="width: 100px;">
        <div><a href="javascript:showTab('采购申请','y.php?r=caigous/index')">采购申请</a></div>
        <div><a href="javascript:showTab('进货单','y.php?r=purchasedrder/index')">进货单</a></div>
        <?php
        if (Yii::app()->user->checkAccess('vendors_module')) {
            ?>
            <div><a href="javascript:showTab('供应商','y.php?r=vendors/index')">供应商</a></div>
            <?php
        }
        if (Yii::app()->user->checkAccess('vcontacts_module')) {
            ?>
            <div><a href="javascript:showTab('供应商联系人','y.php?r=vcontacts/index')">供应商联系人</a>
            </div>
            <?php
        }
        ?>
        <div><a href="javascript:showTab('供应商联系记录','y.php?r=vnotes/index')">供应商联系记录</a></div>
        <div><a href="javascript:showTab('采购退货单','y.php?r=caigoutuihuos/index')">采购退货单</a>
        </div>
    </div>
    <div id="menu_aftersale" class="menunavbar" data-options="noline:true,itemHeight:30"  style="width: 100px;">
        <?php
        if (Yii::app()->user->checkAccess('accountrecordss_module')) {
            ?>
            <div><a href="javascript:showTab('客户服务','y.php?r=accountrecordss/index')">客户服务</a>
            </div>
            <?php
        }
        if (Yii::app()->user->checkAccess('complaints_module')) {
            ?>
            <div><a href="javascript:showTab('客户投诉','y.php?r=complaints/index')">客户投诉</a></div>
            <?php
        }
        if (Yii::app()->user->checkAccess('faq_module')) {
            ?>
            <div><a href="javascript:showTab('常见问题','y.php?r=faq/index')">常见问题</a></div>
            <?php
        }
        if (Yii::app()->user->checkAccess('faqcategory_module')) {
            ?>
            <div><a href="javascript:showTab('常见问题分类','y.php?r=faqcategory/index')">常见问题分类</a>
            </div>
            <?php
        }
        ?>
    </div>
    <div id="menu_stock" class="menunavbar" data-options="noline:true,itemHeight:30"  style="width: 100px;">
        <div><a href="javascript:showTab('入库单','y.php?r=warehousesorder/index')">入库单</a></div>
        <div><a href="javascript:showTab('出库单','y.php?r=deliverys/index')">出库单</a></div>
        <div><a href="javascript:showTab('盘点','y.php?r=checks/index')">盘点</a></div>
        <div><a href="javascript:showTab('库存余额','y.php?r=balances/index')">库存余额</a></div>
        <div><a href="javascript:showTab('初始化库存','y.php?r=prdtnums/index')">初始化库存</a></div>
        <div><a href="javascript:showTab('库间调拨','y.php?r=warehousetransfers/index')">库间调拨</a></div>
        <div><a href="javascript:showTab('库存流水账','y.php?r=wareledgers/index')">库存流水账</a></div>
    </div>
    <div id="menu_finance" class="menunavbar" data-options="noline:true,itemHeight:30"  style="width: 100px;">
        <div><a href="javascript:showTab('应收款','y.php?r=gathers/index')">应收款</a></div>
        <div><a href="javascript:showTab('应付款','y.php?r=charges/index')">应付款</a></div>
        <div><a href="javascript:showTab('付款单','y.php?r=chargesrecords/index')">付款单</a></div>
        <div><a href="javascript:showTab('往来账','y.php?r=forthamounts/index')">往来账</a></div>
        <div><a href="javascript:showTab('进项发票','y.php?r=pobills/index')">进项发票</a></div>
        <div><a href="javascript:showTab('期初余额','y.php?r=openingbalancess/index')">期初余额</a>
        </div>
        <div><a href="javascript:showTab('费用报销','y.php?r=expenses/index')">费用报销</a></div>
        <div><a href="javascript:showTab('发票管理','y.php?r=billings/index')">发票管理</a></div>
    </div>
    <div id="menu_sysconfig" class="menunavbar" data-options="noline:true,itemHeight:30"  style="width: 100px;">
        <?php
        if (Yii::app()->user->checkAccess('sysconfig_user_module')) {
            ?>
            <div><a href="javascript:showTab('系统用户','y.php?r=settings/user/index')">系统用户</a>
            </div>
            <?php
        }
        if (Yii::app()->user->checkAccess('sysconfig_role_module')) {
            ?>
            <div><a href="javascript:showTab('角色权限','y.php?r=settings/role/index')">角色权限</a>
            </div>
            <?php
        }
        if (Yii::app()->user->checkAccess('sysconfig_picklist_module')) {
            ?>
            <div><a href="javascript:showTab('数据字典','y.php?r=settings/picklist/index')">数据字典</a></div>
            <div><a href="javascript:showTab('字段管理','y.php?r=settings/column/index')">字段管理</a></div>
            <div><a href="javascript:showTab('表单排版','y.php?r=settings/panel/index')">表单排版</a></div>
            <div><a href="javascript:showTab('视图管理','y.php?r=settings/view/index')">视图管理</a></div>
            <div><a href="javascript:showTab('首页组件','y.php?r=settings/view/index')">首页组件</a></div>
            <?php
        }
        if (Yii::app()->user->checkAccess('sysconfig_messager_module')) {
            ?>
            <div><a href="javascript:showTab('短信平台','y.php?r=settings/messager/index')">短信平台</a></div>
            <?php
        }
        if (Yii::app()->user->checkAccess('sysconfig_messagetemplate_module')) {
            ?>
            <div><a href="javascript:showTab('短信模板','y.php?r=settings/messagetemplate/index')">短信模板</a>
            </div>
            <?php
        }
        ?>
        <div><a href="javascript:showTab('邮箱设置','y.php?r=settings/emailconfig/index')">邮箱设置</a></div>
        <div><a href="javascript:showTab('邮件模板','y.php?r=settings/emailtemplate/index')">邮件模板</a></div>
        <?php
        if (Yii::app()->user->checkAccess('sysconfig_loginhistory_module')) {
            ?>
            <div><a href="javascript:showTab('登入历史','y.php?r=settings/loginhistory/index')">登入历史</a>
            </div>
            <?php
        }
        if (Yii::app()->user->checkAccess('sysconfig_system_module')) {
            ?>
            <div><a href="javascript:showTab('系统配置','y.php?r=settings/system/index')">系统配置</a>
            </div>
            <?php
        }
        if (Yii::app()->user->checkAccess('sysconfig_workflow_module')) {
            ?>
            <div><a href="javascript:showTab('审批流程','y.php?r=settings/workflow/index')">审批流程</a>
            </div>
            <?php
        }
        if (Yii::app()->user->checkAccess('sysconfig_workflow_module')) {
            ?>
            <div><a href="javascript:showTab('流程模块','y.php?r=settings/workflowmodule/index')">流程模块</a>
            </div>
            <?php
        }
        if (Yii::app()->user->checkAccess('sysconfig_warehouse_module')) {
            ?>
            <div><a href="javascript:showTab('多仓库管理','y.php?r=settings/warehouse/index')">多仓库管理</a></div>
            <?php
        }
        ?>
    </div>
    <div id="menu_settings" class="menunavbar" data-options="noline:true,itemHeight:30"  style="width: 100px;">
        <div><a href="y.php?r=site/loginout">退出</a></div>
            <?php
            if (Yii::app()->user->checkAccess('personalconfig_config')) {
                ?>
                <div><a href="javascript:loadWin('个人设置','y.php?r=site/config')">个人设置</a></div>
                <?php
            }
            ?>
    </div>
</div>
<div data-options="region:'center',border:false">
    <div id="win_main" class="easyui-window" title=""
         data-options="modal:true,closed:true" style="overflow-x:hidden;">
    </div>
    <div id="win_main_2" class="easyui-window" title=""
         data-options="modal:true,closed:true">
    </div>
    <div class="easyui-tabs" data-options="fit:'true',tools:'#tab-tools'" id="sys_tab">

    </div>
    <div id="tab-tools">
        <a href="javascript:void(0)" class="easyui-linkbutton" title="刷新"
           data-options="plain:true,iconCls:'icon-reload'" onclick="reloadCurrentPanel()"></a>
    </div>
</div>
<script>
    $(function () {
        $('#sys_tab').tabs('add', {
            'title': '控制台',
            'href': 'y.php?r=site/index'
        });
        $('#layout_body').layout('hidden', 'east');
//        $('#layout_body').layout('unsplit','east');
        $(".menu_accordion").find("ul li").bind("click", function () {
            //将其他的样式背景的active处理掉，然后在当前的li上面加上active
            $(".menu_accordion").find("ul li").removeClass("checkactive");
            $(this).closest("li").addClass("checkactive");
        });

        removeLastBeforeCss();

    });

    function showTab(title, url) {
        var tab = $('#sys_tab').tabs('getTab', title);
        if (tab != null) {
            $('#sys_tab').tabs('select', title);
        } else {
            $('#sys_tab').tabs('add', {
                'title': title,
                'href': url,
                'closable': true
            });
            $('#layout_body').layout('hidden', 'east');
        }
    }

    function layoutresize() {
        $("body").onresize(function () {
            $('#layout_body').layout("resize");
        });
    }

    function loadWin(title, url) {
        $('#win_main').window({
            width: $(window).width() * 0.618,
            height: $(window).height() * 0.618,
            modal: true,
            title: title,
            href: url,
            collapsible: false,
            minimizable: false,
            maximizable: false
        });
        $('#win_main').window('center');
        $('#win_main').window('open');
    }

    function loadWin2(title, url) {
        $('#win_main_2').window({
            width: $(window).width() * 0.618 * 0.618,
            height: $(window).height() * 0.618 * 0.618,
            modal: true,
            title: title,
            href: url,
            collapsible: false,
            minimizable: false,
            maximizable: false
        });
        $('#win_main_2').window('center');
        $('#win_main_2').window('open');
    }

    function removeLastBeforeCss() {
        var accordions = $(".menu_accordion");
        var lastli = null;
        for (var i = 0; i < accordions.length; i++) {
            lastli = $(accordions[i]).find("li:last");
            $(lastli).removeClass("before");
        }
    }

    function reloadCurrentPanel() {
        var selected = $('#sys_tab').tabs('getSelected');
        $(selected).panel('refresh');
    }
</script>
</body>
</html>
