<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php
        echo $this->systemconfig['systemTitle']['config_value'];
        ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="css/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="easyui/themes/gray/easyui.css">
    <link rel="stylesheet" type="text/css" href="easyui/themes/icon.css">
    <script type="text/javascript" src="easyui/jquery.min.js"></script>
    <script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="easyui/plugin/portal/jquery.portal.js"></script>
    <script type="text/javascript" src="easyui/plugin/easyui.resize.js"></script>
    <link rel="stylesheet" type="text/css" href="easyui/plugin/portal/portal.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="easyui/easyui-lang-zh_CN.js"></script>
    <link rel="stylesheet" type="text/css" href="easyui/plugin/texteditor/texteditor.css">
    <script type="text/javascript" src="easyui/plugin/texteditor/jquery.texteditor.js"></script>
    <style>
        body {
            font-family: "Source Sans Pro", Calibri, Candara, Arial, sans-serif;
            font-size: 12px;
            background-color: #eff0f4 !important;
        }

        .tac {
            text-align: center;
        }

        table.form {
            margin-top:24px;
            display: inline-block;
            padding: 16px 36px 36px;
            background: #fff;
            border-radius:6px !important;
            -moz-border-radius:6px !important;;
            -webkit-border-radius:6px !important;;

        }
        .divform{
            box-shadow: 0 0 2px 1px rgba(0, 0, 0, .12);
            border-bottom: 1px solid rgba(50, 50, 50, .33);
        }


        table.form tr td {
            padding:8px;
            /*font-weight: bold;*/
            font-size: 14px;
            text-align: left;
            /*color: #FFFFFF;*/
            /*border: 1px solid gainsboro;*/
        }

        table.form tr td input, table.form tr td select {
            width: 200px;
        }
        .radius{

        }
        body{
            background-color: #DFE0E2;
            left: 0;
            right: 0;
            top: 0;
            z-index: -1;
        }
        .textbox-label{
            font-size: 16px;
            font-weight: normal !important;
        }
        .submitbtn{
            background-color: #0269B1;
        }
        .submitbtn  .l-btn-text{
            line-height: 30px;
            height: 30px;
            font-size: 14px;
        }
        .copyright{
            line-height: 30px;
            height: 30px;
            color: #478FCA !important;
            font-size: 16px;
        }
        .system{
            color:#777 !important;
            font-size: 30px !important;
        }
        .textbox-text{
            height: 25px;
            line-height: 25px;
            width: 200px;
        }
        .textbox-text .validatebox-text{
            background: #FFFFFF;
        }
    </style>
</head>
<body>
<div class="tac">
    <div class="tac" style="margin-top: 6%;">&nbsp;</div>
    <form id="form_login" method="post">
        <div class="tac"><img src="<?php
            echo $this->systemconfig['systemLoginIcon']['config_value'];
            ?>" width="50px;" style="margin-bottom: -12px;margin-left: -12px;"><span class="system">客户关系管理系统</span></div>
        <div class="tac copyright">©&nbsp;&nbsp;无锡蓝科创想科技有限公司</div>
        <div class="radius">
            <table class="form divform">
                <tr>
                    <td class="tac" style="text-align: center;font-size: 16px;">请输入用户名和密码</td>
                </tr>
                <tr>
                    <td>账&nbsp;户</td>
                </tr>
                <tr>
                    <td><input class="easyui-textbox" name="username" placeholder="账户"
                               data-options="required:true,iconCls:'icon-man'" value="ranko" style="width: 250px;"></td>
                </tr>
                <tr>
                    <td>密&nbsp;码</td>
                </tr>
                <tr>
                    <td><input class="easyui-textbox" name="password"
                               data-options="required:true,type:'password',iconCls:'icon-lock'" placeholder="密码" value="123456" style="width: 250px;"></td>
                </tr>
                <tr>
                    <td><a class="easyui-linkbutton submitbtn"onclick="form_login()" style="display: block;background: #298ECE;color: #FFF;">登&nbsp;&nbsp;入</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" >
                        管理员账号：ranko/123456</br>
                        销售人员：salesman/123456</br>
                        客户人员：servicestuff/123456</br>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</div>
<script>
    $(function () {
        $('#form_login').form({
            url: "y.php?r=site/login",
            onSubmit: function () {
                return $("#form_login").form("validate");
            },
            success: function (data) {
                var json = eval('(' + data + ')');
                if (json.code == '10000') {
                    window.location.href = "y.php?r=site/home";
                } else {
                    //提示信息
                    alert(json.message);
                }
            }
        });
    });
    function form_login() {
        $('#form_login').form("submit");
    }
</script>
</body>
</html>