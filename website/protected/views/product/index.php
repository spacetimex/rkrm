<table id="product_list" class="easyui-datagrid"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true" toolbar="#toolbar_product"
       idField="product_id" url="y.php?r=product/getdata">
    <thead>
    <tr>
        <th field="productname" width="60">产品</th>
        <th field="productcategoryname" width="60">产品分类</th>
        <th field="weight" width="60">重量</th>
        <th field="unit" width="60">单位</th>
        <th field="model" width="60">型号</th>
        <th field="unitprice" width="60">单价</th>
        <th field="costprice" width="60">成本价</th>
        <th field="saleprice" width="60">销售价</th>
        <th field="description" width="60">描述</th>
    </tr>
    </thead>
</table>
<div id="toolbar_product">
    <?php
    if (Yii::app()->user->checkAccess('product_module_view')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-view',plain:true" onclick="product_view()" /a>
        <?php
    }
    if (Yii::app()->user->checkAccess('product_module_add')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加产品','y.php?r=product/showedit&product_id=0')" /a>
        <?php
    }
    if (Yii::app()->user->checkAccess('product_module_edit')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="product_showedit()"/a>
        <?php
    }
    if (Yii::app()->user->checkAccess('product_module_remove')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="product_remove()" />
        <?php
    }
    ?>
</div>
<script>
    function product_showedit() {
        //获取选中的列
        var selected = $("#product_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('添加产品','y.php?r=product/showedit&product_id='+selected.product_id);
        }
    }
    function product_remove() {
        var selected = $("#product_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=product/remove',{product_id:selected.product_id},function (res) {
                        $("#product_list").datagrid("reload");
                    });
                }
            });
        }
    }
    function product_view() {
        //获取选中的列
        var selected = $("#product_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('查看产品','y.php?r=product/view&product_id='+selected.product_id);
        }
    }
</script>
