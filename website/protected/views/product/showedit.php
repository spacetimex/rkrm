<form id="form_product_showedit" method="post" action="y.php?r=product/savedata">
    <input type="hidden" name="product_id" value="">
    <table class="tab_form">
        <tr>
            <td><input class="easyui-textbox" name="productname"  data-options="label:'产品名称:',required:true"></td>
            <td><select class="easyui-combobox" name="productcategory_id" url="y.php?r=productcategory/getcomboboxdata" data-options="label:'产品分类:',required:true,
        valueField: 'value',
        textField: 'label',
" >
                </select></td>
            <td><input class="easyui-textbox" name="weight"  data-options="label:'重量:',required:true"></td>
        </tr>
        <tr>
            <td><input class="easyui-textbox" name="unit"  data-options="label:'单位:',required:true"></td>
            <td><input class="easyui-textbox" name="model"  data-options="label:'型号:',required:true"></td>
            <td><input class="easyui-textbox" name="unitprice"  data-options="label:'单价:',required:true"></td>
        </tr>
        <tr>
            <td><input class="easyui-textbox" name="costprice"  data-options="label:'成本价:',required:true"></td>
            <td><input class="easyui-textbox" name="saleprice"  data-options="label:'销售价:',required:true"></td>
            <td><input class="easyui-textbox" name="description"  data-options="label:'描述:',required:true"></td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">清空</a>
</div>
<script>
    var dataobj = <?php echo json_encode($product) ?>;
    $(function () {
        $('#form_product_showedit').form('load',dataobj);
    });
    function submitForm() {
        $('#form_product_showedit').form('submit',{
            success: function(){
                $('#win_main').window("close");
                $("#product_list").datagrid("reload");
            }
        });
    }
    function clearForm() {
        $('#form_product_showedit').form('clear');
        if(dataobj!=null){
            $("input[name=product_id]").val(dataobj.product_id);
        }
    }
</script>