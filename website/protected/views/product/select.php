<table id="product_select_datagrid" class="easyui-datagrid"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true"
       idField="product_id" url="y.php?r=product/getdata" data-options="onClickRow:product_select_onClickRow,singleSelect:true">
    <thead>
    <tr>
        <th field="productname" width="60">产品</th>
        <th field="productcategoryname" width="60">产品分类</th>
        <th field="weight" width="60">重量</th>
        <th field="unit" width="60">单位</th>
        <th field="model" width="60">型号</th>
        <th field="unitprice" width="60">单价</th>
        <th field="costprice" width="60">成本价</th>
        <th field="saleprice" width="60">销售价</th>
        <th field="description" width="60">描述</th>
    </tr>
    </thead>
</table>
<script>
    function product_select_onClickRow(index, row) {
        var form = '<?php echo $form ?>';
        var fieldname = '<?php echo $fieldname ?>';
        var fieldid =  '<?php echo $fieldid ?>';
        var rowdata = $("#product_select_datagrid").datagrid("getSelected");
        $("#"+form).find("input[name="+fieldname+"]").val(rowdata.product_id);
        $("#"+fieldid).searchbox('setValue',rowdata.productname);
        $('#win_main_2').window("close");
    }
</script>