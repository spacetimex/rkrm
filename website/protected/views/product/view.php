<table class="tab_form">
    <tr>
        <td>产品：<?php echo $product['productname']; ?></td>
        <td>
            产品分类：<?php echo $product['productcategoryname']; ?>
        </td>
        <td>重量：<?php echo $product['weight']; ?></td>
    </tr>
    <tr>
        <td>单位：<?php echo $product['unit']; ?></td>
        <td>
            型号：<?php echo $product['model']; ?>
        </td>
        <td>单价：<?php echo $product['unitprice']; ?></td>
    </tr>
    <tr>
        <td>成本价：<?php echo $product['costprice']; ?></td>
        <td>
            销售价：<?php echo $product['saleprice']; ?>
        </td>
        <td>描述：<?php echo $product['description']; ?></td>
    </tr>
</table>