<table class="easyui-datagrid"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true"
       idField="announcements_id" url="y.php?r=portlet/announcements">
    <thead>
    <tr>
        <th field="title" width="60" formatter="site_index_formatAnnouncementsTitle">主题</th>
        <th field="announcements_type" width="60">公告类型</th>
        <th field="start_time" width="60">开始时间</th>
        <th field="end_time" width="60">结束时间</th>
    </tr>
    </thead>
</table>
<script>
    function site_index_formatAnnouncementsTitle(value, row, index) {
        return "<a onclick='site_index_AnnouncementShow(" + row.announcements_id + ")'>" + value + "</a>";
    }

    function site_index_AnnouncementShow(announcements_id) {
        loadWin('查看公告', 'y.php?r=announcements/view&announcements_id=' + announcements_id);
    }
</script>