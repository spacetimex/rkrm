<table id="site_index_statistics_datagrid" class="easyui-datagrid"
       data-options="border:false,rownumbers:true,singleSelect:true,method:'get',fitColumns:true,
                       toolbar:'#site_index_statistics',url:'y.php?r=portlet/statisticaldata&period=week'">
    <thead>
    <tr>
        <th data-options="field:'statisticaltype',width:80">类型</th>
        <th data-options="field:'statisticaldata',width:80">数据</th>
    </tr>
    </thead>
</table>
<div id="site_index_statistics" style="padding:2px 5px;">
    <select id="site_index_statistics_cbo" class="easyui-combobox" panelHeight="auto"
            style="width:100px">
        <option value="week">周</option>
        <option value="month">月</option>
        <option value="quarter">季</option>
    </select>
    <a href="#" class="easyui-linkbutton" onclick="switchPeriod()" iconCls="icon-search">筛选</a>
</div>
<script>
    function switchPeriod() {
        var period = $('#site_index_statistics_cbo').combobox('getValue');
        $('#site_index_statistics_datagrid').datagrid({
            url: 'y.php?r=portlet/statisticaldata&period=' + period
        });
        $("#site_index_statistics_datagrid").datagrid("reload");
    }
</script>