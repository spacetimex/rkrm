<table class="easyui-datagrid" id="messagegroup_list"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true"
       idField="messagegroup_id" url="y.php?r=messagegroup/getdata" toolbar="#toolbar_messagegroup">
    <thead>
    <tr>
        <th field="title" width="60">标题</th>
        <th field="marketinggroup" width="60">营销群</th>
        <th field="messagetemplate" width="60">模板</th>
        <th field="status" width="60">状态</th>
        <th field="sendtime" width="60">发送时间</th>
    </tr>
    </thead>
</table>
<div id="toolbar_messagegroup">
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加群发短信','y.php?r=messagegroup/showedit&messagegroup_id=0')" /a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="messagegroup_showedit()"/a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="messagegroup_remove()" />
</div>
<script>
    function messagegroup_showedit() {
        //获取选中的列
        var selected = $("#messagegroup_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('编辑群发短信','y.php?r=messagegroup/showedit&messagegroup_id='+selected.messagegroup_id);
        }
    }
    function messagegroup_remove() {
        var selected = $("#messagegroup_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=messagegroup/remove',{messagegroup_id:selected.messagegroup_id},function (res) {
                        $("#messagegroup_list").datagrid("reload");
                    });
                }
            });
        }
    }
</script>