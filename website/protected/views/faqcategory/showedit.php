<form id="form_faqcategory_showedit" method="post" action="y.php?r=faqcategory/savedata">
    <input type="hidden" name="faqcategory_id" value="">
    <table class="tab_form">
        <tr>
            <td><input class="easyui-textbox" name="faqcategoryname"  data-options="label:'问题分类:',required:true"></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">清空</a>
</div>
<script>
    var dataobj = <?php echo json_encode($faqcategory) ?>;
    $(function () {
        $('#form_faqcategory_showedit').form('load',dataobj);
    });
    function submitForm() {
        $('#form_faqcategory_showedit').form('submit',{
            success: function(){
                $('#win_main').window("close");
                $("#faqcategory_list").datagrid("reload");
            }
        });
    }
    function clearForm() {
        $('#form_faqcategory_showedit').form('clear');
        if(dataobj!=null){
            $("input[name=faqcategory_id]").val(dataobj.faqcategory_id);
        }
    }
</script>