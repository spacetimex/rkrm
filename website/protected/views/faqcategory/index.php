<table id="faqcategory_list" class="easyui-datagrid"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true" toolbar="#toolbar_faqcategory"
       idField="faqcategory_id" url="y.php?r=faqcategory/getdata">
    <thead>
    <tr>
        <th field="faqcategoryname" width="60">常见问题分类</th>
    </tr>
    </thead>
</table>
<div id="toolbar_faqcategory">
    <?php
    if (Yii::app()->user->checkAccess('faqcategory_module_add')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加问题分类','y.php?r=faqcategory/showedit&faqcategory_id=0')" /a>
        <?php
    }
    if (Yii::app()->user->checkAccess('faqcategory_module_edit')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="faqcategory_showedit()"/a>
        <?php
    }
    ?>
</div>
<script>
    function faqcategory_showedit() {
        //获取选中的列
        var selected = $("#faqcategory_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('添加问题分类','y.php?r=faqcategory/showedit&faqcategory_id='+selected.faqcategory_id);
        }
    }
</script>
