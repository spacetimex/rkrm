<form id="form_productcategory_showedit" method="post" action="y.php?r=productcategory/savedata">
    <input type="hidden" name="productcategory_id" value="">
    <table class="tab_form">
        <tr>
            <td><input class="easyui-textbox" name="productcategoryname" data-options="label:'产品分类:',required:true"></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">清空</a>
</div>
<script>
    var dataobj = <?php echo json_encode($productcategory) ?>;
    $(function () {
        $('#form_productcategory_showedit').form('load',dataobj);
    });
    function submitForm() {
        $('#form_productcategory_showedit').form('submit',{
            success: function(){
                $('#win_main').window("close");
                $("#productcategory_list").datagrid("reload");
            }
        });
    }
    function clearForm() {
        $('#form_productcategory_showedit').form('clear');
        if(dataobj!=null){
            $("input[name=productcategory_id]").val(dataobj.productcategory_id);
        }
    }
</script>