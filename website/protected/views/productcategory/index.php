<table id="productcategory_list" class="easyui-datagrid"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true" toolbar="#toolbar_productcategory"
       idField="productcategory_id" url="y.php?r=productcategory/getdata">
    <thead>
    <tr>
        <th field="productcategoryname" width="60">产品分类</th>
    </tr>
    </thead>
</table>
<div id="toolbar_productcategory">
    <?php
    if (Yii::app()->user->checkAccess('productcategory_module_add')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加产品分类','y.php?r=productcategory/showedit&productcategory_id=0')" /a>
        <?php
    }
    if (Yii::app()->user->checkAccess('productcategory_module_edit')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="productcategory_showedit()"/a>
        <?php
    }
    if (Yii::app()->user->checkAccess('productcategory_module_remove')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="productcategory_remove()" />
        <?php
    }
    ?>
</div>
<script>
    function productcategory_showedit() {
        //获取选中的列
        var selected = $("#productcategory_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('添加产品分类','y.php?r=productcategory/showedit&productcategory_id='+selected.productcategory_id);
        }
    }
    function productcategory_remove() {
        var selected = $("#productcategory_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=productcategory/remove',{productcategory_id:selected.productcategory_id},function (res) {
                        $("#productcategory_list").datagrid("reload");
                    });
                }
            });
        }
    }
</script>
