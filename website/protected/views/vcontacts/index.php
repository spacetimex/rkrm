<table class="easyui-datagrid" id="vcontact_list"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true"
       idField="vcontacts_id" url="y.php?r=vcontacts/getdata" toolbar="#toolbar_vcontact">
    <thead>
    <tr>
        <th field="vcontacts_name" width="60">供应商联系人</th>
        <th field="phone" width="60">电话</th>
    </tr>
    </thead>
</table>
<div id="toolbar_vcontact">
    <?php
    if (Yii::app()->user->checkAccess('vcontacts_module_add')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加供应商联系人','y.php?r=vcontacts/showedit&vcontacts_id=0')" /a>
        <?php
    }
    if (Yii::app()->user->checkAccess('vcontacts_module_edit')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="vcontact_showedit()"/a>
        <?php
    }
    if (Yii::app()->user->checkAccess('vcontacts_module_remove')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="vcontact_remove()" />
        <?php
    }
    ?>
</div>
<script>
    function vcontact_showedit() {
        //获取选中的列
        var selected = $("#vcontact_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('编辑供应商联系人','y.php?r=vcontacts/showedit&vcontacts_id='+selected.vcontacts_id);
        }
    }
    function vcontact_remove() {
        var selected = $("#vcontact_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=vcontacts/remove',{vcontacts_id:selected.vcontacts_id},function (res) {
                        $("#vcontact_list").datagrid("reload");
                    });
                }
            });
        }
    }
</script>