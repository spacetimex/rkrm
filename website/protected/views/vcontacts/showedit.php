<form id="form_vcontact_showedit" method="post" action="y.php?r=vcontacts/savedata">
    <input type="hidden" name="vcontacts_id" value="">
    <table class="tab_form">
        <tr>
            <td><input class="easyui-textbox" name="vcontacts_name"  data-options="label:'供应商联系人:',required:true"></td>
            <td><input class="easyui-textbox" name="phone"
                       data-options="label:'手机:',required:true"></td>
            <td></td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
</div>
<script>
    var dataobj = <?php echo json_encode($vcontacts) ?>;
    $(function () {
        $('#form_vcontact_showedit').form('load',dataobj);
    });
    function submitForm() {
        $('#form_vcontact_showedit').form('submit',{
            success: function(){
                $('#win_main').window("close");
                $("#vcontact_list").datagrid("reload");
            }
        });
    }
</script>