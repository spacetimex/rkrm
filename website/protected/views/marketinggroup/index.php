<table id="marketinggroup_list" class="easyui-datagrid"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="false" toolbar="#toolbar_marketinggroup"
       idField="marketinggroup_id" url="y.php?r=marketinggroup/getdata">
    <thead>
    <tr>
        <th field="groupname" width="60">群名称</th>
        <th field="group_description" width="60">群描述</th>
    </tr>
    </thead>
</table>
<div id="toolbar_marketinggroup">
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加营销群','y.php?r=marketinggroup/showedit&marketinggroup_id=0')" /a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="marketinggroup_showedit()"/a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="marketinggroup_remove()"/a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-cog',plain:true" onclick="marketinggroup_config()" />
</div>
<script>
    function marketinggroup_showedit() {
        //获取选中的列
        var selected = $("#marketinggroup_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('添加客户服务','y.php?r=marketinggroup/showedit&marketinggroup_id='+selected.marketinggroup_id);
        }
    }
    function marketinggroup_remove() {
        var selected = $("#marketinggroup_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=marketinggroup/remove',{marketinggroup_id:selected.marketinggroup_id},function (res) {
                        $("#marketinggroup_list").datagrid("reload");
                    });
                }
            });
        }
    }
    function marketinggroup_config() {
        var selected = $("#marketinggroup_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            loadWin('编辑营销群','y.php?r=marketinggroup/config&marketinggroup_id='+selected.marketinggroup_id);
        }
    }
</script>
