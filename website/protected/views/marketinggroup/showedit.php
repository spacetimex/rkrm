<form id="form_marketinggroup_showedit" method="post" action="y.php?r=marketinggroup/savedata">
    <input type="hidden" name="marketinggroup_id" value="">
    <table class="tab_form">
        <tr>
            <td><input class="easyui-textbox" name="groupname"  data-options="label:'群名称:',required:true"></td>
            <td><input class="easyui-textbox" name="group_description"  data-options="label:'群描述:',required:true"></td>
            <td></td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
</div>
<script>
    var dataobj = <?php echo json_encode($marketinggroup) ?>;
    $(function () {
        $('#form_marketinggroup_showedit').form('load',dataobj);
    });
    function submitForm() {
        $('#form_marketinggroup_showedit').form('submit',{
            success: function(){
                $('#win_main').window("close");
                $("#marketinggroup_list").datagrid("reload");
            }
        });
    }
</script>