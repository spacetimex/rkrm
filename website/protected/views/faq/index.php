<table id="faq_list" class="easyui-datagrid"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true" toolbar="#toolbar_faq"
       idField="faq_id" url="y.php?r=faq/getdata">
    <thead>
    <tr>
        <th field="product_name" width="60">产品</th>
        <th field="faqcategoryname" width="60">分类</th>
        <th field="status" width="60">状态</th>
        <th field="keyword" width="60">关键字</th>
        <th field="question" width="60">问题</th>
        <th field="solution" width="60">解决方案</th>
    </tr>
    </thead>
</table>
<div id="toolbar_faq">
    <?php
    if (Yii::app()->user->checkAccess('faq_module_add')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加常见问题','y.php?r=faq/showedit&faq_id=0')" /a>
        <?php
    }
    if (Yii::app()->user->checkAccess('faq_module_edit')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="faq_showedit()"/a>
        <?php
    }
    ?>
</div>
<script>
    function faq_showedit() {
        //获取选中的列
        var selected = $("#faq_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('添加常见问题','y.php?r=faq/showedit&faq_id='+selected.faq_id);
        }
    }
</script>
