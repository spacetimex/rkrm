<form id="form_faq_showedit" method="post" action="y.php?r=faq/savedata">
    <input type="hidden" name="faq_id" value="">
    <table class="tab_form">
        <tr>
            <td><input class="easyui-textbox" name="product_name"  data-options="label:'产品名称:',required:true"></td>
            <td><select class="easyui-combobox" name="faqcategory_id" url="y.php?r=faqcategory/getcomboboxdata" data-options="label:'产品分类:',required:true,
        valueField: 'value',
        textField: 'label',
" >
                </select></td>
            <td><select class="easyui-combobox" name="status" label="状态" >
                    <option value="无">无</option>
                    <option value="草稿">草稿</option>
                    <option value="已审阅">已审阅</option>
                    <option value="已发布">已发布</option>
                    <option value="作废">作废</option>
                </select></td>
        </tr>
        <tr>
            <td><input class="easyui-textbox" name="keyword" 
                       data-options="label:'关键字:',required:true"></td>
            <td><input class="easyui-textbox" name="question" 
                       data-options="label:'问题:',required:true"></td>
            <td><input class="easyui-textbox" name="solution" 
                       data-options="label:'解决方案:',required:true"></td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">清空</a>
</div>
<script>
    var dataobj = <?php echo json_encode($faq) ?>;
    $(function () {
        $('#form_faq_showedit').form('load',dataobj);
    });
    function submitForm() {
        $('#form_faq_showedit').form('submit',{
            success: function(){
                $('#win_main').window("close");
                $("#faq_list").datagrid("reload");
            }
        });
    }
    function clearForm() {
        $('#form_faq_showedit').form('clear');
        if(dataobj!=null){
            $("input[name=faq_id]").val(dataobj.faq_id);
        }
    }
</script>