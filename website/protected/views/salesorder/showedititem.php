<div class="easyui-layout" data-options="fit:true,border:false">
    <div data-options="region:'west'" style="width: 400px;" >
        <form id="form_campaigns_showedit" method="post" action="y.php?r=campaigns/savedata">
            <input type="hidden" name="campaigns_id" value="">
            <table class="tab_form">
                <tr>
                    <td><input class="easyui-searchbox input_text" id="compaigns_showedit_product" name="product"
                               data-options="label:'商品名称:',prompt:'请选择商品',searcher:doSearchProduct"  >
                        <input type="hidden" name="product_id" >
                    </td>
                </tr>
                <tr>
                    <td>

                    </td>
                </tr>
            </table>
        </form>
        <div style="text-align:center;padding:5px 0">
            <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">清空</a>
        </div>
        <script>
            function doSearchProduct() {
                loadWin2('选择商品', 'y.php?r=product/select&form=form_campaigns_showedit&fieldname=product_id&fieldid=compaigns_showedit_product');
            }
        </script>
    </div>
    <div data-options="region:'center',border:false"  >
        <table class="easyui-datagrid"
               data-options="fitColumns:true,singleSelect:true,fit:true" >
            <thead>
            <tr>
                <th data-options="field:'productname',width:10">产品名称</th>
                <th data-options="field:'model',width:10">型号</th>
                <th data-options="field:'salenum',width:10">数量</th>
                <th data-options="field:'saleprice',width:10">售价</th>
                <th data-options="field:'summary',width:10">备注</th>
                <th data-options="field:'amount',width:10">小计</th>
            </tr>
            </thead>
        </table>
    </div>
</div>


