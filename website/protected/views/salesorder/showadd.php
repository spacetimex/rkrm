<div class="p8">
    <script>
        var salesorder_add_products = [];
    </script>
    <table class="easyui-datagrid" style="height:250px"
           data-options="fitColumns:true,singleSelect:true" toolbar="#toolbar_salesorder_showadd">
        <thead>
        <tr>
            <th data-options="field:'productname',width:10">产品名称</th>
            <th data-options="field:'model',width:10">型号</th>
            <th data-options="field:'salenum',width:10">数量</th>
            <th data-options="field:'saleprice',width:10">售价</th>
            <th data-options="field:'summary',width:10">备注</th>
            <th data-options="field:'amount',width:10">小计</th>
        </tr>
        </thead>
    </table>
    <div id="toolbar_salesorder_showadd">
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin2('添加产品','y.php?r=caigoutuihuos/showedit&caigoutuihuos_id=0')" >
            添加产品
        </a>
    </div>
</div>