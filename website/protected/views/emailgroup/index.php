<table class="easyui-datagrid" id="emailgroup_list"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true"
       idField="emailgroup_id" url="y.php?r=emailgroup/getdata" toolbar="#toolbar_emailgroup">
    <thead>
    <tr>
        <th field="title" width="60">标题</th>
        <th field="marketinggroup" width="60">营销群</th>
        <th field="status" width="60">状态</th>
    </tr>
    </thead>
</table>
<div id="toolbar_emailgroup">
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加群发邮件','y.php?r=emailgroup/showedit&emailgroup_id=0')" /a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="emailgroup_showedit()"/a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="emailgroup_remove()" />
</div>
<script>
    function emailgroup_showedit() {
        //获取选中的列
        var selected = $("#emailgroup_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('编辑出库单','y.php?r=emailgroup/showedit&emailgroup_id='+selected.emailgroup_id);
        }
    }
    function emailgroup_remove() {
        var selected = $("#emailgroup_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=emailgroup/remove',{emailgroup_id:selected.emailgroup_id},function (res) {
                        $("#emailgroup_list").datagrid("reload");
                    });
                }
            });
        }
    }
</script>