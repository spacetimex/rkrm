<table class="easyui-datagrid" id="<?php echo $moduleinfo['moduleenname']; ?>_list"
       data-options="url: 'y.php?r=module/getdata&module=<?php echo $moduleinfo['moduleenname']; ?>',
       fitColumns:true,singleSelect:true,pagination:true,nowrap:true,fit: true,border:true,queryParams:{
                'viewid':<?php echo $view['view_id']; ?>
            },onClickRow:window_<?php echo $moduleinfo['moduleenname']; ?>_click" idField= "<?php echo $moduleinfo['moduleentityid']; ?>" >
    <thead>
    <tr>
        <?php
        $columns = json_decode($view['viewcolumnsjson']);
        if (sizeof($columns) > 0) {
            foreach ($columns as $c) {
                ?>
                <th data-options="field:'<?php echo $c->field; ?>',width:<?php echo $c->width; ?>"><?php echo $c->title; ?></th>
                <?php
            }
        }
        ?>
    </tr>
    </thead>
</table>
<script>
    function bak() {
        $("#<?php echo $moduleinfo['moduleenname']; ?>_list").datagrid({
            url:'',
            pagination: true,
            fitColumns: true,
            singleSelect: true,
            border: true,
            fit: true,
            idField: "window_<?php echo $moduleinfo['moduleenname']; ?>_click",
            onClickRow:'',
            queryParams: {
                'viewid':<?php echo $view['view_id']; ?>
            },
            columns: [<?php echo $view['viewcolumnsjson'] ?>]
        });
        $("#<?php echo $moduleinfo['moduleenname']; ?>_list").datagrid("load");
    }
    var form = "<?php echo $form;?>";
    function window_<?php echo $moduleinfo['moduleenname']; ?>_click(index, row) {
        $("#"+form).find("#<?php echo $text_id;?>").textbox('setText',row.<?php echo $moduleinfo['modulecoulumname']; ?>);
        $("#"+form).find("input[name=<?php echo $moduleinfo['moduleentityid']; ?>]").val(row.<?php echo $moduleinfo['moduleentityid']; ?>);
        $('#win_main_2').window("close");
    }
</script>