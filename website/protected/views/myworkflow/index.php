<table id="myworkflow_list" class="easyui-datagrid"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true" toolbar="#toolbar_myworkflow"
       idField="myworkflow_id" url="y.php?r=myworkflow/getdata">
    <thead>
    <tr>
        <th field="name" width="60">工作流</th>
        <th field="realname" width="60">提报人</th>
    </tr>
    </thead>
</table>
<div id="toolbar_myworkflow">
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-ok',plain:true" title="审核工作流" onclick="myworkflow_approve()" />
</div>
<script>
    function myworkflow_approve() {
        var selected = $("#myworkflow_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            loadWin('工作流审核','y.php?r=myworkflow/approve&myworkflow_id='+selected.myworkflow_id);
        }
    }
</script>
