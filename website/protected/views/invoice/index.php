<table class="easyui-datagrid" id="invoce_list"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true"
       idField="invoce_id" url="y.php?r=invoce/getdata" toolbar="#toolbar_invoce">
    <thead>
    <tr>
        <th field="invoice_num" width="60">发货单</th>
        <th field="customer" width="60">客户</th>
        <th field="salesorder" width="80">订单</th>
        <th field="delivery_time" width="60">发货日期</th>
    </tr>
    </thead>
</table>
<div id="toolbar_invoce">
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加发货单','y.php?r=invoce/showedit&invoce_id=0')" /a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="invoce_showedit()"/a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="invoce_remove()" />
</div>
<script>
    function invoce_showedit() {
        //获取选中的列
        var selected = $("#invoce_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('编辑发货单','y.php?r=invoce/showedit&invoce_id='+selected.invoce_id);
        }
    }
    function invoce_remove() {
        var selected = $("#invoce_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=invoce/remove',{invoce_id:selected.invoce_id},function (res) {
                        $("#invoce_list").datagrid("reload");
                    });
                }
            });
        }
    }
</script>