<table id="dailylogs_list" class="easyui-datagrid"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true" toolbar="#toolbar_dailylogs"
       idField="dailylogs_id" url="y.php?r=dailylogs/getdata">
    <thead>
    <tr>
        <th field="realname" width="60">填写人</th>
        <th field="createdate" width="60">日报日期</th>
    </tr>
    </thead>
</table>
<div id="toolbar_dailylogs">
    <?php
    if (Yii::app()->user->checkAccess('workspace_dailylogs_view')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-view',plain:true" onclick="dailylogs_view()" /a>
        <?php
    }
    if (Yii::app()->user->checkAccess('workspace_dailylogs_add')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加日报','y.php?r=dailylogs/showedit&dailylogs_id=0')" /a>
        <?php
    }
    if (Yii::app()->user->checkAccess('workspace_dailylogs_edit')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="dailylogs_showedit()" />
        <?php
    }
    ?>
</div>
<script>
    var todaystr = "<?php echo date("Y-m-d"); ?>";
    function dailylogs_showedit() {
        //获取选中的列
        var selected = $("#dailylogs_list").datagrid("getSelected");

        //编辑的时候，需要判断，日报是不是今天的，如果不是今天的，则不允许修改
        if(selected.createdate != todaystr){
            alert("不能修改非当天的日报内容！");
            return;
        }

        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('编辑日报','y.php?r=dailylogs/showedit&dailylogs_id='+selected.dailylogs_id);
        }
    }
    function dailylogs_view() {
        var selected = $("#dailylogs_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            loadWin('查看日报','y.php?r=dailylogs/view&dailylogs_id='+selected.dailylogs_id);
        }
    }
</script>
