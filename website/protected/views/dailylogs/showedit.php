<form id="form_dailylogs_showedit" method="post" action="y.php?r=dailylogs/savedata">
    <input type="hidden" name="dailylogs_id" value="">
    <table class="tab_form">
        <?php
        if(!is_null($dailylogs)){
            ?>
            <tr>
                <td>周报日期：<?php echo $dailylogs['createdate']; ?></td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td>今日总结：</td>
        </tr>
        <tr>
            <td>
                <textarea style="width: 61%;resize: none;height: 80px;" class="iptborder" data-options="label:'今日总结:',required:true" name="summary" ></textarea>
            </td>
        </tr>
        <tr>
            <td>明日计划：</td>
        </tr>
        <tr>
            <td>
                <textarea style="width: 61%;resize: none;height: 80px;" class="iptborder" data-options="label:'明日计划:',required:true" name="tomorrow_plan" ></textarea>
            </td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">清空</a>
</div>
<script>
    var dataobj = <?php echo json_encode($dailylogs) ?>;
    $(function () {
        $('#form_dailylogs_showedit').form('load',dataobj);
    });
    function submitForm() {
        $('#form_dailylogs_showedit').form('submit',{
            success: function(data){
                var data = eval('(' + data + ')');
                if(data.code == 10000){
                    $('#win_main').window("close");
                    $("#dailylogs_list").datagrid("reload");
                }else{
                    alert(data.message);
                }
            }
        });
    }
    function clearForm() {
        $('#form_dailylogs_showedit').form('clear');
        if(dataobj!=null){
            $("input[name=dailylogs_id]").val(dataobj.dailylogs_id);
        }
    }
</script>