<form id="form_weeklylogs_showedit" method="post" action="y.php?r=weeklylogs/savedata">
    <input type="hidden" name="weeklylogs_id" value="">
    <table class="tab_form">
        <tr>
            <td>本周总结：</td>
        </tr>
        <tr>
            <td>
                <textarea style="width: 61%;resize: none;height: 80px;" class="iptborder" data-options="label:'今日总结:',required:true" name="summary" ></textarea>
            </td>
        </tr>
        <tr>
            <td>下周计划：</td>
        </tr>
        <tr>
            <td>
                <textarea style="width: 61%;resize: none;height: 80px;" class="iptborder" data-options="label:'下周计划:',required:true" name="nextweek_plan" ></textarea>
            </td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">清空</a>
</div>
<script>
    var dataobj = <?php echo json_encode($weeklylogs) ?>;
    $(function () {
        $('#form_weeklylogs_showedit').form('load',dataobj);
    });
    function submitForm() {
        $('#form_weeklylogs_showedit').form('submit',{
            success: function(data){
                var data = eval('(' + data + ')');
                if(data.code == 10000){
                    $('#win_main').window("close");
                    $("#weeklylogs_list").datagrid("reload");
                }else{
                    alert(data.message);
                }
            }
        });
    }
    function clearForm() {
        $('#form_weeklylogs_showedit').form('clear');
        if(dataobj!=null){
            $("input[name=weeklylogs_id]").val(dataobj.weeklylogs_id);
        }
    }
</script>