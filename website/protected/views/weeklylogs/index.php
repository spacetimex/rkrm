<table id="weeklylogs_list" class="easyui-datagrid"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true" toolbar="#toolbar_weeklylogs"
       idField="weeklylogs_id" url="y.php?r=weeklylogs/getdata">
    <thead>
    <tr>
        <th field="year" width="60">年</th>
        <th field="week" width="60">周</th>
        <th field="createdate" width="60">填写日期</th>
    </tr>
    </thead>
</table>
<div id="toolbar_weeklylogs">
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-view',plain:true" onclick="weeklylogs_view()" /a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加周报','y.php?r=weeklylogs/showedit&weeklylogs_id=0')" /a>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="weeklylogs_showedit()" />
</div>
<script>
    function weeklylogs_showedit() {
        //获取选中的列
        var selected = $("#weeklylogs_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('添加周报','y.php?r=weeklylogs/showedit&weeklylogs_id='+selected.weeklylogs_id);
        }
    }
    function weeklylogs_remove() {
        var selected = $("#weeklylogs_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=weeklylogs/remove',{weeklylogs_id:selected.weeklylogs_id},function (res) {
                        $("#weeklylogs_list").datagrid("reload");
                    });
                }
            });
        }
    }
    function weeklylogs_view() {
        var selected = $("#weeklylogs_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            loadWin('查看周报','y.php?r=weeklylogs/view&weeklylogs_id='+selected.weeklylogs_id);
        }
    }
</script>
