<table class="tab_form">
    <tr>
        <td>填写人：<?php echo $weeklylogs['realname'] ?></td>
        <td>年度：<?php echo $weeklylogs['year'] ?></td>
        <td>第几周：<?php echo $weeklylogs['week'] ?></td>
    </tr>
    <tr>
        <td colspan="3">今周总结：</td>
    </tr>
    <tr>
        <td colspan="3"><?php echo $weeklylogs['summary'] ?></td>
    </tr>
    <tr>
        <td colspan="3">
            下周总结：
        </td>
    </tr>
    <tr>
        <td colspan="3"><?php echo $weeklylogs['nextweek_plan'] ?></td>
    </tr>
    <tr>
        <td colspan="3">填写日期：<?php echo $weeklylogs['createdate'] ?></td>
    </tr>
</table>
