<table class="easyui-datagrid"  id="module_index_<?php echo $module; ?>_list"
       data-options="url:'y.php?r=module/getdata&module=<?php echo $moduleinfo['moduleenname']; ?>',
       fitColumns:true,singleSelect:true,pagination:true,nowrap:true,fit: true,border:true,queryParams:{
                'viewid':<?php echo $view['view_id']; ?>
            }"
       toolbar="#toolbar_<?php echo $module; ?>"  idField="<?php echo $moduleinfo['moduleentityid']; ?>" >
    <thead>
    <tr>
        <?php
        $columns = json_decode($view['viewcolumnsjson']);
        if (sizeof($columns) > 0) {
            foreach ($columns as $c) {
                ?>
                <th data-options="field:'<?php echo $c->field; ?>',width:<?php echo $c->width; ?>"><?php echo $c->title; ?></th>
                <?php
            }
        }
        ?>
    </tr>
    </thead>
</table>
<div id="toolbar_<?php echo $module; ?>">
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-view',plain:true"
       onclick="<?php echo $module; ?>_view()" >查看</a>
    <?php
    //    if (Yii::app()->user->checkAccess($module . '_module_add')) {
    ?>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true"
       onclick="loadWin('添加<?php echo $moduleinfo['modulename']; ?>','y.php?r=module/showadd&moduleentityid=0&module=<?php echo $module; ?>')"
    >添加</a>
    <?php
    //    }
    //    if (Yii::app()->user->checkAccess($module . '_module_edit')) {
    ?>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true"
       onclick="<?php echo $module; ?>_showedit()">编辑</a>
    <?php
    //    }
    //    if (Yii::app()->user->checkAccess($module . '_module_remove')) {
    ?>
    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true"
       onclick="<?php echo $module; ?>_remove()" >删除</a>
    <?php
    //    }

    if($module=='salesorder'){
        $this->renderPartial("../".$module."/toolbar",$this->moduledata);
    }

    ?>
</div>
<script>
    $(function () {
    });
    function <?php echo $module; ?>_initPage() {
        $("#module_index_<?php echo $module; ?>_list").datagrid({
            nowrap: true,
            pagination: true,
            fitColumns: true,
            singleSelect: true,
            border: true,
            fit: true,
            toolbar: '#toolbar_<?php echo $module; ?>',
            idField: '<?php echo $moduleinfo['moduleentityid']; ?>',
            queryParams: {
                'viewid':<?php echo $view['view_id']; ?>
            },
            url: 'y.php?r=module/getdata&module=<?php echo $moduleinfo['moduleenname']; ?>'
        });
    }
    function <?php echo $module; ?>_showedit() {
        //获取选中的列
        var selected = $("#module_index_<?php echo $module; ?>_list").datagrid("getSelected");
        if (selected == null) {
            alert('请选择行！');
        } else {
            //进行编辑操作
            loadWin('添加<?php echo $moduleinfo['modulename']; ?>', 'y.php?r=module/showedit&moduleentityid='
                + selected.<?php echo $module; ?>_id + '&module=<?php echo $module; ?>');
        }
    }
    function <?php echo $module; ?>_remove() {
        var selected = $("#module_index_<?php echo $module; ?>_list").datagrid("getSelected");
        if (selected == null) {
            alert('请选择行！');
        } else {
            $.messager.confirm('确认', '您确认想要删除记录吗？', function (r) {
                if (r) {
                    $.post('y.php?r=module/remove', {
                        moduleentityid: selected.<?php echo $moduleinfo['moduleentityid']; ?>,
                        module: '<?php echo $module; ?>'
                    }, function (res) {
                        $("#module_index_<?php echo $module; ?>_list").datagrid("reload");
                    });
                }
            });
        }
    }
    function <?php echo $module; ?>_view() {
        var selected = $("#module_index_<?php echo $module; ?>_list").datagrid("getSelected");
        if (selected == null) {
            alert('请选择行！');
        } else {
            loadWin('<?php echo $moduleinfo['modulename']; ?>', 'y.php?r=module/view&moduleentityid=' + selected.<?php echo $moduleinfo['moduleentityid']; ?>
                + '&module=<?php echo $module; ?>');
        }
    }
</script>
