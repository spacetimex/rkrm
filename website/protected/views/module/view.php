<table class="tab_form">
    <?php
    foreach ($modulepanels as $mp) {
        ?>
        <tr>
            <td colspan="3"><strong><?php echo $mp['panelname']; ?></strong></td>
        </tr>
        <?php
        if (sizeof($mp['columns']) > 0) {
            $list_idx = 0;
            $tr_idx = 0;
            foreach ($mp['columns'] as $c) {
                if($list_idx==0){
                    echo "<tr>";
                }else if($tr_idx%3==0){
                    echo "<tr>";
                }
                if ($c['columndatatype'] == 'text' && $list_idx ==0) {
                    ?>
                        <td td_index="<?php echo $list_idx; ?>" colspan="3" class="td_item"><?php echo $c['columnlabel']; ?>：
                            <?php echo $c['columntype']!='relation'?$entityinfo[$c['columnname']]:$this->setviewtext($c['columnname'],$entityinfo[$c['columnname']]); ?></td>
                    <?php
                    $list_idx++;
                }else if ($c['columndatatype'] == 'text' && $list_idx !=0) {
                    $residue = 0;
                    if($tr_idx<3){
                        $residue = 3 - $tr_idx;
                    }else{
                        $residue = intval($tr_idx%3);
                    }
                    $tr_idx +=$residue;
                    if($residue!=0){
                        echo "<td colspan='".$residue."'></td>";
                        echo "</tr><tr>";
                    }else{
                        echo "<tr>";
                    }
                    ?>

                    <td td_index="<?php echo $list_idx; ?>" colspan="3" class="td_item"><?php echo $c['columnlabel']; ?>：<?php echo $c['columntype']!='relation'?$entityinfo[$c['columnname']]:$this->setviewtext($c['columnname'],$entityinfo[$c['columnname']]); ?></td>
                    <?php
                    echo "</tr>";
                    $list_idx++;
                }else{
                    ?>
                    <td class="td_item" tr_idx="<?php echo $tr_idx; ?>" td_index="<?php echo $list_idx; ?>" ><?php echo $c['columnlabel']; ?>：<?php echo $c['columntype']!='relation'?$entityinfo[$c['columnname']]:$this->setviewtext($c['columnname'],$entityinfo[$c['columnname']]); ?>
                    </td>
                    <?php
                    $list_idx++;
                    $tr_idx++;
                }

                if($c['columndatatype'] == 'text'){
                    echo "</tr>";
                }else if($tr_idx%3==0 && $tr_idx!=0){
                    echo "</tr>";
                }else if($list_idx == sizeof($mp['columns'])){
                    $tr_idx++;
                    $residue = 0;
                    if($tr_idx<3){
                        $residue = 3 - $tr_idx;
                    }else{
                        $residue = intval($tr_idx%3);
                    }
                    echo "<td colspan='".$residue."'></td>";
                    echo "</tr>";
                }
            }
        }
    }
    ?>
</table>
<?php
if($module=='salesorder'){
    $this->renderPartial("../".$module."/view");
}
