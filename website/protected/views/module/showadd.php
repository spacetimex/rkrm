<form id="form_<?php echo $moduleinfo['moduleenname']; ?>_showedit" method="post" action="y.php?r=module/savedata">
    <input type="hidden" name="moduleentityid" value="<?php echo $moduleentityid; ?>">
    <input type="hidden" name="module" value="<?php echo $module; ?>">
    <table class="tab_form">
        <?php
        foreach ($modulepanels as $mp) {
            ?>
            <tr>
                <td colspan="3"><strong><?php echo $mp['panelname']; ?></strong></td>
            </tr>
            <?php
            if (sizeof($mp['columns']) > 0) {
                $list_idx = 0;
                $tr_idx = 0;
                foreach ($mp['columns'] as $c) {
                    if ($list_idx == 0) {
                        echo "<tr>";
                    } else if ($tr_idx % 3 == 0) {
                        echo "<tr>";
                    }
                    if ($c['columndatatype'] == 'text' && $list_idx == 0) {
                        ?>
                        <td td_index="<?php echo $list_idx; ?>" colspan="3" class="td_item">
                            <div class="easyui-texteditor" id="texteditor_<?php echo $c['columnname'] ?>"
                                 title="<?php echo $c['columnlabel'] ?>"
                                 style="height:300px;padding:20px;">
                            </div>
                            <input type="hidden" class="texteditoritem" name="<?php echo $c['columnname'] ?>"/>
                        </td>
                        <?php
                        $list_idx++;
                    } else if ($c['columndatatype'] == 'text' && $list_idx != 0) {
                        $residue = 0;
                        if ($tr_idx < 3) {
                            $residue = 3 - $tr_idx;
                        } else {
                            $residue = intval($tr_idx % 3);
                        }
                        $tr_idx += $residue;
                        if($residue!=0){
                            echo "<td colspan='".$residue."'></td>";
                            echo "</tr><tr>";
                        }else{
                            echo "<tr>";
                        }
                        ?>
                        <td td_index="<?php echo $list_idx; ?>" colspan="3" class="td_item">
                            <div class="easyui-texteditor" id="texteditor_<?php echo $c['columnname'] ?>"
                                 title="<?php echo $c['columnlabel'] ?>"
                                 style="height:300px;padding:20px;">
                            </div>
                            <input type="hidden" class="texteditoritem" name="<?php echo $c['columnname'] ?>"/>
                        </td>
                        <?php
                        echo "</tr>";
                        $list_idx++;
                    } else {
                        ?>
                        <td class="td_item" tr_idx="<?php echo $tr_idx; ?>" td_index="<?php echo $list_idx; ?>">
                            <?php
                            if ($c['columndatatype'] == 'datetime') {
                                ?>
                                <input class="easyui-datetimebox" name="<?php echo $c['columnname'] ?>"
                                       style="width: 90%;"
                                       data-options="label:'<?php echo $c['columnlabel'] ?>:',required:<?php echo $c['ismust'] == 0 ? 'true' : 'false'; ?>"/>
                                <?php
                            } else if ($c['isselected'] == 1) {
                                ?>
                                <select class="easyui-combobox" name="<?php echo $c['columnname'] ?>"
                                        data-options="label:'<?php echo $c['columnlabel'] ?>:',required:true,
                                        url:'y.php?r=middle/picklist/getdata&picklistkey=<?php echo $c['columnname'] ?>
                                        &picklistvalue=',valueField:'id',textField:'text'">
                                </select>
                                <?php
                            } else if ($c['columntype'] == 'relation') {
                                //关系字段
                                ?>
                                <input class="easyui-textbox" id="<?php echo $c['columnname'] ?>_text_id"
                                       data-options="label:'<?php echo $c['columnlabel'] ?>:',required:true" style="width: 75%;"/>
                                <a href="#" class="easyui-linkbutton fr" data-options="iconCls:'icon-search'"
                                   onclick="loadWin2('选择<?php echo $c['columnlabel'] ?>','y.php?r=relation/select&form=form_<?php echo $moduleinfo['moduleenname']; ?>_showedit&text_id=<?php echo $c['columnname'] ?>_text_id&column=<?php echo $c['columnname'] ?>')">选择</a>
                                <input type="hidden" name="<?php echo $c['columnname'] ?>" value=""/>
                                <?php
                            } else {
                                ?>
                                <input class="easyui-textbox" name="<?php echo $c['columnname'] ?>" style="width: 90%;"
                                       data-options="label:'<?php echo $c['columnlabel'] ?>:',required:<?php echo $c['ismust'] == 0 ? 'true' : 'false'; ?>">
                                <?php
                            }
                            ?>

                        </td>
                        <?php
                        $list_idx++;
                        $tr_idx++;
                    }

                    if ($c['columndatatype'] == 'text') {
                        echo "</tr>";
                    } else if ($tr_idx % 3 == 0 && $tr_idx != 0) {
                        echo "</tr>";
                    } else if ($list_idx == sizeof($mp['columns'])) {
                        $tr_idx++;
                        $residue = 0;
                        if ($tr_idx < 3) {
                            $residue = 3 - $tr_idx;
                        } else {
                            $residue = intval($tr_idx % 3);
                        }
                        echo "<td colspan='" . $residue . "'></td>";
                        echo "</tr>";
                    }
                }
            }
        }
        ?>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
</div>
<script>
    function submitForm() {
        var texteditoritems = $(".texteditoritem");
        for (var i = 0; i < texteditoritems.length; i++) {
            $("input[name=" + $(texteditoritems[i]).attr("name") + "]").val($("#texteditor_" + $(texteditoritems[i]).attr("name")).texteditor("getValue"));
        }

        $('#form_<?php echo $moduleinfo['moduleenname']; ?>_showedit').form('submit', {
            success: function () {
                $('#win_main').window("close");
                $("#module_index_<?php echo $moduleinfo['moduleenname']; ?>_list").datagrid("reload");
            }
        });
    }
</script>