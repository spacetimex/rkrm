<form id="form_complaints_showedit" method="post" action="y.php?r=complaints/savedata">
    <input type="hidden" name="complaints_id" value="">
    <table class="tab_form">
        <tr>
            <td><input class="easyui-textbox" name="title"  data-options="label:'主题:',required:true"></td>
            <td><input class="easyui-textbox" name="customer"  data-options="label:'客户:',required:true"></td>
            <td><input class="easyui-textbox" name="complaints_type"  data-options="label:'投诉类型:',required:true"></td>
        </tr>
        <tr>
            <td><input class="easyui-datebox" name="complaints_date"  data-options="label:'投诉时间:',required:true"></td>
            <td><input class="easyui-textbox" name="result"  data-options="label:'处理结果:',required:true"></td>
            <td><select class="easyui-combobox" name="urgencydegree" label="紧急度" >
                    <option value="无">无</option>
                    <option value="高">高</option>
                    <option value="中">中</option>
                    <option value="低">低</option>
                </select></td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">清空</a>
</div>
<script>
    var dataobj = <?php echo json_encode($complaints) ?>;
    $(function () {
        $('#form_complaints_showedit').form('load',dataobj);
    });
    function submitForm() {
        $('#form_complaints_showedit').form('submit',{
            success: function(){
                $('#win_main').window("close");
                $("#complaints_list").datagrid("reload");
            }
        });
    }
    function clearForm() {
        $('#form_complaints_showedit').form('clear');
        if(dataobj!=null){
            $("input[name=complaints_id]").val(dataobj.complaints_id);
        }
    }
</script>