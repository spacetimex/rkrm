<table id="complaints_list" class="easyui-datagrid"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true" toolbar="#toolbar_complaints"
       idField="complaints_id" url="y.php?r=complaints/getdata">
    <thead>
    <tr>
        <th field="title" width="60">主题</th>
        <th field="customer" width="60">客户</th>
        <th field="complaints_type" width="60">投诉类型</th>
        <th field="complaints_date" width="60">投诉时间</th>
        <th field="result" width="60">结果</th>
        <th field="urgencydegree" width="60">紧急度</th>
    </tr>
    </thead>
</table>
<div id="toolbar_complaints">
    <?php
    if (Yii::app()->user->checkAccess('complaints_module_add')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加客户投诉','y.php?r=complaints/showedit&complaints_id=0')" /a>
        <?php
    }
    if (Yii::app()->user->checkAccess('complaints_module_edit')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="complaints_showedit()"/a>
        <?php
    }
    ?>
</div>
<script>
    function complaints_showedit() {
        //获取选中的列
        var selected = $("#complaints_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('添加客户投诉','y.php?r=complaints/showedit&complaints_id='+selected.complaints_id);
        }
    }
</script>
