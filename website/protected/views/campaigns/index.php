<table id="campaigns_list" class="easyui-datagrid"
       fit="true" border="false"
       singleSelect="true" fitColumns="true" pagination="true" toolbar="#toolbar_campaigns"
       idField="campaigns_id" url="y.php?r=campaigns/getdata">
    <thead>
    <tr>
        <th field="campaigns_name" width="60">活动名称</th>
        <th field="status" width="60">状态</th>
        <th field="productname" width="60">产品</th>
        <th field="campaigns_type" width="60">营销活动类型</th>
        <th field="end_date" width="60">结束日期</th>
        <th field="targetaudience" width="60">目标听众</th>
        <th field="targetsize" width="60">目标大小</th>
    </tr>
    </thead>
</table>
<div id="toolbar_campaigns">
    <?php
    if (Yii::app()->user->checkAccess('campaigns_view')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-view',plain:true" onclick="campaigns_view()" /a>
        <?php
    }
    if (Yii::app()->user->checkAccess('campaigns_add')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="loadWin('添加营销活动','y.php?r=campaigns/showedit&campaigns_id=0')" /a>
        <?php
    }
    if (Yii::app()->user->checkAccess('campaigns_edit')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="campaigns_showedit()"/a>
        <?php
    }
    if (Yii::app()->user->checkAccess('campaigns_remove')) {
        ?>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="campaigns_remove()"/a>
        <?php
    }
    ?>
</div>
<script>
    function campaigns_showedit() {
        //获取选中的列
        var selected = $("#campaigns_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('添加营销活动','y.php?r=campaigns/showedit&campaigns_id='+selected.campaigns_id);
        }
    }
    function campaigns_remove() {
        var selected = $("#campaigns_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            $.messager.confirm('确认','您确认想要删除记录吗？',function(r){
                if (r){
                    $.post('y.php?r=campaigns/remove',{campaigns_id:selected.campaigns_id},function (res) {
                        $("#campaigns_list").datagrid("reload");
                    });
                }
            });
        }
    }
    /**
     * 查看营销活动
     */
    function campaigns_view() {
        var selected = $("#campaigns_list").datagrid("getSelected");
        if(selected==null){
            alert('请选择行！');
        }else{
            //进行编辑操作
            loadWin('查看营销活动','y.php?r=campaigns/view&campaigns_id='+selected.campaigns_id);
        }
    }
</script>
