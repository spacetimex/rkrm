<form id="form_campaigns_showedit" method="post" action="y.php?r=campaigns/savedata">
    <input type="hidden" name="campaigns_id" value="">
    <table class="tab_form">
        <tr>
            <td><input class="easyui-textbox" name="campaigns_name"
                       data-options="label:'活动名称:',required:true"></td>
            <td>
                <select class="easyui-combobox" name="status" label="状态" >
                    <option value="无">无</option>
                    <option value="计划中">计划中</option>
                    <option value="激活">激活</option>
                    <option value="禁止">禁止</option>
                    <option value="完成">完成</option>
                    <option value="取消">取消</option>
                </select></td>
            <td><input class="easyui-searchbox" id="compaigns_showedit_product" name="product"
                       data-options="label:'活动名称:',prompt:'请选择商品',searcher:doSearchProduct" value="<?php echo $campaigns['productname']; ?>">
                <input type="hidden" name="product_id" value="<?php echo $campaigns['product_id']; ?>">
            </td>
        </tr><tr>
            <td><select class="easyui-combobox" name="campaigns_type" label="活动类型" >
                    <option value="无">无</option>
                    <option value="计划中">计划中</option>
                    <option value="激活">激活</option>
                    <option value="禁止">禁止</option>
                    <option value="完成">完成</option>
                    <option value="取消">取消</option>
                </select></td>
            <td>
                <input class="easyui-datetimebox" name="end_date"
                       data-options="label:'结束时间:',required:true"></td>
            <td><input class="easyui-textbox" name="targetaudience"
                       data-options="label:'目标听众:',required:true">
            </td>
        </tr>
        <tr>
            <td><input class="easyui-textbox" name="targetsize"
                       data-options="label:'目标大小:',required:true"></td>
            <td colspan="2">
               </td>
        </tr>
        <tr>
            <td colspan="3">
                <div class="easyui-texteditor" id="product_campaigns_texteditor_description" title="内容" style="height:300px;padding:20px;">
                    <?php
                    if(!is_null($campaigns)){
                        echo $campaigns['description'];
                    }
                    ?>
                </div>
                <input type="hidden" name="description"/>
            </td>
        </tr>
    </table>
</form>
<div style="text-align:center;padding:5px 0">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">清空</a>
</div>
<script>
    var dataobj = <?php echo json_encode($campaigns) ?>;
    $(function () {
        $('#form_campaigns_showedit').form('load', dataobj);
    });
    function submitForm() {
        $("input[name=description]").val($('#product_campaigns_texteditor_description').texteditor("getValue"));
        $('#form_campaigns_showedit').form('submit', {
            success: function () {
                $('#win_main').window("close");
                $("#campaigns_list").datagrid("reload");
            }
        });
    }
    function clearForm() {
        $('#form_campaigns_showedit').form('clear');
        if (dataobj != null) {
            $("input[name=campaigns_id]").val(dataobj.campaigns_id);
        }
    }
    //查询产品
    function doSearchProduct() {
        loadWin2('选择商品','y.php?r=product/select&form=form_campaigns_showedit&fieldname=product_id&fieldid=compaigns_showedit_product');
    }
</script>