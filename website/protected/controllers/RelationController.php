<?php
class RelationController extends Controller
{
    public function actionSelect(){
        $form = $_REQUEST['form'];
        $text_id = $_REQUEST['text_id'];
        $column = $_REQUEST['column'];
        $moduleinfo = $this->connection->createCommand("select * from crm_module where moduleentityid = :moduleentityid")
            ->bindParam(':moduleentityid', $column, PDO::PARAM_STR)
            ->queryRow();
        $view = $this->connection->createCommand("select * from crm_module_view where modulename = :module and isdefault = 1")
            ->bindParam(':module', $moduleinfo['moduleenname'], PDO::PARAM_STR)
            ->queryRow();
        $this->render('select',['form'=>$form,'text_id'=>$text_id,'moduleinfo'=>$moduleinfo,'view'=>$view]);
    }
}