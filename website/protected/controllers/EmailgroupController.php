<?php
class EmailgroupController extends Controller
{

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata()
    {

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page - 1) * $rows;
        $total = $this->connection->createCommand("select count(1) from crm_emailgroup cm ")
            ->queryScalar();
        $result = $this->connection->createCommand("select * from crm_emailgroup cm order by cm.emailgroup_id desc limit {$rows} offset {$start}")
            ->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;
        echo json_encode($data);
    }



}