<?php
//盘点
class ChecksController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_checks where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select c.*,u.realname from crm_checks c where c.deleted = 0  limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $checks_id = $_REQUEST['checks_id'];

        $checks = null;
        if($checks_id==0){
            //新增
        }else{
            //编辑
            $checks = $this->connection->createCommand("select c.*,u.realname from crm_checks c where c.checks_id = :checks_id")
                ->bindParam(':checks_id',$checks_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['checks'=>$checks]);
    }

    public function actionSavedata(){

        $checks_id = $_REQUEST['checks_id'];
        $title = $_REQUEST['title'];
        $status = $_REQUEST['status'];
        $warehouse_id = $_REQUEST['warehouse_id'];

        if($checks_id!=null){
            //编辑
            $this->connection->createCommand("update crm_checks set title = :title,status = :status
,warehouse_id = :warehouse_id
where checks_id = :checks_id")
                ->bindParam(':checks_id',$checks_id,PDO::PARAM_INT)
                ->bindParam(':title',$title,PDO::PARAM_STR)
                ->bindParam(':status',$status,PDO::PARAM_INT)
                ->bindParam(':warehouse_id',$warehouse_id,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_checks (title,status,warehouse_id) values 
(:title,:status,:warehouse_id) ")
                ->bindParam(':title',$title,PDO::PARAM_STR)
                ->bindParam(':status',$status,PDO::PARAM_INT)
                ->bindParam(':warehouse_id',$warehouse_id,PDO::PARAM_STR)
                ->execute();
        }
    }


    public function actionRemove(){
        $checks_id = $_REQUEST['checks_id'];
        $this->connection->createCommand("update crm_checks set deleted = 1 
where checks_id = :checks_id")
            ->bindParam(':checks_id',$checks_id,PDO::PARAM_INT)
            ->execute();
    }
}