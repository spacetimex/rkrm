<?php
class WarehousetransfersController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_warehousetransfers where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select c.*,u.realname from crm_warehousetransfers c where c.deleted = 0  limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $warehousetransfers_id = $_REQUEST['warehousetransfers_id'];

        $vendors = null;
        if($warehousetransfers_id==0){
            //新增
        }else{
            //编辑
            $vendors = $this->connection->createCommand("select c.*,u.realname from crm_warehousetransfers c where c.warehousetransfers_id = :warehousetransfers_id")
                ->bindParam(':warehousetransfers_id',$warehousetransfers_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['vendors'=>$vendors]);
    }

    public function actionSavedata(){

        $warehousetransfers_id = $_REQUEST['warehousetransfers_id'];
        $title = $_REQUEST['title'];
        $outwarehouse_id = $_REQUEST['outwarehouse_id'];
        $inwarehouse_id = $_REQUEST['inwarehouse_id'];
        $checkuser = $_REQUEST['checkuser'];
        $status = $_REQUEST['status'];
        $summary = $_REQUEST['summary'];

        if($warehousetransfers_id!=null){
            //编辑
            $this->connection->createCommand("update crm_warehousetransfers set title = :title,outwarehouse_id = :outwarehouse_id
,inwarehouse_id = :inwarehouse_id,checkuser = :checkuser,status = :status,nextstep = :nextstep,status=:status,summary=:summary
where warehousetransfers_id = :warehousetransfers_id")
                ->bindParam(':warehousetransfers_id',$warehousetransfers_id,PDO::PARAM_INT)
                ->bindParam(':title',$title,PDO::PARAM_STR)
                ->bindParam(':outwarehouse_id',$outwarehouse_id,PDO::PARAM_INT)
                ->bindParam(':inwarehouse_id',$inwarehouse_id,PDO::PARAM_STR)
                ->bindParam(':checkuser',$checkuser,PDO::PARAM_STR)
                ->bindParam(':status',$status,PDO::PARAM_STR)
                ->bindParam(':summary',$summary,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_warehousetransfers (title,outwarehouse_id,inwarehouse_id,checkuser,status,nextstep,status,summary) values 
(:title,:outwarehouse_id,:inwarehouse_id,:checkuser,:status,:nextstep,:status,:summary) ")
                ->bindParam(':title',$title,PDO::PARAM_STR)
                ->bindParam(':outwarehouse_id',$outwarehouse_id,PDO::PARAM_INT)
                ->bindParam(':inwarehouse_id',$inwarehouse_id,PDO::PARAM_STR)
                ->bindParam(':checkuser',$checkuser,PDO::PARAM_STR)
                ->bindParam(':status',$status,PDO::PARAM_STR)
                ->bindParam(':summary',$summary,PDO::PARAM_STR)
                ->execute();
        }
    }


    public function actionRemove(){
        $warehousetransfers_id = $_REQUEST['warehousetransfers_id'];
        $this->connection->createCommand("update crm_warehousetransfers set deleted = 1 
where warehousetransfers_id = :warehousetransfers_id")
            ->bindParam(':warehousetransfers_id',$warehousetransfers_id,PDO::PARAM_INT)
            ->execute();
    }
}