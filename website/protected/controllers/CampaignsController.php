<?php
class CampaignsController extends Controller
{

    private $module = 'campaigns';

    public function actionIndex()
    {
        $data = $this->viewIndex($this->module);
        $this->render('../module/index',$data);
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_campaigns where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select c.*,p.productname from crm_campaigns c left join crm_product p on c.product_id = p.product_id where c.deleted = 0  limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $campaigns_id = $_REQUEST['campaigns_id'];

        $campaigns = null;
        if($campaigns_id==0){
            //新增
        }else{
            //编辑
            $campaigns = $this->connection->createCommand("select c.*,p.productname  from crm_campaigns c left join crm_product p on c.product_id = p.product_id where c.campaigns_id = :campaigns_id")->bindParam(':campaigns_id',$campaigns_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['campaigns'=>$campaigns]);
    }

    public function actionSavedata(){

        $campaigns_id = $_REQUEST['campaigns_id'];
        $campaigns_name = $_REQUEST['campaigns_name'];
        $status = $_REQUEST['status'];
        $product_id = $_REQUEST['product_id'];
        $campaigns_type = $_REQUEST['campaigns_type'];
        $end_date = $_REQUEST['end_date'];
        $targetaudience = $_REQUEST['targetaudience'];
        $targetsize = $_REQUEST['targetsize'];
        $description = $_REQUEST['description'];
        
        if($campaigns_id!=null){
            //编辑
            $this->connection->createCommand("update crm_campaigns set campaigns_name = :campaigns_name,campaigns_type = :campaigns_type,status=:status,
end_date=:end_date,description=:description ,product_id=:product_id,targetaudience=:targetaudience,targetsize=:targetsize 
where campaigns_id = :campaigns_id")
                ->bindParam(':campaigns_id',$campaigns_id,PDO::PARAM_INT)
                ->bindParam(':campaigns_name',$campaigns_name,PDO::PARAM_STR)
                ->bindParam(':campaigns_type',$campaigns_type,PDO::PARAM_STR)
                ->bindParam(':status',$status,PDO::PARAM_STR)
                ->bindParam(':end_date',$end_date,PDO::PARAM_STR)
                ->bindParam(':description',$description,PDO::PARAM_STR)
                ->bindParam(':product_id',$product_id,PDO::PARAM_INT)
                ->bindParam(':targetaudience',$targetaudience,PDO::PARAM_STR)
                ->bindParam(':targetsize',$targetsize,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_campaigns (campaigns_name,campaigns_type,status,end_date,description,product_id,targetaudience,targetsize) values 
(:campaigns_name,:campaigns_type,:status,:end_date,:description,:product_id,:targetaudience,:targetsize ) ")
                ->bindParam(':campaigns_name',$campaigns_name,PDO::PARAM_STR)
                ->bindParam(':campaigns_type',$campaigns_type,PDO::PARAM_STR)
                ->bindParam(':status',$status,PDO::PARAM_STR)
                ->bindParam(':end_date',$end_date,PDO::PARAM_STR)
                ->bindParam(':description',$description,PDO::PARAM_STR)
                ->bindParam(':product_id',$product_id,PDO::PARAM_INT)
                ->bindParam(':targetaudience',$targetaudience,PDO::PARAM_STR)
                ->bindParam(':targetsize',$targetsize,PDO::PARAM_STR)
                ->execute();
        }
    }


    public function actionRemove(){
        $campaigns_id = $_REQUEST['campaigns_id'];
        $this->connection->createCommand("update crm_campaigns set deleted = 1 
where campaigns_id = :campaigns_id")
            ->bindParam(':campaigns_id',$campaigns_id,PDO::PARAM_INT)
            ->execute();
    }

    public function actionView(){

        $campaigns_id = $_REQUEST['campaigns_id'];
        $campaigns = $this->connection->createCommand("select c.*,p.productname  from crm_campaigns c left join crm_product p on c.product_id = p.product_id where c.campaigns_id = :campaigns_id")->bindParam(':campaigns_id',$campaigns_id,PDO::PARAM_INT)->queryRow();

        $this->render('view',['campaigns'=>$campaigns]);
    }

}