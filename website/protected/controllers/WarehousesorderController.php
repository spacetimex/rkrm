<?php
//入库单
class WarehousesorderController extends Controller
{

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_warehousesorder where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select c.*,u.realname from crm_warehousesorder c where c.deleted = 0  limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $warehousesorder_id = $_REQUEST['warehousesorder_id'];

        $vendors = null;
        if($warehousesorder_id==0){
            //新增
        }else{
            //编辑
            $vendors = $this->connection->createCommand("select c.*,u.realname from crm_warehousesorder c where c.warehousesorder_id = :warehousesorder_id")
                ->bindParam(':warehousesorder_id',$warehousesorder_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['vendors'=>$vendors]);
    }

    public function actionSavedata(){

        $warehousesorder_id = $_REQUEST['warehousesorder_id'];
        $warehousesorder_num = $_REQUEST['warehousesorder_num'];
        $purchaseorder_id = $_REQUEST['purchaseorder_id'];
        $status = $_REQUEST['status'];
        $amount = $_REQUEST['amount'];

        if($warehousesorder_id!=null){
            //编辑
            $this->connection->createCommand("update crm_warehousesorder set warehousesorder_num = :warehousesorder_num,purchaseorder_id = :purchaseorder_id
,status = :status,amount = :amount,status = :status,nextstep = :nextstep
where warehousesorder_id = :warehousesorder_id")
                ->bindParam(':warehousesorder_id',$warehousesorder_id,PDO::PARAM_INT)
                ->bindParam(':warehousesorder_num',$warehousesorder_num,PDO::PARAM_STR)
                ->bindParam(':purchaseorder_id',$purchaseorder_id,PDO::PARAM_INT)
                ->bindParam(':status',$status,PDO::PARAM_STR)
                ->bindParam(':amount',$amount,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_warehousesorder (warehousesorder_num,purchaseorder_id,status,amount,status,nextstep) values 
(:warehousesorder_num,:purchaseorder_id,:status,:amount,:status,:nextstep) ")
                ->bindParam(':warehousesorder_num',$warehousesorder_num,PDO::PARAM_STR)
                ->bindParam(':purchaseorder_id',$purchaseorder_id,PDO::PARAM_INT)
                ->bindParam(':status',$status,PDO::PARAM_STR)
                ->bindParam(':amount',$amount,PDO::PARAM_STR)
                ->execute();
        }
    }


    public function actionRemove(){
        $warehousesorder_id = $_REQUEST['warehousesorder_id'];
        $this->connection->createCommand("update crm_warehousesorder set deleted = 1 
where warehousesorder_id = :warehousesorder_id")
            ->bindParam(':warehousesorder_id',$warehousesorder_id,PDO::PARAM_INT)
            ->execute();
    }

}