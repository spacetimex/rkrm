<?php
//发货单
class InvoiceController extends Controller
{
    private $module = 'invoice';

    public function actionIndex()
    {
        $data = $this->viewIndex($this->module);
        $this->render('../module/index',$data);
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_invoice where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select c.*,u.realname from crm_invoice c left join crm_user
 u on c.customer_id = u.user_id where c.deleted = 0  limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $invoice_id = $_REQUEST['invoice_id'];

        $potentials = null;
        if($invoice_id==0){
            //新增
        }else{
            //编辑
            $potentials = $this->connection->createCommand("select c.*,u.realname from crm_invoice c left join 
crm_user u on c.customer_id = u.user_id  where c.invoice_id = :invoice_id")->bindParam(':invoice_id',$invoice_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['potentials'=>$potentials]);
    }

    public function actionSavedata(){

        $invoice_id = $_REQUEST['invoice_id'];
        $invoice_num = $_REQUEST['invoice_num'];
        $customer_id = $_REQUEST['customer_id'];
        $salesorder_id = $_REQUEST['salesorder_id'];
        $delivery_time = $_REQUEST['delivery_time'];

        if($invoice_id!=null){
            //编辑
            $this->connection->createCommand("update crm_invoice set invoice_num = :invoice_num,customer_id = :customer_id
,salesorder_id = :salesorder_id,delivery_time = :delivery_time where invoice_id = :invoice_id")
                ->bindParam(':invoice_id',$invoice_id,PDO::PARAM_INT)
                ->bindParam(':invoice_num',$invoice_num,PDO::PARAM_STR)
                ->bindParam(':customer_id',$customer_id,PDO::PARAM_INT)
                ->bindParam(':salesorder_id',$salesorder_id,PDO::PARAM_STR)
                ->bindParam(':delivery_time',$delivery_time,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_invoice (invoice_num,customer_id,salesorder_id,delivery_time) values 
(:invoice_num,:customer_id,:salesorder_id,:delivery_time) ")
                ->bindParam(':invoice_num',$invoice_num,PDO::PARAM_STR)
                ->bindParam(':customer_id',$customer_id,PDO::PARAM_INT)
                ->bindParam(':salesorder_id',$salesorder_id,PDO::PARAM_STR)
                ->bindParam(':delivery_time',$delivery_time,PDO::PARAM_STR)
                ->execute();
        }
    }


    public function actionRemove(){
        $invoice_id = $_REQUEST['invoice_id'];
        $this->connection->createCommand("update crm_invoice set deleted = 1 
where invoice_id = :invoice_id")
            ->bindParam(':invoice_id',$invoice_id,PDO::PARAM_INT)
            ->execute();
    }
}