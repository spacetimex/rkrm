<?php
class VendorsController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_vendors where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select c.* from crm_vendors c where c.deleted = 0  limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $vendors_id = $_REQUEST['vendors_id'];

        $vendors = null;
        if($vendors_id==0){
            //新增
        }else{
            //编辑
            $vendors = $this->connection->createCommand("select c.* from crm_vendors c where c.vendors_id = :vendors_id")
                ->bindParam(':vendors_id',$vendors_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['vendors'=>$vendors]);
    }

    public function actionSavedata(){

        $vendors_id = $_REQUEST['vendors_id'];
        $industry = $_REQUEST['industry'];
        $phone = $_REQUEST['phone'];
        $website = $_REQUEST['website'];
        $city = $_REQUEST['city'];
        $vendor_name = $_REQUEST['vendor_name'];

        if($vendors_id!=null){
            //编辑
            $this->connection->createCommand("update crm_vendors set industry = :industry,phone = :phone
,website = :website,city = :city ,vendor_name = :vendor_name 
where vendors_id = :vendors_id")
                ->bindParam(':vendors_id',$vendors_id,PDO::PARAM_INT)
                ->bindParam(':industry',$industry,PDO::PARAM_STR)
                ->bindParam(':phone',$phone,PDO::PARAM_STR)
                ->bindParam(':website',$website,PDO::PARAM_STR)
                ->bindParam(':city',$city,PDO::PARAM_STR)
                ->bindParam(':vendor_name',$vendor_name,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_vendors (industry,phone,website,city,vendor_name) values 
(:industry,:phone,:website,:city,:vendor_name) ")
                ->bindParam(':industry',$industry,PDO::PARAM_STR)
                ->bindParam(':phone',$phone,PDO::PARAM_STR)
                ->bindParam(':website',$website,PDO::PARAM_STR)
                ->bindParam(':city',$city,PDO::PARAM_STR)
                ->bindParam(':vendor_name',$vendor_name,PDO::PARAM_STR)
                ->execute();
        }
    }


    public function actionRemove(){
        $vendors_id = $_REQUEST['vendors_id'];
        $this->connection->createCommand("update crm_vendors set deleted = 1 
where vendors_id = :vendors_id")
            ->bindParam(':vendors_id',$vendors_id,PDO::PARAM_INT)
            ->execute();
    }

}