<?php
//门户组件的控制器
class PortletController extends Controller
{
    //统计数据
    public function actionStatisticaldata($period){
        $startdate = null;
        $enddate = null;
        if($period == 'week'){
            $date=new DateTime();
            $date->modify('this week');
            $startdate=$date->format('Y-m-d');
            $date->modify('this week +6 days');
            $enddate=$date->format('Y-m-d');
        }else if($period == 'month'){
            $startdate=date('Y-m-01', strtotime(date("Y-m-d")));
            $enddate= date('Y-m-d', strtotime("$startdate +1 month -1 day"));
        }else if($period =='quarter'){
            $season = ceil((date('n'))/3);//当月是第几季度
            $startdate = date('Y-m-d', mktime(0, 0, 0,$season*3-3+1,1,date('Y')));
            $enddate = date('Y-m-d', mktime(23,59,59,$season*3,date('t',mktime(0, 0 , 0,$season*3,1,date("Y"))),date('Y')));
        }

        $data = Array();
        $countOfNewCustomers = $this->connection->createCommand("select count(1) from crm_customer where deleted = 0 and createtime > :startdate and createtime < :enddate ")
            ->bindParam(':startdate',$startdate,PDO::PARAM_STR)
            ->bindParam(':enddate',$enddate,PDO::PARAM_STR)
            ->queryScalar();
        $data[] = Array(
            'statisticaltype'=>'新增客户数',
            'statisticaldata'=>$countOfNewCustomers
        );

        $countOfNewContactlogs = $this->connection->createCommand("select count(1) from crm_contactlog where deleted = 0  and contact_time > :startdate and contact_time < :enddate ")
            ->bindParam(':startdate',$startdate,PDO::PARAM_STR)
            ->bindParam(':enddate',$enddate,PDO::PARAM_STR)
            ->queryScalar();
        $data[] = Array(
            'statisticaltype'=>'新增联系记录数',
            'statisticaldata'=>$countOfNewContactlogs
        );

        $countOfNewContacters = $this->connection->createCommand("select count(1) from crm_contacter where deleted = 0  and createtime > :startdate and createtime < :enddate ")
            ->bindParam(':startdate',$startdate,PDO::PARAM_STR)
            ->bindParam(':enddate',$enddate,PDO::PARAM_STR)
            ->queryScalar();
        $data[] = Array(
            'statisticaltype'=>'新增联系人',
            'statisticaldata'=>$countOfNewContacters
        );
        echo json_encode($data);
    }

    public function actionAnnouncements(){
        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_announcements where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select * from crm_announcements  where deleted = 0  limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }


}