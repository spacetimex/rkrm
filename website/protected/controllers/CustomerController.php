<?php
class CustomerController extends Controller
{
    private $module = 'customer';

    public function actionIndex()
    {
        $data = $this->viewIndex($this->module);
        $this->render('../module/index',$data);
    }

}