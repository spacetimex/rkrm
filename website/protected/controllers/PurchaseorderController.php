<?php
class PurchasedrderController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_purchaseorder where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select c.*,u.realname from crm_purchaseorder c  where c.deleted = 0  limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $purchaseorder_id = $_REQUEST['purchaseorder_id'];

        $purchaseorder = null;
        if($purchaseorder_id==0){
            //新增
        }else{
            //编辑
            $purchaseorder = $this->connection->createCommand("select c.* from crm_purchaseorder c where 
c.purchaseorder_id = :purchaseorder_id")->bindParam(':purchaseorder_id',$purchaseorder_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['purchaseorder'=>$purchaseorder]);
    }

    public function actionSavedata(){

        $purchaseorder_id = $_REQUEST['purchaseorder_id'];
        $purchaseorder_num = $_REQUEST['purchaseorder_num'];
        $vendors_id = $_REQUEST['vendors_id'];
        $status = $_REQUEST['status'];
        $purchase_date = $_REQUEST['purchase_date'];
        $total = $_REQUEST['total'];
        $paystatus = $_REQUEST['paystatus'];

        if($purchaseorder_id!=null){
            //编辑
            $this->connection->createCommand("update crm_purchaseorder set purchaseorder_num = :purchaseorder_num,vendors_id = :vendors_id
,status = :status,purchase_date = :purchase_date,total = :total,paystatus = :paystatus
where purchaseorder_id = :purchaseorder_id")
                ->bindParam(':purchaseorder_id',$purchaseorder_id,PDO::PARAM_INT)
                ->bindParam(':purchaseorder_num',$purchaseorder_num,PDO::PARAM_STR)
                ->bindParam(':vendors_id',$vendors_id,PDO::PARAM_INT)
                ->bindParam(':status',$status,PDO::PARAM_STR)
                ->bindParam(':purchase_date',$purchase_date,PDO::PARAM_STR)
                ->bindParam(':total',$total,PDO::PARAM_STR)
                ->bindParam(':paystatus',$paystatus,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_purchaseorder (purchaseorder_num,vendors_id,status,purchase_date,total,paystatus) values 
(:purchaseorder_num,:vendors_id,:status,:purchase_date,:total,:paystatus) ")
                ->bindParam(':purchaseorder_num',$purchaseorder_num,PDO::PARAM_STR)
                ->bindParam(':vendors_id',$vendors_id,PDO::PARAM_INT)
                ->bindParam(':status',$status,PDO::PARAM_STR)
                ->bindParam(':purchase_date',$purchase_date,PDO::PARAM_STR)
                ->bindParam(':total',$total,PDO::PARAM_STR)
                ->bindParam(':paystatus',$paystatus,PDO::PARAM_STR)
                ->execute();
        }
    }


    public function actionRemove(){
        $purchaseorder_id = $_REQUEST['purchaseorder_id'];
        $this->connection->createCommand("update crm_purchaseorder set deleted = 1 
where purchaseorder_id = :purchaseorder_id")
            ->bindParam(':purchaseorder_id',$purchaseorder_id,PDO::PARAM_INT)
            ->execute();
    }
}
?>