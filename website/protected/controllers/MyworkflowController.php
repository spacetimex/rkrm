<?php
//我的工作流
class MyworkflowController extends Controller
{

    public function actionIndex()
    {
        $this->render('index');
    }

    /*
     * 查询需要我审核的工作流
     */
    public function actionGetdata()
    {

        $user_id = Yii::app()->user->user_id;

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page - 1) * $rows;
        $total = $this->connection->createCommand("select count(1) from oa_workflow_log  where user_id = :user_id")
            ->bindParam(':user_id', $user_id, PDO::PARAM_INT)
            ->queryScalar();
        $result = $this->connection->createCommand("select * from oa_workflow_log where user_id = :user_id limit {$rows} offset {$start}")
            ->bindParam(':user_id', $user_id, PDO::PARAM_INT)
            ->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

}