<?php

class DailylogsController extends Controller
{

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata()
    {

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page - 1) * $rows;
        $total = $this->connection->createCommand("select count(1) from crm_dailylogs where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select *,crm_user.realname from crm_dailylogs left join crm_user on crm_dailylogs.user_id = crm_user.user_id where crm_dailylogs.deleted = 0 limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit()
    {
        $dailylogs_id = $_REQUEST['dailylogs_id'];
        $dailylogs = null;
        if ($dailylogs_id == 0) {
            //新增
        } else {
            //编辑
            $dailylogs = $this->connection->createCommand("select * from crm_dailylogs where dailylogs_id = :dailylogs_id")->bindParam(':dailylogs_id', $dailylogs_id, PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit', ['dailylogs' => $dailylogs]);
    }

    public function actionSavedata(){

        $dailylogs_id = $_REQUEST['dailylogs_id'];
        $summary = $_REQUEST['summary'];
        $tomorrow_plan = $_REQUEST['tomorrow_plan'];

        if($dailylogs_id!=null && $dailylogs_id!=""){
            //编辑
            $this->connection->createCommand("update crm_dailylogs set summary = :summary,
tomorrow_plan=:tomorrow_plan where dailylogs_id = :dailylogs_id")
                ->bindParam(':dailylogs_id',$dailylogs_id,PDO::PARAM_INT)
                ->bindParam(':summary',$summary,PDO::PARAM_STR)
                ->bindParam(':tomorrow_plan',$tomorrow_plan,PDO::PARAM_STR)
                ->execute();

            echo json_encode(Appcode::success);
        }else{
            //添加

            //添加的时候，需要判断当天是否已经录入过对应的日报，如果录入过，则不允许重复添加
            $user_id = Yii::app()->user->user_id;
            $createdate = date("Y-m-d");

            $todayDailylogs = $this->connection->createCommand("select * from crm_dailylogs where user_id = :user_id and createdate = :createdate")
                ->bindParam(':user_id', $user_id, PDO::PARAM_INT)
                ->bindParam(':createdate', $createdate, PDO::PARAM_STR)
                ->queryRow();

            if($todayDailylogs!=false){
                echo json_encode(Appcode::error_dailylogsexist);
            }else{
                $this->connection->createCommand("insert into crm_dailylogs (summary,tomorrow_plan,createdate,user_id) 
values (:summary,:tomorrow_plan,now(),{$user_id}) ")
                    ->bindParam(':summary',$summary,PDO::PARAM_STR)
                    ->bindParam(':tomorrow_plan',$tomorrow_plan,PDO::PARAM_STR)
                    ->execute();
                echo json_encode(Appcode::success);
            }
        }
    }

    public function actionView()
    {
        $dailylogs_id = $_REQUEST['dailylogs_id'];
        $dailylogs = $this->connection->createCommand("select *,crm_user.realname from crm_dailylogs left join crm_user on crm_dailylogs.user_id = crm_user.user_id  where dailylogs_id = :dailylogs_id")->bindParam(':dailylogs_id', $dailylogs_id, PDO::PARAM_INT)->queryRow();
        $this->render('view', ['dailylogs' => $dailylogs]);
    }

}