<?php
class FaqcategoryController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_faqcategory")->queryScalar();
        $result = $this->connection->createCommand("select * from crm_faqcategory limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionGetcomboboxdata(){
        $data = $this->connection->createCommand("select faqcategory_id as value,faqcategoryname label from crm_faqcategory ")->queryAll();
        echo json_encode($data);
    }

    public function actionShowedit(){

        $faqcategory_id = $_REQUEST['faqcategory_id'];

        $faqcategory = null;
        if($faqcategory_id==0){
            //新增
        }else{
            //编辑
            $faqcategory = $this->connection->createCommand("select * from crm_faqcategory where faqcategory_id = :faqcategory_id")->bindParam(':faqcategory_id',$faqcategory_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['faqcategory'=>$faqcategory]);
    }

    public function actionSavedata(){

        $faqcategory_id = $_REQUEST['faqcategory_id'];
        $faqcategoryname = $_REQUEST['faqcategoryname'];

        if($faqcategory_id!=null){
            //编辑
            $this->connection->createCommand("update crm_faqcategory set faqcategoryname = :faqcategoryname  where faqcategory_id = :faqcategory_id")
                ->bindParam(':faqcategory_id',$faqcategory_id,PDO::PARAM_INT)
                ->bindParam(':faqcategoryname',$faqcategoryname,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_faqcategory (faqcategoryname) values (:faqcategoryname) ")
                ->bindParam(':faqcategoryname',$faqcategoryname,PDO::PARAM_STR)
                ->execute();
        }
    }

}