<?php

class MemdaysController extends Controller
{
    private $module = 'memdays';

    public function actionIndex()
    {
        $data = $this->viewIndex($this->module);
        $this->render('../module/index',$data);
    }

}