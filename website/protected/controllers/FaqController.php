<?php
class FaqController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_faq")->queryScalar();
        $result = $this->connection->createCommand("select f.*,c.faqcategoryname from crm_faq f LEFT JOIN crm_faqcategory c on f.faqcategory_id = c.faqcategory_id limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $faq_id = $_REQUEST['faq_id'];

        $faq = null;
        if($faq_id==0){
            //新增
        }else{
            //编辑
            $faq = $this->connection->createCommand("select * from crm_faq where faq_id = :faq_id")->bindParam(':faq_id',$faq_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['faq'=>$faq]);
    }

    public function actionSavedata(){

        $faq_id = $_REQUEST['faq_id'];
        $product_name = $_REQUEST['product_name'];
        $keyword = $_REQUEST['keyword'];
        $status = $_REQUEST['status'];
        $question = $_REQUEST['question'];
        $solution = $_REQUEST['solution'];
        $faqcategory_id = $_REQUEST['faqcategory_id'];

        if($faq_id!=null){
            //编辑
            $this->connection->createCommand("update crm_faq set product_name = :product_name,keyword = :keyword,status=:status,
  question=:question,solution=:solution,faqcategory_id=:faqcategory_id
 where faq_id = :faq_id")
                ->bindParam(':faq_id',$faq_id,PDO::PARAM_INT)
                ->bindParam(':faqcategory_id',$faqcategory_id,PDO::PARAM_INT)
                ->bindParam(':product_name',$product_name,PDO::PARAM_STR)
                ->bindParam(':keyword',$keyword,PDO::PARAM_STR)
                ->bindParam(':status',$status,PDO::PARAM_STR)
                ->bindParam(':question',$question,PDO::PARAM_STR)
                ->bindParam(':solution',$solution,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_faq (product_name,keyword,status,question,solution,faqcategory_id) values (:product_name,:keyword,:status,:question,:solution,:faqcategory_id) ")
                ->bindParam(':faqcategory_id',$faqcategory_id,PDO::PARAM_INT)
                ->bindParam(':product_name',$product_name,PDO::PARAM_STR)
                ->bindParam(':keyword',$keyword,PDO::PARAM_STR)
                ->bindParam(':status',$status,PDO::PARAM_STR)
                ->bindParam(':question',$question,PDO::PARAM_STR)
                ->bindParam(':solution',$solution,PDO::PARAM_STR)
                ->execute();
        }
    }

}