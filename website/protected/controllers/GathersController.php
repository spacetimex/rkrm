<?php
class GathersController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_gathers where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select c.*,u.realname from crm_gathers c where c.deleted = 0  limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $gathers_id = $_REQUEST['gathers_id'];

        $gathers = null;
        if($gathers_id==0){
            //新增
        }else{
            //编辑
            $gathers = $this->connection->createCommand("select c.*,u.realname from crm_gathers c where c.gathers_id = :gathers_id")
                ->bindParam(':gathers_id',$gathers_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['gathers'=>$gathers]);
    }

    public function actionSavedata(){

        $gathers_id = $_REQUEST['gathers_id'];
        $gathers_num = $_REQUEST['gathers_num'];
        $customer_id = $_REQUEST['customer_id'];
        $saleorder_id = $_REQUEST['saleorder_id'];
        $invoice_id = $_REQUEST['invoice_id'];
        $amount = $_REQUEST['amount'];
        $gather_date = $_REQUEST['gather_date'];
        $summary = $_REQUEST['summary'];

        if($gathers_id!=null){
            //编辑
            $this->connection->createCommand("update crm_gathers set gathers_num = :gathers_num,customer_id = :customer_id
,saleorder_id = :saleorder_id,invoice_id = :invoice_id,status = :status,nextstep = :nextstep
where gathers_id = :gathers_id")
                ->bindParam(':gathers_id',$gathers_id,PDO::PARAM_INT)
                ->bindParam(':gathers_num',$gathers_num,PDO::PARAM_STR)
                ->bindParam(':customer_id',$customer_id,PDO::PARAM_INT)
                ->bindParam(':saleorder_id',$saleorder_id,PDO::PARAM_STR)
                ->bindParam(':invoice_id',$invoice_id,PDO::PARAM_STR)
                ->bindParam(':amount',$amount,PDO::PARAM_STR)
                ->bindParam(':gather_date',$gather_date,PDO::PARAM_STR)
                ->bindParam(':summary',$summary,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_gathers (gathers_num,customer_id,saleorder_id,invoice_id,status,nextstep,amount,gather_date,summary) values 
(:gathers_num,:customer_id,:saleorder_id,:invoice_id,:status,:nextstep,:amount,:gather_date,:summary) ")
                ->bindParam(':gathers_num',$gathers_num,PDO::PARAM_STR)
                ->bindParam(':customer_id',$customer_id,PDO::PARAM_INT)
                ->bindParam(':saleorder_id',$saleorder_id,PDO::PARAM_STR)
                ->bindParam(':invoice_id',$invoice_id,PDO::PARAM_STR)
                ->bindParam(':amount',$amount,PDO::PARAM_STR)
                ->bindParam(':gather_date',$gather_date,PDO::PARAM_STR)
                ->bindParam(':summary',$summary,PDO::PARAM_STR)
                ->execute();
        }
    }


    public function actionRemove(){
        $gathers_id = $_REQUEST['gathers_id'];
        $this->connection->createCommand("update crm_gathers set deleted = 1 
where gathers_id = :gathers_id")
            ->bindParam(':gathers_id',$gathers_id,PDO::PARAM_INT)
            ->execute();
    }
}