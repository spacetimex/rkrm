<?php
//合同订单
class SalesorderController extends Controller
{

    private $module = 'salesorder';

    public function actionIndex()
    {
        $data = $this->viewIndex($this->module);
        $this->render('../module/index',$data);
    }

    public function actionShowedititem(){
        $moduleentityid = $_REQUEST['moduleentityid'];
        $this->render('showedititem',['moduleentityid'=>$moduleentityid]);
    }

    public function actionShowaddproduct(){
        $moduleentityid = $_REQUEST['moduleentityid'];
        $salesorderitemid = $_REQUEST['salesorderitemid'];
        $this->render('showaddproduct',['salesorderitemid'=>$salesorderitemid,'moduleentityid'=>$moduleentityid]);
    }

}