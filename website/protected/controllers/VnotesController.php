<?php
//供应商联系记录
class VnotesController extends Controller
{

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_vnotes where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select c.*,c_v.vendor_name from crm_vnotes c LEFT JOIN crm_vendors 
c_v on c.vendors_id = c_v.vendors_id where c.deleted = 0  limit {$start},{$rows}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $vnotes_id = $_REQUEST['vnotes_id'];

        $vnotes = null;
        if($vnotes_id==0){
            //新增
        }else{
            //编辑
            $vnotes = $this->connection->createCommand("select c.* from crm_vnotes c where c.vnotes_id = :vnotes_id")
                ->bindParam(':vnotes_id',$vnotes_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['vnotes'=>$vnotes]);
    }

    public function actionSavedata(){

        $vnotes_id = $_REQUEST['vnotes_id'];
        $title = $_REQUEST['title'];
        $vendors_id = $_REQUEST['vendors_id'];
        $contact_time = $_REQUEST['contact_time'];
        $contact_type = $_REQUEST['contact_type'];
        $content = $_REQUEST['content'];


        if($vnotes_id!=null){
            //编辑
            $this->connection->createCommand("update crm_vnotes set title = :title,vendors_id = :vendors_id
,contact_time = :contact_time,contact_type = :contact_type,content = :content
where vnotes_id = :vnotes_id")
                ->bindParam(':vnotes_id',$vnotes_id,PDO::PARAM_INT)
                ->bindParam(':title',$title,PDO::PARAM_STR)
                ->bindParam(':vendors_id',$vendors_id,PDO::PARAM_INT)
                ->bindParam(':contact_time',$contact_time,PDO::PARAM_STR)
                ->bindParam(':contact_type',$contact_type,PDO::PARAM_STR)
                ->bindParam(':content',$content,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_vnotes (title,vendors_id,contact_time,contact_type,content,nextstep) values 
(:title,:vendors_id,:contact_time,:contact_type,:content) ")
                ->bindParam(':title',$title,PDO::PARAM_STR)
                ->bindParam(':vendors_id',$vendors_id,PDO::PARAM_INT)
                ->bindParam(':contact_time',$contact_time,PDO::PARAM_STR)
                ->bindParam(':contact_type',$contact_type,PDO::PARAM_STR)
                ->bindParam(':content',$content,PDO::PARAM_STR)
                ->execute();
        }
    }


    public function actionRemove(){
        $vnotes_id = $_REQUEST['vnotes_id'];
        $this->connection->createCommand("update crm_vnotes set deleted = 1 
where vnotes_id = :vnotes_id")
            ->bindParam(':vnotes_id',$vnotes_id,PDO::PARAM_INT)
            ->execute();
    }

}