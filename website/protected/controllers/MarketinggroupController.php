<?php
class MarketinggroupController extends Controller
{

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata(){

        $result = $this->connection->createCommand("select * from crm_marketinggroup where deleted = 0")->queryAll();

        $data = Array();
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $marketinggroup_id = $_REQUEST['marketinggroup_id'];

        $marketinggroup = null;
        if($marketinggroup_id==0){
            //新增
        }else{
            //编辑
            $marketinggroup = $this->connection->createCommand("select c.* from crm_marketinggroup c 
 where c.marketinggroup_id = :marketinggroup_id")->bindParam(':marketinggroup_id',$marketinggroup_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['marketinggroup'=>$marketinggroup]);
    }

    public function actionSavedata(){

        $marketinggroup_id = $_REQUEST['marketinggroup_id'];
        $groupname = $_REQUEST['groupname'];
        $group_description = $_REQUEST['group_description'];

        if($marketinggroup_id!=null){
            //编辑
            $this->connection->createCommand("update crm_marketinggroup set groupname = :groupname,group_description = :group_description
 where marketinggroup_id = :marketinggroup_id")
                ->bindParam(':marketinggroup_id',$marketinggroup_id,PDO::PARAM_INT)
                ->bindParam(':groupname',$groupname,PDO::PARAM_STR)
                ->bindParam(':group_description',$group_description,PDO::PARAM_INT)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_marketinggroup (groupname,group_description) values 
(:groupname,:group_description) ")
                ->bindParam(':groupname',$groupname,PDO::PARAM_STR)
                ->bindParam(':group_description',$group_description,PDO::PARAM_INT)
                ->execute();
        }
    }


    public function actionRemove(){
        $marketinggroup_id = $_REQUEST['marketinggroup_id'];
        $this->connection->createCommand("update crm_marketinggroup set deleted = 1 
where marketinggroup_id = :marketinggroup_id")
            ->bindParam(':marketinggroup_id',$marketinggroup_id,PDO::PARAM_INT)
            ->execute();
    }

    public function actionConfig(){
        $marketinggroup_id = $_REQUEST['marketinggroup_id'];
        $marketinggroup = $this->connection->createCommand("select c.* from crm_marketinggroup c 
 where c.marketinggroup_id = :marketinggroup_id")->bindParam(':marketinggroup_id',$marketinggroup_id,PDO::PARAM_INT)->queryRow();

        $marketinggroupinfos = $this->connection->createCommand("select * from crm_marketinggroupinfo where marketinggroup_id = :marketinggroup_id")
            ->bindParam(':marketinggroup_id',$marketinggroup_id,PDO::PARAM_INT)
            ->queryAll();
        $marketinggroupinfosstr = "";
        if(sizeof($marketinggroupinfos)>0){
            $i=1;
            foreach ($marketinggroupinfos as $info){
                if($i==sizeof($marketinggroupinfos)){
                    $marketinggroupinfosstr.=$info['phone'];
                }else{
                    $marketinggroupinfosstr.=$info['phone']."\r\n";
                }
                $i ++;
            }
        }

        $this->render('config',['marketinggroup'=>$marketinggroup,'marketinggroupinfosstr'=>$marketinggroupinfosstr]);
    }

    public function actionSaveconfig(){

        $marketinggroup_id = $_REQUEST['marketinggroup_id'];
        $marketinggroupinfosstr = trim($_REQUEST['marketinggroupinfosstr']);

        //清理数据
        $infos = explode("\r\n", $marketinggroupinfosstr);
        $this->connection->createCommand("delete from crm_marketinggroupinfo where marketinggroup_id = :marketinggroup_id ")
            ->bindParam(':marketinggroup_id',$marketinggroup_id,PDO::PARAM_STR)
            ->execute();
        //进行数据录入
        foreach ($infos as $info){
            $info = trim($info);
            $this->connection->createCommand("insert into crm_marketinggroupinfo (marketinggroup_id,phone) values (:marketinggroup_id,:phone) ")
                ->bindParam(':marketinggroup_id',$marketinggroup_id,PDO::PARAM_STR)
                ->bindParam(':phone',$info,PDO::PARAM_STR)
                ->execute();
        }
    }


}