<?php

//公告管理
class AnnouncementsController extends Controller
{
    private $module = 'announcements';

    public function actionIndex()
    {
        $data = $this->viewIndex($this->module);
        $this->render('../module/index',$data);
    }

}