<?php
class CaigoutuihuosController extends Controller
{

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_caigoutuihuos where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select c.*,u.realname from crm_caigoutuihuos c where c.deleted = 0  limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $caigoutuihuos_id = $_REQUEST['caigoutuihuos_id'];

        $vendors = null;
        if($caigoutuihuos_id==0){
            //新增
        }else{
            //编辑
            $vendors = $this->connection->createCommand("select c.*,u.realname from crm_caigoutuihuos c where c.caigoutuihuos_id = :caigoutuihuos_id")
                ->bindParam(':caigoutuihuos_id',$caigoutuihuos_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['vendors'=>$vendors]);
    }

    public function actionSavedata(){

        $caigoutuihuos_id = $_REQUEST['caigoutuihuos_id'];
        $title = $_REQUEST['title'];
        $status = $_REQUEST['status'];
        $vendors_id = $_REQUEST['vendors_id'];
        $purchaseorder_id = $_REQUEST['purchaseorder_id'];
        $reason = $_REQUEST['reason'];

        if($caigoutuihuos_id!=null){
            //编辑
            $this->connection->createCommand("update crm_caigoutuihuos set title = :title,status = :status
,vendors_id = :vendors_id,purchaseorder_id = :purchaseorder_id,status = :status,nextstep = :nextstep,reason=:reason
where caigoutuihuos_id = :caigoutuihuos_id")
                ->bindParam(':caigoutuihuos_id',$caigoutuihuos_id,PDO::PARAM_INT)
                ->bindParam(':title',$title,PDO::PARAM_STR)
                ->bindParam(':status',$status,PDO::PARAM_INT)
                ->bindParam(':vendors_id',$vendors_id,PDO::PARAM_STR)
                ->bindParam(':purchaseorder_id',$purchaseorder_id,PDO::PARAM_STR)
                ->bindParam(':reason',$reason,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_caigoutuihuos (title,status,vendors_id,purchaseorder_id,status,nextstep,reason) values 
(:title,:status,:vendors_id,:purchaseorder_id,:status,:nextstep,:reason) ")
                ->bindParam(':title',$title,PDO::PARAM_STR)
                ->bindParam(':status',$status,PDO::PARAM_INT)
                ->bindParam(':vendors_id',$vendors_id,PDO::PARAM_STR)
                ->bindParam(':purchaseorder_id',$purchaseorder_id,PDO::PARAM_STR)
                ->bindParam(':reason',$reason,PDO::PARAM_STR)
                ->execute();
        }
    }


    public function actionRemove(){
        $caigoutuihuos_id = $_REQUEST['caigoutuihuos_id'];
        $this->connection->createCommand("update crm_caigoutuihuos set deleted = 1 
where caigoutuihuos_id = :caigoutuihuos_id")
            ->bindParam(':caigoutuihuos_id',$caigoutuihuos_id,PDO::PARAM_INT)
            ->execute();
    }

}