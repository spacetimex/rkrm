<?php
//供应商联系人
class VcontactsController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_vcontacts where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select c.*  from crm_vcontacts c where c.deleted = 0  limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $vcontacts_id = $_REQUEST['vcontacts_id'];

        $vcontacts = null;
        if($vcontacts_id==0){
            //新增
        }else{
            //编辑
            $vcontacts = $this->connection->createCommand("select c.*  from crm_vcontacts c where c.vcontacts_id = :vcontacts_id")
                ->bindParam(':vcontacts_id',$vcontacts_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['vcontacts'=>$vcontacts]);
    }

    public function actionSavedata(){

        $vcontacts_id = $_REQUEST['vcontacts_id'];
        $vcontacts_name = $_REQUEST['vcontacts_name'];
        $phone = $_REQUEST['phone'];

        if($vcontacts_id!=null){
            //编辑
            $this->connection->createCommand("update crm_vcontacts set vcontacts_name = :vcontacts_name,phone = :phone 
where vcontacts_id = :vcontacts_id")
                ->bindParam(':vcontacts_id',$vcontacts_id,PDO::PARAM_INT)
                ->bindParam(':vcontacts_name',$vcontacts_name,PDO::PARAM_STR)
                ->bindParam(':phone',$phone,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_vcontacts (vcontacts_name,phone) values 
(:vcontacts_name,:phone) ")
                ->bindParam(':vcontacts_name',$vcontacts_name,PDO::PARAM_STR)
                ->bindParam(':phone',$phone,PDO::PARAM_STR)
                ->execute();
        }
    }


    public function actionRemove(){
        $vcontacts_id = $_REQUEST['vcontacts_id'];
        $this->connection->createCommand("update crm_vcontacts set deleted = 1 
where vcontacts_id = :vcontacts_id")
            ->bindParam(':vcontacts_id',$vcontacts_id,PDO::PARAM_INT)
            ->execute();
    }


}

