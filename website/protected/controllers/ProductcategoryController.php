<?php
class ProductcategoryController extends Controller
{

    private $module = 'productcategory';

    public function actionIndex()
    {
        $data = $this->viewIndex($this->module);
        $this->render('../module/index',$data);
    }

}