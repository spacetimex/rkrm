<?php
class MonthlylogsController extends Controller
{

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata()
    {

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page - 1) * $rows;
        $total = $this->connection->createCommand("select count(1) from crm_monthlylogs where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select *,crm_user.realname from crm_monthlylogs left join crm_user on crm_monthlylogs.user_id = crm_user.user_id where crm_monthlylogs.deleted = 0 limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit()
    {
        $monthlylogs_id = $_REQUEST['monthlylogs_id'];
        $monthlylogs = null;
        if ($monthlylogs_id == 0) {
            //新增
        } else {
            //编辑
            $monthlylogs = $this->connection->createCommand("select * from crm_monthlylogs where monthlylogs_id = :monthlylogs_id")->bindParam(':monthlylogs_id', $monthlylogs_id, PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit', ['monthlylogs' => $monthlylogs]);
    }

    public function actionSavedata(){

        $monthlylogs_id = $_REQUEST['monthlylogs_id'];
        $summary = $_REQUEST['summary'];
        $nextmonth_plan = $_REQUEST['nextmonth_plan'];

        if($monthlylogs_id!=null && $monthlylogs_id!=""){
            //编辑
            $this->connection->createCommand("update crm_monthlylogs set summary = :summary,
nextmonth_plan=:nextmonth_plan where monthlylogs_id = :monthlylogs_id")
                ->bindParam(':monthlylogs_id',$monthlylogs_id,PDO::PARAM_INT)
                ->bindParam(':summary',$summary,PDO::PARAM_STR)
                ->bindParam(':nextmonth_plan',$nextmonth_plan,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $user_id = Yii::app()->user->user_id;
            $this->connection->createCommand("insert into crm_monthlylogs (summary,nextmonth_plan,createdate,user_id) 
values (:summary,:nextmonth_plan,now(),{$user_id}) ")
                ->bindParam(':summary',$summary,PDO::PARAM_STR)
                ->bindParam(':nextmonth_plan',$nextmonth_plan,PDO::PARAM_STR)
                ->execute();
        }
    }

    public function actionView()
    {
        $monthlylogs_id = $_REQUEST['monthlylogs_id'];
        $monthlylogs = $this->connection->createCommand("select *,crm_user.realname from crm_monthlylogs left join crm_user on crm_monthlylogs.user_id = crm_user.user_id  where monthlylogs_id = :monthlylogs_id")->bindParam(':monthlylogs_id', $monthlylogs_id, PDO::PARAM_INT)->queryRow();
        $this->render('view', ['monthlylogs' => $monthlylogs]);
    }

}