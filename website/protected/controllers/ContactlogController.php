<?php

class ContactlogController extends Controller
{
    private $module = 'contactlog';

    public function actionIndex()
    {
        $data = $this->viewIndex($this->module);
        $this->render('../module/index',$data);
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_contactlog where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select *,DATE_FORMAT(contact_time,'%Y-%m-%d %T') as contact_time_format from crm_contactlog where deleted = 0 limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $contactlog_id = $_REQUEST['contactlog_id'];

        $contactlog = null;
        if($contactlog_id==0){
            //新增
        }else{
            //编辑
            $contactlog = $this->connection->createCommand("select * from crm_contactlog where contactlog_id = :contactlog_id")->bindParam(':contactlog_id',$contactlog_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['contactlog'=>$contactlog]);
    }

    public function actionSavedata(){

        $contactlog_id = $_REQUEST['contactlog_id'];
        $title = $_REQUEST['title'];
        $customer = $_REQUEST['customer'];
        $contact_type = $_REQUEST['contact_type'];
        $status = $_REQUEST['status'];

        if($contactlog_id!=null && $contactlog_id!=""){
            //编辑
            $this->connection->createCommand("update crm_contactlog set title = :title,customer = :customer,
contact_type=:contact_type,status=:status where contactlog_id = :contactlog_id")
                ->bindParam(':contactlog_id',$contactlog_id,PDO::PARAM_INT)
                ->bindParam(':title',$title,PDO::PARAM_STR)
                ->bindParam(':customer',$customer,PDO::PARAM_STR)
                ->bindParam(':contact_type',$contact_type,PDO::PARAM_STR)
                ->bindParam(':status',$status,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_contactlog (title,customer,contact_type,status,contact_time) 
values (:title,:customer,:contact_type,:status,now()) ")
                ->bindParam(':title',$title,PDO::PARAM_STR)
                ->bindParam(':customer',$customer,PDO::PARAM_STR)
                ->bindParam(':contact_type',$contact_type,PDO::PARAM_STR)
                ->bindParam(':status',$status,PDO::PARAM_STR)
                ->execute();
        }
    }

    public function actionRemove(){
        $contactlog_id = $_REQUEST['contactlog_id'];
        $this->connection->createCommand("update crm_contactlog set deleted = 1 
where contactlog_id = :contactlog_id")
            ->bindParam(':contactlog_id',$contactlog_id,PDO::PARAM_INT)
            ->execute();
    }

    public function actionView(){

        $contactlog_id = $_REQUEST['contactlog_id'];
        $contactlog = $this->connection->createCommand("select * from crm_contactlog where contactlog_id = :contactlog_id")->bindParam(':contactlog_id',$contactlog_id,PDO::PARAM_INT)->queryRow();

        $this->render('view',['contactlog'=>$contactlog]);
    }


}