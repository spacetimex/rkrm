<?php
//采购单对应逻辑代码
class CaigousController extends Controller
{

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_caigous where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select c.*,u.realname from crm_caigous c where c.deleted = 0  limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $caigous_id = $_REQUEST['caigous_id'];

        $potentials = null;
        if($caigous_id==0){
            //新增
        }else{
            //编辑
            $potentials = $this->connection->createCommand("select c.*,u.realname from crm_caigous c where c.caigous_id = :caigous_id")
                ->bindParam(':caigous_id',$caigous_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['potentials'=>$potentials]);
    }

    public function actionSavedata(){

        $caigous_id = $_REQUEST['caigous_id'];
        $caigous_num = $_REQUEST['caigous_num'];
        $vendor_id = $_REQUEST['vendor_id'];
        $salesorder_id = $_REQUEST['salesorder_id'];
        $caigou_date = $_REQUEST['caigou_date'];
        $status = $_REQUEST['status'];

        if($caigous_id!=null){
            //编辑
            $this->connection->createCommand("update crm_caigous set caigous_num = :caigous_num,vendor_id = :vendor_id
,salesorder_id = :salesorder_id,caigou_date = :caigou_date,status = :status,nextstep = :nextstep
where caigous_id = :caigous_id")
                ->bindParam(':caigous_id',$caigous_id,PDO::PARAM_INT)
                ->bindParam(':caigous_num',$caigous_num,PDO::PARAM_STR)
                ->bindParam(':vendor_id',$vendor_id,PDO::PARAM_INT)
                ->bindParam(':salesorder_id',$salesorder_id,PDO::PARAM_STR)
                ->bindParam(':caigou_date',$caigou_date,PDO::PARAM_STR)
                ->bindParam(':status',$status,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_caigous (caigous_num,vendor_id,salesorder_id,caigou_date,status,nextstep) values 
(:caigous_num,:vendor_id,:salesorder_id,:caigou_date,:status,:nextstep) ")
                ->bindParam(':caigous_num',$caigous_num,PDO::PARAM_STR)
                ->bindParam(':vendor_id',$vendor_id,PDO::PARAM_INT)
                ->bindParam(':salesorder_id',$salesorder_id,PDO::PARAM_STR)
                ->bindParam(':caigou_date',$caigou_date,PDO::PARAM_STR)
                ->bindParam(':status',$status,PDO::PARAM_STR)
                ->execute();
        }
    }


    public function actionRemove(){
        $caigous_id = $_REQUEST['caigous_id'];
        $this->connection->createCommand("update crm_caigous set deleted = 1 
where caigous_id = :caigous_id")
            ->bindParam(':caigous_id',$caigous_id,PDO::PARAM_INT)
            ->execute();
    }

}