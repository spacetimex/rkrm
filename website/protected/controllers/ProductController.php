<?php
class ProductController extends Controller
{
    private $module = 'product';

    public function actionIndex()
    {
        $data = $this->viewIndex($this->module);
        $this->render('../module/index',$data);
    }

    public function actionSearchproducts(){
        $q = isset($_POST['q']) ? $_POST['q'] : '';
        if($q != ''){
            $result = $this->connection->createCommand("select * from crm_product where productname like '%{$q}%'")
                ->queryAll();
            echo json_encode($result);
        }
    }

}