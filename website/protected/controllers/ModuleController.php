<?php
class ModuleController extends Controller
{

    public function actionGetdata()
    {
        $module = $_REQUEST['module'];
        $viewid = $_REQUEST['viewid'];
        $moduleinfo = $this->connection->createCommand("select * from crm_module where moduleenname = :module")
            ->bindParam(':module', $module, PDO::PARAM_STR)
            ->queryRow();
        $view = $this->connection->createCommand("select * from crm_module_view where view_id = :viewid")
            ->bindParam(':viewid', $viewid, PDO::PARAM_STR)
            ->queryRow();

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page - 1) * $rows;

        $total = $this->connection->createCommand($view['viewcountsql'])->queryScalar();
        $result = $this->connection->createCommand($view['viewquerysql']." limit {$rows}
 offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionView()
    {
        $module = $_REQUEST['module'];
        $moduleentityid = $_REQUEST['moduleentityid'];

        $moduleinfo = $this->connection->createCommand("select * from crm_module where moduleenname = :module")
            ->bindParam(':module', $module, PDO::PARAM_STR)
            ->queryRow();

        $entityinfo = $this->connection->createCommand("select * from {$moduleinfo['moduletable']} where {$moduleinfo['moduleentityid']} = :moduleentityid")
            ->bindParam(':moduleentityid', $moduleentityid, PDO::PARAM_INT)
            ->queryRow();

        $modulepanels = $this->connection->createCommand("select * from crm_module_panel where deleted = 0 and 
moduleenname = '{$module}' order by seq desc")->queryAll();
        foreach ($modulepanels as &$mp){
            $mp['columns'] =  $this->connection->createCommand("select cmc.* from crm_module_column cmc 
RIGHT JOIN crm_module_panel_column cmpc 
on cmc.columnname = cmpc.columnname and cmc.modulename = cmpc.moduleenname
where cmpc.panel_id = :panel_id and cmc.deleted = 0 ORDER BY cmpc.seq desc")
                ->bindParam(':panel_id',$mp['panel_id'],PDO::PARAM_INT)
                ->queryAll();
        }

        $this->render('view',['modulepanels'=>$modulepanels,'moduleinfo'=>$moduleinfo,'entityinfo'=>$entityinfo,'module'=>$module]);
    }

    public function actionShowadd(){
        $module = $_REQUEST['module'];
        $moduleentityid = $_REQUEST['moduleentityid'];

        $moduleinfo = $this->connection->createCommand("select * from crm_module where moduleenname = :module")
            ->bindParam(':module', $module, PDO::PARAM_STR)
            ->queryRow();

        $modulepanels = $this->connection->createCommand("select * from crm_module_panel where deleted = 0 and 
moduleenname = '{$module}' order by seq desc")->queryAll();
        foreach ($modulepanels as &$mp){
            $mp['columns'] =  $this->connection->createCommand("select cmc.* from crm_module_column cmc 
RIGHT JOIN crm_module_panel_column cmpc 
on cmc.columnname = cmpc.columnname and cmc.modulename = cmpc.moduleenname
where cmpc.panel_id = :panel_id and cmc.deleted = 0 ORDER BY cmpc.seq desc")
                ->bindParam(':panel_id',$mp['panel_id'],PDO::PARAM_INT)
                ->queryAll();
        }

        $this->render('showadd',['moduleinfo'=>$moduleinfo,'moduleentityid'=>$moduleentityid,'modulepanels'=>$modulepanels,'module'=>$module]);
    }

    public function actionShowedit(){
        $module = $_REQUEST['module'];
        $moduleentityid = $_REQUEST['moduleentityid'];

        $moduleinfo = $this->connection->createCommand("select * from crm_module where moduleenname = :module")
            ->bindParam(':module', $module, PDO::PARAM_STR)
            ->queryRow();

        $entityinfo = $this->connection->createCommand("select * from {$moduleinfo['moduletable']} where {$moduleinfo['moduleentityid']} = :moduleentityid")
            ->bindParam(':moduleentityid', $moduleentityid, PDO::PARAM_INT)
            ->queryRow();

        $modulepanels = $this->connection->createCommand("select * from crm_module_panel where deleted = 0 and 
moduleenname = '{$module}' order by seq desc")->queryAll();
        foreach ($modulepanels as &$mp){
            $mp['columns'] =  $this->connection->createCommand("select cmc.* from crm_module_column cmc 
RIGHT JOIN crm_module_panel_column cmpc 
on cmc.columnname = cmpc.columnname and cmc.modulename = cmpc.moduleenname
where cmpc.panel_id = :panel_id and cmc.deleted = 0 ORDER BY cmpc.seq desc")
                ->bindParam(':panel_id',$mp['panel_id'],PDO::PARAM_INT)
                ->queryAll();
        }

        $this->render('showedit',['moduleinfo'=>$moduleinfo,'moduleentityid'=>$moduleentityid,'modulepanels'=>$modulepanels,'module'=>$module,'entityinfo'=>$entityinfo]);
    }

    public function actionSavedata(){
        $module = $_REQUEST['module'];
        $moduleentityid = $_REQUEST['moduleentityid'];

        $moduleinfo = $this->connection->createCommand("select * from crm_module where moduleenname = :module")
            ->bindParam(':module', $module, PDO::PARAM_STR)
            ->queryRow();

        $modulepanelscolumns = $this->connection->createCommand("select * from crm_module_panel_column where moduleenname = :moduleenname")
            ->bindParam(':moduleenname',$module,PDO::PARAM_STR)
            ->queryAll();

        //进行保存
        $insertsql = 'insert into  '.$moduleinfo['moduletable'].' (  ';
        $columnparam = '';
        $i=0;
        foreach ($modulepanelscolumns as $mpc){
            $i++;
            if($i==sizeof($modulepanelscolumns)){
                $insertsql.=$mpc['columnname'];
                $columnparam.=':'.$mpc['columnname'];
            }else{
                $insertsql.=$mpc['columnname'].',';
                $columnparam.=':'.$mpc['columnname'].',';
            }
        }
        $insertsql .=') values (';
        $insertsql .=$columnparam;
        $insertsql .=')';

        $sqlcommand = $this->connection->createCommand($insertsql);
        foreach ($modulepanelscolumns as $mpc){
            $sqlcommand->bindParam(':'.$mpc['columnname'],$_REQUEST[$mpc['columnname']],PDO::PARAM_STR);
        }
        $sqlcommand->execute();

    }

    public function actionEditdata(){
        $module = $_REQUEST['module'];
        $moduleentityid = $_REQUEST['moduleentityid'];

        $moduleinfo = $this->connection->createCommand("select * from crm_module where moduleenname = :module")
            ->bindParam(':module', $module, PDO::PARAM_STR)
            ->queryRow();

        $modulepanelscolumns = $this->connection->createCommand("select * from crm_module_panel_column where moduleenname = :moduleenname")
            ->bindParam(':moduleenname',$module,PDO::PARAM_STR)
            ->queryAll();

        $updatesql = 'update '.$moduleinfo['moduletable'].' set  ';
        $i=0;
        foreach ($modulepanelscolumns as $mpc){
            $i++;
            if($i==sizeof($modulepanelscolumns)){
                $updatesql.=$mpc['columnname'].'=:'.$mpc['columnname'];
            }else{
                $updatesql.=$mpc['columnname'].'=:'.$mpc['columnname'].',';
            }
        }
        $updatesql.=" where ".$moduleinfo['moduleentityid']." = :moduleentityid ";

        $sqlcommand = $this->connection->createCommand($updatesql);
        foreach ($modulepanelscolumns as $mpc){
            $sqlcommand->bindParam(':'.$mpc['columnname'],$_REQUEST[$mpc['columnname']],PDO::PARAM_STR);
        }
        $sqlcommand->bindParam(':moduleentityid',$moduleentityid,PDO::PARAM_INT);
        $sqlcommand->execute();
    }

    public function actionRemove()
    {
        $module = $_REQUEST['module'];
        $moduleentityid = $_REQUEST['moduleentityid'];

        $moduleinfo = $this->connection->createCommand("select * from crm_module where moduleenname = :module")
            ->bindParam(':module', $module, PDO::PARAM_STR)
            ->queryRow();

        $this->connection->createCommand("update ".$moduleinfo['moduletable']." set deleted = 1 
where ".$moduleinfo['moduleentityid']." = :moduleentityid")
            ->bindParam(':moduleentityid', $moduleentityid, PDO::PARAM_INT)
            ->execute();
    }

    public function actionGettext(){
        $entityid = $_REQUEST['entityid'];
        $entityidvalue = $_REQUEST['entityidvalue'];

        $moduleinfo = $this->connection->createCommand("select * from crm_module where moduleentityid = :entityid")
            ->bindParam(':entityid', $entityid, PDO::PARAM_STR)
            ->queryRow();

        $modulebasicinfo = $this->connection->createCommand("select {$moduleinfo['moduleentityid']} as id,{$moduleinfo['modulecoulumname']} as txt from {$moduleinfo['moduletable']} where {$moduleinfo['moduleentityid']} = :moduleentityid")
            ->bindParam(':moduleentityid', $entityidvalue, PDO::PARAM_STR)
            ->queryRow();

        echo json_encode($modulebasicinfo);
    }

    public function setviewtext($entityid,$entityidvalue){
        $moduleinfo = $this->connection->createCommand("select * from crm_module where moduleentityid = :entityid")
            ->bindParam(':entityid', $entityid, PDO::PARAM_STR)
            ->queryRow();
        $entityinfo = $this->connection->createCommand("select * from {$moduleinfo['moduletable']} where {$moduleinfo['moduleentityid']} = :entityid")
            ->bindParam(':entityid', $entityidvalue, PDO::PARAM_STR)
            ->queryRow();
        return $entityinfo[$moduleinfo['modulecoulumname']];
    }

}