<?php
class chargesController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_charges where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select c.*,u.realname from crm_charges c where c.deleted = 0  limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $charges_id = $_REQUEST['charges_id'];

        $gathers = null;
        if($charges_id==0){
            //新增
        }else{
            //编辑
            $gathers = $this->connection->createCommand("select c.*,u.realname from crm_charges c where c.charges_id = :charges_id")
                ->bindParam(':charges_id',$charges_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['gathers'=>$gathers]);
    }

    public function actionSavedata(){

        $charges_id = $_REQUEST['charges_id'];
        $charges_num = $_REQUEST['charges_num'];
        $amount = $_REQUEST['amount'];
        $changes_date = $_REQUEST['changes_date'];
        $summary = $_REQUEST['summary'];

        if($charges_id!=null){
            //编辑
            $this->connection->createCommand("update crm_charges set charges_num = :charges_num,amount = :amount
,changes_date = :changes_date,summary = :summary
where charges_id = :charges_id")
                ->bindParam(':charges_id',$charges_id,PDO::PARAM_INT)
                ->bindParam(':charges_num',$charges_num,PDO::PARAM_STR)
                ->bindParam(':amount',$amount,PDO::PARAM_INT)
                ->bindParam(':changes_date',$changes_date,PDO::PARAM_STR)
                ->bindParam(':summary',$summary,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_charges (charges_num,amount,changes_date,summary) values 
(:charges_num,:amount,:changes_date,:summary) ")
                ->bindParam(':charges_num',$charges_num,PDO::PARAM_STR)
                ->bindParam(':amount',$amount,PDO::PARAM_INT)
                ->bindParam(':changes_date',$changes_date,PDO::PARAM_STR)
                ->bindParam(':summary',$summary,PDO::PARAM_STR)
                ->execute();
        }
    }


    public function actionRemove(){
        $charges_id = $_REQUEST['charges_id'];
        $this->connection->createCommand("update crm_charges set deleted = 1 
where charges_id = :charges_id")
            ->bindParam(':charges_id',$charges_id,PDO::PARAM_INT)
            ->execute();
    }
}