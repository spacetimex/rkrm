<?php

class WeeklylogsController extends Controller
{

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata()
    {

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page - 1) * $rows;
        $total = $this->connection->createCommand("select count(1) from crm_weeklylogs where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select *,crm_user.realname from crm_weeklylogs left join crm_user on crm_weeklylogs.user_id = crm_user.user_id where crm_weeklylogs.deleted = 0 order by crm_weeklylogs.weeklylogs_id desc limit {$rows} offset {$start} ")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit()
    {
        $weeklylogs_id = $_REQUEST['weeklylogs_id'];
        $weeklylogs = null;
        if ($weeklylogs_id == 0) {
            //新增
        } else {
            //编辑
            $weeklylogs = $this->connection->createCommand("select * from crm_weeklylogs where weeklylogs_id = :weeklylogs_id")->bindParam(':weeklylogs_id', $weeklylogs_id, PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit', ['weeklylogs' => $weeklylogs]);
    }

    public function actionSavedata()
    {

        $weeklylogs_id = $_REQUEST['weeklylogs_id'];
        $summary = $_REQUEST['summary'];
        $nextweek_plan = $_REQUEST['nextweek_plan'];

        $year = date("Y");
        //一年中的第几周
        $week = date('W');
        $user_id = Yii::app()->user->user_id;

        if ($weeklylogs_id != null && $weeklylogs_id != "") {

            $prevweeklylogs = $this->connection->createCommand("select * from crm_weeklylogs where user_id = :user_id and weeklylogs_id = :weeklylogs_id")
                ->bindParam(':user_id', $user_id, PDO::PARAM_INT)
                ->bindParam(':weeklylogs_id', $weeklylogs_id, PDO::PARAM_STR)
                ->queryRow();

            if($prevweeklylogs['year']==$year && $prevweeklylogs['week']==$week){
                //编辑
                $this->connection->createCommand("update crm_weeklylogs set summary = :summary,
nextweek_plan=:nextweek_plan where weeklylogs_id = :weeklylogs_id")
                    ->bindParam(':weeklylogs_id', $weeklylogs_id, PDO::PARAM_INT)
                    ->bindParam(':summary', $summary, PDO::PARAM_STR)
                    ->bindParam(':nextweek_plan', $nextweek_plan, PDO::PARAM_STR)
                    ->execute();
                echo json_encode(Appcode::success);
            }else{
                echo json_encode(Appcode::error_weeklycannotupdate);
            }

        } else {
            //添加
            $existweeklylogs = $this->connection->createCommand("select * from crm_weeklylogs where user_id = :user_id and year = :year and week = :week")
                ->bindParam(':user_id', $user_id, PDO::PARAM_INT)
                ->bindParam(':year', $year, PDO::PARAM_STR)
                ->bindParam(':week', $week, PDO::PARAM_STR)
                ->queryRow();

            if ($existweeklylogs != false) {
                echo json_encode(Appcode::error_weeklylogsexist);
            } else {
                $this->connection->createCommand("insert into crm_weeklylogs (summary,nextweek_plan,createdate,user_id,year,week) 
values (:summary,:nextweek_plan,now(),{$user_id},:year,:week) ")
                    ->bindParam(':summary', $summary, PDO::PARAM_STR)
                    ->bindParam(':nextweek_plan', $nextweek_plan, PDO::PARAM_STR)
                    ->bindParam(':year', $year, PDO::PARAM_STR)
                    ->bindParam(':week', $week, PDO::PARAM_STR)
                    ->execute();
                echo json_encode(Appcode::success);
            }
        }
    }

    public function actionView()
    {
        $weeklylogs_id = $_REQUEST['weeklylogs_id'];
        $weeklylogs = $this->connection->createCommand("select *,crm_user.realname from crm_weeklylogs left join crm_user on crm_weeklylogs.user_id = crm_user.user_id  where weeklylogs_id = :weeklylogs_id")->bindParam(':weeklylogs_id', $weeklylogs_id, PDO::PARAM_INT)->queryRow();
        $this->render('view', ['weeklylogs' => $weeklylogs]);
    }

}