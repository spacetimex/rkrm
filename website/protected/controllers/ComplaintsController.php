<?php
class ComplaintsController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_complaints")->queryScalar();
        $result = $this->connection->createCommand("select * from crm_complaints limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $complaints_id = $_REQUEST['complaints_id'];

        $complaints = null;
        if($complaints_id==0){
            //新增
        }else{
            //编辑
            $complaints = $this->connection->createCommand("select * from crm_complaints where complaints_id = :complaints_id")->bindParam(':complaints_id',$complaints_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['complaints'=>$complaints]);
    }

    public function actionSavedata(){

        $complaints_id = $_REQUEST['complaints_id'];
        $title = $_REQUEST['title'];
        $customer = $_REQUEST['customer'];
        $complaints_type = $_REQUEST['complaints_type'];
        $complaints_date = $_REQUEST['complaints_date'];
        $result = $_REQUEST['result'];
        $urgencydegree = $_REQUEST['urgencydegree'];

        if($complaints_id!=null){
            //编辑
            $this->connection->createCommand("update crm_complaints set title = :title,
customer = :customer,complaints_type=:complaints_type,complaints_date=:complaints_date,result=:result,urgencydegree=:urgencydegree where complaints_id = :complaints_id")
                ->bindParam(':complaints_id',$complaints_id,PDO::PARAM_INT)
                ->bindParam(':title',$title,PDO::PARAM_STR)
                ->bindParam(':customer',$customer,PDO::PARAM_STR)
                ->bindParam(':complaints_type',$complaints_type,PDO::PARAM_STR)
                ->bindParam(':complaints_date',$complaints_date,PDO::PARAM_STR)
                ->bindParam(':result',$result,PDO::PARAM_STR)
                ->bindParam(':urgencydegree',$urgencydegree,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_complaints (title,customer,complaints_type,complaints_date,result,urgencydegree) values 
(:title,:customer,:complaints_type,:complaints_date,:result,:urgencydegree) ")
                ->bindParam(':title',$title,PDO::PARAM_STR)
                ->bindParam(':customer',$customer,PDO::PARAM_STR)
                ->bindParam(':complaints_type',$complaints_type,PDO::PARAM_STR)
                ->bindParam(':complaints_date',$complaints_date,PDO::PARAM_STR)
                ->bindParam(':result',$result,PDO::PARAM_STR)
                ->bindParam(':urgencydegree',$urgencydegree,PDO::PARAM_STR)
                ->execute();
        }
    }

}