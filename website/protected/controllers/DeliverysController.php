<?php
//出库单
class DeliverysController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetdata(){

        $page = $_REQUEST['page'];
        $rows = $_REQUEST['rows'];
        $start = ($page-1)*$rows;
        $total = $this->connection->createCommand("select count(1) from crm_deliverys where deleted = 0 ")->queryScalar();
        $result = $this->connection->createCommand("select c.*,u.realname from crm_deliverys c left join crm_user
 u on c.status = u.user_id where c.deleted = 0  limit {$rows} offset {$start}")->queryAll();

        $data = Array();
        $data['total'] = $total;
        $data['rows'] = $result;

        echo json_encode($data);
    }

    public function actionShowedit(){

        $deliverys_id = $_REQUEST['deliverys_id'];

        $deliverys = null;
        if($deliverys_id==0){
            //新增
        }else{
            //编辑
            $deliverys = $this->connection->createCommand("select c.*,u.realname from crm_deliverys c left join 
crm_user u on c.status = u.user_id  where c.deliverys_id = :deliverys_id")->bindParam(':deliverys_id',$deliverys_id,PDO::PARAM_INT)->queryRow();
        }

        $this->render('showedit',['deliverys'=>$deliverys]);
    }

    public function actionSavedata(){

        $deliverys_id = $_REQUEST['deliverys_id'];
        $deliverys_num = $_REQUEST['deliverys_num'];
        $status = $_REQUEST['status'];
        $salesorder_id = $_REQUEST['salesorder_id'];
        $invoice_id = $_REQUEST['invoice_id'];
        $deliverystime = $_REQUEST['deliverystime'];

        if($deliverys_id!=null){
            //编辑
            $this->connection->createCommand("update crm_deliverys set deliverys_num = :deliverys_num,status = :status
,salesorder_id = :salesorder_id,invoice_id = :invoice_id,deliverystime = :deliverystime
where deliverys_id = :deliverys_id")
                ->bindParam(':deliverys_id',$deliverys_id,PDO::PARAM_INT)
                ->bindParam(':deliverys_num',$deliverys_num,PDO::PARAM_STR)
                ->bindParam(':status',$status,PDO::PARAM_INT)
                ->bindParam(':salesorder_id',$salesorder_id,PDO::PARAM_STR)
                ->bindParam(':invoice_id',$invoice_id,PDO::PARAM_STR)
                ->bindParam(':deliverystime',$deliverystime,PDO::PARAM_STR)
                ->execute();
        }else{
            //添加
            $this->connection->createCommand("insert into crm_deliverys (deliverys_num,status,salesorder_id,invoice_id,deliverystime) values 
(:deliverys_num,:status,:salesorder_id,:invoice_id,:deliverystime) ")
                ->bindParam(':deliverys_num',$deliverys_num,PDO::PARAM_STR)
                ->bindParam(':status',$status,PDO::PARAM_INT)
                ->bindParam(':salesorder_id',$salesorder_id,PDO::PARAM_STR)
                ->bindParam(':invoice_id',$invoice_id,PDO::PARAM_STR)
                ->bindParam(':deliverystime',$deliverystime,PDO::PARAM_STR)
                ->execute();
        }
    }


    public function actionRemove(){
        $deliverys_id = $_REQUEST['deliverys_id'];
        $this->connection->createCommand("update crm_deliverys set deleted = 1 
where deliverys_id = :deliverys_id")
            ->bindParam(':deliverys_id',$deliverys_id,PDO::PARAM_INT)
            ->execute();
    }



}